<?php

class AsyncTest extends WP_UnitTestCase {
    public function setUp() {
        parent::setUp();
    }

    public function testGetZeroExpiredExperiences() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-enero-de-2020';
        $name     = '10 de enero de 2020';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $expired_experiences = get_experiences_with_past_dates();

        $this->assertInternalType('array', $expired_experiences); 
        $this->assertCount(0, $expired_experiences['result']); 
    }

    public function testGetOneExpiredExperience() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-enero-de-2014';
        $name     = '10 de enero de 2014';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $expired_experiences = get_experiences_with_past_dates();

        $this->assertInternalType('array', $expired_experiences); 
        $this->assertCount(1, $expired_experiences['result']); 
        $this->assertEquals($post_id, $expired_experiences['result'][0]['id']); 
    }


    public function testGetOneExpiredExperiencesAvg() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-enero-de-2014';
        $name     = '10 de enero de 2014';

        $slug_1   = '10-de-enero-de-2020';
        $name_1   = '10 de enero de 2020';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $post_id_1 = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Gran Sabana')); 
        wp_set_post_terms( $post_id_1, array($term_id), $taxonomy );

        $term_id_1 = $this->factory->term->create( array('name' => $name_1, 'taxonomy' => $taxonomy, 'slug' => $slug_1)); 
        $post_id_2 = $this->factory->post->create( array('post_type' => 'product', 'post_title' => 'Gran Sabana 2')); 
        wp_set_post_terms( $post_id_2, array($term_id_1), $taxonomy );


        $expired_experiences = get_experiences_with_past_dates();

        $this->assertInternalType('array', $expired_experiences); 
        $this->assertCount(2, $expired_experiences['result']); 
        $this->assertEquals($post_id, $expired_experiences['result'][0]['id']); 
        $this->assertEquals($post_id_1, $expired_experiences['result'][1]['id']); 
    }

    public function testGetOneExpiredExperiencesAvgWithExtraInfo() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-enero-de-2014-opcion-los-nevados';
        $name     = '10 de enero de 2014 - Opcion Los Nevados';

        $slug_1   = '10-de-enero-de-2020-opcion-los-nevados';
        $name_1   = '10 de enero de 2020 - Opcion Los Nevados';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $post_id_1 = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Gran Sabana')); 
        wp_set_post_terms( $post_id_1, array($term_id), $taxonomy );

        $term_id_1 = $this->factory->term->create( array('name' => $name_1, 'taxonomy' => $taxonomy, 'slug' => $slug_1)); 
        $post_id_2 = $this->factory->post->create( array('post_type' => 'product', 'post_title' => 'Gran Sabana 2')); 
        wp_set_post_terms( $post_id_2, array($term_id_1), $taxonomy );


        $expired_experiences = get_experiences_with_past_dates();

        $this->assertInternalType('array', $expired_experiences); 
        $this->assertCount(2, $expired_experiences['result']); 
        $this->assertEquals($post_id_1, $expired_experiences['result'][0]['id']); 
        $this->assertEquals($post_id, $expired_experiences['result'][1]['id']); 
    }


    public function testCleanUpExperience() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-enero-de-2014';
        $name     = '10 de enero de 2014';

        $slug_1   = '10-de-enero-de-2020';
        $name_1   = '10 de enero de 2020';

        register_taxonomy( $taxonomy, 'post' ); 

        $term_id   = $this->factory->term->create( array('name' => $name, 'taxonomy'   => $taxonomy, 'slug' => $slug) );
        $term_id_1 = $this->factory->term->create( array('name' => $name_1, 'taxonomy' => $taxonomy, 'slug' => $slug_1) );

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id, $term_id_1), $taxonomy );

        clean_up_experience($post_id);

        $this->assertFalse(has_term($term_id, $taxonomy, $post_id)); 
        $this->assertTrue(has_term($term_id_1, $taxonomy, $post_id)); 
    }

    public function testCleanExperienceDatabase() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-enero-de-2014';
        $name     = '10 de enero de 2014';

        $slug_1   = '10-de-enero-de-2020';
        $name_1   = '10 de enero de 2020';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $post_id_1 = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Gran Sabana')); 
        wp_set_post_terms( $post_id_1, array($term_id), $taxonomy );

        $term_id_1 = $this->factory->term->create( array('name' => $name_1, 'taxonomy' => $taxonomy, 'slug' => $slug_1)); 
        $post_id_2 = $this->factory->post->create( array('post_type' => 'product', 'post_title' => 'Gran Sabana 2')); 
        wp_set_post_terms( $post_id_2, array($term_id_1), $taxonomy );

        clean_up_experience_database(); 

        $this->assertFalse(has_term($term_id, $taxonomy, $post_id)); 
        $this->assertFalse(has_term($term_id, $taxonomy, $post_id_1)); 
        $this->assertTrue(has_term($term_id_1, $taxonomy, $post_id_2)); 
    }

    public function testCleanExperienceDatabaseWithExtraInfo() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-enero-de-2014-opcion-los-nevados';
        $name     = '10 de enero de 2014 - Opción los nevados';

        $slug_1   = '10-de-enero-de-2020-masaje-incluido';
        $name_1   = '10 de enero de 2020 - Masaje incluido';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $post_id_1 = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Gran Sabana')); 
        wp_set_post_terms( $post_id_1, array($term_id), $taxonomy );

        $term_id_1 = $this->factory->term->create( array('name' => $name_1, 'taxonomy' => $taxonomy, 'slug' => $slug_1)); 
        $post_id_2 = $this->factory->post->create( array('post_type' => 'product', 'post_title' => 'Gran Sabana 2')); 
        wp_set_post_terms( $post_id_2, array($term_id_1), $taxonomy );

        clean_up_experience_database(); 

        $this->assertFalse(has_term($term_id, $taxonomy, $post_id)); 
        $this->assertFalse(has_term($term_id, $taxonomy, $post_id_1)); 
        $this->assertTrue(has_term($term_id_1, $taxonomy, $post_id_2)); 
    }

    public function testCleanUpOneDateDatabase() {

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-enero-de-2014';
        $name     = '10 de enero de 2014';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        clean_up_date_database(false); 

        $term_exists = term_exists($term_id, $taxonomy); 
        $this->assertNull($term_exists); 
        $this->assertFalse(has_term($term_id, $taxonomy, $post_id)); 
    }

    public function testCleanUpZeroDatesDatabase() {

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-diciembre-de-2020';
        $name     = '10 de diciembre de 2020';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        clean_up_date_database(false); 

        $term_exists = term_exists($term_id, $taxonomy); 
        $this->assertInternalType('array',$term_exists); 
        $this->assertTrue(has_term($term_id, $taxonomy, $post_id)); 
    }

    public function testCleanUpDatesDatabaseAvg() {

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-diciembre-de-2020';
        $name     = '10 de diciembre de 2020';
        $slug_1   = '10-de-diciembre-de-2014';
        $name_1   = '10 de diciembre de 2014';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id   = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 
        $term_id_1 = $this->factory->term->create( array('name' => $name_1, 'taxonomy' => $taxonomy, 'slug' => $slug_1)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id, $term_id_1), $taxonomy );

        clean_up_date_database(false); 

        $term_exists = term_exists($term_id, $taxonomy); 
        $this->assertInternalType('array',$term_exists); 
        $this->assertTrue(has_term($term_id, $taxonomy, $post_id)); 
        $term_exists = term_exists($term_id_1, $taxonomy); 
        $this->assertNull($term_exists); 
        $this->assertFalse(has_term($term_id_1, $taxonomy, $post_id)); 
    }

    public function testGetExperiencesWithoutDatesEmpty() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-diciembre-de-2020';
        $name     = '10 de diciembre de 2020';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $experiences = get_experiences_without_dates();

        $this->assertEmpty($experiences); 
    }

    public function testGetExperiencesWithoutDatesOneResult() {

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        register_taxonomy( $taxonomy, 'post' ); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 

        $experiences = get_experiences_without_dates();

        $this->assertCount(1,$experiences); 

        $this->assertEquals($post_id, $experiences[0]); 
    }

    public function testGetExperiencesWithoutDatesAvg() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug     = '10-de-diciembre-de-2020';
        $name     = '10 de diciembre de 2020';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $name, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $post_id_1 = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Roraima')); 

        $experiences = get_experiences_without_dates();

        $this->assertCount(1,$experiences); 
        $this->assertEquals($post_id_1, $experiences[0]); 
    }
}
