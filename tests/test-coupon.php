<?php

class CouponTest extends WP_UnitTestCase {
    
    public function setUp() {
        parent::setUp();
    }

    public function testTruncateEmptyCodeCoupon(){
    	$code = "";
    	$result = truncate_string($code,5);
    	$this->assertEquals("",$result);
    }

    public function testTruncateNullCodeCoupon(){
    	$code = null;
    	$result = truncate_string($code,5);
    	$this->assertFalse($result);
    }

    public function testTruncateCodeCoupon(){
    	$code = "12345678";
    	$result = truncate_string($code,6);
    	$this->assertEquals(6,strlen($result));
    	$this->assertEquals("12**78",$result);
    }






}
