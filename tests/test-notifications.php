<?php

class NotificationsTest extends WP_UnitTestCase {
    
    public function setUp() {
        parent::setUp();
    }

    public function testUpdatedNotificationsTypeInterest(){

    	$user_id = $this->factory->user->create();
        $interests = array('pesca','bicicleta','hiking');
        $result = add_meta_notifications($user_id,'interest',$interests);
        $this->assertTrue($result);
        $result = get_user_meta($user_id,'interest_notifications');
        $this->assertInternalType('array', $result[0]);
        $this->assertEquals($interests, $result[0]);



    }

    public function testUpdatedNotificationsTypeFrequency(){

        $user_id = $this->factory->user->create();
        $frequency = array('daily');
        $result = add_meta_notifications($user_id,'frequency',$frequency);
        $this->assertTrue($result);
        $result = get_user_meta($user_id,'frequency_notifications');
        $this->assertInternalType('array', $result[0]);
        $this->assertEquals($frequency, $result[0]);



    }

    public function testSettingsNotificationsTypeFrequency(){

        $user_id = $this->factory->user->create();
        $frequency = array('never','esto es una prueba');
        $result = add_meta_notifications($user_id,'frequency',$frequency);
        $result = get_meta_notifications($user_id,'frequency');
        $this->assertInternalType('array', $result);
        $this->assertEquals($frequency, $result);

    }

    


}