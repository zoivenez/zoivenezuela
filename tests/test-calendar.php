<?php

/**
 * Test for Website calendar experiences
 */
class CalendarTest extends WP_UnitTestCase {
    public function setUp() {
        parent::setUp();
    }

    public function testGetExperiencesBetweenEmptyRange() {
        $from = 1000;
        $to   = 999;

        $result = get_experiences_between($from, $to); 

        $this->assertInternalType('array', $result); 
        $this->assertArrayHasKey('success', $result); 
        $this->assertArrayHasKey('error', $result); 
        $this->assertEquals($result['success'], 0); 
        $this->assertEquals($result['error'], 'Empty range'); 
    }

    public function testGetExperiencesBetweenRangeWithoutResults() {

        $from = strtotime(date('Y-m-d', time())); 
        $to   = strtotime(date('Y-m-d', strtotime("+30 days")));

        $result = get_experiences_between($from, $to); 
        $this->assertInternalType('array', $result); 
        $this->assertArrayHasKey('success', $result); 
        $this->assertArrayHasKey('result', $result); 
        $this->assertArrayNotHasKey('error', $result); 
        $this->assertEquals($result['success'], 1); 
        $this->assertEmpty($result['result']); 
    }

    public function testGetExperiencesBetweenRangeWithOneResult() {

        $from = strtotime('2015-09-19'); 
        $to   = strtotime('2015-10-19');

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        register_taxonomy( $taxonomy, 'post' ); 
        $term_id  = $this->factory->term->create( array('name' => '20 de septiembre de 2015', 'taxonomy' => $taxonomy, 'slug' => '20-de-septiembre-de-2015')); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $result = get_experiences_between($from, $to); 
        $this->assertInternalType('array', $result); 

        $this->assertArrayHasKey('success', $result); 
        $this->assertArrayHasKey('result', $result); 
        $this->assertArrayNotHasKey('error', $result); 

        $this->assertEquals($result['success'], 1); 
        $this->assertNotEmpty($result['result']); 

        $post = $result['result'][0]; 

        $this->assertArrayHasKey('id',$post ); 
        $this->assertArrayHasKey('title', $post); 
        $this->assertArrayHasKey('url', $post); 
        $this->assertArrayHasKey('start', $post); 

        $this->assertEquals($post_id, $post['id']); 
        $this->assertEquals('Costas mirandinas', $post['title']); 
        $this->assertEquals(strtotime('2015-09-20 +1 day') . '000', $post['start']); 
        $this->assertEquals('http://example.org/?p=' . $post_id, $post['url']); 

    }

    public function testGetExperiencesBetweenRangeWithOneResultTwice() {

        $from = strtotime('2015-09-19'); 
        $to   = strtotime('2015-10-19');

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        register_taxonomy( $taxonomy, 'post' ); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 

        $term_id  = $this->factory->term->create( array('name' => '20 de septiembre de 2015', 'taxonomy' => $taxonomy, 'slug' => '20-de-septiembre-de-2015')); 
        $term_id_1 = $this->factory->term->create( array('name' => '21 de septiembre de 2015', 'taxonomy' => $taxonomy, 'slug' => '21-de-septiembre-de-2015')); 

        wp_set_post_terms( $post_id, array($term_id, $term_id_1), $taxonomy );

        $result = get_experiences_between($from, $to); 
        $this->assertInternalType('array', $result); 

        $this->assertArrayHasKey('success', $result); 
        $this->assertArrayHasKey('result', $result); 
        $this->assertArrayNotHasKey('error', $result); 

        $this->assertEquals($result['success'], 1); 
        $this->assertNotEmpty($result['result']); 
        $this->assertCount(2, $result['result']); 

        $post = $result['result'][0]; 

        $this->assertArrayHasKey('id',$post ); 
        $this->assertArrayHasKey('title', $post); 
        $this->assertArrayHasKey('url', $post); 
        $this->assertArrayHasKey('start', $post); 

        $this->assertEquals($post_id, $post['id']); 
        $this->assertEquals('Costas mirandinas', $post['title']); 
        $this->assertEquals(strtotime('2015-09-20 +1 day') . '000', $post['start']); 
        $this->assertEquals('http://example.org/?p=' . $post_id, $post['url']); 

        $post = $result['result'][1]; 

        $this->assertArrayHasKey('id',$post ); 
        $this->assertArrayHasKey('title', $post); 
        $this->assertArrayHasKey('url', $post); 
        $this->assertArrayHasKey('start', $post); 

        $this->assertEquals($post_id, $post['id']); 
        $this->assertEquals('Costas mirandinas', $post['title']); 
        $this->assertEquals(strtotime('2015-09-21 +1 day') . '000', $post['start']); 
        $this->assertEquals('http://example.org/?p=' . $post_id, $post['url']); 
    }

    public function testGetExperiencesBetweenRangeWithOneResultAll() {

        $from = strtotime('2013-09-19'); 
        $to   = strtotime('2013-10-19');

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        register_taxonomy( $taxonomy, 'post' ); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 

        $term_id  = $this->factory->term->create( array('name' => '20 de septiembre de 2015', 'taxonomy' => $taxonomy, 'slug' => '20-de-septiembre-de-2015')); 
        $term_id_1 = $this->factory->term->create( array('name' => '21 de septiembre de 2015', 'taxonomy' => $taxonomy, 'slug' => '21-de-septiembre-de-2015')); 

        wp_set_post_terms( $post_id, array($term_id, $term_id_1), $taxonomy );

        $result = get_experiences_between($from, $to, true); 
        $this->assertInternalType('array', $result); 

        $this->assertArrayHasKey('success', $result); 
        $this->assertArrayHasKey('result', $result); 
        $this->assertArrayNotHasKey('error', $result); 

        $this->assertEquals($result['success'], 1); 
        $this->assertNotEmpty($result['result']); 
        $this->assertCount(2, $result['result']); 

        $post = $result['result'][0]; 

        $this->assertArrayHasKey('id',$post ); 
        $this->assertArrayHasKey('title', $post); 
        $this->assertArrayHasKey('url', $post); 
        $this->assertArrayHasKey('start', $post); 

        $this->assertEquals($post_id, $post['id']); 
        $this->assertEquals('Costas mirandinas', $post['title']); 
        $this->assertEquals(strtotime('2015-09-20 +1 day') . '000', $post['start']); 
        $this->assertEquals('http://example.org/?p=' . $post_id, $post['url']); 

        $post = $result['result'][1]; 

        $this->assertArrayHasKey('id',$post ); 
        $this->assertArrayHasKey('title', $post); 
        $this->assertArrayHasKey('url', $post); 
        $this->assertArrayHasKey('start', $post); 

        $this->assertEquals($post_id, $post['id']); 
        $this->assertEquals('Costas mirandinas', $post['title']); 
        $this->assertEquals(strtotime('2015-09-21 +1 day') . '000', $post['start']); 
        $this->assertEquals('http://example.org/?p=' . $post_id, $post['url']); 
    }

    public function testGetExperiencesWithExtraInfo() {

        $from = strtotime('2015-09-19'); 
        $to   = strtotime('2015-10-19');

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        register_taxonomy( $taxonomy, 'post' ); 
        $term_id  = $this->factory->term->create( array('name' => '20 de septiembre de 2015 - extra info', 'taxonomy' => $taxonomy, 'slug' => '20-de-septiembre-de-2015-extra-info')); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $result = get_experiences_between($from, $to); 
        $this->assertInternalType('array', $result); 

        $this->assertArrayHasKey('success', $result); 
        $this->assertArrayHasKey('result', $result); 
        $this->assertArrayNotHasKey('error', $result); 

        $this->assertEquals($result['success'], 1); 
        $this->assertNotEmpty($result['result']); 

        $post = $result['result'][0]; 

        $this->assertArrayHasKey('id',$post ); 
        $this->assertArrayHasKey('title', $post); 
        $this->assertArrayHasKey('url', $post); 
        $this->assertArrayHasKey('start', $post); 

        $this->assertEquals($post_id, $post['id']); 
        $this->assertEquals('Costas mirandinas', $post['title']); 
        $this->assertEquals(strtotime('2015-09-20 +1 day') . '000', $post['start']); 
        $this->assertEquals('http://example.org/?p=' . $post_id, $post['url']); 
    }

    public function testGetExperiencesWithExtraInfoDuplicated() {
        $from = strtotime('2015-09-19'); 
        $to   = strtotime('2015-10-19');

        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        register_taxonomy( $taxonomy, 'post' ); 
        $term_id    = $this->factory->term->create( array('name' => '20 de septiembre de 2015 - extra info', 'taxonomy' => $taxonomy, 'slug' => '20-de-septiembre-de-2015-extra-info')); 
        $term_id_1  = $this->factory->term->create( array('name' => '20 de septiembre de 2015 - extra info duplicated', 'taxonomy' => $taxonomy, 'slug' => '20-de-septiembre-de-2015-extra-info-duplicated')); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id, $term_id_1), $taxonomy );

        $result = get_experiences_between($from, $to); 
        $this->assertInternalType('array', $result); 

        $this->assertArrayHasKey('success', $result); 
        $this->assertArrayHasKey('result', $result); 
        $this->assertArrayNotHasKey('error', $result); 

        $this->assertEquals($result['success'], 1); 
        $this->assertCount(1,$result['result']); 
        $this->assertEquals($post_id,$result['result'][0]['id']); 
    }


}
