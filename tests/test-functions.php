<?php


class FunctionTest extends WP_UnitTestCase {
    private $product; 
    public function setUp() {
        parent::setUp();
    }

    public function testSortExperienceDatesEmpty() {
        $empty_array = array(); 
        $dates = sort_experience_dates($empty_array);
        $this->assertEmpty($dates);
    }

    public function testSortExperienceDatesWithoutDate() {

        $date_1 = (new stdClass()); 
        $date_1->name ="Eliges";
        $terms = array(
            $date_1
        );

        $dates = sort_experience_dates($terms);
        $this->assertEquals($dates, $terms);
    }

    public function testParseSpanishDate() {
            $date = parse_spanish_date('12 de agosto'); 
            $this->assertTrue(date('d/m/Y',$date) === '12/08/2016'); 
    }

    public function testSortExperienceDatesTwoDates() {

        $date_1 = (new stdClass()); 
        $date_1->name ="12 de diciembre";
        $date_2 = (new stdClass()); 
        $date_2->name ="12 de agosto";

        $terms = array(
            $date_1,
            $date_2
        );

        $dates = sort_experience_dates($terms);
        $this->assertNotEquals($dates, $terms); 
        $this->assertEquals($dates[0], $terms[1]); 
        $this->assertEquals($dates[1], $terms[0]); 
    }
    public function testSortExperienceDatesTwoDatesAndNoDate() {

        $date_1 = (new stdClass()); 
        $date_1->name ="Eliges tú";
        $date_2 = (new stdClass()); 
        $date_2->name ="12 de diciembre";
        $date_3 = (new stdClass()); 
        $date_3->name ="12 de agosto";

        $terms = array(
            $date_1,
            $date_2,
            $date_3
        );

        $dates = sort_experience_dates($terms);
        $this->assertNotEquals($dates, $terms); 
        $this->assertEquals($dates[0], $terms[0]); 
        $this->assertEquals($dates[1], $terms[2]); 
        $this->assertEquals($dates[2], $terms[1]); 
    }

    public function testRemovingGuruAndItsMetaFromPosts() {


        $user_id = $this->factory->user->create(array('role' => 'Gurú')); 
        $post_id = $this->factory->post->create(array('post_type' => 'product')); 

        add_post_meta( $post_id, 'guru', $user_id ); 
        $guru = get_post_meta($post_id); 
        $this->assertNotEmpty($guru); 

        remove_guru_from_posts($user_id); 
        $guru = get_post_meta($post_id); 
        $this->assertEmpty($guru); 
    }

    // Commented to avoid internet usage
/*    public function testGravatarImage() {*/
        //$user_email = "arochajuan2@gmail.com"; // My email has gravatar account
        //$this->assertTrue(validate_gravatar($user_email)); 

        //$user_email = "example@zoivenezuela.com";
        //$this->assertFalse(validate_gravatar($user_email)); 
    //}

    public function testGoogleImage() {
        $url = "https://lh4.googleusercontent.com/-3Km4rPxMgrI/AAAAAAAAAAI/AAAAAAAAADk/neK7U8GJsnE/photo.jpg?sz=200"; 
        $this->assertTrue(validate_google_avatar($url)); 

        $url = "anyurl.com"; // My email has gravatar account
        $this->assertFalse(validate_google_avatar($url)); 
    }

    public function testFacebookImage() {
        $url = "https://graph.facebook.com/10155690620275142/picture?width=150&height=150"; 
        $this->assertTrue(validate_facebook_avatar($url)); 

        $url = "anyurl.com"; 
        $this->assertFalse(validate_facebook_avatar($url)); 
    }

    public function testGetProductAttributes() {

        $taxonomy = 'pa_destination'; // This is for worpress attributes

        register_taxonomy( $taxonomy, 'post' ); 
        $slug = 'miranda';
        $term_id = $this->factory->term->create( array('name' => $slug, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $this->assertTrue($term_id > 0); 
        $this->assertEquals($slug, get_product_attributes($taxonomy, 1, 0)[0]['slug']); 
    }

    public function testSearchEmptyArgs() {
        $this->assertFalse(filter_experiences_by()); 
    }

    public function testSearchByKeyword() {
        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 

        $results = filter_experiences_by_query('costas'); 

        $this->assertInternalType('array', $results); 
        $this->assertCount(1, $results); 
        $this->assertEquals($post_id, $results[0]); 
    }

    public function testSearchByLocation() {
        $taxonomy = 'pa_destination'; // This is for worpress attributes
        $slug = 'miranda';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $slug, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $results = filter_experiences_by_taxonomy($taxonomy, $slug); 
        $this->assertInternalType('array', $results); 
        $this->assertCount(1, $results); 
        $this->assertEquals($post_id, $results[0]); 
    }

    public function testSearchByDate() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug = '10-de-enero-de-2016';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $slug, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $results = filter_experiences_by_date('enero'); 

        $this->assertInternalType('array', $results); 
        $this->assertCount(1, $results); 
        $this->assertEquals($post_id, $results[0]); 
    }

    public function testNoResultByKeyword() {
        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        $results = filter_experiences_by_query('aldeas'); 
        $this->assertFalse($results); 
    }

    public function testNoResultByLocation() {
        $taxonomy = 'pa_destination'; // This is for worpress attributes
        $slug = 'miranda';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $slug, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $results = filter_experiences_by_taxonomy($taxonomy, 'caracas'); 
        $this->assertFalse($results); 
    }

    public function testNoResultByDate() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug = '10-de-enero-de-2016';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $slug, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );

        $results = filter_experiences_by_date('marzo'); 
        $this->assertFalse($results); 
    }

    public function testSearchKeywordLocation() {
        $taxonomy = 'pa_destination'; // This is for worpress attributes
        $slug = 'miranda';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $slug, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );
        
        $posts = filter_experiences_by(array('s' => 'costas', $taxonomy => 'miranda')); 

        $this->assertInternalType('array', $posts); 
        $this->assertCount(1, $posts); 
        $this->assertEquals($post_id, $posts[0]); 
    }

    public function testSearchKeywordDate() {
        $taxonomy = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug = '10-de-enero-de-2016';

        register_taxonomy( $taxonomy, 'post' ); 
        $term_id = $this->factory->term->create( array('name' => $slug, 'taxonomy' => $taxonomy, 'slug' => $slug)); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id), $taxonomy );
        
        $posts = filter_experiences_by(array('s' => 'costas', $taxonomy => 'enero')); 

        $this->assertInternalType('array', $posts); 
        $this->assertCount(1, $posts); 
        $this->assertEquals($post_id, $posts[0]); 
    }

    public function testSearchLocationDate() {
        $taxonomy_1 = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug_1     = 'enero';

        register_taxonomy( $taxonomy_1, 'post' ); 
        $term_id_1 = $this->factory->term->create( array('name' => $slug_1, 'taxonomy' => $taxonomy_1, 'slug' => '1-de-enero-de-2016')); 

        $taxonomy_2 = 'pa_destination'; // This is for worpress attributes
        $slug_2     = 'miranda';

        register_taxonomy( $taxonomy_2, 'post' ); 
        $term_id_2 = $this->factory->term->create( array('name' => $slug_2, 'taxonomy' => $taxonomy_2, 'slug' => $slug_2) ); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id_1), $taxonomy_1 );
        wp_set_post_terms( $post_id, array($term_id_2), $taxonomy_2 ); 

        $posts = filter_experiences_by(array($taxonomy_1 => $slug_1, $taxonomy_2 => $slug_2)); 
        $this->assertInternalType('array', $posts); 
        $this->assertCount(1, $posts); 
        $this->assertEquals($post_id, $posts[0]); 
    }

    public function testSearchKeywordLocationDate() {
        $taxonomy_1 = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug_1     = 'enero';

        register_taxonomy( $taxonomy_1, 'post' ); 
        $term_id_1 = $this->factory->term->create( array('name' => $slug_1, 'taxonomy' => $taxonomy_1, 'slug' => '1-de-enero-de-2016')); 

        $taxonomy_2 = 'pa_destination'; // This is for worpress attributes
        $slug_2     = 'miranda';

        register_taxonomy( $taxonomy_2, 'post' ); 
        $term_id_2 = $this->factory->term->create( array('name' => $slug_2, 'taxonomy' => $taxonomy_2, 'slug' => $slug_2) ); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id_1), $taxonomy_1 );
        wp_set_post_terms( $post_id, array($term_id_2), $taxonomy_2 ); 

        $posts = filter_experiences_by(array('s' => 'costas', $taxonomy_1 => $slug_1, $taxonomy_2 => $slug_2)); 
        $this->assertInternalType('array', $posts); 
        $this->assertCount(1, $posts); 
        $this->assertEquals($post_id, $posts[0]); 
    }

    public function testEmptyIntersectionSearch() {
        $taxonomy_1 = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug_1     = 'enero';

        register_taxonomy( $taxonomy_1, 'post' ); 
        $term_id_1 = $this->factory->term->create( array('name' => $slug_1, 'taxonomy' => $taxonomy_1, 'slug' => '1-de-enero-de-2016')); 

        $taxonomy_2 = 'pa_destination'; // This is for worpress attributes
        $slug_2     = 'miranda';

        register_taxonomy( $taxonomy_2, 'post' ); 
        $term_id_2 = $this->factory->term->create( array('name' => $slug_2, 'taxonomy' => $taxonomy_2, 'slug' => $slug_2) ); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id_1), $taxonomy_1 );
        wp_set_post_terms( $post_id, array($term_id_2), $taxonomy_2 ); 

        $posts = filter_experiences_by(array('s' => 'costas', $taxonomy_1 => $slug_1, $taxonomy_2 => 'caracas')); 
        $this->assertInternalType('bool', $posts); 
        $this->assertFalse($posts); 
    }

    public function testThreeFiltersSearch() {
        $taxonomy_1 = 'pa_fechas-de-experiencia'; // This is for worpress attributes
        $slug_1     = 'enero';

        register_taxonomy( $taxonomy_1, 'post' ); 
        $term_id_1 = $this->factory->term->create( array('name' => $slug_1, 'taxonomy' => $taxonomy_1, 'slug' => '1-de-enero-de-2016')); 

        $taxonomy_2 = 'pa_destination'; // This is for worpress attributes
        $slug_2     = 'miranda';

        register_taxonomy( $taxonomy_2, 'post' ); 
        $term_id_2 = $this->factory->term->create( array('name' => $slug_2, 'taxonomy' => $taxonomy_2, 'slug' => $slug_2) ); 

        $post_id = $this->factory->post->create(array('post_type' => 'product', 'post_title' => 'Costas mirandinas')); 
        wp_set_post_terms( $post_id, array($term_id_1), $taxonomy_1 );
        wp_set_post_terms( $post_id, array($term_id_2), $taxonomy_2 ); 

        $posts = filter_experiences_by(array('s' => 'costas', $taxonomy_1 => $slug_1, $taxonomy_2 => 'miranda')); 

        $this->assertInternalType('array', $posts); 
        $this->assertCount(1, $posts); 
        $this->assertEquals($post_id, $posts[0]); 
    }

    public function testSendGridUrl() {
        $action = 'add';
        $data   = array(
                    'name'     => 'bar',
                    'email'    => 'qux'
                );
        $args = array(
            array(
                'key' => 'list',
                'value' => 'foo',
                'JSON' => false
            ),
            array(
                'key' => 'data',
                'value' => $data,
                'JSON' => true
            )
        );
        // Manual procedure
        $json_data = json_encode($data); 
        $query = array(
            'api_user' => 'sendgrid_user',
            'api_key'  => 'sendgrid_password',
            'list' => 'foo',
            'data' => $json_data
        );
        $request_info = generate_sendgrid_api_url('newsletter/lists/email', $action, $args); 
        $url     = $request_info['request_url'];
        $request = $request_info['request'];
        $this->assertEquals('https://api.sendgrid.com/api/newsletter/lists/email/add.json', $url);
        $this->assertEquals(http_build_query($query), $request);
	}

    public function testEmailTokenGeneration() {
        $validation_regex = "/^([a-zA-Z0-9])+$/"; 
        $token_length = 32; 
        $token = generate_token($token_length); 

        $this->assertInternalType('string', $token); 
        $this->assertEquals($token_length, strlen($token)); 
        $this->assertEquals(1,preg_match($validation_regex, $token)); 
    }

    public function testUnconfirmedUserEmail() {
        $user_id = $this->factory->user->create(); 
        $confirmed = is_email_confirmed($user_id); 
        $this->assertFalse($confirmed); 
    }

    public function testConfirmedUserEmail() {
        $user_id = $this->factory->user->create(); 
        $confirmation_meta_key = 'email_confirmed';
        delete_user_meta($user_id, $confirmation_meta_key);
        add_user_meta($user_id, $confirmation_meta_key, array('value' => true, 'token' => ''));
        //add_user_meta($user_id, $confirmation_meta_key, true);
        //var_dump(get_user_meta($user_id, $confirmation_meta_key, true)); 

        $confirmed = is_email_confirmed($user_id); 
        $this->assertInternalType('array', $confirmed); 
        $this->assertTrue($confirmed['value']); 
    }

    public function testUnexistentUserEmailConfirmation() {
        $user_id = -1;
        $confirmation_meta_key = 'email_confirmed';
        $confirmed = is_email_confirmed($user_id); 
        $this->assertFalse($confirmed); 
    }

    public function testUserEmailConfirmation()  {
        $token_length = 32; 
        $token = generate_token($token_length); 

        $user_id = $this->factory->user->create(); 
        $confirmation_meta_key = 'email_confirmed';
        delete_user_meta($user_id, $confirmation_meta_key);
        add_user_meta($user_id, $confirmation_meta_key, array('value' => false, 'token' => $token));
        // Unconfirmed before confirmation
        $confirmed = is_email_confirmed($user_id); 
        $this->assertFalse($confirmed); 

        $user = new WP_User( $user_id ); 

        confirm_email($user->user_login, $token); 

        // Confirmed after confirmation
        $confirmed = is_email_confirmed($user_id); 
        $this->assertInternalType('array', $confirmed); 
        $this->assertTrue($confirmed['value']); 
        $this->assertEquals($token, $confirmed['token']); 
    }

    public function testConfirmationURL() {
        $user_login   = 'juan';
        $token_length = 32;
        $token        = generate_token($token_length);

        $url = generate_confirmation_url($user_login, $token); 
        $expected_url = get_bloginfo('url') . '/email_confirmation?user_login=' . $user_login . '&conf_token=' . $token; 
        $this->assertEquals($expected_url, $url); 
    }
}
