<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */


/*
 * Define the type of server (dev, staging or production) 
 */

define ( 'DB_CREDENTIALS_PATH', dirname(ABSPATH) ); 

/*
 * If you are in local, create local-wp-config.php with your database credentials
 * and same for staging
 */
define ( 'WP_LOCAL_SERVER', file_exists( DB_CREDENTIALS_PATH . '/local-wp-config.php') ); 
define ( 'WP_STAGING_SERVER', file_exists( ABSPATH . '/staging-wp-config.php') ); 


// Load the correct credentials depending on variables defined earlier
if ( WP_LOCAL_SERVER ) {
    require DB_CREDENTIALS_PATH . '/local-wp-config.php';
    /**
     * Para desarrolladores: modo debug de WordPress.
     *
     * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
     * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
     * en sus entornos de desarrollo.
     */
    define('WP_DEBUG', true);
    define('WP_DEBUG_LOG', true );
    define('WP_MEMORY_LIMIT', '96M' );

} elseif ( WP_STAGING_SERVER ) {
    require ABSPATH . '/staging-wp-config.php';
} else {
    require DB_CREDENTIALS_PATH . '/prod-wp-config.php';
}
/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[JP{{&/v|;}< (J`N=]ia37=wSx 88h_i4}20-S$wC.!7osZpsBT*dH>L8[)_iY4');
define('SECURE_AUTH_KEY',  '7HoaTqc@GKfV7gnTLH#L(@InRD1Fx>%()$tx&oh%)A6deW;-2O_~UBLu^6S9[#gL');
define('LOGGED_IN_KEY',    'uV!=_IBqYdpxq/1M4=XOXuuXgu5vMl86Ze0}A0_$5|(ocvT[=WWJt#t5QIlaM5RL');
define('NONCE_KEY',        '?w-02Bf3 >-+z*85|zgkpJU3|Xc6G.q-:z;uh5e_#M({ZORjffn)^FdjO>.*(6*y');
define('AUTH_SALT',        'mLN|;HKQ-=G=Dz@meeYCwGJILSM+khmHY_/Kt+3HY8t+_*xh(+vB2Ay5j[liUt>U');
define('SECURE_AUTH_SALT', 'aH+^8.aBE+:)} Ia&u5UFE3JaX6b,XEC`uOs=i+ve_g-~!2K-[<ZHQu&-Cw-4vi3');
define('LOGGED_IN_SALT',   'POD`*)Hb3+F_T{Ce`gvbJY`_sqB,n#gK>-l-%IN$*s`2E71n{P8ipaW+9A7CLDO3');
define('NONCE_SALT',       'IKj!.#n?*%+*ccAb5RBY5W&`M8i1;7fH? PcP4IR[-+BN-xVnI^IKMC4E`zZ.HH5');

// Sendgrid configuration
define('SENDGRID_USERNAME', 'zoivzla'); 
define('SENDGRID_PASSWORD', 'Cachimbo0101'); 
define('SENDGRID_FROM_NAME', 'ZOI Venezuela'); 
define('SENDGRID_FROM_EMAIL', 'hola@zoivenezuela.com'); 
define('SENDGRID_REPLY_TO', 'hola@zoivenezuela.com'); 

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';

/**
 * Ensures events are scheduled
 */
//define('ALTERNATE_WP_CRON', true);

// define ('WPLANG', 'es_ES');

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Changing file permissions on admin panel*/
if(is_admin()) {
    add_filter('filesystem_method', create_function('$a', 'return "direct";' ));
    define( 'FS_CHMOD_DIR', 0751 );
}
