<?php
/**
 * The template used for displaying the Featured testimonials
 *
*/
$j = 0; 
$i = 0; 
$testimonials = CFS()->get('content_informacion_testimonial', 111);
$number = sizeof($testimonials);
?>

<?php if ($testimonials != '') { ?>
<section>
<div id="carousel-example-generic-2" class="carousel slide testimonials-slider" data-ride="carousel">
  <!-- Indicators -->

		<ol class="carousel-indicators">
		<?php while ($j < $number) { 
			if ($j==0){
				echo '<li data-target="#carousel-example-generic-2" data-slide-to="0" class="active"></li>';
			} 
			else{
				echo '<li data-target="#carousel-example-generic-2" data-slide-to="'.$j.'"></li>';
			} 
			$j++; 
		 }?>
		</ol>

  <!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
	<?php foreach ($testimonials as $testimonial) { ?>

			<div class="item <?php if ($i==0) { echo 'active'; } ?> block_featured_testominial ">
				<img src="<?php echo $testimonial['foto_fondo']; ?>" alt="<?php echo $testimonial['nombre_autor']; ?>" style="border-radius:0px">
				<div class="carousel-caption gradient-color-content">
						<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-centered text-center">
							<div class="box_avatar_block"><img src="<?php echo $testimonial['foto_autor']; ?>" alt="<?php echo $testimonial['nombre_autor']; ?>"></div>
							<h4><?php echo $testimonial['nombre_autor']; ?></h4>
							<p><?php echo $testimonial['contenido_testimonial']; ?></p>
							<span><?php echo $testimonial['ubicacion__fecha_experiencia']; ?></span>
						</div>
				</div>
			</div>

			<?php $i=1;?>

	<?php }?>
	</div>
</div>
</section>

<div style="clear: both;"></div>
<?php } ?>


