<?php


DEFINE('MINOR_VERSION', 1); 
DEFINE('MEDIUM_VERSION', 0); 
DEFINE('MAJOR_VERSION', 1); 
DEFINE('VERSION', MAJOR_VERSION . '.' . MEDIUM_VERSION . '.' .MINOR_VERSION); 

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
    wp_enqueue_style( 'genericons-style', get_template_directory_uri().'/genericons/genericons.css' );

    wp_dequeue_style('twentyfifteen-style');
}

// XXX 
// This file contains all the dependencies to be used in the whole project.
// If you consider any function written in here belongs to another file, feel
// free to move it and require the file if it hasn't been required already.
//
// The source code is located in <theme root>/src
require_once('src/helpers.php');
require_once('src/setup.php');
require_once('src/woocommerce-setup.php');
require_once('src/woocommerce-products.php');
require_once('src/layout.php');
require_once('src/transactionals.php');
require_once('src/async.php');
require_once('src/reportes.php');
require_once('src/subscription.php');
require_once('src/calendar.php');
require_once('src/guru.php');
require_once('src/mailer.php');
require_once('src/cart.php');
require_once('src/user-confirmation.php');
require_once('src/user-invitations.php');
require_once('src/search-helpers.php');
require_once('src/create-experience.php');
require_once('src/social.php');
require_once('src/contact.php');
require_once('src/accounts.php');


// Other requires
require_once('wp_bootstrap_navwalker.php');

tarful_send_mail();

// Hide panels for users in WP admin panel (on Product detail)
add_action( 'admin_menu', 'tarful_hide_tags_panel');
function tarful_hide_tags_panel(){
    if ( !is_admin() ) { // only remove for for user not-admins
        remove_meta_box( 'tagsdiv-product_tag','product','normal' ); // Product Tags panel
    }
}

//Hide Prodcut Sub Menus  left menu for users in WP admin panel 
add_action( 'admin_menu', 'tarful_hide_tags_menu', 999 );
function tarful_hide_tags_menu() { 
    if ( !is_admin() ) { // only remove for for user not-admins
        remove_submenu_page( 'edit.php?post_type=product', 'woocommerce_attributes');
        remove_submenu_page( 'edit.php?post_type=product', 'edit-tags.php?taxonomy=product_shipping_class&post_type=product');
        remove_submenu_page( 'edit.php?post_type=product', 'edit.php?post_type=product&page=product_attributes');

    }
}


//Hide Prodcut Tags left menu for users in WP admin panel 
add_action( 'init', 'remove_taxonomy_menu_pages', 999 );
function remove_taxonomy_menu_pages() {
    // 	if ( !is_admin() ) { // only remove for for user not-admins
    register_taxonomy('product_tag', 
        'woocommerce_taxonomy_objects_product_tag', array('show_ui', false)
    );
    // 	}
}

/**
 *
 * Hide Woocommerce left menu for users in WP admin panel. Available submenu slugs are:
 *    - wc-addons - the Add-ons submenu
 *    - wc-status - the System Status submenu
 *    - wc-reports - the Reports submenu
 *    - edit.php?post_type=shop_order - the Orders submenu
 *    - edit.php?post_type=shop_coupon - the Coupons submenu
 * @return void
 */
function tarful_remove_woocoomerce_items() {
    $remove = array( 'wc-settings', 'wc-status', 'wc-addons', );
    foreach ( $remove as $submenu_slug ) {
        // 	if ( !is_admin() ) { // only remove for for user not-admins
        //remove_submenu_page( 'woocommerce', $submenu_slug );
        //remove_submenu_page( 'woocommerce', $submenu_slug );
        //	 }
    }
}
add_action( 'admin_menu', 'tarful_remove_woocoomerce_items', 99, 0 );


/**
 * Hide Woocommerce option fields on product detail panel
 * @return void
 */
function tarful_remove_woocommerce_options() {
    wp_enqueue_style( 'style-hide-panel', get_stylesheet_directory_uri() . "/style-hide-panel.css", false, "1.0", "all" );	
}
add_action('admin_enqueue_scripts', 'tarful_remove_woocommerce_options');
add_action( 'admin_init', 'tarful_remove_woocommerce_options' );


/** Footer Admin Panel
 * @return void
 */
function tarful_footer_admin () {
    echo 'Diseñado y Desarrollado por <a href="http://www.tarful.com" target="_blank">Tarful</a> - Expertos en UX, Web y Apps</p>';
}
add_filter('admin_footer_text', 'tarful_footer_admin');

/** 
 * Limit content lenght and add 3 dot at the end
 * @param string $content
 * @param integer $length
 * @return $string truncated string
 */
function tarful_fixed_lenght_notags($content, $lenght) {

    $lenght_fixed = $lenght - 3;					 
    $string = strip_tags($content);
    $string = (strlen($string) > $lenght) ? substr($string,0,$lenght_fixed).'...' : $string;

    echo $string;								
}


/** 
 * Custom logo in WP login
 * @return void
 */
function tarful_login_logo() { ?>
    <style type="text/css">
        body {
            background: #1C212A;
        }
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo_zoi.png);
        }

        input#wp-submit {
            background-color: #E74C3C !important;
            color: #FFF !important;
            text-transform: uppercase !important;
            border-radius: 3px;
            padding: 2px 5%;
            padding-bottom: 3px;
            text-shadow: none;
            box-shadow: 0px 3px 0px 0px #B32E20;
            border: 0px;
            font-size: 10px;
            font-weight: bold;
        }		

        input[type="text"]:focus,
        input[type="password"]:focus {
            box-shadow: 0px 0px 0px;
            background: white !important;
            border-color: none !important;
        }



    </style>
<?php }
add_action( 'login_enqueue_scripts', 'tarful_login_logo' );


/**
 * Custom Styles for WP login page
 * @return void
 */
function tarful_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/style-login.css' );
    //wp_enqueue_script( 'custom-login', get_template_directory_uri() . '/style-login.js' );
}
add_action( 'login_enqueue_scripts', 'tarful_login_stylesheet' );


/**
 * Change Shop's title on Browser
 * @param string $title
 * @return void
 */
function wc_custom_shop_archive_title( $title ) {
    if ( is_shop() ) {
        return str_replace( __( 'Products', 'woocommerce' ), 'Experiencias', $title );
    }

    return $title;
}
add_filter( 'wp_title', 'wc_custom_shop_archive_title' );


/** 
 * Display in WC order panel link for related supoort payment uploaded by client for order 
 * @param object order
 * @return void
 */
function tarful_display_order_data( $order ){  
    global $wpdb;
    $order_id = $order->id;
?>

    <div class="order_data_column" style="width:100%;">
        <h4><?php _e( 'Soporte de Pago', 'woocommerce' ); ?></h4>			     

<?php 
    $payment_method = get_post_meta( $order_id, '_payment_method', true ); 
    if ($payment_method == 'bacs') { // bacs is Depost or Transfer 
        $payment_support_id = '';
        $payment_support_id = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_type = 'payment_support' AND post_parent = $order_id" );
        if ($payment_support_id != '') {

            $payment_support_meta = $wpdb->get_results( "SELECT guid, post_modified FROM $wpdb->posts WHERE post_type = 'attachment' AND post_parent = $payment_support_id" );
?>
                 <?php foreach ($payment_support_meta as $meta) { ?>
                    <p><a style="color:#0073AA !important;" href="<?php echo $meta->guid; ?>" target="_blank"><?php echo '<strong>' . __( 'Ver soporte' ) . '</strong>'; ?></a><span style="font-size: 11px;"> - Enviado: <?php echo $meta->post_modified; ?></span></p>
<?php }
        } else {
            echo '<p>Cliente no ha cargado Soporte de Pago todavía</p>';
        }
    }

?>

    </div>
<?php }
add_action( 'woocommerce_admin_order_data_after_order_details', 'tarful_display_order_data' );


/**
 * Change link in logo of WP-admin login page
 * @return $link string with link
 */
function tarful_wpadmin_custom_link() {
    return 'http://zoivenezuela.com';
}
add_filter('login_headerurl','tarful_wpadmin_custom_link');


/**
 * Change tooltip in logo of WP-admin login page
 * @return $tooltip string with tooltip
 */
function tarful_wpadmin_custom_tooltip() {
    return 'Portal ZOI Venezuela - Diseño y Desarrollo por Tarful.com';
}
add_filter('login_headertitle', 'tarful_wpadmin_custom_tooltip');


/**
 * Adding extra info in user profile
 * @param object $user
 * @return void
 */
function tarful_custom_user_profile_fields( $user ) {

?>
    <h3><?php _e('Información Adicional', 'your_textdomain'); ?></h3>

    <table class="form-table">
        <tr>
            <th>
                <label for="twitter"><?php _e('twitter', 'your_textdomain'); ?>
            </label></th>
            <td>
                <input type="text" name="twitter" id="twitter" placeholder="" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
            </td>
        </tr>
         <tr>
            <th>
                <label for="facebook"><?php _e('Facebook', 'your_textdomain'); ?>
            </label></th>
            <td>
                <input type="text" name="facebook" id="facebook" placeholder="" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
            </td>
        </tr>
        <tr>
            <th>
                <label for="instagram"><?php _e('Instagram', 'your_textdomain'); ?>
            </label></th>
            <td>
                <input type="text" name="instagram" id="instagram" placeholder="" value="<?php echo esc_attr( get_the_author_meta( 'instagram', $user->ID ) ); ?>" class="regular-text" /><br />
            </td>
        </tr>

    </table>
<?php }

/**
 * Display ZOI comunity
 * @return void
 */
function taful_comunidad_zoi(){
?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 block_network text-center">
        <div class="col-xs-16 col-sm-16 col-md-10 col-lg-10 col-centered text-center">
            <center>

                <h2>Ya somos una comunidad de <u><?php tarful_users_count(); ?></u> aventureros</h2>

                <?php tarful_networks_avatars(9, 0); // Cambiar 2 por 9 ?>
                <div style="clear:both;"></div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 block_network text-cente box_main_social_login">
                    <?php do_action( 'wordpress_social_login' ); // only shows when user is not logged-in ?>
                </div>

                <div class="box_stamp">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/stamp.png" alt="Calidad ZOI"/>
                </div>

            </center>

        </div>
    </div>
<?php
}

/**
 * Box to download excel from experience
 * @param object $post
 * @param object $args
 * @return void
 */
function myplugin_meta_box_callback( $post, $args ) {
    global $wpdb;

    $related_orders = array(); 
    $current_product_id = $post->ID;

    $users = 0; 

    $orders = get_posts( array(
        'post_type'   => 'shop_order',
        'post_status' => array( 'wc-processing', 'wc-on-hold' ),
        'numberposts' => -1
    )
);

    foreach($orders as $post_order) {    
        $order_id = $post_order->ID;
        $order = new WC_Order($order_id);

        $items = $order->get_items(); 

        foreach ( $items as $item ) {
            $product_id   = $item['product_id'];
            $variation_id = $item['variation_id'];
            $traveler_id  = $product_id . '_' . $variation_id;
            // Save order id if the current product was bought on it
            if ($product_id === $current_product_id) {
                $users += get_post_meta( $order_id, "number_of_clients_$traveler_id", true );
            }
        }
    }


?>

    <div style="text-align:center;">
        <div style="margin:10px;"> Total de usuarios registrados en Experiencia</div>
        <h2 style="margin-top: 5px;"><?php echo $users ?></h2>
        <div style="padding: 10px;clear: both;border-top: 1px solid #DDD;background: #F5F5F5 none repeat scroll 0% 0%;">
            <a href="<?php echo get_site_url(); ?>/exportar-excel/?post_id=<?php echo $post->ID; ?>" class="button-primary">Descargar listado (excel) </a>
        </div>
    </div>
<?php
}

add_filter('wp_footer', 'page_gui_js'); 
/**
 * Enqueues necessary javascript to footer
 * @return void
 */
function page_gui_js() {
    // datepicker and bootstrap
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script( 'bootstrapjs', get_bloginfo('stylesheet_directory').'/bootstrap/js/bootstrap.min.js' );

    // Fullpage calendar
    wp_enqueue_script( 'fullpage-calendarESjs', get_bloginfo('stylesheet_directory').'/bootstrap-fullpage-calendar/js/language/es-ES.js' );
    wp_enqueue_script( 'fullpage-calendarjs', get_bloginfo('stylesheet_directory').'/bootstrap-fullpage-calendar/js/calendar.min.js' );
    wp_enqueue_style( 'fullpage-calendar', get_bloginfo('stylesheet_directory').'/bootstrap-fullpage-calendar/css/calendar.min.css');	
    wp_enqueue_style( 'underscore', get_bloginfo('stylesheet_directory').'/underscore/underscore-min.js');	

    // Local page gui javascript
    wp_enqueue_script('page-gui', get_bloginfo('stylesheet_directory') . '/js/page-gui.js');
}

add_filter('wp_footer', 'show_login_dialog'); 

/**
 * Shows login/register dialog to not auth users in front page
 * @return void
 */
function show_login_dialog() {
    // Get URL subscribed get var
    $subscribed = isset($_GET['subscribed']) ? true : false; 
    if (!is_user_logged_in() && is_front_page() && !$subscribed) { ?>
        <div id="login-modal" class="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4> ¿Quieres enterarte de </br>las mejores experiencias?</h4>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="social-login">
                                <div id="social" class="tab-buttons-panel">

                                    <div class="wp-social-login-provider-list">
                                        <form onsubmit=" return false;">
                                          <div class="form-group">
                                            <label for="inputEmail1">Email:</label>
                                            <input type="email" class="form-control" id="emailSubscription" placeholder="leonardo@ejemplo.com">
                                          </div>

                                          <div class='login-email-tab'><a id="submitEmailSubscription" href="#"  class='login-next-tab login-footer-btn' rel='1'>Suscribirme</a></div>

                                        </form>
                                        <p class="extra-msg">Suscríbete, es gratis y toma un segundo</p>
                                    </div>


                                </div> <!-- social -->
                            </div> <!-- tab panel --> 
                        </div> <!-- tab content -->
                    </div> <!-- /.modal-body -->
                    <div class="modal-footer">
                        ¿Aún no tienes cuenta? <a href="<?=get_permalink(7)?>"> ¡Regístrate!</a>
                    </div> <!-- /.modal-footer -->
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script type="text/javascript">
        jQuery(function($) {
            if (!jQuery.cookie('register-cookie')) {
                $('#login-modal').attr('style', ''); 
                $('#login-modal').modal('show') 
                    var minutes = 1800000; 
                var date = new Date(); 
                date.setTime(date.getTime() + minutes); 
                jQuery.cookie('register-cookie', true, {expires: date});
    }  
    })
        </script>
<?php }
}

add_filter('wp_footer', 'hide_nav_search_form'); 
/**
 * Hides the search bar when viewport is top 0 
 * @return void
 */
function hide_nav_search_form() {
    wp_enqueue_script( 'jquery-visible', get_bloginfo('stylesheet_directory').'/js/jquery-visible/jquery.visible.min.js' );
    if (is_front_page()) { ?>
    <script type="text/javascript">
        jQuery(function($){
            if (!$('.main-search-form').visible(true)) {
                $('.navbar-search').hide(); 
    }
    $(window).on("scroll", function(){
        if ($('.main-search-form').visible(true) && $('.navbar-search').css('display') !== 'none') {
            $('.navbar-search').hide('fast'); 
    } else if (!$('.main-search-form').visible(true) && $('.navbar-search').css('display') === 'none') {
        $('.navbar-search').show('fast'); 
    }
    }); 
    });
    </script>
<?php
    }
}

add_filter('wp_footer', 'new_experience_modal'); 

/**
 * Adds some click events to footer
 * @return void
 */
function new_experience_modal() {
?>
    <script type="text/javascript">
    jQuery(function($){
        $(document).on('click', "#create_experiencie", function( event ){
            event.preventDefault();
            $('#new-experience-modal').modal('show'); 
});

$(document).on('submit', 'form#experience-form', function() {
    var hasError = false;
    $('form#experience-form .required').each(function() {
        if(!$(this).val()) {
            var labelText = $(this).prev('label').text();
            $(this).addClass("form-error");
            hasError = true;
}else{
    $(this).removeClass('form-error');
}
});
if (hasError) {
    return false;
}else{
    document.forms["experience-form"].submit();
    return true;
}
});


});
</script>
    <?php form_create_experience(); ?>
<?php
}



// Save variation fields
//woocommerce_add_to_cart

add_action( 'woocommerce_add_to_cart', 'store_custom_fields_in_session', 10, 6 );

/**
 * Stores custom fields in session 
 * @param $cart_item_key 
 * @param $product_id 
 * @param $quantity 
 * @param $variation_id 
 * @param $variation
 * @param $cart_item_data 
 * @return void
 */
function store_custom_fields_in_session( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {

    if ( isset($_POST['experience_date']) && !empty($_POST['experience_date']) ) {
        $experience_date = $_POST['experience_date'];
        lm_set_item_data( $cart_item_key, 'experience_date', $experience_date ); 
    }
}




add_action('woocommerce_add_order_item_meta', 'save_cart_item_fields', 10, 3); 

/**
 * Saves the item cart fields in the order
 * @param integer $post_id
 * @return void
 */

function save_cart_item_fields( $item_id, $values, $cart_item_key ) {


    $experience_date = lm_get_item_data( $cart_item_key, 'experience_date' ); 

    if ( !empty($experience_date) ) {
        wc_add_order_item_meta( $item_id, 'experience-date', $experience_date ); 
    }
    $extra_people = lm_get_item_data( $cart_item_key, 'extra_people' ); 

    if ( !empty($extra_people) ) {
        wc_add_order_item_meta( $item_id, 'extra_people', $experience_date ); 
    }

}

add_filter('woocommerce_add_cart_item_data', 'custom_add_item_data', 10, 2);
add_filter('woocommerce_get_cart_item_from_session','wc_get_cart_item_from_session',10,2);
add_filter('woocommerce_get_item_data', 'wc_get_item_data', 10, 2);


/**
 * Add custom item data to cart
 * @param object $cart_item_meta
 * @param integer $product_id
 * @return $cart_item_meta modified with new data
 */
function custom_add_item_data($cart_item_meta, $product_id){

    if ( isset($_POST['experience_date']) && !empty($_POST['experience_date']) ) {

        $cart_item_meta['experience_date'] = $_POST['experience_date'];
    }

    $cart_item_meta['extra_people'] = 0;

    if ( isset($_POST['additional-children']) && !empty($_POST['additional-children']) ) {
        $cart_item_meta['extra_people'] += intval($_POST['additional-children']); 
        $cart_item_meta['additional_children'] += intval($_POST['additional-children']); 
    }

    if ( isset($_POST['additional-adult']) && !empty($_POST['additional-adult']) ) {
        $cart_item_meta['extra_people'] += intval($_POST['additional-adult']); 
        $cart_item_meta['additional_adult'] += intval($_POST['additional-adult']); 
    }

    return $cart_item_meta;
}

/**
 * Gets the cart item from the session
 * @param object $cart_item 
 * @param object $values
 * @return $cart_item modified with new data
 */
function wc_get_cart_item_from_session($cart_item, $values) {

    if ( isset($values['experience_date']) && !empty($values['experience_date']) ) {
        $cart_item['experience_date'] = $values['experience_date'];
    }

    return $cart_item;
}


function wc_get_item_data($other_data, $cart_item) {

    if (isset($cart_item['experience_date'])) {

        $data = $cart_item['experience_date'];

    }
    else
        $data = "";
    return $data;
}
/**
 * Removing single product summary actions
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );



add_filter('wp_footer', 'show_woocommerce_checkout_inline_errors_js'); 
/**
 * Enqueues javascript with custom checkout inline errors
 * @return void
 */
function show_woocommerce_checkout_inline_errors_js() { 
    if ( is_checkout() ) {
        wp_enqueue_script('custom-checkout', get_bloginfo('stylesheet_directory') . '/js/custom-checkout.js');
    }
}
