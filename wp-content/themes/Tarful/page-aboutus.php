<?php
/**
 * Template Name:About Us
 *
 * by Tarful
 *
 */
get_header();
?>
			<section class="about-us">
				<div class="row">
					<?php $url = wp_get_attachment_url (get_post_thumbnail_id ($post->ID)); ?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 archive-banner archive-banner-aboutus" style="width:100%; height: 700px; overflow:hidden; padding:0; background: url('<?php echo $url; ?>') no-repeat; background-size:cover;background-position:center center;">
						<?php // the_post_thumbnail('large'); ?>
						<div class="about-us-title text-center">
							<img src="<?php bloginfo('url'); ?>/wp-content/themes/Tarful/images/logo_zoi.png" alt="zoi">
							<img  class="quienes" src="<?php bloginfo('url'); ?>/wp-content/themes/Tarful/images/quienes.png" alt="quienes">
						</div>
					</div>
				</div>
				<div class="about-us-content">
					<div class="container">
						<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 col-lg-5 " style="background: url('<?php bloginfo('url'); ?>/wp-content/themes/Tarful/images/stamp.png') no-repeat;background-position:right bottom; padding:0;">
							<?php the_content(); ?>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2 about-us-user-info text-center">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<span><?= empty(CFS()->get('equipo')) ? '13' : CFS()->get('equipo') ?></span>
								<p><?php _e('EQUIPO'); ?></p>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<?php $count_experiences = wp_count_posts('product'); ?>
								<span><?php echo $count_experiences->publish; ?></span>
								<p><?php _e('EXPERIENCIAS'); ?></p>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<span><?php tarful_gurus_users_count(); ?></span>
								<p><?php _e('GURÚS'); ?></p>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<span><?php tarful_users_count(); ?></span>
								<p><?php _e('COMUNIDAD'); ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid about-us-valores">
					<div class="row varios">
						<div class="hidden-xs col-sm-6 col-md-6 col-lg-6" style="background: url('<?php echo CFS()->get('imagen_varios_1'); ?>') no-repeat;background-size:contain;background-position:left bottom;">
							<div class="col-xs-5 col-xs-offset-7 col-sm-4 col-sm-offset-6 col-md-4 col-md-offset-6 col-lg-2 col-lg-offset-8" style="padding-top:80px;">
								<span><?php echo CFS()->get('numero_varios_1'); ?> </span>
								<p><?php echo CFS()->get('texto_varios_1'); ?> </p>
							</div>
						</div>
						<div class="hidden-xs col-sm-6 col-md-6 col-lg-6" style="background: url('<?php echo CFS()->get('imagen_varios_2'); ?>') no-repeat;background-size:contain;background-position:right bottom;">
							<div class="col-xs-5 col-sm-4 col-md-4 col-md-offset-1 col-lg-2 col-lg-offset-3 text-right" style="padding-top:50px;">
								<span><?php echo CFS()->get('numero_varios_2'); ?> </span>
								<p><?php echo CFS()->get('texto_varios_2'); ?> </p>
							</div>
						</div>
						<div class="hidden-xs col-sm-6 col-md-6 col-lg-6" style="background: url('<?php echo CFS()->get('imagen_varios_3'); ?>') no-repeat;background-size:contain;background-position:right bottom;">
							<div class="col-xs-5 col-sm-4 col-md-4 col-md-offset-1 col-lg-2 col-lg-offset-3 text-right" style="padding-top:150px;">
								<span><?php echo CFS()->get('numero_varios_3'); ?> </span>
								<p><?php echo CFS()->get('texto_varios_3'); ?> </p>
							</div>
						</div>
						<div class="hidden-xs col-sm-6 col-md-6 col-lg-6" style="background: url('<?php echo CFS()->get('imagen_varios_4'); ?>') no-repeat;background-size:contain;background-position:right bottom;">
							<div class="col-xs-5 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-2 col-lg-2 col-lg-offset-4 text-right" style="padding-top:200px;">
								<span><?php echo CFS()->get('numero_varios_4'); ?> </span>
								<p><?php echo CFS()->get('texto_varios_4'); ?> </p>
							</div>
						</div>
                        <div class="col-xs-12 hidden-sm hidden-md hidden-lg" style="background: url('<?php echo CFS()->get('imagen_varios_movil'); ?>') no-repeat;background-size:auto 425px;background-position:right bottom;">
                        </div>
                    </div>
                    <div class="contaner-fluid team">
                        <div class="row">
                                <?php
                                $team = CFS()->get('integrantes');
                                foreach ($team as $loop => $trow) { ?>
                                    <div class="col-xs-12 col-lg-2 col-md-4 col-sm-6  member">
                                        <div style="background: url('<?php echo $trow['fotografia']; ?>') no-repeat; background-size:auto 400px;background-position:center center;height:100%">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right member-name" style="background: url('<?php echo $trow['fotografia_hover']; ?>') no-repeat; background-size:auto 400px;background-position:center center;height:100%">
                                                <span><?php echo $trow['nombre']; ?></span>
                                            </div>						
                                        </div>
                                    </div>
                                <?php }	?>
                        </div>
                    </div>
				

					<div class="row" style="background: url('<?php echo CFS()->get('fondo'); ?>') no-repeat;background-size:cover;background-position:center center;">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center valores">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="font-size:30px;margin-bottom:40px;">
								<strong><?php _e('NUESTROS VALORES'); ?></strong>
							</div>
							<?php 
							$valores = CFS()->get('valores');
							foreach ($valores as $row) { ?>
								<div class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-3 col-md-offset-2 col-lg-3 col-lg-offset-2" style="height:350px;">
									<?php if ( $row['imagen'] != '') { ?>
										<img class="img-responsive" src="<?php echo $row['imagen']; ?>" alt="">		
									<?php }else{ ?>
										<div class="valores-placehoolder"></div>
									<?php } ?>
									<p><?php echo $row['texto']; ?></p>
								</div>
							<?php
							}
							?>
						</div>							
					</div>
				</div>

            <!-- FRIENDS -->
            <?php
                $friends = CFS()->get('friends');
                if ($friends && !empty($friends)): ?>
                    <div class="container" style="padding-top:70px">

                        <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="color:#000;font-size:30px;margin-bottom:40px;">
                            <strong><?php _e('AMIGOS DE LA CASA'); ?></strong>
                        </div>
                        <?php foreach ($friends as $friend): ?>
                            <div class="col-xs-6 col-sm-3">
                                <div class="thumbnail">
                                <img style="width:360px;margin:0" src="<?= array_key_exists('image', $friend) ? $friend['image'] : 'https://placehold.it/300x300'?>" /> 
                                    <div class="caption">
                                    <h4 style="text-transform:none"> <?=$friend['name']?> </h4>
                                        <p><i><?=$friend['company']?></i></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
				<div class="container" style="padding-top:70px;">
					<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 about-us-galery">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="height:350px;">
							<div style="height:100%;background: url('<?php echo CFS()->get('imagen_1'); ?>') no-repeat; background-size:cover;background-position:center center;"></div>
							<!--img src="<?php echo CFS()->get('imagen_1'); ?>" alt="">-->
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="height:250px;">
							<div style="height:100%;background: url('<?php echo CFS()->get('imagen_2'); ?>') no-repeat; background-size:cover;background-position:center center;"></div>
							<!--<img src="<?php echo CFS()->get('imagen_2'); ?>" alt="">-->
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="height:250px;">
							<div style="height:100%;background: url('<?php echo CFS()->get('imagen_3'); ?>') no-repeat; background-size:cover;background-position:center center;"></div>
							<!--<img src="<?php echo CFS()->get('imagen_3'); ?>" alt="">-->
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8" style="height:450px;float:right;">
							<div style="height:100%;background: url('<?php echo CFS()->get('imagen_5'); ?>') no-repeat; background-size:cover;background-position:center center;"></div>
							<!--<img src="<?php echo CFS()->get('imagen_5'); ?>" alt="">-->
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="height:350px;">
							<div style="height:100%;background: url('<?php echo CFS()->get('imagen_4'); ?>') no-repeat; background-size:cover;background-position:left center;"></div>
							<!--<img src="<?php echo CFS()->get('imagen_4'); ?>" alt="">-->
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="height:250px;">
							<div style="height:100%;background: url('<?php echo CFS()->get('imagen_6'); ?>') no-repeat; background-size:cover;background-position:center center;"></div>
							<!--<img src="<?php echo CFS()->get('imagen_6'); ?>" alt="">-->
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="height:250px;">
							<div style="height:100%;background: url('<?php echo CFS()->get('imagen_7'); ?>') no-repeat; background-size:cover;background-position:center center;"></div>
							<!--<img src="<?php echo CFS()->get('imagen_7'); ?>" alt="">-->
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="height:250px;">
							<div style="height:100%;background: url('<?php echo CFS()->get('imagen_8'); ?>') no-repeat; background-size:cover;background-position:center center;"></div>
							<!--<img src="<?php echo CFS()->get('imagen_8'); ?>" alt="">-->
						</div>

					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
							<div class="row">
								<div class="hidden-xs">
									<h3 class="titulo_instagram">SÍGUENOS EN INSTAGRAM Y COMPARTE TUS EXPERIENCIAS</h3>
								</div>
								<div class="hidden-xs">
									<div id="instafeed"></div>
								</div>
								<div class="hidden-xs" style="text-align: center; margin-bottom: 11%; margin-top: 2%;"><a class="link_instagram" href="https://instagram.com/zoivenezuela" target="_blank"><?php echo _e("Ver Más"); ?></a></div>
							</div>
						</div>
					</div>
				</div>

				<div class="contaner-fluid team">
					<div class="row" style="background: url('<?php echo CFS()->get('fondo_banner_equipo'); ?>') no-repeat;background-size:cover;background-position:center center;">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 team-info">
							<div class="container">
								<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1">
									<strong><?php echo CFS()->get('titulo_banner_equipo'); ?></strong>
									<p><?php echo CFS()->get('descripcion'); ?></p>
									<span class="col_prefooter"><?php echo CFS()->get('boton'); ?></span>
								</div>
							</div>
						</div>
					</div>
                    <div class="container organizaciones">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom:30px;">
									<strong><?= CFS()->get('org_title') ?></strong>
								</div>
								<?php
								$organizaciones = CFS()->get('organizaciones');
                        foreach ($organizaciones as $orow) {
?>
									<div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 org"  style="margin-bottom: 50px;">
										<img src="<?php echo $orow['logo_organizacion']; ?>" alt="">
									</div>
								<?php } ?>						
							</div>
						</div>
					</div>
				</div>
			</section>
<?php get_footer(); ?>
