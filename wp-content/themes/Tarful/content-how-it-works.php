<?php
/**
 * The template used for displaying how it works
 *
*/
?>

<?php  { ?>
	<section>
		<div id="how_it_works_div" style="display:none" class="carousel" data-ride="carousel">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 text-center">
					<h2 class="h2-como-funciona">¿Cómo Funciona?</h2>
					<span id="close_how_works" class="close-how-it-works glyphicon glyphicon-remove" style="color:black;"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-4">
				    <div class="thumbnail thumbnail-como-funciona text-center">
				      <img class="img-responsive" src="<?php bloginfo('url'); ?>/wp-content/themes/Tarful/images/como-funciona-01.png" alt="como funcion 1?>">
				      <div class="caption">
                        <p style='font-family:"open_sansregular"'><strong>Escoge tu próxima experiencia</strong> </p>
                        <p style='font-family:"open_sansregular"'>Descubre las mejores actividades y destinos, todo en un solo lugar.</p>
				      </div>
				    </div>
				</div>
				<div class="col-xs-12 col-sm-4">
				    <div class="thumbnail thumbnail-como-funciona text-center">
				      <img class="img-responsive" src="<?php bloginfo('url'); ?>/wp-content/themes/Tarful/images/como-funciona-02.png" alt="como funcion 2?>">
				      <div class="caption">
                        <p style='font-family:"open_sansregular"'><strong>Reserva, ¡Es fácil y seguro! </strong></p>
                        <p style='font-family:"open_sansregular"'>Nos encargaremos de darte la atención que mereces.</p>
				      </div>
				    </div>
				</div>
				<div class="col-xs-12 col-sm-4">
				    <div class="thumbnail thumbnail-como-funciona text-center">
				      <img class="img-responsive" src="<?php bloginfo('url'); ?>/wp-content/themes/Tarful/images/como-funciona-03.png" alt="como funcion 3?>">
				      <div class="caption">
                        <p style='font-family:"open_sansregular"'><strong>¡Inspírate y viaja!</strong></p>
                        <p style='font-family:"open_sansregular"'>Nuestra misión es animarte a redescubrir Venezuela.</p>
				      </div>
				    </div>
				</div>
			</div>
		</div>
	</section>

<div style="clear: both;"></div>
<?php } ?>


