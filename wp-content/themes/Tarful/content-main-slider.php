<?php
/**
 * The template used for displaying the main slider in the homepage.
 *
*/

global $cfs;
$j=0; $i=0;	 
?>
<section>
	<div id="carousel-example-generic" class="carousel slide main-slider" data-ride="carousel">
	<?php $args = array('post_type' => 'product', 'meta_key' => '_featured', 'meta_value' => 'yes', 'posts_per_page' => -1 );  
		$featured_query = new WP_Query( $args );  
		$number = $featured_query->found_posts;
		
		$args = array( 'meta_key' => '_featured', 'meta_value' => 'yes', 'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => -1 );
		$featured_blog_query = new WP_Query( $args );

		if ($featured_query->have_posts()) :   ?>
		
		<ol class="carousel-indicators">
		<?php while ($j < $number) { 
			if ($j==0){
				echo '<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>';
			} else {
				echo '<li data-target="#carousel-example-generic" data-slide-to="'.$j.'"></li>';
			} 
			$j++; 
		}
	    while ( $featured_blog_query->have_posts() ) : $featured_blog_query->the_post(); 
			if (CFS()->get('imagen_slider')) {
				if ($j==0){
					echo '<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>';
				} else {
					echo '<li data-target="#carousel-example-generic" data-slide-to="'.$j.'"></li>';
				}
				$j++;
			}
		endwhile;
		?>
		</ol>
	
	<div class="carousel-inner" role="listbox">
	<?php
		while ($featured_query->have_posts()) : 
			$featured_query->the_post();  
			$product = get_product( $featured_query->post->ID );
			$product_cats = wp_get_post_terms( get_the_ID(), 'product_cat');
			$single_cat = array_shift( $product_cats );
			$product_place = wp_get_post_terms( get_the_ID(), 'localidad');
			$single_place = array_shift( $product_place);
			

			if (CFS()->get('slider_img') != '') { ?> 
			<div class="item
				<?php if ($i==0) {
					echo 'active';
				} ?> ">
				 <img src="<?php echo CFS()->get('slider_img') ?>" alt="<?php the_title(); ?>">
				
				<div class="carousel-caption"> 
					<!-- <div class="category"><a href="<?php /*echo*/ get_site_url();?>/experiencia/<?php /*echo $single_cat->slug*/; ?>"><?php  /*echo $single_cat->name*/  ?></a></div> --> <!-- Eliminated as requested since category is no longer needed at the banner -->
					<div class="location"><a href="<?php echo get_site_url();?>/localidad/<?php echo $single_place->slug; ?>"><?php if (!empty($single_place)) { echo $single_place->name;}  ?></a></div>
					<a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1><span> - <?php echo CFS()->get('lenght');  ?></span></a>
					<div class="avalaible"><a href="<?php the_permalink(); ?>"><?php _e('Ver Más','tarful'); ?></a></div>
				</div>
			</div>

			<?php $i=1; } ?>

	    <?php endwhile; ?>		
		<?php 
	    while ( $featured_blog_query->have_posts() ) : $featured_blog_query->the_post();

			$thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'large', true);
			$category = get_the_category();
			if (CFS()->get('imagen_slider')) { ?> 

			<div class="item
				<?php if ($i==0) {
					echo 'active';
				} ?> ">
				<img src="<?php echo CFS()->get('imagen_slider'); ?>" alt="<?php the_title(); ?>">
				<div class="carousel-caption">
					<div class="category"><a href="<?php echo get_site_url();?>/blog/category/<?php echo $category[0]->slug; ?>"><?php echo $category[0]->slug;  ?></a></div>
					<div class="link_blog_slider"><a href="<?php echo get_site_url();?>/blog"><?php echo "Blog"; ?></a></div>
					<a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>
					<div class="avalaible"><a href="<?php the_permalink(); ?>"><?php _e('Ver Más','tarful'); ?></a></div>
				</div>
			</div>

			<?php $i=1; }?>
	<?php endwhile; ?> 
	</div> 

	<?php  endif;  
	wp_reset_query();
	wp_reset_postdata();
	?>
	</div>
</section>



