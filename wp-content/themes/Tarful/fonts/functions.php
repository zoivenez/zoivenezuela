<?php //Tarful 


add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
	wp_enqueue_style( 'genericons-style', get_template_directory_uri().'/genericons/genericons.css' );
	wp_dequeue_style('twentyfifteen-style');
}

add_action( 'after_setup_theme', 'wpt_setup' );
if ( ! function_exists( 'wpt_setup' ) ):
	function wpt_setup() {  
		register_nav_menu( 'primary', __( 'Primary navigation', 'wptuts' ) );
} endif;

 require_once('wp_bootstrap_navwalker.php');

// Register custom menu nav locations
function tarful_register_menus() {
  register_nav_menus(
	array(
		'credits-1' => __( 'Footer menu' )
	)
  );
}
add_action( 'init', 'tarful_register_menus' );



/**
 * Set a custom add to cart URL to redirect to
 * @return string
 */
/*


/* remove parent sidebars */
function remove_sidebars() {
  unregister_sidebar( 'sidebar' ); // primary on left
}
add_action( 'widgets_init', 'remove_sidebars', 11 );

// Add currency / symbol
add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {
	 $currencies['VEF'] = __( 'Bolivar Fuerte - Venezuela ( Bs)', 'woocommerce' );
	 return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
	 switch( $currency ) {
		  case 'VEF': $currency_symbol = '<span class="money-currency-symbol"> Bs. </span>'; break;
	 }
	 return $currency_symbol;
}

add_action( 'init', 'create_place_tax' );

function create_place_tax() {
  register_taxonomy(
	'localidad',
	'product',
	array(
	  'label' => __( 'Destinos' ),
	  'rewrite' => array( 'slug' => 'destino' ),
	  'hierarchical' => true,
	)
  );
}

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'tarful_custom_override_checkout_fields' );

function tarful_custom_override_checkout_fields( $fields ) {

	global $wpdb;

	header ('Content-type: text/html; charset=UTF-8');

	// Delete the deafult fields to create new ones for BILLING
	unset($fields['billing']['billing_first_name']);
	unset($fields['billing']['billing_last_name']);
	unset($fields['billing']['billing_cirif']);
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_address_1']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['billing']['billing_city']);
	unset($fields['billing']['billing_postcode']);
	unset($fields['billing']['billing_country']);
	unset($fields['billing']['billing_state']);
	unset($fields['billing']['billing_email']);
	unset($fields['billing']['billing_phone']);
  
	// Create new fields
	$fields['billing']['billing_first_name'] = array(
	'label'	  => __('Nombre', 'woocommerce'),
	'placeholder'	=> _x('Nombre', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6'),
	'clear'	  => false
	);

	$fields['billing']['billing_last_name'] = array(
	'label'	  => __('Apellido', 'woocommerce'),
	'placeholder'	=> _x('Apellido', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6'),
	'clear'	  => true
	);

	$fields['billing']['billing_cirif'] = array(
	'label'	  => __('Cédula', 'woocommerce'),
	'placeholder'	=> _x('Cédula o RIF', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6'),
	'clear'	  => false
	);

	$fields['billing']['billing_email'] = array(
	'label'	  => __('Email', 'woocommerce'),
	'placeholder'	=> _x('Email', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => true
	);

	$fields['billing']['billing_phone']= array(
	'label'	  => __('Teléfono', 'woocommerce'),
	'placeholder'	=> _x('Teléfono móvil', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => false
	);

	$fields['billing']['billing_phone_o']= array(
	'label'	  => __('Teléfono 2', 'woocommerce'),
	'placeholder'	=> _x('Téléfono fijo (Opcional)', 'placeholder', 'woocommerce'),
	'required'  => false,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => true
	);

	$fields['billing']['billing_address_1']= array(
	'label'	  => __('Dirección 1', 'woocommerce'),
	'placeholder'	=> _x('Dirección 1', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => false
	);

	$fields['billing']['billing_address_2']= array(
	'label'	  => __('Dirección', 'woocommerce'),
	'placeholder'	=> _x('Dirección 2 (opcional)', 'placeholder', 'woocommerce'),
	'required'  => false,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => true
	);

	$fields['billing']['billing_city'] = array(
	'label'	  => __('Ciudad', 'woocommerce'),
	'placeholder'	=> _x('Ciudad', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => false
	);

	$fields['billing']['billing_state'] = array(
	'label'	  => __('Estado', 'woocommerce'),
	'placeholder'	=> _x('Estado', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => true
	);

	$fields['billing']['billing_country'] = array(
	'label'	  => __('País', 'woocommerce'),
	'placeholder'	=> _x('VE', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => false
	);

	$fields['billing']['billing_postcode'] = array(
	'label'	  => __('Código postal', 'woocommerce'),
	'placeholder'	=> _x('código postal', 'placeholder', 'woocommerce'),
	'required'  => true,
	'class'	  => array('col-lg-6 col-md-6 '),
	'clear'	  => true
	);

	$fields['billing']['billing_edad'] = array(
	'type' => 'text',
	'label'	  => __('Edad', 'woocommerce'),
	'class'	  => array('col-lg-6 col-md-6 '),
	'placeholder'	=> _x('Edad', 'placeholder', 'woocommerce'),
	'required'  => true,
	);

	return $fields;
}

// Display custom checkout fields on the order edit page
add_action( 'woocommerce_admin_order_data_after_billing_address', 'tarful_custom_checkout_field_display_admin_order_meta', 10, 1 );

function tarful_custom_checkout_field_display_admin_order_meta($order){
	echo '<p><strong>'.__('Cédula o RIF').':</strong><br />' . get_post_meta( $order->id, '_billing_cirif', true ) . '</p>';
}


function tarful_box_guru(){ ?> 
<script>
	$(document).ready(function(){

		$('.product').each(function(){

			var img_objetc = $(this).find("img");

			var price_object = $(this).find(".avalaible");

			var guru_object = $(this).find(".box_guru_grid");

			if ( (price_object.outerWidth() + guru_object.outerWidth()) > img_objetc.outerWidth() ) {

				if((img_objetc.outerWidth() - price_object.outerWidth()) < 40 ){

					guru_object.addClass("separate_box");
					$(this).find(".guru_info h5").addClass("separate_name");

				}else {
					$(this).find(".guru_info h5").addClass("separate_name");
				}
			};
		});
	});
</script> 
<?php  }


// Shows the group of users' avatars
function tarful_users_count() {

	global $wpdb;
	$user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->users" );

	echo $user_count;

}

// Shows the group of users' (team)
function tarful_team_users_count() {

	global $wpdb;
	$team_user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->usermeta WHERE meta_key='wp_capabilities' AND meta_value LIKE '%administrator%'" );

	echo $team_user_count;

}


// Shows the group of users' (gurus)
function tarful_gurus_users_count() {

	global $wpdb;
	$gurus_user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->usermeta WHERE meta_key='wp_capabilities' AND meta_value LIKE '%gurú%'" );

	echo $gurus_user_count;

}
						
// Count total registered users and show info if they like product
function tarful_networks_avatars($quantity, $post_id) { 
	
	global $wpdb;

	if ($post_id != 0) {
		
		$users_liked = $wpdb->get_results("SELECT * FROM like_post WHERE id_post = '$post_id' LIMIT $quantity");
		$array_users_like = array();
		
		foreach ($users_liked as $user_info) {
			$temp_user_id = $user_info->id_user_registered;
			$temp_user_like = $user_info->like;
			
			if ($temp_user_like == '1') {
				$current_user = get_userdata( $temp_user_id );
				?>
				<a href="#" class="hint  hint-top" data-hint="<?php echo $current_user->first_name . ' ' . $current_user->last_name; ?>"><?php echo get_avatar( $temp_user_id, 40 ); ?></a>
			<?php
			}		 
		}
	
	} else {
		$userids = $wpdb->get_results("SELECT ID FROM $wpdb->users ORDER BY RAND() LIMIT $quantity");
		foreach ($userids as $user) {
			if ($user->ID != '') { 
				$current_user = get_userdata( $user->ID );			
				?>
				<a href="#" class="hint  hint-top" data-hint="<?php echo $current_user->first_name . ' ' . $current_user->last_name; ?>"><?php echo get_avatar( $user->ID, 40 ); ?></a>
				<?php
			}
		}

	}
	
}

function tarful_networks_avatars_all($post_id) { 
	
	global $wpdb;

	if ($post_id != 0) {
		
		$users_liked = $wpdb->get_results("SELECT * FROM like_post WHERE id_post = '$post_id'");
		$array_users_like = array();
		
		foreach ($users_liked as $user_info) {
			$temp_user_id = $user_info->id_user_registered;
			$temp_user_like = $user_info->like;
			
			if ($temp_user_like == '1') {
				$current_user = get_userdata( $temp_user_id );
				?>
				<div class="row">
					<a href="#" class="hint  hint-top" data-hint="<?php echo $current_user->first_name . ' ' . $current_user->last_name; ?>"><?php echo get_avatar( $temp_user_id, 40 ); ?></a>
					<span class=""><?php echo $current_user->first_name . ' ' . $current_user->last_name; ?></span>
				</div>
			<?php
			}		 
		}
	
	} else {
		$userids = $wpdb->get_results("SELECT ID FROM $wpdb->users ORDER BY RAND()");
		foreach ($userids as $user) {
			if ($user->ID != '') { 
				$current_user = get_userdata( $user->ID );			
				?>
				<div class="row">
					<a href="#" class="hint  hint-top" data-hint="<?php echo $current_user->first_name . ' ' . $current_user->last_name; ?>"><?php echo get_avatar( $user->ID, 40 ); ?></a>
					<span class=""><?php echo $current_user->first_name . ' ' . $current_user->last_name; ?></span>
				</div>
				<?php
			}
		}

	}
	
}


function woo_related_products_limit() {
  global $product;
  
  $args['posts_per_page'] = 6;
  return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {

  $args['posts_per_page'] = 4; // 4 related products
  $args['columns'] = 4; // arranged in 2 columns
  return $args;
}

function menu_blog(){
	global $post;
	$category = get_the_category();
	$cm = get_category_meta(false, get_term_by('slug', $category[0]->slug, 'category'));
	?>
	<div class="col-md-12 blog_header" style="border-bottom: 4px solid <?php echo $cm['cat_color']; ?>;">
		<div class="ajustador">
			<a href="<?php bloginfo('url')?>" class="logo_blog">
				<img src="<?php bloginfo('stylesheet_directory');?>/images/logo_zoi.png" alt="Zoi Venezuela" style="width: 49px;">
			</a>
			<div class="blog_menu">
				<div href="#" class="blog_coments_continer item_blog_menu item_blog_menu_principal">
					<p class="item_blog_menu"><?php echo get_comments_number(); ?></p>
					<img class="item_blog_menu" src="<?php echo Get_stylesheet_directory_uri();?>/images/icon_comentarios.png">
				</div>
				<?php
				tarful_facebook_share(get_the_ID() , apply_filters( 'woocommerce_short_description', $post->post_content ), array("class"=>"item_blog_menu item_blog_menu_principal"));
				tarful_twitter_share(array("class"=>"item_blog_menu item_blog_menu_principal"));
				?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$('body').on({
	    'touchmove': function(e) { 
			if ($(this).scrollTop() > 1) {
				$('#navbar, .menu-mobile').fadeOut();
			} else {
				$('#navbar, .menu-mobile').fadeIn();
			}
	    }
	});
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1) {
			$('#navbar, .menu-mobile').fadeOut();
		} else {
			$('#navbar, .menu-mobile').fadeIn();
		}
	});
	</script>
<?php }

// Custom Header for the project
function tarful_custom_header() {
global $wpdb, $woocommerce;
?>
	<div id="navbar" class="top-navbar menu-desktop">
		<nav>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-header-box">
				<ul style="width:100%; max-width:1440px; margin: 0 auto;">
					<li class="navbar-main-logo">
						<a class="navbar-brand" href="<?php bloginfo('url')?>">
						<img src="<?php bloginfo('stylesheet_directory');?>/images/logo_zoi.png" alt="Zoi Venezuela" width="68" height="44">
					  </a>
					</li>
					<li class="main-menu-li"><a href="<?php echo get_bloginfo('url') . '/experiencias/'; ?>">Experiencias</a>
						<ul class="box-main-menu">
							<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 box-main-menu-container">
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 box_menu_feat_cat">
									<?php
									$args = array(
									'order' => 'ASC',
									'hide_empty' => 0
									);
									$categories = get_terms('product_cat', $args);		
									foreach($categories as $category) { 	
										$cat_id = $category->term_id;
										$cat_name = $category->name;
										$cat_slug = $category->slug;
										$cat_link = get_bloginfo('url') . '/experiencia/';
										$cat_link_specific = get_term_link($category);
			
										$temp_cat_featured = array();														
										$temp_cat_featured = get_category_meta(false, get_term_by('slug', $cat_slug, 'product_cat')); 
										if (!empty($temp_cat_featured) && $temp_cat_featured['featured'] != "") {
											$thumbnail_id = get_woocommerce_term_meta( $cat_id, 'thumbnail_id', true );
											$image = wp_get_attachment_url( $thumbnail_id );
											echo '<a href="'.$cat_link_specific.'"><img src="'.$image.'" alt="" /></a>';
										}
									}	
									?>
								</div>
								
								<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 box-right-main-menu">
									<div class="cat_list_title" ><h4>Categorías <a href="<?php echo get_permalink(158); ?>">ver todas</a></h4> </div>		
									<?php
									$taxonomy = 'product_cat';
									$args = array(
									'order' => 'ASC',
									'hide_empty' => 1
									);
									$categories = get_terms('product_cat', $args);
									foreach($categories as $category) { 	
										$cat_id = $category->term_id;
										$cat_name = $category->name;
										$cat_slug = $category->slug;

										$term = get_term_by( 'slug', $cat_slug, $taxonomy );
										$st = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts p
										INNER JOIN $wpdb->term_relationships tr
										ON (p.ID = tr.object_id)
										INNER JOIN $wpdb->term_taxonomy tt
										ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
										WHERE
										p.post_status = 'publish'
										AND tt.taxonomy = %s
										AND tt.term_id = %d;",
										$taxonomy,
										$term->term_id);
										$cat_count_post_published = $wpdb->get_var($st);	

										$cat_link = get_bloginfo('url') . '/experiencia/';
										$cat_link_specific = get_term_link($category);
										?>
										<?php if ($cat_count_post_published > 0) { ?>
										<li class="menu-item-<?php echo $cat_id; ?>">
											<a href="<?php echo $cat_link_specific; ?>">
												<?php echo $cat_name; ?>
											</a>
										</li>
										<?php } ?>
									<?php
									} ?>
								
								</div>					
							</div>	
						</ul>
					</li>

					<li class="main-menu-li"><a href="<?php echo get_permalink(24); ?>">Destinos</a>
						<ul class="box-main-menu">
							<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 box-main-menu-container">
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 box_menu_feat_cat">

									<?php 
									$args = array(
									'taxonomy' => 'localidad', 
									'hide_empty' => 1
									);
									$categories_p = get_categories($args);
									$cat_link = get_bloginfo('url') . '/destinos/';
									
									 foreach ($categories_p as $category) {
									 
										$temp_p = get_category_meta(false, get_term_by('id',  $category->term_id , 'localidad'));
										
										if ((!empty($temp_p)) && ($temp_p['featured'])) {
											$temp_image = $temp_p['image'];
											$temp_link = get_term_link($category);
											break;
										}
									 }
									 ?>
									 <a href="<?php echo $temp_link; ?>">
										<img src="<?php echo $temp_image; ?>" alt="">
									</a>

								</div>
								
								<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 box-right-main-menu">
									<div class="cat_list_title" ><h4>Destinos <a href="<?php echo get_permalink(24); ?>">ver todos</a></h4> </div>	
									
									<?php
									$taxonomy = 'localidad';	
									$args = array(
									'order' => 'ASC',
									'hide_empty' => 0,
									);
									$categories = get_terms('localidad', $args);
									
									foreach($categories as $category) { 	
										$cat_id = $category->term_id;
										$cat_name = $category->name;
										$cat_slug = $category->slug;

										$term = get_term_by( 'slug', $cat_slug, $taxonomy );
										$st = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts p
										INNER JOIN $wpdb->term_relationships tr
										ON (p.ID = tr.object_id)
										INNER JOIN $wpdb->term_taxonomy tt
										ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
										WHERE
										p.post_status = 'publish'
										AND tt.taxonomy = %s
										AND tt.term_id = %d;",
										$taxonomy,
										$term->term_id);
										$cat_count_post_published = $wpdb->get_var($st);	
										
										$cat_link = get_bloginfo('url') . '/destinos/';
										$cat_link_specific = get_term_link($category);
										?>
										
										<?php if ($cat_count_post_published > 0) { ?>
											<li class="menu-item-<?php echo $cat_id; ?>">
												<a href="<?php echo $cat_link_specific; ?>">
													<?php echo $cat_name; ?>
												</a>
											</li>
										<?php } ?>
									<?php
									} ?>
								
								</div>					
							</div>	
						</ul>
					</li>

					<li class="main-menu-li"><a href="<?php echo get_permalink(22); ?>">Contacto</a>
					
					<li class="main-menu-li header-cart-menu">
						<?php if (is_user_logged_in()) { 
							$current_user = wp_get_current_user();
							?>
							<div class="login-menu myaccount-dropdown">

								<a href="<?php echo get_permalink(7); ?>">
									<?php echo get_avatar( $current_user->user_email, 32 ); ?>
								</a>
								<ul class="myaccount-dropdown-content">
									<li>
										<a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ) ?>#userinfo">
										Datos de usuario
										</a>
									</li>					
									<li>
										<a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ) ?>#personalinfo">
										Datos personales
										</a>
									</li>					
									<li>
										<a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ) ?>#purchases">
										Histórico de compras
										</a>
									</li>					
									<li>
										<a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ) ?>#likeexperiences">
										Mis experiencias favoritas
										</a>
									</li>					
									<li>
										<a href="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ) ?>">
										Cerrar Sesión
										</a>
									</li>					
								</ul>
							
							<?php }else{ ?>
							
							<div id="overlay" class="overlay" style="display: none;">
								<div class="cerrarOverlay"></div>
								<div id="tab-buttons-container" class='tab-container'>

									<ul class='etabs' style="display: none;">
										<li class='tab'><a href="#social">Social</a></li>
										<li class='tab'><a href="#email">Email</a></li>
									</ul>
									
									<div class="panel-container">
										
										<div class="box-header-popup-login">
																						
											<h4>Inicia Sesión y Vive la Experiencia</h4>
											
											<div id="social" class="tab-buttons-panel">
												<?php do_action( 'wordpress_social_login' ); // only shows when user is not logged-in ?> 
												<div style="clear: both;"></div>
												<center>
													<span>o también</span>
												</center>
												<div style="clear: both;"></div>
								
												<div class="wp-social-login-provider-list">
													<div class='login-email-tab'><a href='#' class='next-tab login-footer-btn' rel='1'>Inicia sesión con tu email</a></div>	
												</div>
												
												<div style="clear: both;"></div>
								
												<?php if ( is_user_logged_in() ) { ?>
													<center>
														<a href="<?php echo wp_logout_url(); ?>">Cerrar Sesión</a>	
													</center>
												<?php } ?> 
												
											</div>
											
											<div id="email" class="tab-buttons-panel">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-container">
													<?php wp_login_form(); ?>							
													<div style="clear:both;"></div>			
												</div>
												
												<?php if ( is_user_logged_in() ) { ?>
													<center>
														<a href="<?php echo wp_logout_url(); ?>">Cerrar Sesión</a>	
													</center>
												<?php } ?> 
												
												<div style="clear:both;"></div>				
												<center>
													<div class='login-footer'><a href='#' class='next-tab login-footer-btn' rel='2'>También puedes utilizar tus redes sociales</a></div>		
												</center>
											</div>
							
										</div>
										
										<div class="box-main-popup-login"><a href="<?php echo get_permalink(7); ?>">¿Aún no tienes cuenta?  <u>Crea una ahora</u></a></div>
							
									</div>
							
													
								</div>
								<script type="text/javascript">
								$('#tab-buttons-container').easytabs();
								
								var $tabContainer = $('#tab-buttons-container'),
									$tabs = $tabContainer.data('easytabs').tabs,
									$tabPanels = $(".tab-buttons-panel")
									totalSize = $tabPanels.length;
								
								$tabPanels.each(function(i){
								});
								
								$('.next-tab, .prev-tab').click(function() {
								  var i = parseInt($(this).attr('rel'));
								  var tabSelector = $tabs.children('a:eq(' + i + ')').attr('href');
								  $tabContainer.easytabs('select', tabSelector);
								  return false;
								});
								</script>
									
							</div>
							<div class="login-menu">
								<a id="loginLink">							
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img_user.png" width="32" height="32" alt="car-shop" />
								</a>
							<?php } ?>
						</div>
					</li>

					<li class="main-menu-li header-cart-menu mini-cart-dropdown">
						<div class="container-mini-cart">
							<div class="mini-cart">
								<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="Ver Carrito de Compras">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img_cart.png" width="32" height="32" alt="car-shop" />
									<span class="cart-contents"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
								</a>
								<ul class="mini-cart-content">
									
									<?php do_action( 'woocommerce_before_cart_contents' ); ?>

									<?php if ( sizeof( WC()->cart->get_cart() ) > 0 ) : ?>

										<?php
										foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
											$_product	  = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
											$product_id	= apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
								
											if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
												?>
								
												<li>
													<a href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>">
														<div class="product-thumbnail">
															<?php echo apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key ); ?>
														</div>
									
														<div class="product-name">
															<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ); ?>
															<?php echo WC()->cart->get_item_data( $cart_item ); ?>								
														</div>
														<div class="product-price">															
															<span class="product-quantity"><?php echo $cart_item['quantity']; ?></span>
															<span class="x-sep">x</span>
														<?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?>
														</div>
													</a>
												</li>					
								
												<?php
											}
										}
								
										do_action( 'woocommerce_cart_contents' );
										?>
								
										<?php do_action( 'woocommerce_after_cart_contents' ); ?>
									
										<li class="mini-cart-footer">
											<div class="footer_content">
												<a href="<?php echo WC()->cart->get_checkout_url(); ?>" class="button checkout wc-forward">Realizar Compra</a>
												<div class="mini-cart-sub-total"><?php _e( 'Subtotal', 'woocommerce' ); ?>: <?php echo WC()->cart->get_cart_subtotal(); ?></div>
												<div style="clear: both"></div>
											</div>
									
										</li>
								
									<?php else : ?>

										<li class="empty"><center><?php _e( 'No products in the cart.', 'woocommerce' ); ?></center></li>

									<?php endif; ?>
								</ul>
							</div>	
						<div>
					</li>

					<li class="main-menu-li header-search-menu"><a href=""><img src="<?php bloginfo('stylesheet_directory'); ?>/images/img_search.png" width="32" height="32" alt="car-shop" /></a>
						<ul class="box-main-menu box-main-search">
							<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 box-main-menu-container">


								<form role="search" method="get" id="searchform" action="<?php bloginfo('url'); ?>">
									<div class="col-xs-12 col-sm-12 col-md-8 col-lg-7" style="margin: 0 auto; float: none;">
										<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
										<label class="screen-reader-text" for="s">Buscar Experiencias</label>
										<input type="text" value="" placeholder="Buscar Experiencias" placeholder="Buscar Experiencias" name="s" id="s" />
										<input type="hidden" name="post" value="<?php echo get_search_query() ?>" />
										</div>
										<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
										<input type="submit" id="searchsubmit" value="Buscar" />
										
										</div>
									</div>
								</form>
							</div>	
						</ul>
					</li>
				</ul>
			</div>		
		</nav>
		
	</div><!-- #navbar -->

	<script>
		$(document).ready(function() {
			enderizeTarfulCenterCall(".main-menu-li.header-cart-menu #tab-buttons-container",true,true);
			$("#loginLink").click(function( event ){
				event.preventDefault();
				$(".overlay").fadeIn("fast");
			});
			$(".cerrarOverlay").click(function( event ){
				event.preventDefault();
				$(".overlay").fadeOut("fast");
			});
			$(document).keyup(function(e) {
				if(e.keyCode == 27 && $(".overlay").css("display") != "none" ) { 
					event.preventDefault();
					$(".overlay").fadeOut("fast");
				}
			});
			$(this).on("click",".main-menu-li.header-search-menu a",function(e){
				e.preventDefault();
				var ulSearch = $(this).parent(".main-menu-li.header-search-menu");
				if(ulSearch.find("ul").css("display") == "none"){
					ulSearch.find("ul").css("display","block");
					$("#searchform input:text").focus();
				}else{
					ulSearch.find("ul").css("display","none");
				}
			});
		});
	</script>

<?php	
}

function tarful_mobile_menu(){
	global $wpdb, $woocommerce; ?>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="navbar-collapse espaciable" id="bs-example-navbar-collapse-1">
		
		  <ul class="nav navbar-nav">
			<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('experiencias'))); ?>">Experiencias</a></li>
			<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('destinos'))); ?>">Destinos</a></li>
			<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('mi cuenta'))); ?>">Mi cuenta</a></li>
			<!--<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('nosotros'))); ?>">Nosotros</a></li>-->
			<!--<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('blog'))); ?>">Blog</a></li>-->
			<li><a href="<?php echo get_permalink(22); ?>">Contacto</a></li>
		  </ul>

		  <form class="navbar-form" role="search" action="<?php bloginfo('url'); ?>">
			<input type="text" class="form-control" name="s" placeholder="Buscar experiencias">
			<input type="hidden" name="post" value="<?php echo get_search_query() ?>" />
		  </form>  

		</div><!-- /.navbar-collapse-->
		<nav class="navbar-default menu-mobile navbar-fixed-top">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" id="boton_menu_mobile">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="contenedor-caption">
					<a class="navbar-brand logo-mobile" href="<?php bloginfo('url')?>">
						<img src="<?php bloginfo('stylesheet_directory');?>/images/logo_zoi.png" alt="Zoi Venezuela" width="50">
					</a>
				</div>
				<div class="cart mini-cart">
					<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="Ver Carrito de Compras">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img_cart.png" width="32" height="32" alt="car-shop" />
						<span class="cart-contents"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
					</a>
				</div>
			</div>
		  </div><!-- /.container-fluid -->
		</nav>	
		<script type="text/javascript">
		$(document).ready(function(){
			$(this).on("click","#boton_menu_mobile",function(){

				$(".espaciable").toggleClass("menu_espacio_menu_responsive");
			});
		});
		</script>
		
<?php }
tarful_send_mail();

function tarful_send_mail(){
	error_reporting(E_ALL ^ E_NOTICE);
	//If the form is submitted
	if(isset($_POST['form-experience'])) {


		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		// Get information related to visitor, location, state, city, country.
		$location_data = array();
		$location_data = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		
		$continent_code = $location_data["geoplugin_continentCode"];
		$country_code = $location_data["geoplugin_countryCode"];
		$latitude = $location_data["geoplugin_latitude"];
		$longitude = $location_data["geoplugin_longitude"];
		$city = $location_data["geoplugin_city"];
		$state = $location_data["geoplugin_regionName"];
		$country = $location_data["geoplugin_countryName"];


		if(trim($_POST['category']) != '') {
			$category = trim($_POST['category']);
		}
		if(trim($_POST['location']) != '') {
			$location = trim($_POST['location']);
		}
		if(trim($_POST['date']) != '') {
			$date = trim($_POST['date']);
		}
		if(trim($_POST['budget']) != '') {
			$budget = trim($_POST['budget']);
		}
		if(trim($_POST['email']) != '') {
			$email = trim($_POST['email']);
		}
		if(trim($_POST['description']) != '') {
			$description = trim($_POST['description']);
		}

			$emailTo = get_option( 'admin_email' );
			$subject = '[ZOI Venezuela] Solicitud de nueva experiencia';
			$message = '<html><body><br />';
			$message .= '<table border="0" cellpadding="10" width="600px">';
			$message .= '<tr style="'.$tr_style.'"><td>Has recibido un mensaje desde el formulario de contacto</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Ubicación: '.$location.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Email: '.$email.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>categoria: '.$category.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Fecha: '.$date.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Presupuesto: '.$budget.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Descripción: '.$description.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>-------</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Dirección IP: '.$ip.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Ciudad: '.$city.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Estado: '.$state.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>País: '.$country.'</td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
			$message .= '<tr style="'.$tr_style.'"><td>Enviado desde el Formulario de crear nueva experiencia en'.get_bloginfo('url').'</td></tr>';
			$message .= '</table>';
			$message .= '</body></html>';

			$headers = 'From: ZOI VENEZUELA' .' <'.$email.'>' . "\r\n" . 'Reply-To: ' . $email;
			
			add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
			wp_mail( $emailTo, $subject, $message, $headers);
			tarful_thank_you_mail($email);

	}
}


function tarful_thank_you_mail($email){

		// Email to user 
		$subject_user = '[ZOI Venezuela] ¡Gracias por contactarnos!';	 
		$message_user = '<html><body><br />';
		$message_user .= '<table border="0" cellpadding="10" width="600px">';
		$message_user .= '<tr style="'.$tr_style.'"><td>Hola '.$user_fullname.', Gracias por Contactarnos</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>¡Hemos recibido tu email, vamos a revisarlo y te contactaremos próximamente!</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>Equipo ZOI Venezuela</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td><a href="'.get_bloginfo('url').'">zoivenezuela.com - Vive la Aventura</a></td></tr>';
		$message_user .= '</table>';
		$message_user .= '</body></html>';

		$from = get_option( 'admin_email' );
		$headers = 'From: ZOI VENEZUELA' .' <'.$from.'>' . "\r\n" . 'Reply-To:  <'.$from.'>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $email, $subject_user, $message_user, $headers);
}

function form_create_experience(){ ?>

				<h4><img src="<?php bloginfo('stylesheet_directory'); ?>/images/title_create_experience.png" alt="crea tu experiencia" width="250" height="80" /></h4>
				<div class="col-xs-11 col-sm-11 col-md-11 col-lg-11 box_form_create_experience">
					<form id="experience-form" action="#" method="post" role="form">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<input class="required" type="text" id="location" name="location" value="" placeholder="Escribe la Ubicación" tabindex="1">
								<input class="required" type="text" id="category" name="category" value="" placeholder="Selecciona la Categoría" tabindex="2">
								<input class="required" type="text" id="date" name="date" value="" placeholder="Fechas - Ej. 29 / 08/ 2015" tabindex="3">
								<input class="required" type="text" id="budget" name="budget" value="" placeholder="Presupuesto por persona" tabindex="4">
								<input class="required" type="email" id="email" name="email" value="" placeholder="Escribe tu email" tabindex="5">
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<textarea class="required" id="description" name="description" tabindex="6" placeholder="Describe la Experiencia"></textarea>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box_send_form">
								<input type="submit" id="" name="" value="enviar" tabindex="7">
								<input type="hidden" name="form-experience" id="form-experience" value="true" />
							</div>
						</div>
					</form>
				</div>
				<script>
					$(document).ready(function() {

						$('form#experience-form').submit(function() {
							var hasError = false;
							$('form#experience-form .required').each(function() {
								if(!$(this).val()) {
									var labelText = $(this).prev('label').text();
									$(this).addClass("form-error");
									hasError = true;
								}else{
									$(this).removeClass('form-error');
								}
							});
							if (hasError) {
								return false;
							}else{
								document.forms["experience-form"].submit();
								return true;
							}
						});

						$("#create_experiencie").click(function( event ){
							event.preventDefault();
							$(".overlay-experience").fadeToggle("fast");
						});


						$(".overlay-experience").on( 'click', function ( e ) {
							if ( $( e.target ).closest( ".overlay-experience #tab-buttons-container" ).length === 0 ) {
								$( ".overlay-experience" ).hide();
							}
						});

						$(document).keyup(function(e) {
							if(e.keyCode == 27 && $(".overlay-experience").css("display") != "none" ) {

								$(".overlay-experience").hide();
							}
						});

					});
				</script>
<?php
}


// Add box and form of "Create Experience" on experiences (products) list 
function tarful_box_create_experience() {?>

	<li class="product type-product status-publish has-post-thumbnail product-type-create-experience">
		<a id="create_experiencie">
			<div class="product product-grid">
				<img class="attachment-small wp-post-image" width="370" height="370" alt="Crea tu experiencia" src="<?php echo CFS()->get('imagen_para_listado', 4); ?>">	 
			</div>
		</a>
	
		<div class="overlay-experience" style="display: none;">
			<div>
				<div id="tab-buttons-container" class='tab-container'>
					<?php form_create_experience(); ?>
				</div>
			</div>
		</div>

	</li>
<?php
}

// Show loop of custom taxonomy (showing badges for each)
function tarful_custom_taxonomy_loop($taxonomy) {
?>

	<div id="main-content" class="site-main">
	
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
	
				<?php get_template_part('content','main-slider'); ?>
	
				<section>
	
					<ul class="products" style="margin: 0px;">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-main-taxonomies">
							<?php
							if ( get_query_var( 'paged' ) )
								$paged = get_query_var('paged');
							else if ( get_query_var( 'page' ) )
								$paged = get_query_var( 'page' );
							else
								$paged = 1;
							
							$per_page	 = 10;
							$count_taxonomies = count( get_terms( $taxonomy ) );
							$offset		= $per_page * ( $paged - 1) ;
							
							$args = array(
								'order'		  => 'ASC',
								'orderby'		=> 'menu_order',
								'offset'		 => $offset,
								'number'		 => $per_page,
							);
							
							$taxonomies = get_terms( $taxonomy, $args );

							switch ( $taxonomy ) {

								case 'product_cat' :
	
									foreach ($taxonomies as $category) {
										$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
										$temp_image = wp_get_attachment_url( $thumbnail_id );
										$temp_link = get_term_link($category);
										if($temp_image){
										?>
										<li class="product">
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 product-grid">
												<a class="loop_taxonomy" href="<?php echo $temp_link; ?>">
													<img src="<?php echo $temp_image; ?>" alt="">
												</a>
											</div>
										</li>
									<?php } 
										}
		
									break;
			
								case 'localidad' :
	
									foreach($taxonomies as $category) { 
										$temp_p = get_category_meta(false, get_term_by('id',  $category->term_id , $taxonomy));
										$temp_image = $temp_p['image'];
										$temp_link = get_term_link($category);
										?>
										<li class="product">
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 product-grid">
												<a class="loop_taxonomy" href="<?php echo $temp_link; ?>">
													<img src="<?php echo $temp_image; ?>" alt="">
												</a>
											</div>
										</li>
									<?php } 
									
									break;
							} ?>			
														
						</div>					
					</ul>		
					
					<div class="woocommerce-pagination">
						<?php
						echo '<div style="clear:both;"></div>';	
						echo paginate_links( array(
							'format'  => '?paged=%#%',
							'current' => $paged,
							'total'	=> ceil( $count_taxonomies / $per_page )
						) );
						?>
					</div>
				
				</section>
			</div><!-- #content -->
		</div><!-- #primary -->
	</div><!-- #main-content -->
	

<?php	
}


// Redirect Cart if empty goes to shop. If has products goes to checkout
add_action("template_redirect", 'tarful_redirect_cart');
function tarful_redirect_cart(){
	global $woocommerce;
	if( is_cart() && sizeof($woocommerce->cart->cart_contents) == 0){
		wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
	}

	if( is_cart() && sizeof($woocommerce->cart->cart_contents) > 0){
		wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'checkout' ) ) );
	}

}

// Hide panels for users in WP admin panel (on Product detail)
add_action( 'admin_menu', 'tarful_hide_tags_panel');
function tarful_hide_tags_panel(){
	if ( !is_admin() ) { // only remove for for user not-admins
		remove_meta_box( 'tagsdiv-product_tag','product','normal' ); // Product Tags panel
	}
}

//Hide Prodcut Sub Menus  left menu for users in WP admin panel 
add_action( 'admin_menu', 'tarful_hide_tags_menu', 999 );
function tarful_hide_tags_menu() { 
	if ( !is_admin() ) { // only remove for for user not-admins
		remove_submenu_page( 'edit.php?post_type=product', 'woocommerce_attributes');
		remove_submenu_page( 'edit.php?post_type=product', 'edit-tags.php?taxonomy=product_shipping_class&post_type=product');
		remove_submenu_page( 'edit.php?post_type=product', 'edit.php?post_type=product&page=product_attributes');
		
	 }
}


//Hide Prodcut Tags left menu for users in WP admin panel 
add_action( 'init', 'remove_taxonomy_menu_pages', 999 );
function remove_taxonomy_menu_pages() {
// 	if ( !is_admin() ) { // only remove for for user not-admins
		register_taxonomy('product_tag', 
			'woocommerce_taxonomy_objects_product_tag', array('show_ui', false)
		);
// 	}
}

/*
Hide Woocommerce left menu for users in WP admin panel. Available submenu slugs are:
wc-addons - the Add-ons submenu
wc-status - the System Status submenu
wc-reports - the Reports submenu
edit.php?post_type=shop_order - the Orders submenu
edit.php?post_type=shop_coupon - the Coupons submenu
*/
function tarful_remove_woocoomerce_items() {
 $remove = array( 'wc-settings', 'wc-status', 'wc-addons', );
  foreach ( $remove as $submenu_slug ) {
// 	if ( !is_admin() ) { // only remove for for user not-admins
		//remove_submenu_page( 'woocommerce', $submenu_slug );
		//remove_submenu_page( 'woocommerce', $submenu_slug );
//	 }
  }
}
add_action( 'admin_menu', 'tarful_remove_woocoomerce_items', 99, 0 );


//Hide Woocommerce option fields on product detail panel {
function tarful_remove_woocommerce_options() {
	wp_enqueue_style( 'style-hide-panel', get_stylesheet_directory_uri() . "/style-hide-panel.css", false, "1.0", "all" );	
}
add_action('admin_enqueue_scripts', 'tarful_remove_woocommerce_options');
add_action( 'admin_init', 'tarful_remove_woocommerce_options' );


// Tarful Footer Admin Panel
function tarful_footer_admin () {
	echo 'Diseñado y Desarrollado por <a href="http://www.tarful.com" target="_blank">Tarful</a> - Expertos en UX, Web y Apps</p>';
}
add_filter('admin_footer_text', 'tarful_footer_admin');

// Limit content lenght and add 3 dot at the end
function tarful_fixed_lenght_notags($content, $lenght) {

	$lenght_fixed = $lenght - 3;					 
	$string = strip_tags($content);
	$string = (strlen($string) > $lenght) ? substr($string,0,$lenght_fixed).'...' : $string;
	
	echo $string;								
}


// Custom Twitter Share Buttom
function tarful_twitter_share($atributos = array()) {
?>
	<script type="text/javascript">
		
	// We bind a new event to our link
	$(document).ready(function(){
		$('a.tweet').click(function(e){

			//We tell our browser not to follow that link
			e.preventDefault();

			//We get the URL of the link
			var loc = $(this).attr('href');

			//We get the title of the link
			var title  = encodeURIComponent($(this).attr('title'));

			//We trigger a new window with the Twitter dialog, in the middle of the page
			window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});
	});
	</script>
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?> #zoivenezuela" class="btn btn_avalaible tw_btn tweet <?php echo $atributos["class"]; ?>" target="_blank">COMPARTIR</a>

<?php	
}

// Custom Facebook Share Buttom
function tarful_facebook_share($post_id, $content, $atributos = array()) {
	
	$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
	$title= urlencode(get_the_title($post_id));
	$url= urlencode(get_permalink($post_id));
	$summary= urlencode($content);
	$image= urlencode($feat_image_url);
	?>
	<a class="btn btn_avalaible fb_btn fb_share <?php echo $atributos["class"]; ?>" onclick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[summary]=<?php echo $summary;?>&amp;p[url]=<?php echo $url; ?>&amp;&p[images][0]=<?php echo $image;?>', 'sharer', 'toolbar=0,status=0,width=620,height=280');" href="javascript: void(0)"> 
	COMPARTIR
	</a>
<?php
}


// Contact Page form
function tarful_box_contact_form() {
?>
	<?php if (isset($_POST['submit'])) { ?>
		<div class="box_success_message">¡ Muchas Gracias por tu mensaje !</div>
	<?php } ?>

	<form id="contact-form" action="<?php echo tarful_send_contact_form(); ?>"  method="post">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<input type="text" id="fullname" name="fullname" value="" placeholder="Nombre" tabindex="1">
				<input type="email" id="email" name="email" value="" placeholder="Email" tabindex="2">
				<input type="text" id="phone" name="phone" value="" placeholder="Teléfono" tabindex="3">
				<input type="text" id="type" name="type" value="" placeholder="Tipo de Pregunta" tabindex="4">
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<textarea id="message" name="message" tabindex="6" placeholder="Mensaje"></textarea>
			</div>
			
			<div style="clear:both"></div>
			
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box_send_form">
				<input type="submit" id="submit" name="submit" value="Enviar" tabindex="7">
			</div>
		</div>
	</form>
</div>

<script src="<?php bloginfo('stylesheet_directory'); ?>/js/formvalidator/js/jquery.validate.js"></script> 
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/formvalidator/js/script.js"></script> 

<script>
	$(document).ready(function(){
		$('pre').addClass('prettyprint linenums');
	});
</script> 


<?php
}

// Send contact form via email
function tarful_send_contact_form() {
		
	if (isset($_POST['submit'])) {

		$tr_style = 'border:1px solid #FFFFFF';
		$admin_email = get_option('admin_email');
		
		$user_fullname = $_POST['fullname'];
		$user_email = $_POST['email'];
		$user_phone = $_POST['phone'];
		$user_type = $_POST['type'];
		$user_message = $_POST['message'];

		// Get visitor's IP
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		// Get information related to visitor, location, state, city, country.
		$location_data = array();
		$location_data = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		
		$continent_code = $location_data["geoplugin_continentCode"];
		$country_code = $location_data["geoplugin_countryCode"];
		$latitude = $location_data["geoplugin_latitude"];
		$longitude = $location_data["geoplugin_longitude"];
		$city = $location_data["geoplugin_city"];
		$state = $location_data["geoplugin_regionName"];
		$country = $location_data["geoplugin_countryName"];


		// Email to admin 
		$subject = '[ZOI Venezuela] Contacto desde Sitio Web';	
		$message = '<html><body><br />';
		$message .= '<table border="0" cellpadding="10" width="600px">';
		$message .= '<tr style="'.$tr_style.'"><td>Has recibido un mensaje desde el formulario de contacto</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Nombre: '.$user_fullname.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Email: '.$user_email.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Teléfono: '.$user_phone.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Tipo de Mensaje: '.$user_type.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Mensaje: '.$user_message.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>-------</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Dirección IP: '.$ip.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Ciudad: '.$city.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Estado: '.$state.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>País: '.$country.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Enviado desde el Formulario de Contacto en'.get_bloginfo('url').'</td></tr>';
		$message .= '</table>';
		$message .= '</body></html>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $admin_email, $subject, $message );


		// Email to user 
		$subject_user = '[ZOI Venezuela] ¡Gracias por contactarnos!';	
		$message_user = '<html><body><br />';
		$message_user .= '<table border="0" cellpadding="10" width="600px">';
		$message_user .= '<tr style="'.$tr_style.'"><td>Hola '.$user_fullname.', Gracias por Contactarnos</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>¡Hemos recibido tu email, vamos a revisarlo y te contactaremos próximamente!</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>Equipo ZOI Venezuela</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td><a href="'.get_bloginfo('url').'">zoivenezuela.com - Vive la Aventura</a></td></tr>';
		$message_user .= '</table>';
		$message_user .= '</body></html>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $user_email, $subject_user, $message_user );
	}

}


// Send Sent payment support notificacion via email
function tarful_send_payment_support($order_number, $user_id) {
		
	if (isset($_POST['submit'])) {

		$tr_style = 'border:1px solid #FFFFFF';
		$admin_email = get_option('admin_email');

		$user_info = get_userdata($user_id);
		$user_firstname = $user_info->first_name;
		$user_lastname = $user_info->last_name;
		$user_fullname = $user_firstname . ' ' . $user_lastname;
		$user_email = $user_info->user_email;
		$user_order_number = $order_number;

		// Get visitor's IP
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		// Get information related to visitor, location, state, city, country.
		$location_data = array();
		$location_data = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		
		$continent_code = $location_data["geoplugin_continentCode"];
		$country_code = $location_data["geoplugin_countryCode"];
		$latitude = $location_data["geoplugin_latitude"];
		$longitude = $location_data["geoplugin_longitude"];
		$city = $location_data["geoplugin_city"];
		$state = $location_data["geoplugin_regionName"];
		$country = $location_data["geoplugin_countryName"];


		// Email to admin 
		$subject = '[ZOI Venezuela] Recibido Soporte de Pago del pedido: '.$user_order_number.'';	
		$message = '<html><body><br />';
		$message .= '<table border="0" cellpadding="10" width="600px">';
		$message .= '<tr style="'.$tr_style.'"><td>Se ha recibido un nuevo soporte de pago desde plataforma:</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Cliente: '.$user_fullname.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Email: '.$user_email.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Número de Pedido: '.$user_order_number.' - <a href="'.get_bloginfo('url').'/wp-admin/post.php?post='.$user_order_number.'&action=edit">ver detalle</a></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>-------</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Dirección IP: '.$ip.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Ciudad: '.$city.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Estado: '.$state.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>País: '.$country.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Enviado desde el Portal de ZOI Venezuela'.get_bloginfo('url').'</td></tr>';
		$message .= '</table>';
		$message .= '</body></html>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $admin_email, $subject, $message );


		// Email to user 
		$subject_user = '[ZOI Venezuela] Recibimos tu soporte de pago para el Pedido: '.$user_order_number.'';	
		$message_user = '<html><body><br />';
		$message_user .= '<table border="0" cellpadding="10" width="600px">';
		$message_user .= '<tr style="'.$tr_style.'"><td>Hola '.$user_firstname.', Hemos recibido tu soporte de pago satisfactoriamente.</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td> Estaremos revisando el documento y actualizaremos tu pedido. Puedes revisar el estado de tu orden desde tu cuenta.</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td><a href="'.wc_get_endpoint_url('ver-orden').$user_order_number.'">Ver detalle de pedido '.$user_order_number.'</a></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>Equipo ZOI Venezuela</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td><a href="'.get_bloginfo('url').'">zoivenezuela.com - Vive la Aventura</a></td></tr>';
		$message_user .= '</table>';
		$message_user .= '</body></html>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $user_email, $subject_user, $message_user );
	}

}

// Custom logo in WP login
function tarful_login_logo() { ?>
	<style type="text/css">
		body {
			background: #1C212A;
		}
		.login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo_zoi.png);
		}
		
		input#wp-submit {
			background-color: #E74C3C !important;
			color: #FFF !important;
			text-transform: uppercase !important;
			border-radius: 3px;
			padding: 2px 5%;
			padding-bottom: 3px;
			text-shadow: none;
			box-shadow: 0px 3px 0px 0px #B32E20;
			border: 0px;
			font-size: 10px;
			font-weight: bold;
		}		

		input[type="text"]:focus,
		input[type="password"]:focus {
			box-shadow: 0px 0px 0px;
			background: white !important;
			border-color: none !important;
		}
		
		
		
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'tarful_login_logo' );


// Custom Styles for WP login page
function tarful_login_stylesheet() {
	wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/style-login.css' );
	//wp_enqueue_script( 'custom-login', get_template_directory_uri() . '/style-login.js' );
}
add_action( 'login_enqueue_scripts', 'tarful_login_stylesheet' );


// Change Shop's title on Browser
function wc_custom_shop_archive_title( $title ) {
	if ( is_shop() ) {
		return str_replace( __( 'Products', 'woocommerce' ), 'Experiencias', $title );
	}

	return $title;
}
add_filter( 'wp_title', 'wc_custom_shop_archive_title' );

// Update database with values for my account forms
function tarful_update_user_personal_info() {

	global $wpdb, $current_user;
	
	if (isset($_POST['updateuser'])) {
		
		if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
			if ( $_POST['pass1'] == $_POST['pass2'] ) {
				wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] )));
			}	
		}
	
		if ( !empty( $_POST['first-name'] )) {
			update_user_meta( $current_user->ID, 'billing_first_name', esc_attr( $_POST['first-name'] ) );
			update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
		}
		if ( !empty( $_POST['last-name'] )) {
			update_user_meta( $current_user->ID, 'billing_last_name', esc_attr( $_POST['last-name'] ) );
			update_user_meta( $current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
		}
		if ( !empty( $_POST['cedula'] )) {
			update_user_meta( $current_user->ID, 'billing_cirif', esc_attr( $_POST['cedula'] ) );
		}
		if ( !empty( $_POST['email'] )) {
			update_user_meta( $current_user->ID, 'billing_email', esc_attr( $_POST['email'] ) );
			wp_update_user( array( 'ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
		}
		if ( !empty( $_POST['phone1'] )) {
			update_user_meta( $current_user->ID, 'billing_phone', esc_attr( $_POST['phone1'] ) );
		}
		if ( !empty( $_POST['phone2'] )) {
			update_user_meta( $current_user->ID, 'billing_phone_o', esc_attr( $_POST['phone2'] ) );
		}
		if ( !empty( $_POST['address1'] )) {
			update_user_meta( $current_user->ID, 'billing_address_1', esc_attr( $_POST['address1'] ) );
		}
		if ( !empty( $_POST['address2'] )) {
			update_user_meta( $current_user->ID, 'billing_address_2', esc_attr( $_POST['address2'] ) );
		}
		if ( !empty( $_POST['city'] )) {
			update_user_meta( $current_user->ID, 'billing_city', esc_attr( $_POST['city'] ) );
		}
		if ( !empty( $_POST['state'] )) {
			update_user_meta( $current_user->ID, 'billing_state', esc_attr( $_POST['state'] ) );
		}
		if ( !empty( $_POST['country'] )) {
			update_user_meta( $current_user->ID, 'billing_country', esc_attr( $_POST['country'] ) );
		}
		if ( !empty( $_POST['postcode'] )) {
			update_user_meta( $current_user->ID, 'billing_postcode', esc_attr( $_POST['postcode'] ) );
		}
		if ( !empty( $_POST['billing_edad'] )) {
			update_user_meta( $current_user->ID, 'billing_edad', esc_attr( $_POST['billing_edad'] ) );
		}
		if ( !empty( $_POST['billing_fechnac_dia'] ) ) {
			update_user_meta( $current_user->ID, 'billing_fechnac_dia', esc_attr( $_POST['billing_fechnac_dia'] ) );
		}
		if ( !empty( $_POST['billing_fechnac_mes'] ) ) {
			update_user_meta( $current_user->ID, 'billing_fechnac_mes', esc_attr( $_POST['billing_fechnac_mes'] ) );
		}
		if ( !empty( $_POST['billing_fechnac_anno'] ) ) {
			update_user_meta( $current_user->ID, 'billing_fechnac_anno', esc_attr( $_POST['billing_fechnac_anno'] ) );
		}
		
		do_action('edit_user_profile_update', $current_user->ID);

		
	}
		
}

add_filter('show_admin_bar', '__return_false');
add_filter('woocommerce_customer_meta_fields','tarful_customer_meta_fields');
function tarful_customer_meta_fields(){
	return array(
			'billing' => array(
				'title' => __( 'Customer Billing Address', 'woocommerce' ),
				'fields' => array(
					'billing_first_name' => array(
						'label'       => __( 'First name', 'woocommerce' ),
						'description' => ''
					),
					'billing_last_name' => array(
						'label'       => __( 'Last name', 'woocommerce' ),
						'description' => ''
					),
					'billing_cirif' => array(
						'label' => 'Cedula',
						'description' => ''
					),
					'billing_company' => array(
						'label'       => __( 'Company', 'woocommerce' ),
						'description' => ''
					),
					'billing_address_1' => array(
						'label'       => __( 'Address 1', 'woocommerce' ),
						'description' => ''
					),
					'billing_address_2' => array(
						'label'       => __( 'Address 2', 'woocommerce' ),
						'description' => ''
					),
					'billing_city' => array(
						'label'       => __( 'City', 'woocommerce' ),
						'description' => ''
					),
					'billing_postcode' => array(
						'label'       => __( 'Postcode', 'woocommerce' ),
						'description' => ''
					),
					'billing_country' => array(
						'label'       => __( 'Country', 'woocommerce' ),
						'description' => '',
						'class'       => 'js_field-country',
						'type'        => 'select',
						'options'     => array( '' => __( 'Select a country&hellip;', 'woocommerce' ) ) + WC()->countries->get_allowed_countries()
					),
					'billing_state' => array(
						'label'       => __( 'State/County', 'woocommerce' ),
						'description' => __( 'State/County or state code', 'woocommerce' ),
						'class'       => 'js_field-state'
					),
					'billing_phone' => array(
						'label'       => __( 'Telephone', 'woocommerce' ),
						'description' => ''
					),
					'billing_email' => array(
						'label'       => __( 'Email', 'woocommerce' ),
						'description' => ''
					),
					'billing_edad' => array(
						'label'       => __( 'Edad', 'woocommerce' ),
						'description' => ''
					),
					'billing_fechnac_dia' => array(
						'label'       => __( 'Fecha de Nacimiento', 'woocommerce' ),
						'description' => ''
					),
					'billing_fechnac_mes' => array(
						'label'       => __( 'Fecha de Nacimiento', 'woocommerce' ),
						'description' => ''
					),
					'billing_fechnac_anno' => array(
						'label'       => __( 'Fecha de Nacimiento', 'woocommerce' ),
						'description' => ''
					)
				)
			),
			'shipping' => array(
				'title' => __( 'Customer Shipping Address', 'woocommerce' ),
				'fields' => array(
					'shipping_first_name' => array(
						'label'       => __( 'First name', 'woocommerce' ),
						'description' => ''
					),
					'shipping_last_name' => array(
						'label'       => __( 'Last name', 'woocommerce' ),
						'description' => ''
					),
					'billing_cirif' => array(
						'label' => 'Cedula',
						'description' => ''
					),
					'shipping_company' => array(
						'label'       => __( 'Company', 'woocommerce' ),
						'description' => ''
					),
					'shipping_address_1' => array(
						'label'       => __( 'Address 1', 'woocommerce' ),
						'description' => ''
					),
					'shipping_address_2' => array(
						'label'       => __( 'Address 2', 'woocommerce' ),
						'description' => ''
					),
					'shipping_city' => array(
						'label'       => __( 'City', 'woocommerce' ),
						'description' => ''
					),
					'shipping_postcode' => array(
						'label'       => __( 'Postcode', 'woocommerce' ),
						'description' => ''
					),
					'shipping_country' => array(
						'label'       => __( 'Country', 'woocommerce' ),
						'description' => '',
						'class'       => 'js_field-country',
						'type'        => 'select',
						'options'     => array( '' => __( 'Select a country&hellip;', 'woocommerce' ) ) + WC()->countries->get_allowed_countries()
					),
					'shipping_state' => array(
						'label'       => __( 'State/County', 'woocommerce' ),
						'description' => __( 'State/County or state code', 'woocommerce' ),
						'class'       => 'js_field-state'
					)
				)
			)
		);
}

// Display in WC order panel link for related supoort payment uploaded by client for order 
function tarful_display_order_data( $order ){  
	global $wpdb;
	$order_id = $order->id;
	?>

	<div class="order_data_column" style="width:100%;">
		<h4><?php _e( 'Soporte de Pago', 'woocommerce' ); ?></h4>			     

		<?php 
		$payment_method = get_post_meta( $order_id, '_payment_method', true ); 
		if ($payment_method == 'bacs') { // bacs is Depost or Transfer 
			$payment_support_id = '';
			$payment_support_id = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_type = 'payment_support' AND post_parent = $order_id" );
			if ($payment_support_id != '') {

				$payment_support_meta = $wpdb->get_results( "SELECT guid, post_modified FROM $wpdb->posts WHERE post_type = 'attachment' AND post_parent = $payment_support_id" );
				?>
				 <?php foreach ($payment_support_meta as $meta) { ?>
					<p><a style="color:#0073AA !important;" href="<?php echo $meta->guid; ?>" target="_blank"><?php echo '<strong>' . __( 'Ver soporte' ) . '</strong>'; ?></a><span style="font-size: 11px;"> - Enviado: <?php echo $meta->post_modified; ?></span></p>
				<?php }
			} else {
				echo '<p>Cliente no ha cargado Soporte de Pago todavía</p>';
			}
		}
			
		?>
	   
	</div>
<?php }
add_action( 'woocommerce_admin_order_data_after_order_details', 'tarful_display_order_data' );


// Change link in logo of WP-admin login page
function tarful_wpadmin_custom_link() {
	return 'http://zoivenezuela.com';
}
add_filter('login_headerurl','tarful_wpadmin_custom_link');


// Change tooltip in logo of WP-admin login page
function tarful_wpadmin_custom_tooltip() {
	return 'Portal ZOI Venezuela - Diseño y Desarrollo por Tarful.com';
}
add_filter('login_headertitle', 'tarful_wpadmin_custom_tooltip');


function tarful_related_posts($author_id, $post_id){ 

wp_reset_query();

	$args_f = array(
	'author'        =>  $author_id,
	'orderby'       =>  'post_date',
	'order'         =>  'DES',
	'type'          => 'post',
	'taxonomy'      => 'category',
	'post__not_in'  => array($post_id),
	'posts_per_page' => 3
	);
	
	$related_posts = get_posts($args_f);

?>
	<h1 style="text-align: center;">artículos relacionados</h1>

	<?php
	foreach ($related_posts as $post) : setup_postdata( $post );
	$id = get_the_author_meta('ID');
	$thumb_url_f = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), '',true);
	$category_f = get_the_category($post->ID);
	$cm_f = get_category_meta(false, get_term_by('slug', $category_f[0]->slug, 'category'));
	?>
	<div class="col-sm-4 col-xs-12">
		<div class="blog-post" style= "background: url(<?php echo $thumb_url_f[0];?>)">

			<div class="box_guru_grid post-info"style="border-top: solid 5px <?php echo $cm_f['cat_color'] ?>;"> 

				<a href="<?php echo get_author_posts_url($id); ?>">
					<div class="guru_info">
						 <?php echo get_avatar('',40); ?>
						 <div>
							<h5><?php echo get_the_author_meta('user_nicename') ?> 
							</br><span><?php echo count_user_posts($id);?> artículos</span>
							</h5>
						</div>
					</div>
				</a>

				<div class="aux-info">
					<div class="post-comments">
						<a href="<?php the_permalink(); ?>">
							<p> <?php echo get_comments_number() ?></p>
							<img src="<?php echo Get_stylesheet_directory_uri();?>/images/icon_comentarios.png" alt="">
						</a>
					</div>
					
					<a href="<?php echo get_category_link( $category_f[0]->cat_ID); ?> ">
						<div class="cat" style="background-color:<?php echo $cm_f['cat_color']  ?>">
							<img src="<?php echo $cm_f['cat_img']?>" alt="">
						</div>
					</a>
				</div>
			</div>
			<div class="sombra"> 
				
			</div>
			<div class="caption-articles">
				<a href="<?php the_permalink(); ?>"><?php echo get_the_title($post->ID); ?></a>
			</div>
		</div>
	</div>
	<?php 
	endforeach;
	wp_reset_postdata(); 
	wp_reset_query();
}
function tarful_blog_lastest_product(){

	if($_POST['width'] < 768){ ?>
	<div class="col-xs-offset-1 col-xs-10" style="margin-bottom: 9%;">
	<?php } ?>
		<div class="latest-products">
			<div id="recent">
				<h3>Nuevas experiencias</h3>
				<ul>
					<?php
						$args = array( 'post_type' => 'product',  'posts_per_page' => 3, 'orderby' =>'date','order' => 'DESC' );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
						<li class="span3">
							<a id="id-<?php the_id(); ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="link-product">
								<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="65px" height="115px" />'; ?>
								<h3><?php the_title(); ?></h3>
								<?php echo $product->get_price_html(); ?>
								<p class="av"> ver detalle</p>
							</a>
							<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>
						</li><!-- /span3 -->
					<?php endwhile; ?>

					<?php wp_reset_query(); ?>
				</ul><!-- /row-fluid -->
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-button">
				<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' )); ?>" class="avalaible">CONÓCE MAS EXPERIENCIAS</a>
			</div>
		</div>
	<?php if($_POST['width'] < 768){ ?>
	</div>
	<?php } 
}

function blog_post($col, $float_right = false){ 


	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	$id = get_the_author_meta('ID');
	$category = get_the_category();
	$cm = get_category_meta(false, get_term_by('slug', $category[0]->slug, 'category'));


	?>
 

	<li class=" col-xs-12 col-lg-<?php echo $col;?>" style="<?php if($float_right) echo 'float: right;';?> margin: 15px 0px;">
		<div class="blog-post" style="background: url(<?php echo $thumb_url[0]; ?>)/* repeat scroll center center / 100% auto*/; width: 100%; height: 100%;">
			<div class="box_guru_grid post-info" style="border-top: solid 3px <?php echo $cm['cat_color'] ?>"> 			
				<a href="<?php echo get_author_posts_url($id); ?>">
					<div class="guru_info">
						<?php echo get_avatar('',40); ?>
						<div>
							<h5><?php echo get_the_author_meta('user_nicename') ?></br>
							<span><?php echo count_user_posts($id);?> artículos</span>
							</h5>
						</div>
					</div>
				</a>

				<div class="aux-info">
					<div class="post-comments">
						<a href="<?php the_permalink(); ?>">
							<p> <?php echo get_comments_number() ?></p>
							<img src="<?php echo Get_stylesheet_directory_uri()?>/images/comments_icon.png " alt="">
						</a>
					</div>
					
					<a href="<?php echo get_category_link( $category[0]->cat_ID); ?> ">
					<div class="cat" style="background-color:<?php echo $cm['cat_color']  ?>">
						<img src="<?php echo $cm['cat_img']?>" alt="">
					</div>	
					</a>
				</div>
			</div>

			<div class="carousel-caption"> 
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</div>
		</div>
	</li>

<?php }
function blog_post_replace($col, $float = "", $item_size = ''){

	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	$id = get_the_author_meta('ID');
	$category = get_the_category();
	$cm = get_category_meta(false, get_term_by('slug', $category[0]->slug, 'category'));
	?>
	<div class="contenedor-blog-item-list col-xs-12 col-sm-<?php echo $col;?>" style="<?php $float;?>">
		<div class="blog-item-list <?php echo $item_size; ?>" style="background-image: url(<?php echo $thumb_url[0]; ?>);border-top-color: <?php echo $cm['cat_color'] ?>;">
			<div class="contenedor-caption">
				<a class="link-autor" href="<?php echo get_author_posts_url($id); ?>">
					<div class="guru_info">
						<?php echo get_avatar('',40); ?>
						<div>
							<h5><?php echo get_the_author_meta('user_nicename') ?></br>
							<span><?php echo count_user_posts($id);?> artículos</span>
							</h5>
						</div>
					</div>
				</a>
				<div class="aux-info">
					<div class="post-comments">
						<a href="<?php the_permalink(); ?>">
							<p> <?php echo get_comments_number() ?></p>
							<img src="<?php echo Get_stylesheet_directory_uri()?>/images/icon_comentarios.png " alt="">
						</a>
					</div>
					<a href="<?php echo get_category_link( $category[0]->cat_ID); ?> ">
						<div class="cat" style="background-color:<?php echo $cm['cat_color']  ?>">
							<img src="<?php echo $cm['cat_img']?>" alt="">
						</div>
					</a>
				</div>
			</div>
			<div class="caption-articles"> 
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</div>
		</div>
	</div>
<?php }

function category_menu(){ ?>

	<div class="col-xs-11 col-sm-8 col-md-5 limit blog-menu">
		<?php 
		$args = array(
			'type'                     => 'post',
			'child_of'                 => 0,
			'orderby'                  => 'name',
			'order'                    => 'ASC',
			'hide_empty'               => 1,
			'hierarchical'             => 1,
			'taxonomy'                 => 'category',
			'pad_counts'               => false 
		); 
		$categories  = get_categories( $args);
		$i = 0;
		foreach ($categories as $cat) {
			$cm = get_category_meta(false, get_term_by('slug', $cat->slug, 'category')); ?>
			<div class="col-xs-2 item_menu_blog">
				<a href="<?php echo get_category_link($cat->cat_ID) ?>">
					<div class="cat-menu">
						<img src="<?php echo $cm['cat_img']; ?>" alt="">
						<h5><?php echo $cat->slug ?></h5>
					</div>
				</a>
			</div>
			<?php
			$cat_border_color[$i] = $cm['cat_color'];
			$i++;
		}
		?>
	</div>
	<style>
		<?php for ($k=0 ; $k < sizeof($cat_border_color) ; $k++ ) {
			echo '.blog-menu .item_menu_blog:nth-child('.($k+1).'):hover div.cat-menu{' ;
			echo  'border-bottom: 3.5px solid '.$cat_border_color[$k].';';
			echo '}'.PHP_EOL;
		} ?>
	</style>
<?php }

function tarful_custom_user_profile_fields( $user ) {

?>
	<h3><?php _e('Información Adicional', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="twitter"><?php _e('twitter', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="twitter" id="twitter" placeholder="" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		 <tr>
			<th>
				<label for="facebook"><?php _e('Facebook', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="facebook" id="facebook" placeholder="" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th>
				<label for="instagram"><?php _e('Instagram', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="instagram" id="instagram" placeholder="" value="<?php echo esc_attr( get_the_author_meta( 'instagram', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		
	</table>
<?php }




function tarful_save_custom_user_profile_fields( $user_id ) {
	
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
	update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
	update_user_meta( $user_id, 'instagram', $_POST['instagram'] );
	delete_user_meta( $user_id, 'description');
	add_user_meta( $user_id, 'description' ,$_POST['tf_description'] );
}

add_action( 'show_user_profile', 'tarful_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'tarful_custom_user_profile_fields' );

add_action( 'personal_options_update', 'tarful_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'tarful_save_custom_user_profile_fields' );

function tarful_sort_dates_function( $a, $b ){
	 return strtotime($a) - strtotime($b);
}

function tarful_sort_dates( $array ){
	 uasort($array, "tarful_sort_dates_function");
	 return $array;
}

function taful_comunidad_zoi(){
?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 block_network text-center">
		<div class="col-xs-16 col-sm-16 col-md-10 col-lg-10 col-centered text-center">
			<center>

				<h2>Ya somos una comunidad de <u><?php tarful_users_count(); ?></u> aventureros</h2>

				<?php tarful_networks_avatars(9, 0); // Cambiar 2 por 9 ?>
				<div style="clear:both;"></div>
				
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 block_network text-cente box_main_social_login">
					<?php do_action( 'wordpress_social_login' ); // only shows when user is not logged-in ?>
				</div>
				
				<div class="box_stamp">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/stamp.png" alt="Calidad ZOI"/>
				</div>

			</center>

		</div>
	</div>
<?php
}
function tarful_zoi_prefooter(){
?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content_prefooter">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col_prefooter">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col_single">
					<img src="<?php echo CFS()->get('col_1_icon', 2); ?>" width="52" height="52" alt="<?php echo CFS()->get('col_1_title', 2); ?>">
					<h4><strong><?php echo CFS()->get('col_1_title', 2); ?></strong></h4>
					<?php echo CFS()->get('col_1_content', 2); ?>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col_single">
					<img src="<?php echo CFS()->get('col_2_icon', 2); ?>" width="52" height="52" alt="<?php echo CFS()->get('col_1_title', 2); ?>">
					<h4><strong><?php echo CFS()->get('col_2_title', 2); ?></strong></h4>
					<?php echo CFS()->get('col_2_content', 2); ?>
				</div>	
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col_single">
					<img src="<?php echo CFS()->get('col_3_icon', 2); ?>" width="52" height="52" alt="<?php echo CFS()->get('col_1_title', 2); ?>">
					<h4><strong><?php echo CFS()->get('col_3_title', 2); ?></strong></h4>
					<?php echo CFS()->get('col_3_content', 2); ?>
				</div>
			</div>
		</div>
<?php
}

add_action('woocommerce_after_checkout_billing_form','zoiTarful_checkout_extra_field');
function zoiTarful_checkout_extra_field(){

?>	<div id="billing_sexo_field" class="form-row col-lg-6 col-md-6 validate-required woocommerce-invalid woocommerce-invalid-required-field">
		<label class="" for="billing_sexo">Sexo<abbr class="required" title="obligatorio">*</abbr></label>
		<div class="row-box">
			<div class="text-input" style="padding: 6px 0px; float: left; width: 100%;">
				<select id="billing_sexo" class="select " placeholder="Edad" data-allow_clear="true" name="billing_sexo">
					<option value="">-</option>
					<option value="femenino">femenino</option>
					<option value="masculino">masculino</option>
				</select>
			</div>
		</div>
	</div>
	<div id="billing_fechnac" class="form-row col-lg-6 col-md-6 validate-required">
		<label for="billing_fechnac_dia" class="">Fecha de nacimiento <abbr class="required" title="obligatorio">*</abbr></label>
		<div class="row-box">
			<div class="text-input" style="padding: 6px 0px; float: left; width: 100%;">
				<div class="select_div">
					<select name="billing_fechnac_dia" class="requerido select">
						<option value="">-</option>
						<?php for ($i=1; $i <= 31; $i++) { ?>
							<option value="<?php echo $i; ?>" <?php if(get_the_author_meta( 'billing_fechnac_dia', $current_user->ID ) == $i) echo "selected"; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="select_div">
					<select name="billing_fechnac_mes" class="requerido select">
						<option value="">-</option>
						<?php for ($i=1; $i <= 12; $i++) { ?>
							<option value="<?php echo $i; ?>" <?php if(get_the_author_meta( 'billing_fechnac_mes', $current_user->ID ) == $i) echo "selected"; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="select_div">
					<select name="billing_fechnac_anno" class="requerido select">
						<option value="">-</option>
						<?php $annio_actual = intval(date("Y")); ?>
						<?php for ($i=($annio_actual-12); $i >= ($annio_actual - 60); $i--) { ?>
							<option value="<?php echo $i; ?>" <?php if(get_the_author_meta( 'billing_fechnac_anno', $current_user->ID ) == $i) echo "selected"; ?> ><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
	</div>

<?php
}




function myplugin_meta_box_callback( $post, $args ) {
	global $wpdb;
	$users = $wpdb->get_var("SELECT COUNT(*) FROM wp_woocommerce_order_itemmeta
		INNER JOIN wp_woocommerce_order_items on wp_woocommerce_order_itemmeta.order_item_id = wp_woocommerce_order_items.order_item_id
		INNER JOIN wp_posts on wp_posts.ID = wp_woocommerce_order_items.order_id
		INNER JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.ID
		WHERE wp_woocommerce_order_itemmeta.meta_key = '_product_id' 
		AND wp_woocommerce_order_itemmeta.meta_value = ".$_GET['post']."
		AND wp_woocommerce_order_items.order_item_type = 'line_item'
		AND wp_postmeta.meta_key = '_customer_user' ");
	?>
	<div style="text-align:center;">
		<div style="margin:10px;"> Total de usuarios registrados en Experiencia</div>
		<h2 style="margin-top: 5px;"><?php echo $users ?></h2>
		<div style="padding: 10px;clear: both;border-top: 1px solid #DDD;background: #F5F5F5 none repeat scroll 0% 0%;">
			<a href="<?php echo get_site_url(); ?>/exportar-excel/?post_id=<?php echo $post->ID; ?>" class="button-primary">Descargar listado (excel) </a>
		</div>
	</div>
	<?php
}


function relacion_perfil_guru_input(){
	$guru = get_post_meta ( get_the_ID(), 'guru', true );
	?><select name="id_guru"><?php
		?> <option value=""> - </option> <?php
	foreach (get_users(array('role' => 'guru')) as $user) {
		?><option id="option-guru-<?php echo $user->ID; ?>" value="<?php echo $user->ID; ?>" <?php if($guru == $user->ID){ echo 'selected=true'; } ?> ><?php echo $user->user_login; ?></option><?php
	}
	?></select>
	<?php
}

function relacion_perfil_guru() {

	add_meta_box('perfil-guru','Gurú','relacion_perfil_guru_input','post_perfil','normal','default');
}
add_action( 'add_meta_boxes_post_perfil', 'relacion_perfil_guru', 10, 1);

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function myplugin_add_meta_box() {

	add_meta_box('users-data','Total de usuarios registrados','myplugin_meta_box_callback','product','side','high',array());
	add_meta_box('perfil-guru','Gurú','relacion_perfil_guru_input','product','side','core');

}
add_action( 'add_meta_boxes_product', 'myplugin_add_meta_box', 10, 1);

function save_meta_relacion_perfil_guru( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	// Update the meta field in the database.
	delete_post_meta( $post_id, 'guru', $my_data );

	// Make sure that it is set.
	if ( ! isset( $_POST['id_guru'] ) || !( $_POST['id_guru'] ) ) {
		return;
	}

	/* OK, it's safe for us to save the data now. */
	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['id_guru'] );

	add_post_meta( $post_id, 'guru', $my_data );
}
add_action( 'save_post', 'save_meta_relacion_perfil_guru' );



function my_custom_fonts() {
	?>
	<style>
	#poststuff #users-data .inside {
		margin: 0;
		padding: 0;
	}
	#perfil-guru .inside{
		text-align: center;
	}
	</style>
	<?php
}

add_action('admin_head', 'my_custom_fonts');

// Create custom post type
add_action( 'init', 'tarful_post_type_perfil_guru' );

function tarful_post_type_perfil_guru() {
	register_post_type( 'post_perfil',
		array(
			'labels' => array(
				'name' => __('Perfil Gurú'),
				'singular_name' => __('Perfil Gurú'),
				'add_new' => __('Agregar Nuevo'),
				'add_new_item' => __('Agregar nuevo perfil'),
				'edit_item' => __('Editar perfil'),
				'new_item' => __('Nuevo perfil'),
				'all_items' => __('Todos los perfiles'),
				'view_item' => __('Ver perfil'),
				'search_items' => __('Buscar perfil'),
				'not_found' =>  __('Ningún perfil encontrado'),
				'not_found_in_trash' => __('Ningún perfil en papelera'), 
				'parent_item_colon' => '',
				'menu_name' => 'Perfiles Gurús'
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'query_var' => true,
			'rewrite' => false,
			'capability_type' => 'post',
			'has_archive' => true, 
			'hierarchical' => false,
			'menu_position' => 5,
			'supports' => array( 'title', 'editor', 'revisions','thumbnail','category'),
			'taxonomies' => array('category')

		)
	);
}

// Asociatate custom post type with a permalink

global $wp_rewrite;
$wp_rewrite->add_rewrite_tag("%post_perfil%", '([^/]+)', "post_perfil=");
$wp_rewrite->add_permastruct('post_perfil', 'perfil/%post_perfil%/', false);

//var_dump(get_editable_roles());
add_action('woocommerce_before_main_content','tarful_products_of_gurus');

function tarful_products_of_gurus(){
	if(isset($_GET["by_guru"])){
		query_posts(array('post_type' => 'product', 'meta_query' => array( array( 'key' => 'guru_user', 'value' => 41, 'compare' => '=') ) ));
	}
}

















?>