<?php
/**
 * Template Name: Promise Template
 *
 * by Tarful
 *
 */
get_header(); ?>

	<div id="content" class="site-content promise-template" style="background-color">
		<div class="row imgs_full_width">
			<div class="col-sm-12 banner_principal">
				<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
				<div class="caption_promise caption_promise_gradient">
				</div>
				<div class="caption_promise caption_promise_info centrado_vertical">
					<div class="col-sm-12">
						<div class="container">
							<div class="row centrado_vertical">
								<div class="col-sm-6 contenedor_caption_banner_promise">
									<div class="row caption_banner_promise">
										<div class="col-sm-12" style="padding-bottom: 5%;">
											<span class="titulo_promise"><?php echo get_the_title(); ?></span>
										</div>
										<div class="col-sm-12">
											<div class="centrado_vertical">
												<a href="#" class="avalaible">NUESTRA PROMESA</a>
												<a href="#" class="link_exp">NUESTRAS EXPERIENCIAS</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6 centrado_vertical contenedor_play" style="justify-content:center;-webkit-justify-content: center;-moz-justify-content: center;">
									<div class="row" style="justify-content:center;-webkit-justify-content: center;-moz-justify-content: center;margin-left: 95px;">
										<div class="col-sm-12">
											<a class="link_play_video" href="#">
												<div class="centrado_vertical play_video">
													<span class="glyphicon glyphicon-play"></span>
												</div>
												<span style='font-family: "bebasneue-book";'>VER VIDEO</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 contenido_principal_promesa_zoi">
				<div class="col-sm-12" id="contenido_promise">
					<?php echo the_content(); ?>
				</div>
				<script type="text/javascript">
					$(function(){
						function mitadTexto(texto){
							var mitad = parseInt(53 * texto.length /100 );
							while((texto[mitad] != " ") && (texto[mitad - 1] != " ") && (texto[mitad - 1] != "\n")){
								mitad ++;
							}
							return mitad;
						}
						function enderizarTexto(contenedor){
							var parrafo = $(contenedor).text();
							var mitad = mitadTexto(parrafo);
							if(parrafo.length > 391){
								var columna1 = parrafo.substring(0, mitad);
								var columna2 = parrafo.substring(mitad,parrafo.length);
							}
							if($(window).width() >= 768){
								$(contenedor).html('<div class="col-xs-6 col-sm-6 columna1">'+columna1+'</div><div class="col-xs-6 col-sm-6 columna2">'+columna2+'<div style=\"font-family: \'Permanent Marker\',cursive; font-size: 17px; padding-top: 7%;\">- EQUIPO ZOI</div></div>');
							}
						}
						enderizarTexto("#contenido_promise");
					});
				</script>
			</div>
		</div>
		<div class="container imgs_full_width">
			<div class="row">
				<div class="col-sm-6 col-md-3">
					<div class="row grupo1">
						<div class="col-md-12">
							<div class="item_promise">
								<div class="promise_info centrado_vertical">
									<div class="row">
										<div class="col-md-12">
											<span class="enumerar">1</span>
										</div>
										<div class="col-md-12">
											<p><?php echo CFS()->get('primera_promesa'); ?></p>
										</div>
									</div>
								</div>
							</div>
							<div class="item_promise">
								<img src="<?php echo CFS()->get('img_1_columna_1'); ?>">
							</div>
							<div class="item_promise">
								<img src="<?php echo CFS()->get('img_2_columna_1'); ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="row grupo2">
						<div class="col-md-12">
							<div class="item_promise">
								<img src="<?php echo CFS()->get('img_1_columna_2'); ?>">
							</div>
							<div class="item_promise">
								<div class="promise_info centrado_vertical">
									<div class="row">
										<div class="col-md-12">
											<span class="enumerar">3</span>
										</div>
										<div class="col-md-12">
											<p><?php echo CFS()->get('tercera_promesa'); ?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="row">
						<div class="col-md-12">
							<div class="row grupo3">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="item_promise">
										<div class="promise_info centrado_vertical">
											<div class="row">
												<div class="col-md-12">
													<span class="enumerar">2</span>
												</div>
												<div class="col-md-12">
													<p><?php echo CFS()->get('segunda_promesa'); ?></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="item_promise">
										<img src="<?php echo CFS()->get('img_1_columna_3'); ?>">
									</div>
								</div>
							</div>
							<div class="row grupo4">
								<div class="col-md-12">
									<div class="item_promise">
										<img src="<?php echo CFS()->get('img_2_columna_3'); ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div style="padding-top:6%;" class="col-sm-12 imgs_full_width banner_secundario">
				<div class="row">
					<div class="col-sm-12">
						<img src="<?php echo get_site_url(); ?>/wp-content/themes/Tarful/images/zoipromise_banner2.png">
						<div class="caption_promise" style="background: black none repeat scroll 0% 0%; opacity: 0.55;">
						</div>
						<div class="caption_promise centrado_vertical">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-xs-6 col-sm-6 contenido_banner">
										<div class="row" style="width: 60%; margin: 0px auto; text-align: center;">
											<div class="col-xs-6 col-sm-6">
												<div class="row">
													<div class="col-xs-12 col-sm-12 contador">
														<?php echo tarful_team_users_count(); ?>
													</div>
													<div class="col-xs-12 col-sm-12 nombre">
														EQUIPO ZOI
													</div>
												</div>
											</div>
											<div class="col-xs-6 col-sm-6">
												<div class="row">
													<div class="col-xs-12 col-sm-12 contador">
													<?php
														$products = new WP_Query( array( 'post_type' => 'product', 'post_status' => 'publish', 'posts_per_page' => -1 ) );
														echo $products->found_posts;
													?>
													</div>
													<div class="col-xs-12 col-sm-12 nombre">
														EXPERIENCIA ZOI
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12">
												<hr style="height: 1px; border-bottom: 1px solid black; margin: 10% -10% 12%;">
											</div>
											<div class="col-xs-6 col-sm-6">
												<div class="row">
													<div class="col-xs-12 col-sm-12 contador">
														<?php echo tarful_gurus_users_count(); ?>
													</div>
													<div class="col-xs-12 col-sm-12 nombre">
														GURUS ZOI
													</div>
												</div>
											</div>
											<div class="col-xs-6 col-sm-6">
												<div class="row">
													<div class="col-xs-12 col-sm-12 contador">
														<?php echo tarful_users_count(); ?>
													</div>
													<div class="col-xs-12 col-sm-12 nombre">
														COMUNIDAD ZOI
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<?php taful_comunidad_zoi(); ?>
			</div>
			<div class="col-sm-12 testimonial-featured">
				<?php get_template_part('content','testimonial-featured'); ?>
			</div>
			<div class="col-ms-12">
				<?php tarful_zoi_prefooter(); ?>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function () {
			enderizeTarfulHeightCall(".grupo1 .item_promise", 121);
			enderizeTarfulHeightCall(".grupo2 .item_promise", 77);
			enderizeTarfulHeightCall(".grupo3 .item_promise", 108);
			enderizeTarfulHeightCall(".grupo4 .item_promise", 127);
		});
	</script>
<?php get_footer(); ?>
