<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
global $post;
$category = get_the_category();

if(isset($_POST['recordSize'])) {
	ob_start();
		tarful_related_posts( $post->post_author, get_the_ID(), $category );
	$posts_relacionados = ob_get_clean();
	ob_start();
		tarful_blog_lastest_product();
	$nuevas_exp = ob_get_clean();

	if($_POST['width'] < 768){
		echo wp_send_json( array(
			'lastest_rela_1' => $posts_relacionados,
			'lastest_rela_2' => $nuevas_exp
		));
	}else{
		echo wp_send_json( array(
			'lastest_rela_1' => $nuevas_exp,
			'lastest_rela_2' => $posts_relacionados
		));
	}
	die();
}

get_header(); ?>
	<script type="text/javascript">
		$(document).ready( function() {
			var height = $(window).height();
			var width = $(window).width();
			$.ajax({
				url: '<?php echo get_permalink(); ?>',
				type: 'post',
				data: { 'width' : width, 'height' : height, 'recordSize' : 'true' },
				success: function(response) {

					$(".lastest-rela-1").replaceWith(response.lastest_rela_1);
					$(".lastest-rela-2").replaceWith(response.lastest_rela_2);
				},
			});
		});
	</script>
	<div id="primary" class="content-area">

		<main id="content" class="site-main" role="main">
			<div class="limit row">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php
					$cm = get_category_meta(false, get_term_by('slug', $category[0]->slug, 'category'));
					?>
					<div class="archive-banner archive-banner-blog limit " style="width:100%; height: auto; border-bottom: 5px solid <?php echo $cm['cat_color']; ?>">
						<img src="<?php echo CFS()->get('blog_header'); ?>" alt="" style="max-height:none;">
						<div class="contenedor-caption">
							<div class="ajustador-caption contenedor-centrado-vertical"style="text-align: center;">
								<div class="centrado-vertical">
									<div class="meta-post">
										<h2><?php the_date('d  F  Y'); ?>, por <?php echo get_the_author() ?></h2>
										<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php category_menu(); ?>
				</article><!-- #post-## -->
			</div>
			<div class="limit contenido-single-post">
				<div class="row" style="margin-bottom: 6%;">
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						get_template_part( 'content', 'post' ); 

						get_template_part( 'blog','sidebar' ); 

						$author_id = get_the_author_meta();
						$post_id = get_the_ID();
					// End the loop.
					endwhile;

					wp_reset_query();
					?>
					<div style="clear:both"></div>

					<div class="col-xs-12 col-sm-10 col-md-8 col-lg-8 comments">
						<?php if ( comments_open() ) {
                            comments_template();
						} ?>
					</div>
					<div class="col-xs-12">
						<div class="lastest-rela-2">
						<?php //tarful_related_posts($author_id, $post_id); ?>
						</div>
					</div>
				</div><!-- .row -->
			</div><!-- .limit-->
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
