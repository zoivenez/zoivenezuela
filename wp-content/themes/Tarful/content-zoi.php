<?php
/**
 * The template used the zoi promise section displayed in product detail
 *
*/
?>
<?php global $cfs; ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 zoi-promise-flyer">

	<h3><?php echo get_the_title(12);?></h3>

	<div class="promise-layout">
	<?php echo CFS()->get('zoi_promise_flyer', 12); ?>
	</div>

<!--	<div id="z-extra" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
		<a href="<?php echo get_permalink(12); ?>">Conoce más información</a>
	</div> -->
	
	
</div>
