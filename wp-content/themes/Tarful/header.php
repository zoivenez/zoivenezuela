<?php //tarful
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="Una comunidad en línea donde descubrir y reservar las mejores actividades, viajes y experiencias en toda Venezuela.">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.ico">
	<link href='http://fonts.googleapis.com/css?family=Permanent+Marker' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/bootstrap/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/fonts/typos.css" type="text/css" />
	<link href="<?php bloginfo('stylesheet_directory'); ?>/bxslider/jquery.bxslider.css" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Permanent+Marker' rel='stylesheet' type='text/css'> 
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/enderizeTarful/enderizeTarful.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/bxslider/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" href="<?php echo get_site_url().'/wp-content/themes/twentyfifteen/style.css'.'?'. filemtime( getcwd().'/wp-content/themes/twentyfifteen/style.css'); ?>" type="text/css" media="screen" />

	<?php if ( get_post_type() != 'product' ) { ?>
	<link href="<?php bloginfo('stylesheet_directory'); ?>/js/select2/css/select2.min.css" rel="stylesheet" />
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/select2/js/select2.min.js"></script>

	<script type="text/javascript">
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});		
	</script>

	<?php } ?>
	
	
	<script type="text/javascript">
		$(window).scroll(function() {
			if ($(this).scrollTop()>0) {
				$('.hide-scrolling').fadeOut();
			} else {
				$('.hide-scrolling').fadeIn();
			}
		});
	</script>

	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/easytabs/jquery.hashchange.min.js"></script>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/easytabs/jquery.easytabs.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		enderizeTarfulToHeaderCall($("#content"));
	});
	</script>

<!--	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-53069505-1', 'auto');
	  ga('send', 'pageview');
	
	</script> -->
	
	<?php
		wp_head();
		$current_user = wp_get_current_user();
	?>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style-yuri.css<?php echo '?' . filemtime( get_stylesheet_directory() . '/style-yuri.css'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/jquery-style/jquery-ui.css<?php echo '?' . filemtime( get_stylesheet_directory() . '/jquery-style/jquery-ui.css'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css<?php echo '?' . filemtime( get_stylesheet_directory() . '/css/style.css'); ?>" type="text/css" media="screen" />

</head>


<body <?php body_class(); ?>>
	
	<div class="overlayFondo" style="display:none;"></div>
	
    <?php
        tarful_mobile_menu(); 
        navbar(); 
    ?>
	
	<div id="page" class="hfeed site espaciable">	

		<?php
		//tarful_custom_header();

		if((is_single()) && (get_post_type(get_the_ID()) == "post")) {
			//menu_blog();
		} 
		?>

		
		


