		<?php $product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
		$validation_category = array_shift( $product_cats );?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 newsletter">
				<div class="col-xs-6 col-sm-6 col-md-12 col-lg-6">
					<?php if (is_home()) { ?>
 								
						<h2>Subscríbete a nuestro boletin</h2>
						<h2 class="footer-post">no te pierdas nuestras próximas experiencias </h2>
			
					<?php }else{ 
								if($validation_category->name != "Producto"):?>
									<h2>¿No puedes ir esta vez?</h2>
									<h2 class="footer-post">Suscríbete a nuestro boletin y te avisamos <strong>próximas fechas</strong> </h2>			
			                        <br>
			              <?php else:?>
			              			<h2>¿No conseguiste lo que buscabas?</h2>
									<h2 class="footer-post">Suscríbete a nuestro boletin y te avisamos cuando salgan <br><strong>nuevos productos</strong> </h2>			
			                        <br>
			              <?php endif;?>
					<?php } ?>	
				</div>

				<div class="col-xs-6 col-sm-6 col-md-12 col-lg-6">
<?php
$message_unprocessed = 'Desafortunadamente ocurrió un error. Por favor contáctanos para finalizar la suscripción';
$message_invalid     = 'El correo electrónico provisto no es válido. Por favor arréglalo y vuelve a intentar';
$message_success     = '¡Ahora serás la primera persona en enterarte de nuestras experiencias, eventos y productos!';
$submit_text         = 'Quiero Suscribirme';
?>
                <div class="sendgrid-subscription-widget" data-css="false" data-submit-text="<?=$submit_text?>" data-message-success="<?= $message_success?>" data-message-invalid="<?=$message_invalid?>"  data-message-unprocessed="<?=$message_unprocessed?>" data-token="uFq7W%2FVvangJ6C93kYrj%2Fi15TfGLuzjm4X1UYp3h6VrPgpmD7NmHQ%2BPySp2nFbsR"></div><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://s3.amazonaws.com/subscription-cdn/0.2/widget.min.js";fjs.parentNode.insertBefore(js,fjs);}}(document, "script", "sendgrid-subscription-widget-js");</script>
				</div>
	
				</div>
			</div>

		<?php if (is_home()) { ?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 social">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 FB">
					<h2 class="hashtag">zoi en instagram</h2>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 IN">
					<h2 class="hashtag">zoi en instagram</h2>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 TW">
					<h2 class="hashtag">zoi en instagram</h2>
				</div>
			</div>
		<?php } ?>
