<?php

function menu_blog(){
	global $post;
	$category = get_the_category();
	$cm = get_category_meta(false, get_term_by('slug', $category[0]->slug, 'category'));
	?>
	<div class="col-md-12 blog_header" style="border-bottom: 4px solid <?php echo $cm['cat_color']; ?>;">
		<div class="ajustador">
			<a href="<?php bloginfo('url')?>" class="logo_blog">
				<img src="<?php bloginfo('stylesheet_directory');?>/images/logo_zoi.png" alt="Zoi Venezuela" style="width: 49px;">
			</a>
			<div class="blog_menu">
				<div href="#" class="blog_coments_continer item_blog_menu item_blog_menu_principal">
					<p class="item_blog_menu"><?php echo get_comments_number(); ?></p>
					<img class="item_blog_menu" src="<?php echo Get_stylesheet_directory_uri();?>/images/icon_comentarios.png">
				</div>
				<?php
				tarful_facebook_share(get_the_ID() , apply_filters( 'woocommerce_short_description', $post->post_content ), array("class"=>"item_blog_menu item_blog_menu_principal"));
				tarful_twitter_share(array("class"=>"item_blog_menu item_blog_menu_principal"));
				?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$('body').on({
	    'touchmove': function(e) { 
			if ($(this).scrollTop() > 1) {
				$('#navbar, .menu-mobile').fadeOut();
			} else {
				$('#navbar, .menu-mobile').fadeIn();
			}
	    }
	});
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1) {
			$('#navbar, .menu-mobile').fadeOut();
		} else {
			$('#navbar, .menu-mobile').fadeIn();
		}
	});
	</script>
<?php }


/**
 * Displays an activity image
 * @activity category object
 * @return void
 */
function display_activity_image($activity) {
?>
<?php
    $cat_id            = $activity->term_id;
    $cat_name          = $activity->name;
    $cat_slug          = $activity->slug;
    $cat_link          = get_bloginfo('url') . '/experiencia/';
    $cat_link_specific = get_term_link($category);

    $temp_cat_featured = array();														
    $temp_cat_featured = get_category_meta(false, get_term_by('slug', $cat_slug, 'product_cat')); 
    if (!empty($temp_cat_featured) && $temp_cat_featured['featured'] != "") {
        $thumbnail_id = get_woocommerce_term_meta( $cat_id, 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        echo '<a href="'.$cat_link_specific.'"><img src="'.$image.'" alt="" /></a>';
    }
}

function tarful_related_posts($author_id, $post_id, $category = array() ){ 
	global $wpdb;

	if(isset($category) && is_array($category)){
		foreach ($category as $key => $value) {
			$category[$key] = $value->cat_ID;
		}
	}
/*
	$args_f = array(
		'author'        =>  $author_id,
		'orderby'       =>  'post_date',
		'order'         =>  'DES',
		'type'          => 'post',
		'category__in' => $category,
		'post__not_in'  => array($post_id),
		'posts_per_page' => 3
	);*/
	//$related_posts = get_posts($args_f);

	$related_posts = $wpdb->get_results(
		"SELECT wp_posts.ID, wp_posts.post_author FROM wp_posts
		INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id)
		INNER JOIN wp_terms ON (wp_terms.term_id = wp_term_relationships.term_taxonomy_id)
		WHERE (wp_terms.term_id IN (".implode(", ", $category).") OR wp_posts.id = ".$author_id.")
		AND wp_posts.post_type = 'post'
		AND wp_posts.ID NOT IN (".$post_id.")
		GROUP BY wp_posts.ID
		ORDER BY wp_posts.post_date DESC
		LIMIT 3"
	);

    // Filter only published posts
    $related_posts = array_filter($related_posts,function($post) {
        return get_post_status($post->ID) === 'publish';
    });


	if($related_posts){
	?>
		<h1 style="text-align: center;">artículos relacionados</h1>

		<?php
		foreach ($related_posts as $related_post){
            //if (get_post_status($related_post->ID) !== 'publish') continue; 
			$id = get_the_author_meta('ID', $related_post->post_author);
			$thumb_url_f = wp_get_attachment_image_src(get_post_thumbnail_id($related_post->ID), '',true);
			$category_f = get_the_category($related_post->ID);
			$cm_f = get_category_meta(false, get_term_by('slug', $category_f[0]->slug, 'category'));
			?>
			<div class="col-sm-4 col-xs-12">
				<div class="blog-post" style= "background: url(<?php echo $thumb_url_f[0];?>)">

					<div class="box_guru_grid post-info"style="border-top: solid 5px <?php echo $cm_f['cat_color'] ?>;"> 

						<a href="<?php echo get_author_posts_url($id); ?>">
							<div class="guru_info">
								 <?php echo get_avatar('',40); ?>
								 <div>
									<h5><?php echo get_the_author_meta('user_nicename', $related_post->post_author) ?> 
									</br><span><?php echo count_user_posts($id);?> artículos</span>
									</h5>
								</div>
							</div>
						</a>

						<div class="aux-info">
							<div class="post-comments">
								<a href="<?php echo get_post_permalink($related_post->ID); ?>">
									<p> <?php echo get_comments_number() ?></p>
									<img src="<?php echo Get_stylesheet_directory_uri();?>/images/icon_comentarios.png" alt="">
								</a>
							</div>
							
							<a href="<?php echo get_category_link( $category_f[0]->cat_ID); ?> ">
								<div class="cat" style="background-color:<?php echo $cm_f['cat_color']  ?>">
									<img src="<?php echo $cm_f['cat_img']?>" alt="">
								</div>
							</a>
						</div>
					</div>
					<div class="sombra"> 
						
					</div>
					<div class="caption-articles">
						<a href="<?php echo get_post_permalink($related_post->ID); ?>"><?php echo get_the_title($related_post->ID); ?></a>
					</div>
				</div>
			</div>
			<?php
		}
	}
	wp_reset_query();
}
function tarful_blog_lastest_product(){

	if($_POST['width'] < 768){ ?>
	<div class="col-xs-offset-1 col-xs-10" style="margin-bottom: 9%;">
	<?php } ?>
		<div class="latest-products">
			<div id="recent">
				<h3>Nuevas experiencias</h3>
				<ul>
					<?php
						$args = array( 'post_type' => 'product',  'posts_per_page' => 3, 'orderby' =>'date','order' => 'DESC' );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
						<li class="span3">
							<a id="id-<?php the_id(); ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="link-product">
								<?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" width="65px" height="115px" />'; ?>
								<h3><?php the_title(); ?></h3>
								<?php echo $product->get_price_html(); ?>
								<p class="av"> ver detalle</p>
							</a>
							<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>
						</li><!-- /span3 -->
					<?php endwhile; ?>

					<?php wp_reset_query(); ?>
				</ul><!-- /row-fluid -->
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-button">
				<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' )); ?>" class="avalaible">CONÓCE MAS EXPERIENCIAS</a>
			</div>
		</div>
	<?php if($_POST['width'] < 768){ ?>
	</div>
	<?php } 
}

function blog_post($col, $float_right = false){ 


	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	$id = get_the_author_meta('ID');
	$category = get_the_category();
	$cm = get_category_meta(false, get_term_by('slug', $category[0]->slug, 'category'));


	?>
 

	<li class=" col-xs-12 col-lg-<?php echo $col;?>" style="<?php if($float_right) echo 'float: right;';?> margin: 15px 0px;">
		<div class="blog-post" style="background: url(<?php echo $thumb_url[0]; ?>)/* repeat scroll center center / 100% auto*/; width: 100%; height: 100%;">
			<div class="box_guru_grid post-info" style="border-top: solid 3px <?php echo $cm['cat_color'] ?>"> 			
				<a href="<?php echo get_author_posts_url($id); ?>">
					<div class="guru_info">
						<?php echo get_avatar('',40); ?>
						<div>
							<h5><?php echo get_the_author_meta('user_nicename') ?></br>
							<span><?php echo count_user_posts($id);?> artículos</span>
							</h5>
						</div>
					</div>
				</a>

				<div class="aux-info">
					<div class="post-comments">
						<a href="<?php the_permalink(); ?>">
							<p> <?php echo get_comments_number() ?></p>
							<img src="<?php echo Get_stylesheet_directory_uri()?>/images/comments_icon.png " alt="">
						</a>
					</div>
					
					<a href="<?php echo get_category_link( $category[0]->cat_ID); ?> ">
					<div class="cat" style="background-color:<?php echo $cm['cat_color']  ?>">
						<img src="<?php echo $cm['cat_img']?>" alt="">
					</div>	
					</a>
				</div>
			</div>

			<div class="carousel-caption"> 
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</div>
		</div>
	</li>

<?php }
function blog_post_replace($col, $float = "", $item_size = ''){

	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	$id = get_the_author_meta('ID');
	$category = get_the_category();
	$cm = get_category_meta(false, get_term_by('slug', $category[0]->slug, 'category'));
	?>
	<div class="contenedor-blog-item-list col-xs-12 col-sm-<?php echo $col;?>" style="<?php $float;?>">
		<div class="blog-item-list <?php echo $item_size; ?>" style="background-image: url(<?php echo $thumb_url[0]; ?>);border-top-color: <?php echo $cm['cat_color'] ?>;">
			<div class="contenedor-caption">
				<a class="link-autor" href="<?php echo get_author_posts_url($id); ?>">
					<div class="guru_info">
						<?php echo get_avatar($id,40); ?>
						<div>
							<h5><?php echo get_the_author_meta('user_nicename') ?></br>
							<span><?php echo count_user_posts($id);?> artículos</span>
							</h5>
						</div>
					</div>
				</a>
				<div class="aux-info">
					<div class="post-comments">
						<a href="<?php the_permalink(); ?>">
							<p> <?php echo get_comments_number() ?></p>
							<img src="<?php echo Get_stylesheet_directory_uri()?>/images/icon_comentarios.png " alt="">
						</a>
					</div>
					<a href="<?php echo get_category_link( $category[0]->cat_ID); ?> ">
						<div class="cat" style="background-color:<?php echo $cm['cat_color']  ?>">
							<img src="<?php echo $cm['cat_img']?>" alt="">
						</div>
					</a>
				</div>
			</div>
			<div class="caption-articles"> 
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</div>
		</div>
	</div>
<?php }

function category_menu(){

	global $wpdb;
	$i = 0;

	/*
	$args = array(
		'type'                     => 'post',
		'child_of'                 => 0,
		'orderby'                  => 'name',
		'order'                    => 'ASC',
		'hide_empty'               => 1,
		'hierarchical'             => 1,
		'taxonomy'                 => 'category',
		'pad_counts'               => false 
	);
	$categories  = get_categories( $args);
	*/

	$categories = $wpdb->get_results("SELECT t.*, tt.* FROM wp_terms AS t
	INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id
	INNER JOIN wp_term_relationships ON (t.term_id = wp_term_relationships.term_taxonomy_id)
	INNER JOIN wp_posts ON (wp_posts.ID = wp_term_relationships.object_id)
	WHERE tt.taxonomy IN ('category')
	AND wp_posts.post_type = 'post'
	GROUP BY t.term_id");

	?>
	<div class="limit blog-menu">
        <?php if (!is_page('blog')): ?>
            <div class="item_menu_blog">
                <a href="<?=get_bloginfo('url') . '/blog'?>">
                    <div class="cat-menu">
                    <img src="<?=get_stylesheet_directory_uri() . '/images/flecha-atras.png'?>" alt="">
                        <h5>Volver</h5>
                    </div>
                </a>
            </div>
        <?php endif; ?>
			
		<?php 
		foreach ($categories as $cat) {
			$cm = get_category_meta(false, get_term_by('slug', $cat->slug, 'category'));
			?>
			<div class="item_menu_blog">
				<a href="<?php echo get_category_link($cat->term_id) ?>">
					<div class="cat-menu">
						<img src="<?php echo $cm['cat_img']; ?>" alt="">
						<h5><?php echo $cat->slug ?></h5>
					</div>
				</a>
			</div>
			<?php
			$cat_border_color[$i] = $cm['cat_color'];
			$i++;
		}
		?>
	</div>
	<style>
		<?php for ($k=0 ; $k < sizeof($cat_border_color) ; $k++ ) {
			echo '.blog-menu .item_menu_blog:nth-child('.($k+1).'):hover div.cat-menu{' ;
			echo 'border-bottom: 3.5px solid '.$cat_border_color[$k].';';
			echo '}'.PHP_EOL;
		}
		echo '.item_menu_blog{width: '.(100/(count($categories)+1)).'%;}'
		?>
	</style>
<?php }

