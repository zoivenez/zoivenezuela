<?php
/**
 * File: searchform.php
 * Author: Juan Carlos Arocha
 * Description: Search form to be displayed as main search form
 */



?>

<div class="col-xs-offset-1 col-xs-10 col-sm-offset-0 col-sm-12 main-search-form">
    <form action="<?php echo get_bloginfo( 'url' ); ?>" method="get" id="main-search" class="form-inline">
        <?php
            $activities = get_product_attributes('product_cat', -1, 1); 
            if ($activities) {
        ?>
        <div class="form-group hidden-xs ">
            <label for="product_cat"> ¿Qué quieres hacer? </label>
            <select name="product_cat" class="form-control">
                <option value="">Lo que sea</option>
                <?php 
                    foreach($activities as $activity) {
                        if($activity['name'] === "Producto"){
                            continue;
                        }
                ?>
                    <option value="<?=$activity['slug']?>"><?=$activity['name']?></option>
                <?php 
                    } 
                ?>
            </select>
        </div>
        <?php 
            }
            $destinations = get_product_attributes('localidad', -1, 0); 
            if ($destinations) {
        ?>
        <div class="form-group hidden-xs hidden-sm hidden-md">
            <label for="localidad"> ¿Dónde? </label>
            <select name="localidad" class="form-control">
                <option value="">Donde sea</option>
                <?php 
                    foreach($destinations as $destination) {
                ?>
                    <option value="<?=$destination['slug']?>"><?=$destination['name']?></option>
                <?php 
                    } 
                ?>
            </select>
        </div>
        <?php 
            }
        ?>

        <div class="form-group hidden-xs hidden-sm hidden-md">
            <label for="product_cat"> ¿Cuándo? </label>
            <select name="pa_fechas-de-experiencia" class="form-control">
                <option value="">Cuando sea</option>
                <option value="<?=_('enero')?>"><?=_('Enero')?></option>
                <option value="<?=_('febrero')?>"><?=_('Febrero')?></option>
                <option value="<?=_('marzo')?>"><?=_('Marzo')?></option>
                <option value="<?=_('abril')?>"><?=_('Abril')?></option>
                <option value="<?=_('mayo')?>"><?=_('Mayo')?></option>
                <option value="<?=_('junio')?>"><?=_('Junio')?></option>
                <option value="<?=_('julio')?>"><?=_('Julio')?></option>
                <option value="<?=_('agosto')?>"><?=_('Agosto')?></option>
                <option value="<?=_('septiembre')?>"><?=_('Septiembre')?></option>
                <option value="<?=_('octubre')?>"><?=_('Octubre')?></option>
                <option value="<?=_('noviembre')?>"><?=_('Noviembre')?></option>
                <option value="<?=_('diciembre')?>"><?=_('Diciembre')?></option>
            </select>
        </div>
        <div class="form-group">
            <label for="s"></label>

            <?php $count_experiences = wp_count_posts('product'); ?>
            <input type="text" class="form-control hidden-xs" name="s" id="search" placeholder="Tenemos más de <?=$count_experiences->publish?> experiencias para tí" value="<?php the_search_query(); ?>" />

            <!-- <span class="hidden-sm hidden-md hidden-lg">
                <label style="margin-bottom:-25px" for="s">¿A dónde quieres ir?</label>
            </span> -->
            <input type="text" class="form-control hidden-sm hidden-md hidden-lg col-xs-8" style="width:80%; margin-top:0;" name="s" id="search" placeholder="¿A dónde quieres ir?" value="<?php the_search_query(); ?>" />
            <button type="submit" class="btn btn-danger btn-zoi col-xs-2 hidden-sm hidden-md hidden-lg" style="margin-top:0;"><span class="glyphicon glyphicon-search"></span></button>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-danger btn-zoi hidden-xs"><span class="glyphicon glyphicon-search"></span></button>
        </div>
    </div>
    </form>
</div>
