jQuery(function( $ ) {  
    $(document).ready(function(){
        $('.datepicker').datepicker(); 

        $('#from').datepicker({
            dateFormat: 'yyyy/mm/dd',
            showOtherMonths: true,
            selectOtherMonths: true,
            firstDay : 1,
            yearRange: '-100:+0',
            minDate : new Date(),
            nextText: '',
            prevText: ''

        }); 

        $('#to').datepicker({
            dateFormat: 'yyyy/mm/dd',
            showOtherMonths: true,
            selectOtherMonths: true,
            firstDay : 1,
            yearRange: '-100:+0',
            minDate : new Date(),
            nextText: '',
            prevText: ''

        });
    });

    
    $('#exportSalesButton').click(function(){

        var $from = $("#from").datepicker({ dateFormat: 'yy/mm/dd' }).val();
        var $to = $("#to").datepicker({ dateFormat: 'yy/mm/dd' }).val();
        var url = $(this).data('url');
        var finalUrl = url;

        if($from != "")
        {
            if($to == null || $to == "")
            {
                $to = $from;
            }

            var bool = $('#searchByExperience').is(':checked');

            finalUrl = url + "/exportar-ventas?from=" + ($from) + "&to=" + ($to) + "&searchByExperience=" + bool;

        }
        else {
            alert('Ingrese una fecha para buscar');
            return false;

        }
        
        window.location.replace(finalUrl);

        return false;
    });
        
})

