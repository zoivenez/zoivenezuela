// File: custom-checkout.js
// Description: Handles checkout JS validations 
jQuery(function ( $ ) {
    var $formCheckout = $('form.woocommerce-checkout'); 

    /*********** Travelers forms *************/

    var travelerHash = {}

    var addTraveler = function(event) {
        event.preventDefault(); 

        var traveler_id = $( this ).data('traveler_id');
        var maxPeople   = $( this ).data('max_people');
        var traveler    = travelerHash[traveler_id];
        if (typeof traveler === 'undefined') {
            traveler                  = $('.' + traveler_id + '-traveler-fieldset').length + 1;
            travelerHash[traveler_id] = traveler;
        }        

        // Creating new form
        var travelerForm = '<div class="traveler-wrapper">';
        travelerForm += '<h5 class="fieldset-subtitle"> Viajero #' + traveler.toString();  
        travelerForm += '<span data-max_people="' + maxPeople + '" data-traveler_id="' + traveler_id + '" class="remove-traveler-button pull-right glyphicon glyphicon-remove" style="color:red; font-size:24px; cursor:pointer"></span></h5>';
        travelerForm += $('.' + traveler_id + '-traveler-fieldset').first().html(); 
        travelerForm += '</div>'; 
        // hidding it to have animation on show
        var $travelerObj = $(travelerForm).hide(); 
        $('.' + traveler_id + '-traveler-container').append($travelerObj); 

        // Get date inputs to remove hasDatepicker class
        $dateInput = $travelerObj.find('.input-date').find('input[type="text"]');
        // XXX: IF THE INPUT HAS 'hasDatepicker' CLASS
        // IT DOESN'T SHOW THE DATEPICKER
        if ($dateInput) {
            $dateInput.removeClass('hasDatepicker'); 
        }

        // Loop over inputs to remove errors
        $travelerObj.find('input').each(function(index, input) {
            removeError($(input), '', true); 
        });

        // Show traveler form
        $travelerObj.show('slow'); 

        // Disable add traveler button if the max people is reached
        if ( traveler >= maxPeople ) {
            $( this ).attr('disabled', true); 
        } else {
            travelerHash[traveler_id] = traveler + 1;
        }
    }

    // Removes a traveler form
    var removeTraveler = function() {

        var traveler_id   = $( this ).data('traveler_id');
        var $button       = $( this ).closest('.travelers-wrapper').find('.add-traveler-button');
        var maxPeople     = $button.data('max_people');
        var $travelerForm = $( this ).closest('.traveler-wrapper');
        var traveler      = travelerHash[traveler_id];

        $travelerForm.remove(); 

        if ( $button.attr('disabled') && traveler >= maxPeople ) {
            $button.attr('disabled', false); 
        } else {
            --traveler; 
            travelerHash[traveler_id] = traveler;
        }
    }

    var highlightTraveler = function() {
        var $travelerForm = $( this ).closest('.fieldset-subtitle');
        $travelerForm.addClass('highlight-traveler'); 
    }

    var normalizeTraveler = function() {
        var $travelerForm = $( this ).closest('.fieldset-subtitle');
        $travelerForm.removeClass('highlight-traveler'); 
    }

    $(document).on('click', '.add-traveler-button', addTraveler);
    $(document).on('click', '.remove-traveler-button', removeTraveler);
    //$(document).on('mouseover mouseenter', '.remove-traveler-button', highlightTraveler);
    //$(document).on('mouseout mouseleave', '.remove-traveler-button', normalizeTraveler);
    

    var showWarningAndTerms = function(event) {

        var travelerWarning = '<p class="warning-paragraph">Asegúrate de haber llenado los datos de todos ';
        travelerWarning += "los viajeros que irán a la(s) experiencia(s) que quieres comprar. ";
        travelerWarning += "<strong>Si no lo hiciste, éstos no serán tomados en cuenta por el proveedor</strong>.</p>";

        travelerWarning += '<p class="warning-paragraph"> ';
        travelerWarning += "<strong>Si no compraste experiencias, ignora el mensaje anterior</strong>.</p>";
        
        $('#travelerWarning').html(travelerWarning);

        event.preventDefault(); 
        return false; 
    }

    $(document).on('click', '#place_order', showWarningAndTerms); 
    $(document).on('change', '#accept-terms', function() { 
        if ( $( this ).is(':checked') ) {
            $('input[type="submit"]').each(function() {
                if ($( this ).val() === "CONFIRMAR COMPRA") {
                    this.disabled = false; 
                }
            })
        } else {
            $('input[type="submit"]').each(function() {
                if ($( this ).val() === "CONFIRMAR COMPRA") {
                    this.disabled = true; 
                }
            })
        }
    }); 

    // This array will be filled out with errorType: true/false 
    // saying the error is present or not
    var formErrors = []; 

    // Appends inline errors with msg and custom class
    var appendError = function (input, errorMsg, errorClass) {
        var errorHTML = '<span class="error-msg ' + errorClass + '">' + errorMsg + '</span>'; 
        // Add class input-error
        input.addClass('input-error'); 
        $errorMsgs = input.parent().find('.error-msg'); 
        // If there's an error message already
        if ($errorMsgs.length > 0) {
            var showError = true; 
            // If emptyClass exists, append error
            // otherwise, do nothing because the error
            // is already there
            $errorMsgs.each(function(index, value) {
                if ( $(value).hasClass(errorClass) ) {
                    showError = false; 
                    return false; 
                }
            });

            if ( showError ) {
                input.parent().append('<br>' + errorHTML); 
            }

        } else {
            input.parent().append(errorHTML); 
        }

    }
    

    // Removes inline error matching a class
    var removeError = function(input, errorClass, all) {

        // Default value for all = false
        all = typeof all !== 'undefined' ? all : false; 

        // Check if the input has an error and remove the class
        if ( input.hasClass('input-error') ) {
            var matchingErrors = 0; 

            var errorMsg = input.parent().find('.error-msg');



            // Looks for the errorClass class
            errorMsg.each(function(index, error) {
                // Remove error if the class matches or 
                // if we need to remove all the errors
                if ( $(error).hasClass(errorClass) || all ) {
                    ++matchingErrors; 
                    $(error).remove(); 
                }
            });

            // If all errors were removed, then removed the input-error class
            // as well
            if ( typeof errorMsg !== 'undefined' && matchingErrors === errorMsg.length) {
                input.removeClass('input-error'); 
            }
        }
    }

    // Checks if an input is required
    // by looking at its parent class or required span star
    var isRequired = function(input) {
        $input = $(input); 
        $inputContainer = $input.parent(); 

        return $inputContainer.hasClass('validate-required') || 
                $inputContainer.find('span').first().hasClass('required'); 
    }

    // Validate presence of single field
    var validatePresence = function(input, emptyMsg, emptyClass) {

        var $input = $(input); 
        var anyError = false; 

        // If the input is empty and it's required
         if ( $input.is(':visible') && !$input.val() && isRequired(input) ) {
            appendError($input, emptyMsg, emptyClass); 
            anyError = true; 
        } else {
            // Remove class error and data-msg
            removeError($input, emptyClass); 
        }


        return anyError;
    }

    // Validates field presence in client side
    var validateFieldPresence = function(selector) {

      // Get the wrapper where the presence validation is ran
      var wrapper = typeof selector === 'undefined' ? $formCheckout : selector; 
      var emptyClass = "empty-error";
      var emptyMsg   = 'El campo no puede estar vacío';

      var anyError = false;  // Variable to check if there was any errors

      wrapper.find('input').each(function(index, value){
          anyError |= validatePresence(value, emptyMsg, emptyClass); 
      });

      return !anyError; 
    }

    // Scrolls up to first found error
    var scrollToFirstError = function() {

        // Get the navbar height to make an offset from it
        navbarHeight = $('.navbar').height(); 

        // Loop to get first error
        $formCheckout.find('input').each(function(index, input){

            $input = $(input); 
            if ( $input.hasClass('input-error') ) {

                // Scroll to Input's label
                var $inputLabel = $input.parent('p').find('label').first(); 
                // Scroll to top
                $( 'html, body' ).animate( {
                    scrollTop: ( $inputLabel.offset().top - navbarHeight )
                }, 1000 );

                return false; 
            }
        });
    };

    // Validates email and append corresponding errors
    var validateEmail = function() {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i; 
        if (!re.test($(this).val())) {
            formErrors.email = true; 
            appendError($(this), 'El email es inválido', 'email-error'); 
        } else {
            formErrors.email = false; 
            removeError($(this), 'email-error'); 
        }
    }

    var validatePresenceHelper = function() {
        var emptyClass = "empty-error";
        var emptyMsg   = 'El campo no puede estar vacío';
        return validatePresence(this, emptyMsg, emptyClass); 
    }

    $inputEmail = $formCheckout.find('p.input-email').find('input[type="text"]'); 
    $inputEmail.focusout(validateEmail); 
    $(document).on('focusout', 'p.input-email > input[type="text"]', validateEmail); 
    $(document).on('focusout', 'input.input-text', validatePresenceHelper); 
    $(document).on('change', 'input.input-text', validatePresenceHelper); 
    $(document).on('change', 'p.input-email > input[type="text"]', validateEmail); 

    $('.fill-data-checkbox-input').change(function(event){
        if ( $(this).is(":checked") )  {
          $travelerForm = $(this).closest('.travelers-wrapper').find('.traveler-fieldset').first();
          validateFieldPresence($travelerForm); 
        }
    }); 

    // Validate all checkout fields
    var validateCheckoutFields = function() {

        var anyError    = false;

        var validations = [];
        validations.push(validateFieldPresence); 

        while ( validations.length > 0 ) {
            if ( !(validations.shift())() ) {
                anyError = true; 
            }
        }


        for (var errorType in formErrors) {
            if ( formErrors.hasOwnProperty(errorType) && formErrors[errorType]) {
                anyError = true; 
            }
        }

        if (!anyError) {
            $formCheckout.submit(); 
        } else {
            scrollToFirstError(); 
        }
    }

    $(document).on('click', '#continue-checkout', validateCheckoutFields); 

    // Remove empty error if anything is written
    $formInputs = $formCheckout.find('input');
    $formInputs.focusout(function(){
        if (isRequired(this) && $(this).val()) {
            removeError($(this), 'empty-error'); 
        }
    });

    /****** Travelers form validations ******/
    var preventLetters = function(e) {
        var input;
        if (e.metaKey || e.ctrlKey) {
            return true;
        }
        if (e.which === 32) {
            return false;
        }
        if (e.which === 0) {
            return true;
        }
        if (e.which < 33) {
            return true;
        }
        input = String.fromCharCode(e.which);
        return !!/[\d\s]/.test(input);
    }

    $(document.body).on('keypress', '.input-number', preventLetters); 
    $(document.body).on('keypress', '.input-number-cond', function(e) {
        $nationality = $(this).parent().find('.select').val(); 
        if ($nationality === "Venezolano") {
            return preventLetters(e); 
        }
    }); 

    // Date Pickers
    var monthNamesArray  =  [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                          "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ]; 

    var monthNamesShortArray  =  [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", 
                                  "Jul", "Ago", "Sept", "Oct", "Nov", "Dic" ]; 
    var dayNamesMinArray =  [ "Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab" ]; 

    var birthDateOpt = {
        monthNames : monthNamesArray,
        monthNamesShort : monthNamesShortArray,
        dayNamesMin: dayNamesMinArray,
        dateFormat: 'dd/mm/yy',
        yearRange: '-100:+0',
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        firstDay : 1,
        nextText: '',
        prevText: '',
        showOn: "focus"
    };

    // DatePicker init
    //$('.input-date').find('input[type="text"]').datepicker(birthDateOpt);
    $(document).on('focusin', 'p.input-date input[type="text"]', function() { 
       //$(this).datepicker('destroy');
       $(this).datepicker(birthDateOpt).datepicker("show");
    }); 
}); 
