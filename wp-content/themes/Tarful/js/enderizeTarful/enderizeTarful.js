var navbarDefault = $(".navbar-default");
var cont = 0;
function enderizeToHeader(contenedorPagina){

	if($("#navbar").css("display") != "none"){
		contenedorPagina.css("margin-top",$("#navbar-sticky-wrapper ul").height()+"px");
	}else if(navbarDefault.css("display") != "none"){
		contenedorPagina.css("margin-top",$(".navbar-default").height()+"px");
	}
}

function cetrarFixed(elementCenter, whith, height){
	if(whith){
		elementCenter.css("left",(($(window).width() - elementCenter.width())/2)+"px");
	}
	if(height){
		elementCenter.css("top",(($(window).height() - elementCenter.height())/2)+"px");
	}
}

function enderizeTarfulHeight(elementEnderize, heightPorcent){
	elementEnderize.height((elementEnderize.width() * 100 / heightPorcent) + "px");
}

function enderizeTarfulToHeaderCall(contenedorPagina){

	if(contenedorPagina.length){
		enderizeToHeader(contenedorPagina);
		$(window).resize(function() {
			enderizeToHeader(contenedorPagina);
		});
	}
}

function enderizeTarfulCenterCall(elementCenter, whith, height){
	whith = typeof whith !== 'undefined' ? whith : true;
	height = typeof height !== 'undefined' ? height : false;
	if($(elementCenter).length){
		cetrarFixed($(elementCenter), whith, height);
		$(window).resize(function() {
			cetrarFixed($(elementCenter), whith, height);
		});
		$(document).bind('DOMSubtreeModified', function () {
			cetrarFixed($(elementCenter), whith, height);
		});
	}
}
function enderizeTarfulHeightCall(elementEnderize, heightPorcent){
	if($(elementEnderize).length){
		enderizeTarfulHeight($(elementEnderize), heightPorcent);
		$(window).on("resize",function() {
			enderizeTarfulHeight($(elementEnderize), heightPorcent);
		});
		$(document).bind('DOMSubtreeModified', function () {
			enderizeTarfulHeight($(elementEnderize), heightPorcent);
		});
	}
}
/*
var open = window.XMLHttpRequest.prototype.open,
    send = window.XMLHttpRequest.prototype.send,
    onReadyStateChange;

function openReplacement(method, url, async, user, password) {
    var syncMode = async !== false ? 'async' : 'sync';
    console.warn(
        'Preparing ' +
        syncMode +
        ' HTTP request : ' +
        method +
        ' ' +
        url
    );
    return open.apply(this, arguments);
}

function sendReplacement(data) {
    console.warn('Sending HTTP request data : ', data);

    if(this.onreadystatechange) {
        this._onreadystatechange = this.onreadystatechange;
    }
    this.onreadystatechange = onReadyStateChangeReplacement;

    return send.apply(this, arguments);
}

function onReadyStateChangeReplacement() {
    console.warn('HTTP request ready state changed : ' + this.readyState);
    if(this._onreadystatechange) {
        return this._onreadystatechange.apply(this, arguments);
    }
}

window.XMLHttpRequest.prototype.open = openReplacement;
window.XMLHttpRequest.prototype.send = sendReplacement;
*/