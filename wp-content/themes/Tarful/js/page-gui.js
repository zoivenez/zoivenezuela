jQuery(function( $ ) {

    /******* Calendar's datepickers *******/
    // Global datepicker names setup
    var monthNamesArray  =  [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                              "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ]; 

    var monthNamesShortArray  =  [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", 
                                  "Jul", "Ago", "Sept", "Oct", "Nov", "Dic" ]; 
    var dayNamesMinArray =  [ "Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab" ]; 


    var birthDateOpt = {
            monthNames : monthNamesArray,
            monthNamesShort : monthNamesShortArray,
            dayNamesMin: dayNamesMinArray,
            dateFormat: 'dd/mm/yy',
            yearRange: '-100:+0',
            changeMonth: true,
            changeYear: true,
            showOtherMonths: true,
            firstDay : 1,
            nextText: '',
            prevText: '',
            showOn: "focus"
    };

    //$(document).on('focus', '.regular-date-picker input[type="text"]', function() {
         //$( this ).datepicker( birthDateOpt );
    //});

    // Global variable range to highlight selected dates
    var srcDate = null; 
    var dstDate = null; 

    // GLobal variable range to highlight hover dates
    var srcDateH = null;
    var dstDateH = null;

    // Month map month => number
    var monthMap = {
        'enero'      : '01',
        'febrero'    : '02',
        'marzo'      : '03',
        'abril'      : '04',
        'mayo'       : '05',
        'junio'      : '06',
        'julio'      : '07',
        'agosto'     : '08',
        'septiembre' : '09',
        'octubre'    : '10',
        'noviembre'  : '11',
        'diciembre'  : '12',
    }


    // Get keys of object
    var getKeys = function(obj) {
        var keys = [], name;
        for (name in obj) {
            if (obj.hasOwnProperty(name)) {
                keys.push(name);
            }
        }
        return keys;
    }

    // Get keys of object
    var getValues = function(obj) {
        var values = [], name;
        for (name in obj) {
            if (obj.hasOwnProperty(name)) {
                values.push(obj[name]);
            }
        }
        return values;
    }

    // Gets the date text in the Y-m-d format
    var getDateText = function(date) {

        var dayNum    = date.getDate();
        var day       = dayNum < 10 ? "0" + dayNum : dayNum;
        var monthNum  = date.getMonth() + 1;
        var month     = monthNum < 10 ? "0" + monthNum : monthNum; 
        var year      = date.getFullYear();

        var dateArray = [year, month, day]; 
        return dateArray.join('/'); 
    }

    var parseDateFromOption = function(option) {
            var optArray = option.split('-');

            if ( optArray.length < 3 || isNaN(parseInt(optArray[0])) ) 
                return ''; 

            // 0 => day
            // 1 => de
            // 2 => month
            // 3 => de
            // 4 => year
             
            var dayNum = optArray[0];
            var day    = dayNum < 10 ? "0" + dayNum : dayNum;
            var month  = monthMap[optArray[2]];
            var year   = "2015";

            // Check if the year is there
            if (optArray.length >= 5 && !isNaN(parseInt(optArray[4]))) {
                year  = optArray[4]; 
            }  

            dateArray = [year, month, day]; 

            return dateArray.join('/'); 
        
    }

    //Global experience duration
    var experienceDuration = $('#experience_duration').val(); 
    var tmpDuration = experienceDuration; 
    // The experience duration doesn't exists
    if (typeof experienceDuration == 'undefined' || isNaN(parseInt(experienceDuration))) {
        experienceDuration = 1; 
    }

    // Global variable for choose date options
    var chooseYourDateOpt = "reserva-tu-fecha";
    var chooseYourDateRegex = /reserva-tu-fecha[\w-]*/;

    var showGroupInfo = function() {

        $groupInfo = $('#pa_grupo').val(); 
        if ($groupInfo) {
            $people = parseInt($groupInfo.split('-')[0]); 
            if ( !isNaN($people) ) {
                var groupInfoHtml = "por persona"; 
                if ( $people > 1 ) {
                    groupInfoHtml = "por grupos de " + $people.toString(); 
                }

                $('.people-price').html(groupInfoHtml);
            }

        }
    }

    showGroupInfo(); 

    // get the dates from the variation options
    var getFixedDates = function() {

        var optionDateHash     = {};
        var dateOptionHash     = {};
        var chooseYourDateOpts = [];

        var datesSelect = document.getElementById('pa_fechas-de-experiencia'); 
        if (datesSelect == null) 
            return [optionDateHash, dateOptionHash, chooseYourDateOpts]; 

        var datesOpt = datesSelect.options;

        for (i = 0; i < datesOpt.length; ++i) {

            var optVal   = datesOpt[i].value;

            // Push to array the chooseYourDate option
            if (chooseYourDateRegex.exec(optVal) !== null) {
                chooseYourDateOpts.push(optVal);
                continue; 
            }

            var dateText = parseDateFromOption(optVal); 
            var dateObj = new Date(dateText); 

            if (dateText == '') continue; 

            optionDateHash[optVal]   = dateObj;
            dateOptionHash[dateText] = optVal;

            for (j = 1; j < experienceDuration; ++j) {
                var nextDate = new Date(dateObj);
                nextDate.setDate(dateObj.getDate() + parseInt(j)); 

                var nextDateText = getDateText(nextDate); 
                dateOptionHash[nextDateText] = optVal;
            }
        }

        return [optionDateHash, dateOptionHash]; 
    }

    // Array with: option -> date and date -> option hashes
    var fixedDatesHashes = getFixedDates(); 


    // Global hashes
    var optionDateHash = fixedDatesHashes[0]; 
    var dateOptionHash = fixedDatesHashes[1]; 

    var chooseYourDate = false; 

    // Array with fixed dates
    var fixedDates = getValues(optionDateHash); 


    // Makes clickeable only the days you specify in the admin panel
    var restrictSelectableDates = function(dt) {

        if (dt <= new Date()) {
            return [false, '']; 
        }

        var selectedRange   = false;
        var unselectedRange = false;

        // To highlight selected range
        if (srcDate != null) {
            selectedRange = ( dt >= srcDate && dt <= dstDate ); 
        } 

        // To highlight mouseover range
        if ( !selectedRange && srcDateH != null ) { 
            selectedRange = ( dt >= srcDateH && dt <= dstDateH ); 
        } 


        // We have fixed dates to highlight and select in the calendar
        if (fixedDates.length > 0) {

            var duration = parseInt(experienceDuration); 
            for (i = 0; i < fixedDates.length; ++i) {
                var iniDate = fixedDates[i]; 
                var endDate = new Date(iniDate); 
                endDate.setDate(iniDate.getDate() + duration - 1); 

                if (dt >= iniDate && dt <= endDate) {
                    unselectedRange = true; 
                    break; 
                }
            }
        } 

        if (chooseYourDate) {

            $variation_id   = $('.variation_id').val(); 
            // If the variation id is valid
            if ($variation_id) {

                // Get bookeable days and highlight them
                $variation_config = $('.variations_form').data('product_bookeabledays')[$variation_id];
                $bookeable_days   = $variation_config[0];

                // unselected ranges: bookeable days
                unselectedRange |= $bookeable_days.length === 0 || $.inArray(dt.getDay(), $bookeable_days) !== -1; 
            }
        }


        var highlight = selectedRange    ? 'ui-datepicker-range' : 
                        unselectedRange ? 'ui-datepicker-unselected-range' : '';

        return [ unselectedRange || selectedRange, highlight, ''];
    }



    // Gets the date range according to bookeable days and
    // duration. For this, we apply a simple DFS in the dates linear graph
    var computeRangeOfDates = function(bookeableDays, duration, iniDate) {

        var movs          = [-1, 1];
        var visited       = {};
        var remainingDays = duration;
        var dateStack     = [];

        srcDate = iniDate; 
        dateStack.push(iniDate); 

        while (dateStack.length > 0 && remainingDays > 0 ) {

            var currDate = dateStack.pop(); 

            if ( visited[getDateText(currDate)] == true ) {
                continue; 
            } else {
                // Mark as visited
                visited[getDateText(currDate)] = true;
                --remainingDays; 
            }

            for (var i = 0; i < 2; ++i) {
                var adjDate = new Date(currDate); 
                adjDate.setDate(currDate.getDate() + parseInt(movs[i])); 

                // If is bookeable
                if ( bookeableDays.length == 0 || $.inArray(adjDate.getDay(), bookeableDays) != -1 ) {
                    dateStack.push(adjDate); 
                }
            
            }
        }

        return getKeys(visited); 
    }

    var selectRangeOfDates = function(dateText, inst) {

            srcDate = null; 
            dstDate = null; 

            $durationVariation = $('#pa_duracion').val(); 
            if ($durationVariation) {
                $parts = $durationVariation.split('-');
                experienceDuration = parseInt($parts[0]); 

                if (isNaN(experienceDuration)) {
                    experienceDuration = tmpDuration; 
                }
            }  

            changeSrcAndDst(dateText, true); 

            if (srcDate === null) {
                $variation_id   = $('.variation_id').val(); 

                if (!$variation_id) {
                    return;
                }

                // Get bookeable days and highlight them
                $variation_config = $('.variations_form').data('product_bookeabledays')[$variation_id];
                $bookeable_days   = $variation_config[0];
                var range = computeRangeOfDates($bookeable_days, experienceDuration, $.datepicker.parseDate('dd/mm/yy', dateText)); 

                srcDate = $.datepicker.parseDate('yy/mm/dd', range[0]); 
                dstDate = $.datepicker.parseDate('yy/mm/dd', range[range.length - 1]); 

                $('.experience-calendar').datepicker('setDate', srcDate); 
                $('.experience-calendar').datepicker('refresh'); 
            }
    }

    // XXX: Two functions doing the same, we need refactoring (changeSrcAndDst[H])
    // To change src and dst of mouseover range
    var changeSrcAndDstH = function(dateText, changeSelector) {
        var duration = parseInt(experienceDuration); 

        $variation_id   = $('.variation_id').val(); 
        $bookeable_days = $('.variations_form').data('product_bookeabledays'); 

        // Get date object
        var parts        = dateText.split('/');
        var selectedDate = new Date(parts[2], parts[1] - 1, parts[0]);

        // XXX: getDateText to convert dateText to YYYY/MM/DD format
        var dateOpt = dateOptionHash[getDateText(selectedDate)]; 
        if (typeof dateOpt != 'undefined') {
            if (changeSelector) {
                $('#pa_fechas-de-experiencia').val(dateOpt).change(); 
            }

            srcDateH = optionDateHash[dateOpt]; 

            if (typeof srcDateH != 'undefined' && srcDateH) {
                dstDateH = new Date(srcDateH); 
                dstDateH.setDate(srcDateH.getDate() + duration - 1); 
            }
        }
    }

    // To change src and dst of selected range
    var changeSrcAndDst = function(dateText, changeSelector) {
        var duration = parseInt(experienceDuration); 

        $variation_id   = $('.variation_id').val(); 
        $bookeable_days = $('.variations_form').data('product_bookeabledays'); 

        // Get date object
        var parts        = dateText.split('/');
        var selectedDate = new Date(parts[2], parts[1] - 1, parts[0]);

        // XXX: getDateText to convert dateText to YYYY/MM/DD format
        var dateOpt = dateOptionHash[getDateText(selectedDate)]; 
        if (typeof dateOpt != 'undefined') {
            if (changeSelector) {
                $('#pa_fechas-de-experiencia').val(dateOpt).change(); 
            }

            srcDate = optionDateHash[dateOpt]; 

            if (typeof srcDate != 'undefined') {
                dstDate = new Date(srcDate); 
                dstDate.setDate(srcDate.getDate() + duration - 1); 
            }
        }
    }

    // Set the date selected in experience date select into calendar
    var setDateInCalendar = function() {
        var $selectedDate = $('#pa_fechas-de-experiencia').val(); 
        var dateObj = optionDateHash[$selectedDate]; 
        if ( typeof dateObj !== 'undefined' && dateObj > new Date()) {
            chooseYourDate = false; 

            $('.experience-calendar').datepicker('setDate', dateObj); 

            var dayNum   = dateObj.getDate();
            var day      = dayNum < 10 ? "0" + dayNum : dayNum;
            var monthNum = dateObj.getMonth() + 1;
            var month    = monthNum < 10 ? "0" + monthNum : monthNum;
            var year     = dateObj.getFullYear();

            var dateText = [day, month, year].join('/');

            changeSrcAndDst(dateText, false); 
            $('.experience-calendar').datepicker('refresh');
        } else {
            if (chooseYourDateRegex.exec($selectedDate) !== null) {
                chooseYourDate = true; 
            } else {
                chooseYourDate = false; 
            }
            // To remove selected dates
            srcDate = null;
            dstDate = null; 
            $('.experience-calendar').datepicker('refresh'); 
        }
    }

    var highlightDate = function(ev) {

        var dayNum = $(this).text();
        var day    = dayNum < 10 ? "0" + dayNum : dayNum;
        var month  = $(this).parent().data('month') + 1;
        var year   = $(this).parent().data('year');

        var dateText = [day, month, year].join('/');
        changeSrcAndDstH(dateText, false); 
        $('.experience-calendar').datepicker('refresh');
    }

    var normalizeDate = function(ev) {

        srcDateH = null;
        dstDateH = null; 
        $('.experience-calendar').datepicker('refresh');
    }

    $(document).on('change', '#pa_fechas-de-experiencia', setDateInCalendar);
    //$(document).on('mouseenter','a.ui-state-default', highlightDate);
    //$(document).on('mouseleave', '.ui-datepicker', normalizeDate);

    $(document).ready(function(){
        $( '</br><input id="same_shipping_address_checkbox" class="fill-data-checkbox-input" type="checkbox" /> <span class="fill-data-checkbox">Usar mi direccion actual</span>' ).insertBefore( "#shipping_address_1" );

    });

    $(document).on('click','#same_shipping_address_checkbox', function (){
        var address1 = $('#billing_address_1').val();
        var address2 = $('#billing_address_2').val();
        var shipping_address = address1 + " " + address2;
        if (this.checked)
        {
            $('#shipping_address_1').val(shipping_address);
        }
        else
        {
            $('#shipping_address_1').val("");
        }
    });

    $(document).on('click','#btn_slider', function (){
        jQuery('#how_it_works_div').slideDown();
        jQuery('#close_how_works').show();
        window.scrollTo(0, 0);
    });

    $(document).on('click','#close_how_works', function (){
        jQuery('#how_it_works_div').slideUp();
        jQuery('#close_how_works').hide();
        
    });

    $(window).scroll(function(event){
       var st = $(this).scrollTop();
       var divHeight = $('#how_it_works_div').height();

       if (st > divHeight && $('#how_it_works_div').is(':visible')){
            $('#how_it_works_div').hide();
            window.scrollTo(0, 0);

       }

    });

    $(document).on('ready', function(){
        var url = window.location.href;
        var count = url.indexOf("experiencias");
        if ( count <= -1)
        {
            $("#main_slider_btn_div").show();   
        }
        else{
            $("#main_slider_btn_div").hide();
        }
    });

    $(document).on('ready', function(){
        $('#review-verified').tooltip();
        $('.stars label').tooltip();
    });

    $('.experience-calendar').datepicker({
        monthNames : monthNamesArray,
        dayNamesMin: dayNamesMinArray,
        dateFormat: 'dd/mm/yy',
        showOtherMonths: true,
        selectOtherMonths: true,
        firstDay : 1,
        minDate : new Date(),
        nextText: '',
        prevText: '',
        beforeShowDay: restrictSelectableDates,
        onSelect: selectRangeOfDates
    }); 

    $('#checkin').datepicker({
        monthNames : monthNamesArray,
        dayNamesMin: dayNamesMinArray,
        dateFormat: 'dd/mm/yy',
        showOtherMonths: true,
        selectOtherMonths: true,
        firstDay : 1,
        minDate : new Date(),
        nextText: '',
        prevText: '',
 
    }); 

    $('#checkout').datepicker({
        monthNames : monthNamesArray,
        dayNamesMin: dayNamesMinArray,
        dateFormat: 'dd/mm/yy',
        showOtherMonths: true,
        selectOtherMonths: true,
        firstDay : 1,
        minDate : new Date(),
        nextText: '',
        prevText: '',
 
    });
    // Set first date 
    if (fixedDates.length > 0) {
        $('.experience-calendar').datepicker('setDate', fixedDates[0]); 
    }

    var refreshExpCalendar = function() {
        $('.experience-calendar').datepicker('refresh'); 
        showGroupInfo(); 
    }

    $(document).on('submit', '.variations_form', function(){

        var optVal = $('#pa_fechas-de-experiencia').val(); 
        if (chooseYourDateRegex.exec(optVal) !== null) {
            // Format date
            var experienceDate = $(".experience-calendar").datepicker('getDate'); 

            var day       = experienceDate.getDate().toString();
            var year      = experienceDate.getFullYear().toString();
            var month     = (experienceDate.getMonth() + 1).toString();
            var arrayDate = [day, month, year];

            var formattedDate = arrayDate.join("/"); 

            // Assign to hidden input experience_date
            $('#experience_date').val(formattedDate); 
        }
    });

    $(document).on('change', '.variation_select', refreshExpCalendar);


    /********** Sendgrid Messages ************/
    
    $('.sendgrid-subscription-widget').on('success error', function(event){

        var modalHeaderMsg = event.type === 'success' ? '¡Gracias por suscribirte!' : '¡Alerta!';
        var $responseDiv   = $( this ).find('.response').first(); 

        var responseTxt    = $responseDiv.text(); 

        var modalHTML = '<div class="modal fade" id="subscription-response" role="dialog">'; 
        modalHTML += '<div class="modal-dialog">';
        modalHTML += '<div class="modal-content">';
        modalHTML += '<div class="modal-header">';
        modalHTML += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
        modalHTML += '<h4 class="modal-title">Modal Header</h4>';
        modalHTML += '</div>';
        modalHTML += '<div class="modal-body"><p style="color:black; font-size:20px; font-family: muliregular;">';
        modalHTML += ' </p></div>';
        modalHTML += '<div class="modal-footer">'; 
        modalHTML += '<button type="button" class="btn btn-zoi" data-dismiss="modal">Cerrar</button>'; 
        modalHTML += '</div>'; 
        modalHTML += '</div>';
        modalHTML += '</div>';
        modalHTML += '</div>';

        $responseDiv.wrap(modalHTML); 
        $responseDiv.hide(); 

        var $responseModal = $('#subscription-response'); 
        $responseModal.find('.modal-title').first().html(modalHeaderMsg); 
        $responseModal.find('.modal-body p').first().html(responseTxt); 
        $responseModal.modal('show'); 
        var $emailInput = $( this ).find('input[type="email"]').first(); 
        $emailInput.val('');

    })

    /********* Woocommerce Messages **********/
    var animateMessages = function() {
        var wooMessage = $('.woocommerce-message');
        var wooError   = $('.woocommerce-error'); 

        wooMessage.delay(5000).fadeOut(160); 
        wooError.delay(5000).fadeOut(160); 
        $(document).on('click', '.woocommerce-message-close', function() {
            wooMessage.stop().fadeOut(160);
        });
        $(document).on('click', '.woocommerce-error-close', function() {
            wooError.stop().fadeOut(160);
        }); 
    }

    animateMessages(); 

    /********* LogIn modal **********/

    var showLoginModal = function(event) {
        event.preventDefault();
        $('#login-modal').modal('show'); 
    }

    var changeLoginTab = function(event) {
        event.preventDefault();
        var number = parseInt($(this).attr('rel')); 

        var tabToShow = ( number === 1 ? '#email-tab' : '#social-tab'); 
        $(tabToShow).click(); 
    }

    $(document).on('click', '.login-next-tab', changeLoginTab); 
    //$(document).on('click', '#login-link', showLoginModal); 


    $('#emailSubscription').keypress(function (e) {
          if (e.which == 13) {
            $('#submitEmailSubscription').trigger('click');
            return false;    //<---- Add this line
          }
        });

    /********* Main Search **********/
    $(document).on('submit', '#main-search', function() {
        // Disable inputs without values
        $(this).find('select').each(function() {
            // Only take selects with values
            if ( !$(this).val() ) {
                $(this).attr('disabled', true); 
            }
        });
        $(this).find('input[type="text"]').each(function() {
            // Only take selects with values
            if ( !$(this).val() ) {
                $(this).attr('disabled', true); 
            }
        });
    })

    
})

/*!
 * Variations Plugin XXX: MOVE THIS TO ANOTHER FILE
 */
;(function ( $, window, document, undefined ) {

    $.fn.wc_variation_form = function () {

        $.fn.wc_variation_form.find_matching_variations = function( product_variations, settings ) {
            var matching = [];

            for ( var i = 0; i < product_variations.length; i++ ) {
                var variation = product_variations[i];
                var variation_id = variation.variation_id;

                if ( $.fn.wc_variation_form.variations_match( variation.attributes, settings ) ) {
                    matching.push( variation );
                }
            }

            return matching;
        };

        $.fn.wc_variation_form.variations_match = function( attrs1, attrs2 ) {
            var match = true;

            for ( var attr_name in attrs1 ) {
                if ( attrs1.hasOwnProperty( attr_name ) ) {
                    var val1 = attrs1[ attr_name ];
                    var val2 = attrs2[ attr_name ];

                    if ( val1 !== undefined && val2 !== undefined && val1.length !== 0 && val2.length !== 0 && val1 !== val2 ) {
                        match = false;
                    }
                }
            }

            return match;
        };

        // Unbind any existing events
        this.unbind( 'check_variations update_variation_values found_variation' );
        this.find( '.reset_variations' ).unbind( 'click' );
        this.find( '.variations select' ).unbind( 'change focusin' );

        // Bind events
        $form = this

        // On clicking the reset variation button
        .on( 'click', '.reset_variations', function( event ) {

            $( this ).closest( '.variations_form' ).find( '.variations select' ).val( '' ).change();

            var $sku = $( this ).closest( '.product' ).find( '.sku' ),
                $weight = $( this ).closest( '.product' ).find( '.product_weight' ),
                $dimensions = $( this ).closest( '.product' ).find( '.product_dimensions' );

            if ( $sku.attr( 'data-o_sku' ) )
                $sku.text( $sku.attr( 'data-o_sku' ) );

            if ( $weight.attr( 'data-o_weight' ) )
                $weight.text( $weight.attr( 'data-o_weight' ) );

            if ( $dimensions.attr( 'data-o_dimensions' ) )
                $dimensions.text( $dimensions.attr( 'data-o_dimensions' ) );

            return false;
        } )

        // Upon changing an option
        .on( 'change', '.variations select', function( event ) {

            if ( $( this ).hasClass('addon-select') ) {
                return; 
            }

            $variation_form = $( this ).closest( '.variations_form' );

            if ( $variation_form.find( 'input.variation_id' ).length > 0 )
                $variation_form.find( 'input.variation_id' ).val( '' ).change();
            else
                $variation_form.find( 'input[name=variation_id]' ).val( '' ).change();

            $variation_form
            .trigger( 'woocommerce_variation_select_change' )
            .trigger( 'check_variations', [ '', false ] );

            $( this ).blur();

            if( $().uniform && $.isFunction( $.uniform.update ) ) {
                $.uniform.update();
            }

            // Custom event for when variation selection has been changed
            $variation_form.trigger( 'woocommerce_variation_has_changed' );

        } )

        // Upon gaining focus
        .on( 'focusin touchstart', '.variations select', function( event ) {

            if ( $( this ).hasClass('addon-select') ) {
                return; 
            }
            $variation_form = $( this ).closest( '.variations_form' );

            // Get attribute name from data-attribute_name, or from input name if it doesn't exist
            if ( typeof( $( this ).data( 'attribute_name' ) ) != 'undefined' )
                attribute_name = $( this ).data( 'attribute_name' );
            else
                attribute_name = $( this ).attr( 'name' );

            $variation_form
            .trigger( 'woocommerce_variation_select_focusin' )
            .trigger( 'check_variations', [ attribute_name, true ] );

        } )

        // Check variations
        .on( 'check_variations', function( event, exclude, focus ) {
            var all_set = true,
                any_set = false,
                showing_variation = false,
                current_settings = {},
                $variation_form = $( this ),
                $reset_variations = $variation_form.find( '.reset_variations' );

            $variation_form.find( '.variations select' ).each( function() {

                if ( $( this ).hasClass('addon-select') ) {
                    return true; 
                }

                // Get attribute name from data-attribute_name, or from input name if it doesn't exist
                if ( typeof( $( this ).data( 'attribute_name' ) ) != 'undefined' )
                    attribute_name = $( this ).data( 'attribute_name' );
                else
                    attribute_name = $( this ).attr( 'name' );


                if ( $( this ) === null || $( this ).val().length === 0  ) {
                    all_set = false;
                } else {
                    any_set = true;
                }

                if ( exclude && attribute_name === exclude ) {

                    all_set = false;
                    current_settings[ attribute_name ] = '';

                } else {

                    // Encode entities
                    value = $( this ).val();

                    // Add to settings array
                    current_settings[ attribute_name ] = value;
                }

            });

            var product_id = parseInt( $variation_form.data( 'product_id' ) ),
                all_variations = $variation_form.data( 'product_variations' );

            // Fallback to window property if not set - backwards compat
            if ( ! all_variations )
                all_variations = window.product_variations.product_id;
            if ( ! all_variations )
                all_variations = window.product_variations;
            if ( ! all_variations )
                all_variations = window['product_variations_' + product_id ];

            var matching_variations = $.fn.wc_variation_form.find_matching_variations( all_variations, current_settings );

            if ( all_set ) {

                var variation = matching_variations.shift();

                if ( variation ) {

                    // Found - set ID

                    // Get variation input by class, or by input name if class doesn't exist
                    if ( $variation_form.find( 'input.variation_id' ).length > 0 )
                        $variation_input = $variation_form.find( 'input.variation_id' );
                    else
                        $variation_input = $variation_form.find( 'input[name=variation_id]' );

                    // Set ID
                    $variation_input
                    .val( variation.variation_id )
                    .change();


                    $variation_form.trigger( 'found_variation', [ variation ] );

                } else {

                    // Nothing found - reset fields
                    $variation_form.find( '.variations select' ).val( '' );

                    if ( ! focus )
                        $variation_form.trigger( 'reset_image' );

                    alert( wc_add_to_cart_variation_params.i18n_no_matching_variations_text );

                }

            } else {

                $variation_form.trigger( 'update_variation_values', [ matching_variations ] );

                if ( ! focus )
                    $variation_form.trigger( 'reset_image' );

                if ( ! exclude ) {
                    $variation_form.find( '.single_variation_wrap' ).slideUp( 200 ).trigger( 'hide_variation' );
                }

            }

            if ( any_set ) {

                if ( $reset_variations.css( 'visibility' ) === 'hidden' )
                    $reset_variations.css( 'visibility', 'visible' ).hide().fadeIn();

            } else {

                $reset_variations.css( 'visibility', 'hidden' );
                $sku = $( this ).closest( '.product' ).find( '.sku' );
                $sku.text( $sku.attr( 'data-o_sku' ) );

            }

        } )

        // Reset product image
        .on( 'reset_image', function( event ) {

            var $product = $(this).closest( '.product' ),
                $product_img = $product.find( 'div.images img:eq(0)' ),
                $product_link = $product.find( 'div.images a.zoom:eq(0)' ),
                o_src = $product_img.attr( 'data-o_src' ),
                o_title = $product_img.attr( 'data-o_title' ),
                o_alt = $product_img.attr( 'data-o_alt' ),
                o_href = $product_link.attr( 'data-o_href' );

            if ( o_src !== undefined ) {
                $product_img
                .attr( 'src', o_src );
            }

            if ( o_href !== undefined ) {
                $product_link
                .attr( 'href', o_href );
            }

            if ( o_title !== undefined ) {
                $product_img
                .attr( 'title', o_title );
                $product_link
                .attr( 'title', o_title );
            }

            if ( o_alt !== undefined ) {
                $product_img
                .attr( 'alt', o_alt );
            }
        } )

        // Disable option fields that are unavaiable for current set of attributes
        .on( 'update_variation_values', function( event, variations ) {

            $variation_form = $( this ).closest( '.variations_form' );

            // Loop through selects and disable/enable options based on selections
            $variation_form.find( '.variations select' ).each( function( index, el ) {

                if ( $( this ).hasClass('addon-select') ) {
                    return true; 
                }

                current_attr_select = $( el );

                // Reset options
                if ( ! current_attr_select.data( 'attribute_options' ) )
                    current_attr_select.data( 'attribute_options', current_attr_select.find( 'option:gt(0)' ).get() );

                current_attr_select.find( 'option:gt(0)' ).remove();
                current_attr_select.append( current_attr_select.data( 'attribute_options' ) );
                current_attr_select.find( 'option:gt(0)' ).removeClass( 'attached' );

                current_attr_select.find( 'option:gt(0)' ).removeClass( 'enabled' );
                current_attr_select.find( 'option:gt(0)' ).removeAttr( 'disabled' );

                // Get name from data-attribute_name, or from input name if it doesn't exist
                if ( typeof( current_attr_select.data( 'attribute_name' ) ) != 'undefined' )
                    current_attr_name = current_attr_select.data( 'attribute_name' );
                else
                    current_attr_name = current_attr_select.attr( 'name' );

                // Loop through variations
                for ( var num in variations ) {

                    if ( typeof( variations[ num ] ) != 'undefined' ) {

                        var attributes = variations[ num ].attributes;

                        for ( var attr_name in attributes ) {
                            if ( attributes.hasOwnProperty( attr_name ) ) {
                                var attr_val = attributes[ attr_name ];

                                if ( attr_name == current_attr_name ) {

                                    if ( variations[ num ].variation_is_active )
                                        variation_active = 'enabled';
                                    else
                                        variation_active = '';

                                    if ( attr_val ) {

                                        // Decode entities
                                        attr_val = $( '<div/>' ).html( attr_val ).text();

                                        // Add slashes
                                        attr_val = attr_val.replace( /'/g, "\\'" );
                                        attr_val = attr_val.replace( /"/g, "\\\"" );

                                        // Compare the meerkat
                                        current_attr_select.find( 'option[value="' + attr_val + '"]' ).addClass( 'attached ' + variation_active );

                                    } else {

                                        current_attr_select.find( 'option:gt(0)' ).addClass( 'attached ' + variation_active );

                                    }
                                }
                            }
                        }
                    }
                }

                // Detach unattached
                current_attr_select.find( 'option:gt(0):not(.attached)' ).remove();

                // Grey out disabled
                current_attr_select.find( 'option:gt(0):not(.enabled)' ).attr( 'disabled', 'disabled' );

            });

            // Custom event for when variations have been updated
            $variation_form.trigger( 'woocommerce_update_variation_values' );

        } )

        // Show single variation details (price, stock, image)
        .on( 'found_variation', function( event, variation ) {
            var $variation_form = $( this ),
                $product = $( this ).closest( '.product' ),
                $product_img = $product.find( 'div.images img:eq(0)' ),
                $product_link = $product.find( 'div.images a.zoom:eq(0)' ),
                o_src = $product_img.attr( 'data-o_src' ),
                o_title = $product_img.attr( 'data-o_title' ),
                o_alt = $product_img.attr( 'data-o_alt' ),
                o_href = $product_link.attr( 'data-o_href' ),
                variation_image = variation.image_src,
                variation_link  = variation.image_link,
                variation_title = variation.image_title,
                variation_alt = variation.image_alt;

            $variation_form.find( '.variations_button' ).show();

            // In case the variation price_html is empty, we need to fill it
            if (variation.price_html.length === 0) {
                var price = '<span class="price"><span class="amount">';
                price += '<span class="money-currency-symbol"> Bs. </span>';
                price += variation.display_price
                price += '</span></span>'; 
                $variation_form.find( '.single_variation' ).html( price );
            } else {
                $variation_form.find( '.single_variation' ).html( variation.price_html );
            }
            $variation_form.find( '.single_variation_availability' ).html( variation.availability_html );


            if ( o_src === undefined ) {
                o_src = ( ! $product_img.attr( 'src' ) ) ? '' : $product_img.attr( 'src' );
                $product_img.attr( 'data-o_src', o_src );
            }

            if ( o_href === undefined ) {
                o_href = ( ! $product_link.attr( 'href' ) ) ? '' : $product_link.attr( 'href' );
                $product_link.attr( 'data-o_href', o_href );
            }

            if ( o_title === undefined ) {
                o_title = ( ! $product_img.attr( 'title' ) ) ? '' : $product_img.attr( 'title' );
                $product_img.attr( 'data-o_title', o_title );
            }

            if ( o_alt === undefined ) {
                o_alt = ( ! $product_img.attr( 'alt' ) ) ? '' : $product_img.attr( 'alt' );
                $product_img.attr( 'data-o_alt', o_alt );
            }

            if ( variation_image && variation_image.length > 1 ) {
                $product_img
                .attr( 'src', variation_image )
                .attr( 'alt', variation_alt )
                .attr( 'title', variation_title );
                $product_link
                .attr( 'href', variation_link )
                .attr( 'title', variation_title );
            } else {
                $product_img
                .attr( 'src', o_src )
                .attr( 'alt', o_alt )
                .attr( 'title', o_title );
                $product_link
                .attr( 'href', o_href )
                .attr( 'title', o_title );
            }

            var $single_variation_wrap = $variation_form.find( '.single_variation_wrap' ),
                $sku = $product.find( '.product_meta' ).find( '.sku' ),
                $weight = $product.find( '.product_weight' ),
                $dimensions = $product.find( '.product_dimensions' );

            if ( ! $sku.attr( 'data-o_sku' ) )
                $sku.attr( 'data-o_sku', $sku.text() );

            if ( ! $weight.attr( 'data-o_weight' ) )
                $weight.attr( 'data-o_weight', $weight.text() );

            if ( ! $dimensions.attr( 'data-o_dimensions' ) )
                $dimensions.attr( 'data-o_dimensions', $dimensions.text() );

            if ( variation.sku ) {
                $sku.text( variation.sku );
            } else {
                $sku.text( $sku.attr( 'data-o_sku' ) );
            }

            if ( variation.weight ) {
                $weight.text( variation.weight );
            } else {
                $weight.text( $weight.attr( 'data-o_weight' ) );
            }

            if ( variation.dimensions ) {
                $dimensions.text( variation.dimensions );
            } else {
                $dimensions.text( $dimensions.attr( 'data-o_dimensions' ) );
            }

            $single_variation_wrap.find( '.quantity' ).show();

            if ( ! variation.is_purchasable || ! variation.is_in_stock || ! variation.variation_is_visible ) {
                $variation_form.find( '.variations_button' ).hide();
            }

            if ( ! variation.variation_is_visible ) {
                $variation_form.find( '.single_variation' ).html( '<p>' + wc_add_to_cart_variation_params.i18n_unavailable_text + '</p>' );
            }

            if ( variation.min_qty !== '' )
                $single_variation_wrap.find( '.quantity input.qty' ).attr( 'min', variation.min_qty ).val( variation.min_qty );
            else
                $single_variation_wrap.find( '.quantity input.qty' ).removeAttr( 'min' );

            if ( variation.max_qty !== '' )
                $single_variation_wrap.find( '.quantity input.qty' ).attr( 'max', variation.max_qty );
            else
                $single_variation_wrap.find( '.quantity input.qty' ).removeAttr( 'max' );

            if ( variation.is_sold_individually === 'yes' ) {
                $single_variation_wrap.find( '.quantity input.qty' ).val( '1' );
                $single_variation_wrap.find( '.quantity' ).hide();
            }

            $single_variation_wrap.slideDown( 200 ).trigger( 'show_variation', [ variation ] );

        });

        $form.trigger( 'wc_variation_form' );

        return $form;
    };

    $( function() {

        // wc_add_to_cart_variation_params is required to continue, ensure the object exists
        if ( typeof wc_add_to_cart_variation_params === 'undefined' )
            return false;

        $( '.variations_form' ).wc_variation_form();
        $( '.variations_form .variations select' ).change();
    });

})( jQuery, window, document );
