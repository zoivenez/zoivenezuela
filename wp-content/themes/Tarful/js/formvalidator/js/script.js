$(document).ready(function(){


		$('#contact-form').validate({
	    rules: {
	        minlength: 3,
	       fullname: {
	        required: true,
	       required: true
	      },
		  
		 phone: {
	        minlength: 6,
	        required: true,
	        digits: true
	      },
		  
		  type: {
				required: true,
				minlength: 6
			},
		  
	      email: {
	        required: true,
	        email: true
	      },
		  
	     
		   message: {
	      	minlength: 10,
	        required: true
	      },
		  
		  agree: "required"
		  
	    },
			highlight: function(element) {
				$(element).closest('.control-group').removeClass('success').addClass('error');
			},
			success: function(element) {
				element
				.text('OK').addClass('valid')
				.closest('.control-group').removeClass('error').addClass('success');
			}
	  });

}); // end document.ready