<?php
/*
    Function to delete o edit a review
    return 0 on failure, 1 on success, including a message.
*/
function review_helper(){

    
    if ( !isset($_POST['act']) || !is_user_logged_in() || !isset($_POST['comment_id'])) {
        echo json_encode(array('response' => '0', 'message' => 'Oh, ha ocurrido un error'));
        wp_die(); 
    }

    $comment_id  = sanitize_text_field($_POST['comment_id']);
    $current_user = wp_get_current_user();
    /* verify if comment to affect belongs to user */
    $args = array(
    'ID' => $comment_id,
    'user_id' => $current_user->ID,
    );
    
    if(get_comments($args)){

        if($_POST['act'] == 'edit'){
            if ( !isset($_POST['comment']) || !isset($_POST['rating'])) {
                echo json_encode(array('response' => '0', 'message' => 'Oh, ha ocurrido un error'));
                wp_die();
            }

            $comment  = implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $_POST['comment'] ) ) );
            $rating =   sanitize_text_field($_POST['rating']);
            $commentarr = array();
            $commentarr['comment_ID'] = $comment_id;
            $commentarr['comment_approved'] = 0;
            $commentarr['comment_content'] = $comment;
            wp_update_comment( $commentarr);
            update_comment_meta( $comment_id, 'rating', $rating);
            echo json_encode(array('response' => '1', 'message' => 'Has actualizado tu comentario'));

           
        }else if($_POST['act'] == 'delete'){

            if(wp_delete_comment( $comment_id,true)){
                echo json_encode(array('response' => '1', 'message' => 'Has borrado el comentario exitosamente'));
            }else{
                echo json_encode(array('response' => '0', 'message' => 'Ha ocurrido un error al intentar borrar el comentario'));
            }
        }
    }else{
        echo json_encode(array('response' => '0', 'message' => 'Oh, ha ocurrido un error'));
    }

    wp_die();



}

// Adding AJAX action
add_action('wp_ajax_review_helper', 'review_helper');



