<?php
/**
 * Contains all functionality related to the newsletter and 
 * emails sent
 */


/**
 * AJAX action to add email user to registered sendgrid list
 * Depends of result, the modal is modified.
 * @return void
 */

function main_modal_subscriptions(){

    ?>
     <script type="text/javascript">
        jQuery(function( $ ) {
            // On submit, recheck 
            $(document).on('click', '#submitEmailSubscription', function(event){
                //event.preventDefault(); 
                var $title = $('.modal-body h4');
                var $message = $('div.wp-social-login-provider-list');
                var email  = $('#emailSubscription').val();

                // Need the email to be filled
                if (email) {

                    var user_login = email.substring(0, email.lastIndexOf("@"));
                    var action = 'add';

                    var args = {
                        'user_login' : user_login
                    }

                    var ajaxUrl = "<?=admin_url('admin-ajax.php')?>";
                    
                    // Ajax call to server
                    $.ajax({
                        'type' : 'post',
                        'url'  : "<?=admin_url('admin-ajax.php')?>",
                        'data' : {
                            'action'          : 'manage_sendgrid_recipients_helper',
                            'list'            : 'registered_users',
                            'email'           : email,
                            'sendgrid_action' : 'add',
                            'args'            : args
                        },
                        beforeSend: function(data, textStatus, jqXHR ){
                            var parentButton = $('#submitEmailSubscription').parent();
                            parentButton.html('<img src="<?=get_stylesheet_directory_uri()?>/images/loader.gif" alt="Cargando...">');
                        },
                        sucesss: function(data, textStatus, jqXHR ){
     
                        },
                        complete: function(data, dataStatus, jqXHR) {
                            
                           switch(data.responseText){
                            case '0':
                                $title.text("Disculpe, no pudimos suscribirlo");
                                $message.empty(); // Remove children
                                $message.append( "<p> Error al intentar suscribir el correo, inténtelo más tarde. </p>" );
                                break;

                            case '1':
                                $title.text("Oh, ya estás suscrito");
                                $message.empty(); // Remove children
                                $message.append( "<p> Ya tenemos tu correo. Si no te llegan nuestros boletines revisa en tus correos no deseados</p>" );
                                break;

                            case '2':
                                 $title.text('Felicidades, estás suscrito');
                                 $message.empty(); // Remove children
                                 $message.append( "<p> Bienvenido a ZOI Venezuela. Desde ahora te llegarán noticias, información y ofertas de nuestros planes.</p>" );
                                break;

                           }

                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            var $title = $('.modal-body h4').text('Disculpe, no pudimos suscribirlo');
                            var $message = $('div.wp-social-login-provider-list');
                            $message.empty(); // Remove children
                            $message.append( "<p> Error al intentar suscribir el correo, inténtelo más tarde. </p>" );

                        }

                      });

                    
                } else {

                    /* Email vacío. */
                }
            });
        });
    </script>
    <?php

}

add_filter('wp_footer', 'main_modal_subscriptions');

/**
 * Adds javascript to send ajax request on user newsletter subscription
 * @return void
 */
function handle_my_account_newsletter_subscriptions() {
    if (is_account_page()) {
        $current_user = wp_get_current_user();
?>
    <script type="text/javascript">
        jQuery(function( $ ) {
            // On submit, recheck 
            $(document).on('click', '#updateuser', function(event){
                //event.preventDefault(); 

                var $personalInfoForm = $( this ).closest('form'); 
                var $subscribed       = $('.newsletter-my-account').find('input[type="checkbox"]').first(); 
                var inputEmail        = $personalInfoForm.find('input[name="email"]');
                var email             = inputEmail.first(); 
                // Need the email to be filled
                if (email.is(':visible')) {
                    if (email && email.val()) {
                        var action = $subscribed.is(':checked') ? 'edit' : 'delete';

                        var user_login = "<?=$current_user->user_login?>";
                        var name       = $personalInfoForm.find('input[name="first-name"]').first().val();
                        var lastname   = $personalInfoForm.find('input[name="last-name"]').first().val();
                        var birthdate  = $personalInfoForm.find('input[name="billing_birthdate"]').first().val();
                        var genre      = $personalInfoForm.find('select[name="billing_genre"]').first().val();
                        var city       = $personalInfoForm.find('input[name="city"]').first().val();
                        var state      = $personalInfoForm.find('input[name="state"]').first().val();


                        // Args for add action
                        var add_args = {
                            'user_login' : user_login,
                            'name'       : name ? name : user_login, // if no name is specify, use login
                            'firstname'  : name,
                            'lastname'   : lastname,
                            'birthdate'  : birthdate,
                            'genre'      : genre,
                            'city'       : city,
                            'state'      : state
                        }

                        // Args for delete action
                        var delete_args = {}

                        // Set args depending on action
                        var args = (action === 'add' || action === 'edit') ? add_args : delete_args; 

                        // Ajax call to server
                        $.ajax({
                            'type' : 'post',
                            'url'  : "<?=admin_url('admin-ajax.php')?>",
                            'async': false,
                            'data' : {
                                'action'          : 'manage_sendgrid_recipients_helper',
                                'list'            : 'registered_users',
                                'email'           : email.val(),
                                'sendgrid_action' : action,
                                'user_id'         : <?=$current_user->ID?>,
                                'args'            : args
                            },
                            complete : function(data, dataStatus, jqXHR) {
                                $('.box-myaccount').find('form').submit();
                            },

                        });
                    } else {
                        alert('Debes especificar un correo si quieres recibir ofertas y eventos de ZOI Venezuela'); 
                    }
                }
            });
        });
    </script>
<?php
    }
}
add_filter('wp_footer', 'handle_my_account_newsletter_subscriptions'); 

/**
 * Subscribes registered user to newsletter and adds metadata
 * 'newsletter' => array(email, subscribed)
 * @param integer $user_id
 * @return void
 */
function subscribe_registered_user_to_newsletter( $user_id ) {
    $user_data = get_userdata( $user_id );

    // If user_data isn't null, proceed to add metadata
    if ($user_data) {
        $email = $user_data->user_email;
        $login = $user_data->user_login; 

        // XXX: This list is for now. Later we will segment by location, etc
        $list   = 'registered_users';
        $action = 'add';
        $args = array(
            'email'      => $email,
            'name'       => $login,
            'user_login' => $login
        ); 

        // Try to subscribe the user
        // Adds metadata that means this user_id is subscribed (subscribed => true)
        // with this email (email => email)
        $response = manage_sendgrid_recipients($user_id, $email, $list, $action, $args);

    }
}
// Action that links sends verification email
add_action( 'user_register', 'subscribe_registered_user_to_newsletter', 10, 1 ); 


/**
 * Generates a POST URL to interact with sendgrid API
 * @param string entityurl (newsletter/list, newsletter/list/email, etc) entity url to interact with
 * @param string action action to be performed
 * @param array args data to be sent to sendgrid API (key, value, JSON)
 * @return array(api_url, request)
 */
function generate_sendgrid_api_url($entity_url, $action, $args) {

    $actions = array(
        'lists'      => array('add', 'edit', 'get', 'delete'),
        'email'      => array('add', 'get', 'count', 'delete'),
        'newsletter' => array('add', 'edit', 'get', 'list', 'delete'),
        'schedule'   => array('add', 'get', 'delete'),
        'identity'   => array('add', 'edit', 'get', 'list', 'delete')
    );

    $sendgrid_url = 'https://api.sendgrid.com/api';

    // last element is the entity itself
    $entity_url_parsed = array_filter(explode('/', $entity_url)); 
    $entity            = $entity_url_parsed[sizeof($entity_url_parsed) - 1];

    // Invalid action
    if (!in_array($action, $actions[$entity])) {
        $message = "Invalid action: $action";
        return array('result' => false, 'message' => $message); 
    }

    // Collect request data
    $request_data = array(); 

    $request_data['api_user'] = SENDGRID_USERNAME;
    $request_data['api_key']  = SENDGRID_PASSWORD;

    foreach ( $args as $arg ) {
        // If JSON is True, json_encode value
        $request_data[$arg['key']] = ($arg['JSON'] ? json_encode($arg['value']) : $arg['value']);
    }

    // Generate request URL
    $request_url = "$sendgrid_url/" . implode('/', $entity_url_parsed) . "/$action.json";
    // Generate request string
    $request = http_build_query($request_data); 

    return array(
        'request_url' => $request_url, 
        'request' => $request
    );
}

/**
 * Manage list in sendgrid
 * @param string $name_list name of list
 * @param string $action action to apply
 * @return true if the response is succesful, array(false,message) if it wasn't
 */
function manage_sendgrid_list($name_list = 'no_name', $action = 'add') { 

    $actions = array('add', 'get', 'count', 'delete');

    // Return false if it's an invalid request
    if ( !in_array($action, $actions) ) {
        $message = "Invalid action: $action";
        return array('result' => false, 'msg' => $message); 
    }

    $args[] = array(
                'key'   => 'list',
                'value' => $name_list,
                'JSON'  => false
            );


    $request_info = generate_sendgrid_api_url('newsletter/lists/', $action, $args); 
    $request_url  = $request_info['request_url'];
    $request      = $request_info['request'];

   // Make POST request using WP HTTP API
    $result = wp_remote_post($request_url, array(
        'method' => 'POST',
        'timeout' => 45, 
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking' => true,
        'headers' => array(),
        'body' => $request,
        'cookies' => array()
    )); 


    $decoded_body = json_decode($result['body']);


    return array(
        'result'  => $result['response']['code'] === 200,
        'message' => $result['body']
    );
}


/**
 * Manage user emails inside a sendgrid list
 * @param string $email user email or emails (comma separated) to be used
 * @param string $list Sendgrid list to be used defaults to registered_users
 * @param string $action action performed over lists (add, get, count, delete) defaults to add
 * @param array $data with suscriber arguments
 * @return true if the response is succesful, array(false,message) if it wasn't
 */
function manage_sendgrid_recipients($user_id, $email, $list = 'registered_users', $action = 'add', $data = array()) { 

    $actions = array('add', 'get', 'count', 'delete');
    $edit = $action == 'edit' ? true : false;
    $action = $edit ? 'add' : $action;
    if($edit){
        $response = manage_sendgrid_recipients($user_id,$email,$list,'delete');
    }

    // Return false if it's an invalid request
    if ( !in_array($action, $actions) ) {
        $message = "Invalid action: $action";
        return array('result' => false, 'msg' => $message); 
    }


    $args = array(
        // List is required in every kind of action
        array(
            'key' => 'list',
            'value' => $list,
            'JSON' => false
        )
    );

    $subscription = $list == 'registered_users' ? get_user_meta( $user_id, 'newsletter', true ) : false; 
 

    $subscribed = is_array($subscription) ? $subscription['subscribed'] : false; 

    // Handle action type
    switch ( $action )  {
        case 'add':

            // Don't make the request if the user is already subscribed
            if ( $list == 'registered_users' && is_array($subscription) && $subscription['email'] === $email 
                                        && $subscription['subscribed'] === true) {
                return array(
                    'result' => true,
                    'message' => "$email : User already subscribed"
                );
            }


            // Email and Name are required
            if (!array_key_exists('name', $data)) {
                $data['name'] = ''; 
            }
            $data['email'] = $email;

            $request_data = $data;

            $args[] = array(
                'key'   => 'data',
                'value' => $data,
                'JSON'  => true
            );
            // Now it's subscribed
            $subscribed = true;
            break;
        case 'get':
            $args[] = array(
                'key'   => 'email',
                'value' => is_array($email) ? implode(',', $email) : $email,
                'JSON'  => false
            );
            break;
        case 'count':
            break;
        case 'delete':

            // Don't make the request if the user is not subscribed
            if ($list == 'registered_users' && is_array($subscription) && $subscription['email'] === $email 
                                        && $subscription['subscribed'] === false) {
                return array(
                    'result' => true,
                    'message' => "$email : User is not subscribed"
                );
            }

            $args[] = array(
                'key'   => 'email[]',
                'value' => is_array($email) ? implode(',', $email) : $email,
                'JSON'  => false
            );
            // Now it's not subscribed
            $subscribed = false; 
            break;
    }

    $request_info = generate_sendgrid_api_url('newsletter/lists/email', $action, $args); 
    $request_url  = $request_info['request_url'];
    $request      = $request_info['request'];

   // Make POST request using WP HTTP API
    $result = wp_remote_post($request_url, array(
        'method' => 'POST',
        'timeout' => 45, 
        'redirection' => 5,
        'httpversion' => '1.0',
        'blocking' => true,
        'headers' => array(),
        'body' => $request,
        'cookies' => array()
    )); 

    $decoded_body = json_decode($result['body']);
    $message = "";
    if ($action === 'add') {
        $transactions = $decoded_body->inserted;
     } elseif ($action === 'delete') {
        $transactions = $decoded_body->removed;
     } else {
        $transactions = 0;
     }
    

    // Remove newsletter user meta
    if($list == 'registered_users'){
        delete_user_meta( $user_id, 'newsletter' ); 
        // Add whether the subscription was successful or not
        $added = add_user_meta( $user_id, 'newsletter', array(
            'email' => $email,
            'subscribed' => $subscribed && $result['response']['code'] === 200
        )); 

        $message = $added ? '' : ". Warning: User meta was not updated";
       
    }

    return array(
        'result'  => $result['response']['code'] === 200,
        'message' => $result['body'] . $message,
        'transactions' => $transactions
    );
}

/**
 * AJAX action to manage user emails inside sendgrid list
 * @return string '0' on failure, '1' on user is already subscribed, '2' on success
 */
function manage_sendgrid_recipients_helper() {
    
    if ( !isset($_POST['email']) || !isset($_POST['list']) || !isset($_POST['sendgrid_action']) )  {
        echo '0';
        wp_die(); 
    }

    $user_id = isset($_POST['user_id']) ? intval( $_POST['user_id'] ) : null;

    $email  = sanitize_text_field($_POST['email']);
    $list   = sanitize_text_field($_POST['list']);
    $action = sanitize_text_field($_POST['sendgrid_action']);
    $args   = (array) $_POST['args'];

    // If 
    if (is_array($subscription) && $subscription['email'] === $email 
        && $subscription['subscribed'] === true) {

        $response =  array(
            'result' => true,
            'message' => "$email : Already subscribed"
        );
    } else {

        $response = manage_sendgrid_recipients($user_id, $email, $list, $action, $args);
        

    }

    if ($response['result'] === true) {
        if($response['transactions'] === 0 ){ 
            echo '1';
        }
        else{
            echo '2';
        }
    } else {
        echo '0';
    }

    wp_die(); 
}

// Adding AJAX action
add_action('wp_ajax_manage_sendgrid_recipients_helper', 'manage_sendgrid_recipients_helper');
add_action('wp_ajax_nopriv_manage_sendgrid_recipients_helper', 'manage_sendgrid_recipients_helper');

/**
 * Two loops to check if an invitation is repeated
 * @param array $variable
 * @param string $email
 * @return false if found a repeated invitation, otherwise true
 */

function invite_already_invited($variable,$email){
    $variable = (array)$variable;

    foreach ($variable as $key => $value) {
        foreach ($value as $k => $v) {
            if($k == 'email'){
                if($v == $email) return false;
            }
        }
    }
    return true;
}

/**
 * Manage user invitation
 * @param string $invite_email email of invite
 * @param integer $sender_id id of current user (sender)
 * @return array  with key response -> '0' on 'failure', '1' on user is already 
 * registered, '2' on user is already invited, '3' on success, a message and if
 * is success add the token, '4' on email not confirmed
 */

function manage_sendgrid_invite_user($invite_email,$sender_id){

    /* check if $invite_email is already registered */
    if(email_exists($invite_email)){

        /* email exists */
        return array('response' => '1', 'message' => 'Oh, el email ya se encuentra registrado.');
    }

    if(is_array(is_email_confirmed($sender_id))) {
        if(!is_email_confirmed($sender_id)['value']) {
            return array('response' => '4', 'message' => 'Primero, debes confirmar tu cuenta.');
        }
    }else{
        return array('response' => '4', 'message' => 'Primero, debes confirmar tu cuenta.');
    }
 
    $invitation_token = generate_token(64);
    $invitations = get_user_meta($sender_id,'invitations'); /* get invitations of curret user */
    

    /* check if  */
    if(!invite_already_invited($invitations,$invite_email)) return array('response' => '2', 'message' => 'Ya invitaste este correo');

    if(add_user_meta( $sender_id, 'invitations', array( 'token'=> $invitation_token, 'email' => $invite_email))){
        return array('response' => '3', 'message' => 'Invitación enviada exitosamente', 'token' => $invitation_token);
    }

    
    return array('response' => '0', 'message' => 'Oh, ha ocurrido un error');


}

/**
 * AJAX action to manage user invitation, send a email to invite if all is ok
 * @return array '0' on 'failure', '1' on user is already registered, '2' on user is already invited, '3' on success, and a message, '4' on email not confirmed
 */
function manage_sendgrid_invite_user_helper(){

    /* Check email and logged user */
    if ( !isset($_POST['email']) || !is_user_logged_in()) {
        echo json_encode(array('response' => '0', 'message' => 'Oh, ha ocurrido un error'));
        wp_die(); 
    }

    $email  = sanitize_text_field($_POST['email']);
    $current_user = wp_get_current_user();

    $response = manage_sendgrid_invite_user($email,$current_user->ID);
    if($response['response'] === '3'){
        if (send_invitation_user($email,$current_user, $response['token'])) {
            echo json_encode(array('response' => $response['response'], 'message' => $response['message'] ));
            wp_die(); 
        }
        echo json_encode(array('response' => '0', 'message' => 'Oh, ha ocurrido un error'));
        wp_die();
    }

     echo json_encode($response);
     wp_die();

}

// Adding AJAX action
add_action('wp_ajax_manage_sendgrid_invite_user_helper', 'manage_sendgrid_invite_user_helper');

/**
 * Redirect to notification page when an user is registered 
 * @return void
 */

function registration_redirect() {
    return home_url( '/notificaciones');
}
add_filter( 'woocommerce_registration_redirect', 'registration_redirect');


/**
 * Function to deletea user from a list or add a user into a list
 * NOTE: In case a list doesn't exist, it is created
 * @param array updated is the settings updated
 * @param booleantype_list 'frequency' or 'interest'
 * @return void
 */

function edit_sendgrid_list($updated,$type_list = false){
    $user = wp_get_current_user();

    if($type_list == 'interest'){
        
        $old = get_meta_notifications($user->ID, 'interest');
        $to_remove = array_diff($old, $updated); // remove user from these lists
        $to_append = array_diff($updated, $old); // append user to these lists


        foreach ($to_append as $list) {
            manage_sendgrid_list($list,'add'); // try to create list in case it doesn't exist
            $attemp = 0;
            do{

                $response = manage_sendgrid_recipients($user->ID,$user->user_email,$list,'add');
                ++$attemp;

            } while($attemp < 3 && $response['transactions'] <= 0);
        }


        foreach ($to_remove as $list) {
            $attemp = 0;
            do{

                $response = manage_sendgrid_recipients($user->ID,$user->user_email,$list,'delete');
                ++$attemp;

            } while($attemp < 3 && $response['transactions'] <= 0);
        }




    }else if($type_list == 'frequency'){

        $old = get_meta_notifications($user->ID, 'frequency');
        $attemp = 0;
        do{

            $response = manage_sendgrid_recipients($user->ID,$user->user_email,$updated[0],'add', sizeof($updated) > 1 ? array('comment' => $updated[1]) : array());
            ++$attemp;

        } while($attemp < 3 && $response['transactions'] <= 0);

        $attemp = 0;
        if($old){
            do{

                $response = manage_sendgrid_recipients($user->ID,$user->user_email,$old[0],'delete');
                ++$attemp;

            } while($attemp < 3 && $response['transactions'] <= 0);
        }
    
    }
}

/**
 * Get meta about notifications by type
 * @param integer $user_id  current user ID
 * @param string $type 'frequency' or 'interest' - exists two types of notifications, interest (categories) and frequency.
 * @return array 
 */
function get_meta_notifications($user_id , $type = null){

    if ($user_id) {
        if($type === 'frequency'){

            $notifications = get_user_meta($user_id,'frequency_notifications');
            if(!empty($notifications))
                return $notifications[0];

        }elseif($type === 'interest'){

            $notifications = get_user_meta($user_id,'interest_notifications');
            if(!empty($notifications))
                return $notifications[0];

        }

    
    }

    return array();
}



/**
 * Add meta about notifications by type
 * @param integer $user_id  current user ID
 * @param string $type 'frequency' or 'interest' - exists two types of notifications, interest (categories) and frequency.
 * @return boolean, false on failure, true on success.
 */

function add_meta_notifications($user_id, $type = null, $value = array()){
    if($user_id){

        if($type === 'interest'){
          
            update_user_meta( $user_id, 'interest_notifications', $value);
            return true;
            

        }else if($type === 'frequency'){
           
           update_user_meta( $user_id, 'frequency_notifications', $value);
            return true;
            

        }
    }

    return false;

    
}

/**
 * Helper function to updated interest notifications 
 * @return void
 */
function add_meta_interest_notifications_helper(){


    
    $categories =(array) json_decode(stripslashes($_POST['category']));

    if(!isset($categories) || !is_user_logged_in()){
        echo json_encode(array('response' => '0', 'message' => 'Ha ocurrido un error, intentalo de nuevo'));
        wp_die();
    }
    
    $current_user = wp_get_current_user();

    $meta = array();
        
    foreach($categories as $category) {

        array_push($meta, $category);
        
    }

    edit_sendgrid_list($meta,'interest');

    if(add_meta_notifications($current_user->ID,'interest',$meta)){
        echo json_encode(array('response' => '1', 'message' => 'Se han actualizados los cambios correctamente'));
        wp_die();
    }

   echo json_encode(array('response' => '0', 'message' => 'Ha ocurrido un error, intentalo de nuevo'));
   wp_die();

    
}

// Adding AJAX action
add_action('wp_ajax_add_meta_interest_notifications_helper', 'add_meta_interest_notifications_helper');


/**
 * Helper to updated frequency notifications 
 * @return void
 */
function add_meta_frequency_notifications_helper(){


    if(!isset($_POST['frequency']) || !is_user_logged_in()){
        echo json_encode(array('response' => '0', 'message' => 'Ha ocurrido un error, intentalo de nuevo'));
        wp_die();
    }

    $frequency = sanitize_text_field($_POST['frequency']);
    $current_user = wp_get_current_user();
    $meta = array($frequency);


    if(isset($_POST['comment']) && $frequency == 'never'){
        $comment = sanitize_text_field($_POST['comment']);
        array_push($meta, $comment);
    }

    edit_sendgrid_list($meta,'frequency');
    if(add_meta_notifications($current_user->ID,'frequency',$meta)){
        echo json_encode(array('response' => '1', 'message' => 'Se han actualizados los cambios correctamente'));
        wp_die();
    }

    echo json_encode(array('response' => '0', 'message' => 'Ha ocurrido un error, intentalo de nuevo'));
    wp_die();

    
}

// Adding AJAX action
add_action('wp_ajax_add_meta_frequency_notifications_helper', 'add_meta_frequency_notifications_helper');
