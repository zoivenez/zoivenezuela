<?php
/**
 * Displays responsive navbar. The reponsiveness is made with twitter bootstrap
 * @return void
 */
function navbar() {
    global $wpdb, $woocommerce;
?>
<nav class="navbar navbar-default navbar-fixed-top yamm" role="navigation">
    <div class="container">

        <!-- Navbar header: The elements here will be displayed
            always without toggling -->
        <div class="navbar-header">
            <a class="hidden-xs navbar-brand" href="<?php bloginfo('url')?>">
              <img src="<?php bloginfo('stylesheet_directory');?>/images/logo_zoi.png" alt="ZOI Venezuela" width="68">
            </a> <!-- Brand image desktop -->
            <a class="visible-xs mobile-logo navbar-brand" href="<?php bloginfo('url')?>">
              <img src="<?php bloginfo('stylesheet_directory');?>/images/logo_zoi.png" alt="ZOI Venezuela" width="50">
            </a> <!-- Brand image mobile -->
            <button type="button" class="pull-left navbar-toggle collapsed mobile-button" id="boton_menu_mobile" data-toggle="collapse" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> <!-- Toggle button -->
        </div> <!-- navbar-header -->

        <!-- Links: Hidden in the mobile version -->
        <div class="collapse navbar-collapse nav-stacked nav-pills" id="navbar">
            <ul class="nav navbar-nav">
                <li class="dropdown"> 
                    <a class="dropdown-toggle" data-toggle="experience-menu" href="<?= get_bloginfo('url') . '/experiencias/'?>" >Experiencias</a>
                    <ul class="experience-menu">
                        <div class="yamm-content">
                            <div class="row experience-menu-row">
                                <div class="col-sm-6 cat-container">
                                    <li class="dropdown-header"><h5>Actividades</h5> </li>
                                    <!-- Display activities -->
                                    <?php display_product_terms('product_cat', 8); ?>
                                    <li class="more-link"><a href="<?php echo get_bloginfo('url') . '/categorias/'; ?>"> + Ver todas</a></li>
                                </div> <!-- cat-container (Actividades) -->
                                <div class="col-sm-6 cat-container" style="border-left: 3px solid #181C23">
                                    <li class="dropdown-header"><h5>Destinos</h5></li>
                                    <?php display_product_terms('localidad', 8); ?>
                                    <li class="more-link"><a href="<?=get_permalink(24)?>"> + Ver todos</a></li>
                                </div> <!-- cat-containar (Destinos) -->
                            </div> <!-- row -->
                        </div> <!-- yamm-content -->
                    </ul> <!-- experience-menu -->
                </li> <!-- li class dropdown (Experiencias)  --> 
                <li><a href="<?= get_bloginfo('url') . '/nosotros/'?>" >Nosotros</a></li>
                <li class="dropdown">
                    <a href="<?= get_bloginfo('url') . '/contacto/'?>" >Contacto</a>
                    <ul class="phone-menu" role="menu">
                        <div class="yamm-content">
                            <div class="row phone-menu-row">
                                <li class="dropdown-header"><h5>¿Tienes dudas? Contáctanos.</h5> </li>
                            </div>
                            <?php if (CFS()->get('telefono', 22)): ?>
                                <div class="row phone-menu-row">
                                    <div class="col-sm-12">
                                        <li><span class="glyphicon glyphicon-phone-alt"></span> +58 <?=CFS()->get('telefono', 22)?> </li>					
                                    </div>
                                </div>
                            <?php endif; ?> 
                            <div class="row phone-menu-row">
                                <div class="col-sm-12">
                                    <li><img src="<?php bloginfo('stylesheet_directory');?>/images/whatsapp.png" width="25" height="25"> +58 414 964 4636 </li>					
                                </div>
                            </div>
                        </div>
                    </ul>

                </li>
                <!-- <li class="hidden-md">
                    <a clas href="<?= get_bloginfo('url') . '/calendar/'?>" >Calendario</a>
                </li> -->
                <li class="hidden-md event-yellow">
                    <a clas href="<?= get_bloginfo('url') . '/giraton/'?>" >Giratón</a>
                </li>
                        
            </ul> <!-- nav navbar-nav -->

            <!-- Navbar float right -->
            <ul class="nav navbar-nav navbar-right">
            <li class="hidden-xs hidden-sm <?=is_user_logged_in() ? '' : 'hidden-md small-nav-search'?> nav-search" >
                    <form class="navbar-form navbar-left" role="search" method="get" id="searchform" action="<?php bloginfo('url'); ?>">
                        <div class="navbar-search form-group">
                            <input type="text" value="" placeholder="¿A dónde quieres ir?" name="s" id="s" />
                            <input type="hidden" name="post" value="<?php echo get_search_query() ?>" />
                        </div>
                        <button type="submit" class="navbar-search btn btn-danger btn-zoi"><span class="glyphicon glyphicon-search"></span></button>
                    </form>
                </li>
               <!-- Cart dropdown-->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="cart-menu" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="Ver Carrito de Compras">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/img_cart.png" width="32" height="32" alt="car-shop" />
                        <span class="cart-contents"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
                    </a>
                    <ul class="cart-menu dropdown-menu-right">
                        <div class="yamm-content">
                                <?php 
                                    do_action( 'woocommerce_before_cart_contents' ); 
                                    $items = get_cart_items(); 
                                ?>

                                <?php if ( !$items ) : ?>
                                    <div class="row cart-menu-row">
                                        <div class="col-sm-12">
                                            <li class="empty"><center><?php _e( 'No products in the cart.', 'woocommerce' ); ?></center></li>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <?php foreach($items as $item): ?>
                                    <div class="row cart-menu-row">
                                        <li>
                                            <div class="col-sm-8">
                                                <div class="media">
                                                    <div class="media-middle" >
                                                        <a href="#" style="margin-right: 20px"><?=$item['image']?></a>
                                                    </div>
                                                    <div class="media-body" >
                                                        <a href="<?=$item['url']?>"><h4><?=$item['name']?></h4></a>
                                                        <p><?=$item['data']?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <p class="nav-price"><?=$item['quantity']?> &times; <?=$item['price']?></p> 
                                            </div>
                                        </li>
                                    </div>
                                    <?php endforeach;?>
                                    <div class="row cart-menu-row">
                                        <li>
                                            <hr>
                                            <div class="col-sm-5">
                                                <a href="<?php echo WC()->cart->get_checkout_url(); ?>" class="btn btn-danger btn-zoi nav-checkout">Realizar Compra</a>
                                            </div>
                                            <div class="col-sm-7">
                                                <p class="nav-subtotal"><?php _e( 'Subtotal', 'woocommerce' ); ?>: <?= WC()->cart->get_cart_subtotal(); ?></p>
                                            </div>
                                        </li>
                                    </div>
                            <?php endif; ?>
                        </div>
                    </ul>
                </li>
                <?php if (is_user_logged_in()) { 
                    $current_user = wp_get_current_user();
                    ?>
                    <li class="hidden-xs hidden-sm dropdown"> 
                        <a class="dropdown-toggle" data-toggle="user-menu" href="<?php echo get_permalink(7); ?>">
                            <?php echo get_avatar( $current_user->user_email, 32 ); ?>
                        </a>
                        <ul class="user-menu dropdown-menu-right">
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <li>
                                            <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ) ?>#userinfo"> Datos de usuario </a>
                                        </li>					
                                        <li>
                                            <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ) ?>#personalinfo"> Datos personales </a>
                                        </li>					
                                        <li>
                                            <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ) ?>#purchases"> Histórico de compras </a>
                                        </li>					
                                        <li>
                                            <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ) ?>#likeexperiences"> Mis experiencias favoritas </a>
                                        </li>					
                                        <li>
                                            <a href="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ) ?>"> Cerrar Sesión </a>
                                        </li>					
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </li>
                <?php }else{ ?>
                    <li id="login-link" class="hidden-xs hidden-sm register-btn">
                       <button type="button" onclick="location.href='<?=get_permalink(7)?>'" class="btn btn-danger navbar-btn btn-zoi" >Iniciar Sesión</button>
                    </li>
                <?php } ?>
            </ul>
        </div> <!-- nav-collapse -->
    </div> <!-- container --> 
</nav>


<?php
}

/**
 * Shows ZOI prefooter with 3 cols showing:
 * - 'Vive la experiencia'
 * - 'Tienes preguntas'
 * - 'Escríbemos tus comentarios'
 * However, this is set by CFS backend using the page ID number 2.
 * @return void
 */
function tarful_zoi_prefooter(){
?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content_prefooter">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col_prefooter">
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col_single">
					<img src="<?php echo CFS()->get('col_1_icon', 2); ?>" width="52" height="52" alt="<?php echo CFS()->get('col_1_title', 2); ?>">
					<h4><strong><?php echo CFS()->get('col_1_title', 2); ?></strong></h4>
					<?php echo CFS()->get('col_1_content', 2); ?>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col_single">
					<img src="<?php echo CFS()->get('col_2_icon', 2); ?>" width="52" height="52" alt="<?php echo CFS()->get('col_1_title', 2); ?>">
					<h4><strong><?php echo CFS()->get('col_2_title', 2); ?></strong></h4>
					<?php echo CFS()->get('col_2_content', 2); ?>
				</div>	
				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col_single">
					<img src="<?php echo CFS()->get('col_3_icon', 2); ?>" width="52" height="52" alt="<?php echo CFS()->get('col_1_title', 2); ?>">
					<h4><strong><?php echo CFS()->get('col_3_title', 2); ?></strong></h4>
					<?php echo CFS()->get('col_3_content', 2); ?>
				</div>
			</div>
		</div>
<?php
}

/**
 * Shows mobile menu which slides from the right of the screen
 * @return void
 */
function tarful_mobile_menu() {
	global $wpdb, $woocommerce; ?>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="navbar-collapse espaciable" id="bs-example-navbar-collapse-1">
		  
          <form class="navbar-form" role="search" action="<?php bloginfo('url'); ?>">
            <input type="text" class="form-control" name="s" placeholder="Buscar experiencias">
            <input type="hidden" name="post" value="<?php echo get_search_query() ?>" />
          </form>  

		  <ul class="nav navbar-nav">
			<li class="event-yellow"><a href="<?= get_bloginfo('url') . '/giraton/'?>">Giratón</a></li>
			<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('experiencias'))); ?>">Experiencias</a></li>
			<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('destinos'))); ?>">Destinos</a></li>
			<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('mi cuenta'))); ?>">Mi cuenta</a></li>
			<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('nosotros'))); ?>">Nosotros</a></li>
			<!--<li><a href="<?php echo esc_url( get_permalink( get_page_by_title('blog'))); ?>">Blog</a></li>-->
			<li><a href="<?php echo get_permalink(22); ?>">Contacto</a></li>
		  </ul>

		  

		</div><!-- /.navbar-collapse-->
		<nav class="navbar-default menu-mobile navbar-fixed-top">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" id="boton_menu_mobile">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="contenedor-caption">
					<a class="navbar-brand logo-mobile" href="<?php bloginfo('url')?>">
						<img src="<?php bloginfo('stylesheet_directory');?>/images/logo_zoi.png" alt="Zoi Venezuela" width="50">
					</a>
				</div>
				<div class="cart mini-cart">
					<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="Ver Carrito de Compras">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/img_cart.png" width="32" height="32" alt="car-shop" />
						<span class="cart-contents"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
					</a>
				</div>
			</div>
		  </div><!-- /.container-fluid -->
		</nav>	
		<script type="text/javascript">
		$(document).ready(function(){
			$(this).on("click","#boton_menu_mobile",function(){

				$(".espaciable").toggleClass("menu_espacio_menu_responsive");
			});
		});
		</script>

		
<?php }

