<?php

// Register primary nav menu
add_action( 'after_setup_theme', 'wpt_setup' );
if ( ! function_exists( 'wpt_setup' ) ):
	function wpt_setup() {
		register_nav_menu( 'primary', __( 'Primary navigation', 'wptuts' ) );
} endif;

/**
 * Register custom menu nav locations
 * @return void
 */
function tarful_register_menus() {
  register_nav_menus(
	array(
		'credits-1' => __( 'Footer menu' )
	)
  );
}
add_action( 'init', 'tarful_register_menus' );

/**
 * Remove parent sidebars 
 * @return void
 */
function remove_sidebars() {
  unregister_sidebar( 'sidebar' ); // primary on left
}
add_action( 'widgets_init', 'remove_sidebars', 11 );
