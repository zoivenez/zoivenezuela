<?php
/********************* Product custom fields *********************/

/**
 * Hooks to build the new custom fields
 */
//Display Fields
add_action( 'woocommerce_product_after_variable_attributes', 'variable_fields', 10, 3 );
//JS to add fields for new variations
add_action( 'woocommerce_product_after_variable_attributes_js', 'variable_fields_js' );
//Save variation fields
add_action( 'woocommerce_process_product_meta_variable', 'save_variable_fields', 10, 1 );


/**
 * Create new fields for variations
 * @param integer $loop loop position
 * @param object $variation_data
 * @param object $variation
 * @return void
 */
function variable_fields( $loop, $variation_data, $variation ) {

    $week_days = array('lunes', 'martes', 'miércoles', 
                       'jueves', 'viernes', 'sábado', 'domingo'); 
?>
<p class="form-row form-row-first"><strong>Días reservables:</strong></p>
<?php
    foreach($week_days as $index => $day) {
        woocommerce_wp_checkbox( 
            array( 
                'id'            => 'variable_booking_day['.$loop.']['.$day.']', 
                'label'         => __(ucfirst($day).' ', 'woocommerce' ), 
                'wrapper_class' => 'form-row form-row-first',
                'desc_tip'      => true,
                'description'   => __( 'Seleccionar sólo si este plan puede reservarse para un día '.$day, 'woocommerce' ),
                'value'         => get_post_meta( $variation->ID, 'variable_booking_day['.$loop.']['.$day.']', true ), 
            )
        );	
    }
?>
<?php
}

/**
 * Create new fields for new variations
 * @return void
 */
function variable_fields_js() {
    $week_days = array('lunes', 'martes', 'miércoles', 
        'jueves', 'viernes', 'sábado', 'domingo'); 
?>
    <tr>
        <td>
            <p><strong>Días reservables:</strong></p>

<?php
    foreach($week_days as $index => $day) {
        woocommerce_wp_checkbox( 
            array( 
                'id'            => 'variable_booking_day[ + loop + ]['.$day.']', 
                'label'         => __(ucfirst($day).' ', 'woocommerce' ), 
                'desc_tip'      => true,
                'description'   => __( 'Seleccionar sólo si este plan puede reservarse para un día '.$day, 'woocommerce' ),
                'value'         => ''
            )
        );	
    }
?>
    <tr>
        <td>
<?php
    // Plan duration
    woocommerce_wp_text_input( 
        array( 
            'id'          => 'duration[ + loop + ]', 
            'label'       => __( 'Duración (en días)', 'woocommerce' ), 
            'desc_tip'    => 'true',
            'description' => __( 'Duración de la experiencia en número de días.', 'woocommerce' ),
            'value'       => '',
        )
    );
?>
        </td>
    </tr>
    <tr>
        <td>
<?php
    // Plan duration
    woocommerce_wp_text_input( 
        array( 
            'id'          => 'people[ + loop + ]', 
            'label'       => __( 'Número de personas', 'woocommerce' ), 
            'desc_tip'    => 'true',
            'description' => __( 'Mínimo número de personas para la experiencia.', 'woocommerce' ),
            'value'       => ''
        )
    );
?>
        </td>
    </tr>


        </td>
    </tr>
<?php
}


/**
 * Save new fields for variations
 * @param integer $post_id
 * @return void
 */
function save_variable_fields( $post_id ) {
    if (isset( $_POST['variable_sku'] ) )  {
        $variable_sku          = $_POST['variable_sku'];
        $variable_post_id      = $_POST['variable_post_id'];
    }
    // Checkbox
    $variable_booking_day = $_POST['variable_booking_day'];
    // Days of the week
    $week_days = array('lunes', 'martes', 'miércoles', 
        'jueves', 'viernes', 'sábado', 'domingo'); 

    for ( $i = 0; $i < sizeof( $variable_sku ); $i++ ) {
        $variation_id = (int) $variable_post_id[$i];

        // Foreach variation, check the weekdays
        foreach($week_days as $day) {

            if ( isset( $variable_booking_day[$i][$day] ) ) {
                update_post_meta( $variation_id, 'variable_booking_day['.$i.']['.$day.']', stripslashes( $variable_booking_day[$i][$day] ) );
            } else {
                update_post_meta( $variation_id, 'variable_booking_day['.$i.']['.$day.']', "no" );
            }
        }
    }

    $duration = $_POST['duration'];

    for ( $i = 0; $i < sizeof( $variable_sku ); $i++ ) {
        $variation_id = (int) $variable_post_id[$i];
        if ( isset($duration[$i]) ) {
            update_post_meta( $variation_id, 'duration', stripslashes( $duration[$i] ) );
        } else {
            delete_post_meta( $variation_id, 'duration' );
        }
    }

    $people = $_POST['people'];

    for ( $i = 0; $i < sizeof( $variable_sku ); $i++ ) {
        $variation_id = (int) $variable_post_id[$i];
        if ( isset($people[$i]) ) {
            update_post_meta( $variation_id, 'people', stripslashes( $people[$i] ) );
        } else {
            delete_post_meta( $variation_id, 'people' );
        }
    }
}

// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );

/**
 * Adds custom fields to product
 * @return void; 
 */
function woo_add_custom_general_field() {
    global $woocommerce, $post;

    echo '<div class="options_group">';
    woocommerce_wp_text_input(
        array(
            'id' => 'experience_global_duration',
            'label' => _('Duración', 'woocommerce'), 
            'placeholder' => '12',
            'desc_tip' => 'true', 
            'description' => __('Duración de la experiencia en días. Cualquier duración en las variaciones sobreescribe esta duración') 
        )
    );
    echo '</div>'; 
}

function woo_related_products_limit() {
  global $product;
  
  $args['posts_per_page'] = 6;
  return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {

  $args['posts_per_page'] = 4; // 4 related products
  $args['columns'] = 4; // arranged in 2 columns
  return $args;
}

/**
 * Displays a list of product terms 
 * @param string $taxonomy string with taxonomy to search for
 * @return void
 */
function display_product_terms($taxonomy, $number) {
    global $wpdb, $woocommerce;

    // Args to get taxonomies in ascendant order
    // hide empty taxonomies (without posts)
    // and only 5 taxonomies
    $args = array(
        'order' => 'ASC',
        'hide_empty' => 1,
        'number' => $number
    );

    $categories = get_terms($taxonomy, $args);

    foreach($categories as $category) { 	
        $cat_id = $category->term_id;
        $cat_name = $category->name;
        $cat_slug = $category->slug;

        $term = get_term_by( 'slug', $cat_slug, $taxonomy );
        // Get # published objects from term
        $st = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts p
                                        INNER JOIN $wpdb->term_relationships tr
                                        ON (p.ID = tr.object_id)
                                        INNER JOIN $wpdb->term_taxonomy tt
                                        ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                                        WHERE
                                        p.post_status = 'publish'
                                        AND tt.taxonomy = %s
                                        AND tt.term_id = %d;",
        $taxonomy,
        $term->term_id);
        $cat_count_post_published = $wpdb->get_var($st);	

        $cat_link_specific = get_term_link($category);

        // If there's at least a product published with that term
        if ($cat_count_post_published > 0) { 
?>
            <li> <a href="<?php echo $cat_link_specific; ?>"> <?php echo $cat_name; ?> </a> </li>
<?php  
        }
    } 
}

/**
 * Get all product terms
 * @param string $taxonomy string term taxonomy
 * @param ineger $number int number of terms to return (-1 for all)
 * @return array of string with term slug or false if none is found
 */
function get_product_attributes($taxonomy, $number, $hide_empty) {
    global $wpdb, $woocommerce;

    // Args to get taxonomies in ascendant order
    // hide empty taxonomies (without posts)
    $args = array(
        'order' => 'ASC',
        'hide_empty' => $hide_empty,
    );

    if ($number > 0) {
        $args['number'] = $number; 
    }

    $categories_array = array(); 
    $categories = get_terms($taxonomy, $args);

    foreach($categories as $category) { 	
        $cat_id = $category->term_id;
        $cat_name = $category->name;
        $cat_slug = $category->slug;

        $term = get_term_by( 'slug', $cat_slug, $taxonomy );
        // Get # published objects from term
        $st = $wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts p
                                        INNER JOIN $wpdb->term_relationships tr
                                        ON (p.ID = tr.object_id)
                                        INNER JOIN $wpdb->term_taxonomy tt
                                        ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
                                        WHERE
                                        p.post_status = 'publish'
                                        AND tt.taxonomy = %s
                                        AND tt.term_id = %d;",
        $taxonomy,
        $term->term_id);
        $cat_count_post_published = $wpdb->get_var($st);	

        $cat_link_specific = get_term_link($category);

        // If there's at least a product published with that term
        if ($cat_count_post_published > 0) { 
            $categories_array[] = array(
                                    'slug' => $cat_slug,  
                                    'name' => $cat_name
                                ); 
        }
    } 

    return $categories_array; 
}
