<?php 
/**
 * Custom Twitter Share Buttom
 * @param array $atributos default array()
 * @return void
 */
function tarful_twitter_share($atributos = array()) {
?>
	<script type="text/javascript">
		
	// We bind a new event to our link
	$(document).ready(function(){
		$('a.tweet').click(function(e){

			//We tell our browser not to follow that link
			e.preventDefault();

			//We get the URL of the link
			var loc = $(this).attr('href');

			//We get the title of the link
			var title  = encodeURIComponent($(this).attr('title'));

			//We trigger a new window with the Twitter dialog, in the middle of the page
			window.open('http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

		});
	});
	</script>
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?> #zoivenezuela" class="btn btn_avalaible tw_btn tweet <?php echo array_key_exists('class', $atributos) ? $atributos["class"] : ''; ?>" target="_blank">COMPARTIR</a>

<?php	
}

/**
 * Custom Facebook Share Buttom
 * @param integer $post_id
 * @param string $content
 * @param array atributos default to array()
 * @return void
 */
function tarful_facebook_share($post_id, $content, $atributos = array()) {
	
	$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
	$title= urlencode(get_the_title($post_id));
	$url= urlencode(get_permalink($post_id));
	$summary= urlencode($content);
	$image= urlencode($feat_image_url);
	?>
	<a class="btn btn_avalaible fb_btn fb_share <?php echo array_key_exists('class', $atributos) ? $atributos["class"] : ''; ?>" onclick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[summary]=<?php echo $summary;?>&amp;p[url]=<?php echo $url; ?>&amp;&p[images][0]=<?php echo $image;?>', 'sharer', 'toolbar=0,status=0,width=620,height=280');" href="javascript: void(0)"> 
	COMPARTIR
	</a>
<?php
}
