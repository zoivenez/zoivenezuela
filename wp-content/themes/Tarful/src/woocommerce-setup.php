<?php

add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {
	 $currencies['VEF'] = __( 'Bolivar Fuerte - Venezuela ( Bs)', 'woocommerce' );
	 return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

/**
 * Add currency / symbol
 * @param string $currency_symbol
 * @param string $currency
 * @return $currency_symbol
 */
function add_my_currency_symbol( $currency_symbol, $currency ) {
	 switch( $currency ) {
		  case 'VEF': $currency_symbol = '<span class="money-currency-symbol"> Bs. </span>'; break;
	 }
	 return $currency_symbol;
}


add_action( 'init', 'create_place_tax' );

/**
 * Creates destination taxonomy to add experience destinations
 * @return void
 */
function create_place_tax() {
  register_taxonomy(
	'localidad',
	'product',
	array(
	  'label' => __( 'Destinos' ),
	  'rewrite' => array( 'slug' => 'destino' ),
	  'hierarchical' => true,
	)
  );
}

/******** CHECKOUT SETUP ************/
require_once('woocommerce-checkout.php'); 

/**
 * Send Sent payment support notificacion via email
 * @param integer $order_number
 * @param integer $user_id
 * @return void
 */
function tarful_send_payment_support($order_number, $user_id) {
		
	if (isset($_POST['submit'])) {

		$tr_style = 'border:1px solid #FFFFFF';
		$admin_email = get_option('admin_email');

		$user_info = get_userdata($user_id);
		$user_firstname = $user_info->first_name;
		$user_lastname = $user_info->last_name;
		$user_fullname = $user_firstname . ' ' . $user_lastname;
		$user_email = $user_info->user_email;
		$user_order_number = $order_number;

		// Get visitor's IP
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		// Get information related to visitor, location, state, city, country.
		$location_data = array();
		$location_data = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		
		$continent_code = $location_data["geoplugin_continentCode"];
		$country_code = $location_data["geoplugin_countryCode"];
		$latitude = $location_data["geoplugin_latitude"];
		$longitude = $location_data["geoplugin_longitude"];
		$city = $location_data["geoplugin_city"];
		$state = $location_data["geoplugin_regionName"];
		$country = $location_data["geoplugin_countryName"];


		// Email to admin 
		$subject = '[ZOI Venezuela] Recibido Soporte de Pago del pedido: '.$user_order_number.'';	
		$message = '<html><body><br />';
		$message .= '<table border="0" cellpadding="10" width="600px">';
		$message .= '<tr style="'.$tr_style.'"><td>Se ha recibido un nuevo soporte de pago desde plataforma:</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Cliente: '.$user_fullname.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Email: '.$user_email.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Número de Pedido: '.$user_order_number.' - <a href="'.get_bloginfo('url').'/wp-admin/post.php?post='.$user_order_number.'&action=edit">ver detalle</a></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>-------</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Dirección IP: '.$ip.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Ciudad: '.$city.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Estado: '.$state.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>País: '.$country.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Enviado desde el Portal de ZOI Venezuela'.get_bloginfo('url').'</td></tr>';
		$message .= '</table>';
		$message .= '</body></html>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $admin_email, $subject, $message );


		// Email to user 
		$subject_user = '[ZOI Venezuela] Recibimos tu soporte de pago para el Pedido: '.$user_order_number.'';	
		$message_user = '<html><body><br />';
		$message_user .= '<table border="0" cellpadding="10" width="600px">';
		$message_user .= '<tr style="'.$tr_style.'"><td>Hola '.$user_firstname.', Hemos recibido tu soporte de pago satisfactoriamente.</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td> Estaremos revisando el documento y actualizaremos tu pedido. Puedes revisar el estado de tu orden desde tu cuenta.</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td><a href="'.wc_get_endpoint_url('ver-orden').$user_order_number.'">Ver detalle de pedido '.$user_order_number.'</a></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>Equipo ZOI Venezuela</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td><a href="'.get_bloginfo('url').'">zoivenezuela.com - Vive la Aventura</a></td></tr>';
		$message_user .= '</table>';
		$message_user .= '</body></html>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $user_email, $subject_user, $message_user );
	}

}
