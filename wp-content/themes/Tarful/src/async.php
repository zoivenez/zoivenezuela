<?php
/**
 * Cleans up a single experience (remove old dates)
 * @param integer $experience_id 
 * @return void
 */
function clean_up_experience($experience_id) {

    $dates = wp_get_post_terms( $experience_id, 'pa_fechas-de-experiencia' ); 
    $today = strtotime('today UTC');

    foreach($dates as $date) {
        // Get parsed date and continue if false
        $experience_date = parse_spanish_date($date->name); 
        if (!$experience_date) continue; 

        // Past date, then remove term
        if ($experience_date < $today) {
            wp_remove_object_terms($experience_id, $date->term_id, 'pa_fechas-de-experiencia'); 
        }
    }
}

/**
 * Removes terms pa_fechas-de-experiencia from all experiences that are less than today date
 * @return void
 */
function clean_up_experience_database() {

    $experiences_to_clean = get_experiences_with_past_dates()['result']; 

    // Iterate to clean up experiences
    foreach($experiences_to_clean as $experience) {
        $id = $experience['id']; 
        clean_up_experience($experience['id']); 
    }
}

/**
 * Gets all experiences without dates
 * @return array of integer with experience IDs
 */
function get_experiences_without_dates() {
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
    );

    $query   = new WP_Query($args);
    $results = $query->get_posts();

    $experiences = array(); 
    foreach($results as $result) {
        $dates = wp_get_object_terms($result->ID, 'pa_fechas-de-experiencia'); 

        if (empty($dates)) {
            $experiences[] = $result->ID; 
            add_post_meta($result->ID, 'nodate', true); 
        } else {
            delete_post_meta($result->ID, 'nodate'); 
        }
    }

    return $experiences; 
}

/**
 * Removes terms pa_fechas-de-experiencia from database that are less than today date
 * @param boolean $send_notifications default true
 * @return integer $number_of_experiences_without_date
 */
function clean_up_date_database($send_notifications = true) {

    $taxonomy = 'pa_fechas-de-experiencia';
    $today    = strtotime('today UTC');

    // Want all terms, event hidden ones
    $args     = array(
        'hide_empty' => false
    );

    $dates         = get_terms($taxonomy, $args);

    foreach($dates as $date) {
        $date_object = parse_spanish_date($date->name); 

        // Not a date, continue
        if (!$date_object) { continue; }

        // Remove old dates
        if ($date_object < $today) {
            wp_delete_term($date->term_id, $taxonomy);
        }
    }

    // Clean terms cache so the change is right away
    clean_term_cache(array(), $taxonomy, true); 

    if ($send_notifications) {

        $experiences_without_dates = get_experiences_without_dates(); 
        $number_of_experiences_without_dates = 0; 

        // Send email to admin notifying experiences without date
        foreach($experiences_without_dates as $exp) {
            $post_status = get_post_status($exp); 
            // Send Email only once
            if ($post_status === 'publish' && get_post_meta($exp, 'nodate', true)) {
                send_no_date_experience_email($exp); 
                ++$number_of_experiences_without_dates;
            }
        }

        return $number_of_experiences_without_dates; 
    } else {
        return 0; 
    }
}

/**
 * Helper function to handle AJAX request to clean up database
 * It's called by wp_ajax_clean_up_database_helper
 * @return void
 */
function clean_up_date_database_helper() {
    echo clean_up_date_database(); 
    wp_die(); 
}

add_action('wp_ajax_clean_up_date_database_helper', 'clean_up_date_database_helper');

/** 
 * Function that shows a button to trigger clean database action
 * It's added by the function async_add_meta_box with the wp helper
 * wp_add_dashboard_widget
 * @return void
 */
function add_meta_box_clean_database() { ?>

    <div style="text-align:center;">
        <a href="#" class="button-primary">Eliminar fechas de experiencia</a>
	</div>

<script type="text/javascript">
    jQuery(function( $ ) {
        $cleanDBLink = $('#clean-db').find('a').first(); 


        $cleanDBLink.click(function(event) {
            event.preventDefault(); 
            var ajaxUrl = "<?=admin_url('admin-ajax.php')?>";
                    
                    // Ajax call to server
            $.ajax({
                'type' : 'post',
                'url'  : "<?=admin_url('admin-ajax.php')?>",
                'data' : {
                    'action' : 'clean_up_date_database_helper',
                },

                beforeSend: function(data, textStatus, jqXHR ){
                },

                success: function(data, textStatus, jqXHR ){
                    alert("Fechas borradas!"); 
                },

                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('Error: ' + errorThrown);
                    console.log('Status code: ' + textStatus);
                },

                complete: function(data, dataStatus, jqXHR) {
                    alert(data.responseText + " experiencias no tienen fechas"); 
                }
            }); 
                      
        });
    });
</script>
<?php }

/**
 * Function that adds the clean db button to wp action
 * It's called by the hook wp_dashboard_setup
 * @return void
 */
function async_add_meta_box() {
	wp_add_dashboard_widget('clean-db','Eliminar fechas pasadas de experiencia','add_meta_box_clean_database');
}

add_action( 'wp_dashboard_setup', 'async_add_meta_box' );
//add_action( 'wp', 'prefixsetupschedule' );

/**
 * On an early action hook, check if the hook is scheduled - if not, schedule it.
 * It's called by the hook prefixdailyevent (XXX: Unabled for now)
 * @return void
 */
function prefixsetupschedule() {

    if ( ! wp_next_scheduled( 'prefixdailyevent' ) ) {
        wp_schedule_event(time(), 'daily', 'prefixdailyevent');
    } 
}

//add_action( 'prefixdailyevent', 'clean_up_date_database' );
