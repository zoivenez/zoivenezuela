<?php

/**
 * Shows select input guru profile 
 * @return void
 */
function relacion_perfil_guru_input(){
	$guru = get_post_meta ( get_the_ID(), 'guru', true );
	?><select name="id_guru"><?php
		?> <option value=""> - </option> <?php
	foreach (get_users(array('role' => 'guru')) as $user) {
		?><option id="option-guru-<?php echo $user->ID; ?>" value="<?php echo $user->ID; ?>" <?php if($guru == $user->ID){ echo 'selected=true'; } ?> ><?php echo $user->user_login; ?></option><?php
	}
	?></select>
	<?php
}

/**
 * Adds a metabox in the guru profile with a guru's select
 * @return void
 */
function relacion_perfil_guru() {

	add_meta_box('perfil-guru','Gurú','relacion_perfil_guru_input','post_perfil','normal','default');
}
add_action( 'add_meta_boxes_post_perfil', 'relacion_perfil_guru', 10, 1);

/**
 * Adds a box to the main column on the Post and Page edit screens.
 * Hooked to add_meta_boxes_product action
 * @return void
 */
function myplugin_add_meta_box() {

	add_meta_box('users-data','Total de usuarios registrados','myplugin_meta_box_callback','product','side','high',array());
	add_meta_box('perfil-guru','Gurú','relacion_perfil_guru_input','product','side','core');

}
add_action( 'add_meta_boxes_product', 'myplugin_add_meta_box', 10, 1);

/**
 * Saves the relation between a guru and its post
 * Hooked to save_post action
 * @param integer $post_id
 * @return void
 */
function save_meta_relacion_perfil_guru( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	// Update the meta field in the database.
	delete_post_meta( $post_id, 'guru' );

	// Make sure that it is set.
	if ( ! isset( $_POST['id_guru'] ) || !( $_POST['id_guru'] ) ) {
		return;
	}

	/* OK, it's safe for us to save the data now. */
	// Sanitize user input.
	$my_data = sanitize_text_field( $_POST['id_guru'] );

	add_post_meta( $post_id, 'guru', $my_data );
}
add_action( 'save_post', 'save_meta_relacion_perfil_guru' );

/**
 * Adds custom font to admin views
 * Hooked to action admin_head
 * @return void
 */
function my_custom_fonts() {
	?>
	<style>
	#poststuff #users-data .inside {
		margin: 0;
		padding: 0;
	}
	#perfil-guru .inside{
		text-align: center;
	}
	</style>
	<?php
}

add_action('admin_head', 'my_custom_fonts');

add_action( 'init', 'tarful_post_type_perfil_guru' );

/** 
 * Create custom post type guru
 * Hooked to action init and calling register_post_type helper function
 * @retun void
 */
function tarful_post_type_perfil_guru() {
	register_post_type( 'post_perfil',
		array(
			'labels' => array(
				'name' => __('Perfil Gurú'),
				'singular_name' => __('Perfil Gurú'),
				'add_new' => __('Agregar Nuevo'),
				'add_new_item' => __('Agregar nuevo perfil'),
				'edit_item' => __('Editar perfil'),
				'new_item' => __('Nuevo perfil'),
				'all_items' => __('Todos los perfiles'),
				'view_item' => __('Ver perfil'),
				'search_items' => __('Buscar perfil'),
				'not_found' =>  __('Ningún perfil encontrado'),
				'not_found_in_trash' => __('Ningún perfil en papelera'), 
				'parent_item_colon' => '',
				'menu_name' => 'Perfiles Gurús'
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'query_var' => true,
			'rewrite' => false,
			'capability_type' => 'post',
			'has_archive' => true, 
			'hierarchical' => false,
			'menu_position' => 5,
			'supports' => array( 'title', 'editor', 'revisions','thumbnail','category'),
			'taxonomies' => array('category')

		)
	);
}

/* Function: remove_guru_from_posts
 * Description: Removes a user guru from the posts meta 
 * Hooked to action delete_user
 * @param integer $user_id 
 * @return void
 */
function remove_guru_from_posts( $user_id ) {

    define('GURU_ROL', 'Gurú');  // Just in case it changes

    $user = get_userdata( $user_id ); 

    if ( property_exists( $user, "caps" ) ) {
        //Check whether the user is Gurú or not
        if ($user->caps[GURU_ROL]) {
            $offset = 0; 
            $posts = get_posts(array(
                'post_type' => 'product',
                'offset' => $offset
            ));

            while (!empty($posts)) {
                foreach($posts as $post) {
                    delete_post_meta( $post->ID, 'guru', $user_id); 
                }
                $offset += 5;
                $posts = get_posts(array(
                    'post_type' => 'product',
                    'offset' => $offset
                ));
            }
        }
    } elseif ( property_exists( $user, "role" ) ) {
        if ($user->role == GURU_ROL) {
            echo "hasdiojasodjaosjd"; 
        }
    }
}

add_action('delete_user', 'remove_guru_from_posts'); 

// Asociatate custom post type with a permalink

global $wp_rewrite;
$wp_rewrite->add_rewrite_tag("%post_perfil%", '([^/]+)', "post_perfil=");
$wp_rewrite->add_permastruct('post_perfil', 'perfil/%post_perfil%/', false);

//var_dump(get_editable_roles());
add_action('woocommerce_before_main_content','tarful_products_of_gurus');

/**
 * Adds query by guru checking $_GET variable
 * @return void
 */
function tarful_products_of_gurus(){
	if(isset($_GET["by_guru"])){
		query_posts(array('post_type' => 'product', 'meta_query' => array( array( 'key' => 'guru_user', 'value' => 41, 'compare' => '=') ) ));
	}
}

/**
 * Adds guru box to products
 * @return void
 */
function tarful_box_guru(){ ?> 
<script>
	$(document).ready(function(){

		$('.product').each(function(){

			var img_objetc = $(this).find("img");

			var price_object = $(this).find(".avalaible");

			var guru_object = $(this).find(".box_guru_grid");

			if ( (price_object.outerWidth() + guru_object.outerWidth()) > img_objetc.outerWidth() ) {

				if((img_objetc.outerWidth() - price_object.outerWidth()) < 40 ){

					guru_object.addClass("separate_box");
					$(this).find(".guru_info h5").addClass("separate_name");

				}else {
					$(this).find(".guru_info h5").addClass("separate_name");
				}
			};
		});
	});
</script> 
<?php  }
