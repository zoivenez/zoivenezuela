<?php

/** 
 * Form with new experience modal
 * @return void
 */
function form_create_experience(){ ?>
    <div id="new-experience-modal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <center>
                        <h4><img src="<?php bloginfo('stylesheet_directory'); ?>/images/title_create_experience.png" alt="crea tu experiencia" width="250" height="80" /></h4>
                    </center>
                    <form id="experience-form" action="#" method="post" role="form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="category">¿Qué quieres hacer?</label>
                                    <input class="required" type="text" id="category" name="category" value="" placeholder="Rafting, Trekking, etc" tabindex="2">
                                </div>
                                <div class="form-group">
                                    <label for="checkin">Fecha Inicio</label>
                                    <input class="required date" type="text" id="checkin" name="checkin" value="" placeholder="dd/mm/yyyy" tabindex="3">
                                </div>
                                <div class="form-group">
                                    <label for="checkout">Fecha Fin</label>
                                    <input class="required date" type="text" id="checkout" name="checkout" value="" placeholder="dd/mm/yyyy" tabindex="3">
                                </div>
                            </div> <!-- /.col-md-6 -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="location">¿Dónde lo quieres hacer?</label>
                                    <input class="required" type="text" id="location" name="location" value="" placeholder="Miranda, Mérida, etc" tabindex="1">
                                </div>
                                <div class="form-group">
                                    <label for="budget">Presupuesto por persona</label>
                                    <input class="required" type="text" id="budget" name="budget" value="" placeholder="5000 Bs." tabindex="4">
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo</label>
                                    <input class="required" type="email" id="email" name="email" value="" placeholder="juan@ejemplo.com" tabindex="5">
                                </div>
                            </div> <!-- /.col-md-6 -->
                        </div> <!-- /.row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Número de contacto</label>
                                    <input class="required" type="text" id="budget" name="phone" value="" placeholder="0XXX XXX XXXX" tabindex="4">
                                </div>
                            </div> <!-- /.col-md-6 -->
                        </div> <!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="description">¿Hay algo más que debamos saber?</label>
                                    <textarea class="required" id="description" name="description" tabindex="6" placeholder="&quot;Quiero algo relax para hacer con mi familia&quot;, &quot;Quiero tener contacto con los locales del sitio&quot;, &quot;Un plan romántico con mi pareja&quot;, etc"></textarea>
                                </div>
                            </div>
                        </div> <!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                            <button type="submit" class="btn btn-danger btn-zoi">Enviar</button>
                            <input type="hidden" name="form-experience" id="form-experience" value="true" />
                            </div>
                        </div> <!-- /.row -->
                    </form>
                </div> <!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
}


/**
 * Add box and form of "Create Experience" on experiences (products) list 
 * @return void
 */
function tarful_box_create_experience() {?>
    <li class="product type-product status-publish has-post-thumbnail product-type-create-experience">
		<a id="create_experiencie">
			<div class="product product-grid">
				<img class="attachment-small wp-post-image" width="370" height="370" alt="Crea tu experiencia" src="<?php echo CFS()->get('imagen_para_listado', 4); ?>">	 
			</div>
		</a>
	
		<div class="overlay-experience" style="display: none;">
			<div>
				<div id="tab-buttons-container" class='tab-container'>
				</div>
			</div>
		</div>

	</li>
    
<?php
}


