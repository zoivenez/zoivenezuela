<?php // ZOI 
/**
 * Function to create the html inside the meta box that will contain the datepickers.
 * @return void
 */
function sales_report_content_meta_box() {

    ?>
    
    <div style="text-align:center;">
        <div style="margin:10px;"> Exportar reporte de ventas segun rango de fechas</div>
        <form class="post-box">
            <div id="div-form" class="form-group">
                <p>Desde: <input type="text" id="from" class="datepicker"></p>
                <br>
                <p>Hasta: <input type="text" id="to" class="datepicker"></p>
                <br>
                <label for="checkbox_por_experiencia">Desea usar este rango para busquedas por fecha de experiencia ?</label>
                <input type='checkbox' name='searchByExperience' id='searchByExperience' value="true" />
                <input type="submit" id="exportSalesButton" class="button-primary" data-url="<?php echo get_bloginfo('url'); ?>" value="Exportar Reporte Ventas" ></input>
            </div>
        </form>
    </div>


    <?php
}

/**
 * Adds the meta box containing the datapickers to the admin dashboard.
 * Hooked to action wp_dashboard_setup
 * @return void
 */
function add_dashboard_sales_report_meta_box() {

    wp_add_dashboard_widget(
                 'example_dashboard_widget',         // Widget slug.
                 'Exportar Reporte de Ventas',         // Title.
                 'sales_report_content_meta_box' // Display function.
        );
}
add_action( 'wp_dashboard_setup', 'add_dashboard_sales_report_meta_box' );


/**
 * Adds the jquery datepicker and field date javascript control to the admin dashboard.
 * Hooked to action admin_enqueue_scripts
 * @return void
 */
function admin_javascript(){
   
        // datepicker and bootstrap
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script( 'bootstrapjs', get_bloginfo('stylesheet_directory').'/bootstrap/js/bootstrap.min.js' );
        // Local page gui javascript
        wp_enqueue_script('sales-report-gui', get_bloginfo('stylesheet_directory') . '/js/Field_Date.js', null, null, null);
   
}
add_action('admin_enqueue_scripts', 'admin_javascript');

/**
 * Gets order by date range. If $searchByExperienceDate is true, then the orders
 * are filtered by experiences date, otherwise by creation date
 * @param date $fromDate date from which search will start
 * @param date $toDate date to which search will end.
 * @param boolean $searchByExperienceDate will determine wheter search will be done by experience date or by order date range.
 * @return array $orders
 */
function get_orders_by_date_range($fromDate, $toDate, $searchByExperienceDate, $all = false) {


    if ($toDate < $fromDate) 
        return array('success' => 0, 'error' => 'Empty range');
    else 
    {
        /*if(!class_exists(WC_Order)){
            require_once(dirname(__FILE__) . '/../../plugins/woocommerce/includes/abstracts/abstract-wc-order.php');
            require_once(dirname(__FILE__) . '/../../plugins/woocommerce/includes/class-wc-order.php');
        }*/

        $customer_orders = get_posts( array(
                'post_type'   => 'shop_order',
                'post_status' => array(''),
                'numberposts' => -1
            ));

        // Gets the string that identifies whether to search by experience or by order date range
        if($searchByExperienceDate == "false"){
            $searchByExperienceDate = false;
        }
        else{
            $searchByExperienceDate = true;
        }

        $xls_order_information = array();
        foreach ($customer_orders as $customer_order) 
        {
            $order = new WC_Order();
            $order->populate($customer_order);
            
            // decide whether to search by the date of the experience or by date range of orders.
            if(!$searchByExperienceDate){
                
                $parsedDate = strtotime($customer_order->post_date);

                if ( ($parsedDate >= $fromDate && $parsedDate <= $toDate) || $all )
                {   
                    //creates an xls array and pushes into a newly created array to be sent to the xls file.
                    array_push($xls_order_information ,set_xlsarray(new WC_Order($customer_order)));
                }

            } else{
                $exp_date = parse_date_exp(get_post_meta( $order->id, 'experience_date', true ));
                $exp_date = strtotime($exp_date);
                if($exp_date >= $fromDate && $exp_date <= $toDate){
                    //creates an xls array and pushes into a newly created array to be sent to the xls file.
                    array_push($xls_order_information ,set_xlsarray(new WC_Order($customer_order)));
                }
            }
                
        }

        return $xls_order_information;
    }
}

/**
 * Parses a date format dd/mm/yyyy to date format yyyy/dd/mm
 * @param string $dateToParse 
 * @return $formatted_date an empty string if date array is in the wrong format, or a data with yyyy/dd/mm format.
 */
function parse_date_exp($dateToParse){
    $dateArray = explode("/", $dateToParse);
    if(count($dateArray) == 3){
        return $dateArray[2]."/".$dateArray[1]."/".$dateArray[0];
    }
    else{
        return "";
    }
    
}

/**
 * Parses a date format dd/mm/yyyy to date format yyyy/dd/mm
 * @param string $dateToParse
 * @return an empty string if date array is in the wrong format, or a data with yyyy/dd/mm format.
 */
function parse_date_exp_2($dateToParse){
    $dateArray = explode("/", $dateToParse);
    if(count($dateArray) == 3){
        return $dateArray[2]."/".$dateArray[0]."/".$dateArray[1];
    }
    else{
        return "";
    }
    
}

/**
 * Function used to set the variable data that will populate the xls file.
 * @param object $order 
 * @return $order array that contains a formatted order.
 */
function set_xlsarray( $order )
{
    $data = array(); 
    $ordereditems = array();
    $wantedproduct = array();

    $user = get_user_by('id',get_post_meta( $order->id, '_customer_user', true ));
    $items = $order->get_items();

    $data['order_id']       = $order->id;
    $data['key']            = $order->id.'-'.$order->order_key;
    $data['description']    = "Payment for order id ".$order->id;
    $data['first_name']     =  $order->billing_first_name;
    $data['last_name']      = $order->billing_last_name;
    $data['company']        = $order->billing_company;
    $data['address']        = $order->billing_address_1;
    $data['city']           = $order->billing_city;
    $data['zip']            = $order->billing_postcode;
    $data['country']        = $order->billing_country;
    $data['state']          = $order->billing_state;
    $data['email']          = $order->billing_email;
    $data['phone']          = $order->billing_phone;
    $data['payment_method'] = $order->payment_method;
    $data['date_purchase']  = $order->order_date;
    $data['total_paid']     = number_format($order->get_total(), 2, '.', '');
    $data['status']         = $order->post_status;
    $data['username']       = $user->user_login;
    $data['id']             = $user->billing_cirif;
    $data['reserved_date']  = get_post_meta( $order->id, 'experience_date', true );
    
    // formats the real values into understandable information for the user.
    if($data['payment_method'] == 'bacs'){
        $data['payment_method'] = 'TXF';
    }
    if($data['payment_method'] == 'instapago'){
        $data['payment_method'] = 'TDC';
    }
    if($data['reserved_date'] === NULL){
        $data['reserved_date'] = 'reserva-tu-fecha';
    }
    if($data['status'] == 'wc-processing'){
        $data['status'] = 'procesando';
    }
    if($data['status'] == 'wc-cancelled'){
        $data['status'] = 'cancelado';
    }
    if($data['status'] == 'wc-on-hold'){
        $data['status'] = 'en espera';
    }
    if($data['status'] == 'wc-completed'){
        $data['status'] = 'completado';
    }

    // Lookup for month number in spanish
    $month_inv_lookup = array(
        "enero"      => "1",
        "febrero"    => "2",   
        "marzo"      => "3",   
        "abril"      => "4",   
        "mayo"       => "5",   
        "junio"      => "6",   
        "julio"      => "7",   
        "agosto"     => "8",   
        "septiembre" => "9",   
        "octubre"    => "10",  
        "noviembre"  => "11",  
        "diciembre"  => "12"  
    );
    
    // each iteration will assign the values to an array that contains each order obtained form the database.
    foreach($items as $item)    {
        $product                          = new WC_Product($item['product_id']);
        $var                              = new WC_Product($item['variation_id']);
        $ordereditems['product_id']       = $item['product_id'];
        $ordereditems['producto_name']    = $item['name'];
        $ordereditems['tax']              = $item['line_tax'];
        $ordereditems['subtotal']         = $item['line_subtotal'];
        $ordereditems["costo_individual"] = $product->price;
        $ordereditems["qty_bought"]       = $item['qty'];
        $ordereditems["provider"]         = CFS()->get('nombre_aliado',$product->id);
        $ordereditems['duration']         = CFS()->get('dias',$product->id);
        $ordereditems['difficulty']       = get_post_meta( $product->id, "difficulty", true );

        $date = array_key_exists('pa_fechas-de-experiencia', $item) ? $item['pa_fechas-de-experiencia'] : 'No fixed date';
        // 0 -> day
        // 1 -> de
        // 2 -> month 
        // 3 -> de 
        // 4 -> year
        $parts = explode('-', $date);

        // Change date format 
        if ( sizeof($parts) >= 3 && is_numeric($parts[0]) ) {
            $day   = $parts[0];
            $month = $month_inv_lookup[$parts[2]];
            $year  = ( sizeof($parts) === 5 ? $parts[4] : date("Y") );
            $date  = "$day/$month/$year"; 
        } else {
            $date = ucfirst(implode(" ", $parts)); 
        }

        $ordereditems["fixed_date"] = $date;
        $variation_id               = $item['variation_id'];

        //checks if the order contains a variation id that is valid to the system for searching.
        if(count($variation_id) > 0 && $variation_id != false){
            $traveler_id = $item['product_id']. '_' . $variation_id;
            $number_of_clients = get_post_meta( $order->id, "number_of_clients_$traveler_id", true );
            
            $travelers_information_array = get_traveler_information_xls($order, $traveler_id, $number_of_clients);
            $ordereditems['travelers'] = $travelers_information_array;

        }
        
        // pushes the new formatted order into an array
        array_push($wantedproduct, $ordereditems);
        
    }

    $data['items'] = $wantedproduct;

    return $data; 
}

/**
 * Retrieves the corresponding data of the travelers from each order.
 * @param object $order 
 * @param integer $traveler_id 
 * @param integer $number_of_clients 
 * @return $travelers array that correspond to a certain order
 */
function get_traveler_information_xls($order, $traveler_id , $number_of_clients ){
    
    $traveler_info = array();
    $travelers = array();

    for ($k = 0; $k < $number_of_clients; ++$k){
        $product_variation_id = "_$traveler_id"."_$k";

        $traveler_info['traveler_first_name'] = get_post_meta( $order->id, "name$product_variation_id", true);
        $traveler_info['traveler_last_name'] = get_post_meta( $order->id, "lastname$product_variation_id", true);
        $traveler_info['traveler_id'] = get_post_meta( $order->id, "id$product_variation_id", true); 
        $traveler_info['traveler_nationality'] = get_post_meta( $order->id, "nationality$product_variation_id", true); 
        $traveler_info['traveler_email'] = get_post_meta( $order->id, "email$product_variation_id", true); 
        $traveler_info['traveler_phone'] = get_post_meta( $order->id, "phone$product_variation_id", true); 
        $traveler_info['traveler_genre'] = get_post_meta( $order->id, "genre$product_variation_id", true); 
        $traveler_info['traveler_city'] = get_post_meta( $order->id, "city$product_variation_id", true); 
        $traveler_info['traveler_birthdate'] = get_post_meta( $order->id, "birthdate$product_variation_id", true); 
        $traveler_info['traveler_comment'] = get_post_meta( $order->id, "comments$product_variation_id", true);

        //adds the newly obtained traveler to an array
        array_push($travelers, $traveler_info);
    }
    
    return $travelers;
}

/**
 * Function that creates the xls file with the corresponding data asociation.
 * @param array $orderslist array of orders, the array of orders that will be introduced into the xls document.
 * @return void
 */
function createxls($orderslist){
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt    LGPL
 * @version    1.8.0, 2014-03-02
 */

    require_once dirname(__FILE__) . '/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';
    
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("ZOI")
                                 ->setLastModifiedBy("ZOI")
                                 ->setTitle("Office 2007 XLSX Sales and Travelers")
                                 ->setSubject("Office 2007 XLSX Document")
                                 ->setDescription("Document for Office 2007 XLSX, generated using PHP classes.")
                                 ->setKeywords("office 2007 openxml php")
                                 ->setCategory("Test result file");

    // create document header text
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', '# Pedido')
                ->setCellValue('B1', 'Fecha de Compra')
                ->setCellValue('C1', 'ID')
                ->setCellValue('D1', 'Producto')
                ->setCellValue('E1', 'Aliado')
                ->setCellValue('F1', 'Cantidad Paquetes')
                ->setCellValue('G1', 'Status')
                ->setCellValue('H1', 'Fecha Inicio')
                ->setCellValue('I1', 'Tipo Fecha')
                ->setCellValue('J1', 'Duracion')
                ->setCellValue('K1', 'Usuario')
                ->setCellValue('L1', 'Comprador o Viajero')
                ->setCellValue('M1', 'Metodo Pago')
                ->setCellValue('N1', 'Sub-Total')
                ->setCellValue('O1', 'IVA')
                ->setCellValue('P1', 'Monto Total')
                ->setCellValue('Q1', 'Nombre')
                ->setCellValue('R1', 'Apellido')
                ->setCellValue('S1', 'Nacionalidad')
                ->setCellValue('T1', 'Cedula')
                ->setCellValue('U1', 'Correo')
                ->setCellValue('V1', 'Telefono')
                ->setCellValue('W1', 'Fecha Nacimiento')
                ->setCellValue('X1', 'Ciudad')
                ->setCellValue('Y1', 'Direccion')
                ->setCellValue('Z1', 'Comentarios')
                ->setCellValue('AA1', 'Ubicacion')
                ->setCellValue('AB1', 'Dificultad');

    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Contabilidad');

    $ordernumber = 0;
    $row = 2;

    // Fills the corresponding columns with the corresponding data from the list of orders
    // the list orders is an array of array, each is loop by the for each statement
    foreach ($orderslist as $individualorder) {
        $ordernumber++;
        $date = "No date"; 
        foreach ($individualorder['items'] as $boughtproducts) {

            $date = $boughtproducts['fixed_date'];
            if(!empty($individualorder['reserved_date'])){
                    $reserved = "abierta";
            }else{
                    $reserved = "cerrada";
            }
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A".$row, $individualorder['order_id'])
                    ->setCellValue("B".$row, $individualorder['date_purchase'])
                    ->setCellValue("G".$row, $individualorder['status'])
                    ->setCellValue("K".$row, $individualorder['username'])
                    ->setCellValue("L".$row, "Comprador")
                    ->setCellValue("M".$row, $individualorder['payment_method'])
                    ->setCellValue("P".$row, $individualorder['total_paid'])
                    ->setCellValue("Q".$row, $individualorder['first_name'])
                    ->setCellValue("R".$row, $individualorder['last_name'])
                    ->setCellValue("S".$row, array_key_exists('nationality', $individualorder) ? 
                                                                             $individualorder['nationality'] : 'Venezolano')
                    ->setCellValue("T".$row, $individualorder['id'])
                    ->setCellValue("U".$row, $individualorder['email'])
                    ->setCellValue("V".$row, $individualorder['phone'])
                    ->setCellValue("X".$row, $individualorder['city'])
                    ->setCellValue("Y".$row, $individualorder['address'])
                    ->setCellValue("Z".$row, 'N/A')
                    ->setCellValue("AA".$row, $individualorder['state']);

            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("C".$row, $boughtproducts['product_id'])
                        ->setCellValue("D".$row, $boughtproducts['producto_name'])
                        ->setCellValue("E".$row, $boughtproducts['provider'])
                        ->setCellValue("F".$row, $boughtproducts['qty_bought'])
                        ->setCellValue("I".$row, $reserved)
                        ->setCellValue("J".$row, $boughtproducts['duration'])
                        ->setCellValue("N".$row, $boughtproducts['subtotal'])
                        ->setCellValue("O".$row, $boughtproducts['tax'])
                        ->setCellValue("AB".$row, $boughtproducts['difficulty'])
                        ->setCellValue("H".$row,  !empty($individualorder['reserved_date']) ? 
                                            $individualorder['reserved_date'] : $date);            
                        
            foreach ($boughtproducts['travelers'] as $traveler) {
                $row++;
                
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A".$row, $individualorder['order_id'])
                        ->setCellValue("B".$row, $individualorder['date_purchase'])
                        ->setCellValue("C".$row, $boughtproducts['product_id'])
                        ->setCellValue("D".$row, $boughtproducts['producto_name'])
                        ->setCellValue("E".$row, $boughtproducts['provider'])
                        ->setCellValue("F".$row, 'N/A')
                        ->setCellValue("G".$row, $individualorder['status'])
                        ->setCellValue("H".$row, 'N/A')
                        ->setCellValue("I".$row, 'N/A')
                        ->setCellValue("J".$row, $boughtproducts['duration'])
                        ->setCellValue("K".$row, "N/A")
                        ->setCellValue("L".$row, "Viajero")
                        ->setCellValue("M".$row, $individualorder['payment_method'])
                        ->setCellValue("N".$row, 'N/A')
                        ->setCellValue("O".$row, 'N/A')
                        ->setCellValue("P".$row, 'N/A')
                        ->setCellValue("Q".$row, $traveler['traveler_first_name'])
                        ->setCellValue("R".$row, $traveler['traveler_last_name'])
                        ->setCellValue("S".$row, $traveler['traveler_nationality'])
                        ->setCellValue("T".$row, $traveler['traveler_id'])
                        ->setCellValue("U".$row, $traveler['traveler_email'])
                        ->setCellValue("V".$row, $traveler['traveler_phone'])
                        ->setCellValue("W".$row, $traveler['traveler_birthdate'])
                        ->setCellValue("X".$row, $traveler['traveler_city'])
                        ->setCellValue("Y".$row, 'N/A')
                        ->setCellValue("Z".$row, $traveler['traveler_comment'])
                        ->setCellValue("AA".$row, 'N/A')
                        ->setCellValue("AB".$row, $boughtproducts['difficulty']);
            }
        }
        
    $row++;
    }

    foreach($objPHPExcel->getActiveSheet()->getColumnDimension() as $col) {
        $col->setAutoSize(true);
    }
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Reporte-Ventas.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    ob_end_clean();
    $objWriter->save('php://output');
    exit;
}

?>
