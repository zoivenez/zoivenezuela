<?php

/**
 * Contact Page form
 * @return void
 */
function tarful_box_contact_form() {
?>
	<?php if (isset($_POST['submit'])) { ?>
		<div class="box_success_message">¡ Muchas Gracias por tu mensaje !</div>
	<?php } ?>

	<form id="contact-form" action="<?php echo tarful_send_contact_form(); ?>"  method="post">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<input type="text" id="fullname" name="fullname" value="" placeholder="Nombre" tabindex="1">
				<input type="email" id="email" name="email" value="" placeholder="Email" tabindex="2">
				<input type="text" id="phone" name="phone" value="" placeholder="Teléfono" tabindex="3">
				<input type="text" id="type" name="type" value="" placeholder="Tipo de Pregunta" tabindex="4">
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<textarea id="message" name="message" tabindex="6" placeholder="Mensaje"></textarea>
			</div>
			
			<div style="clear:both"></div>
			
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box_send_form">
				<input type="submit" id="submit" name="submit" value="Enviar" tabindex="7">
			</div>
		</div>
	</form>
</div>

<script src="<?php bloginfo('stylesheet_directory'); ?>/js/formvalidator/js/jquery.validate.js"></script> 
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/formvalidator/js/script.js"></script> 

<script>
	$(document).ready(function(){
		$('pre').addClass('prettyprint linenums');
	});
</script> 


<?php
}

/**
 * Action to send contact form via email
 * @return void
 */
function tarful_send_contact_form() {
		
	if (isset($_POST['submit'])) {

		$tr_style = 'border:1px solid #FFFFFF';
		$admin_email = get_option('admin_email');
		
		$user_fullname = $_POST['fullname'];
		$user_email = $_POST['email'];
		$user_phone = $_POST['phone'];
		$user_type = $_POST['type'];
		$user_message = $_POST['message'];

		// Get visitor's IP
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		// Get information related to visitor, location, state, city, country.
		$location_data = array();
		$location_data = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		
		$continent_code = $location_data["geoplugin_continentCode"];
		$country_code = $location_data["geoplugin_countryCode"];
		$latitude = $location_data["geoplugin_latitude"];
		$longitude = $location_data["geoplugin_longitude"];
		$city = $location_data["geoplugin_city"];
		$state = $location_data["geoplugin_regionName"];
		$country = $location_data["geoplugin_countryName"];


		// Email to admin 
		$subject = '[ZOI Venezuela] Contacto desde Sitio Web';	
		$message = '<html><body><br />';
		$message .= '<table border="0" cellpadding="10" width="600px">';
		$message .= '<tr style="'.$tr_style.'"><td>Has recibido un mensaje desde el formulario de contacto</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Nombre: '.$user_fullname.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Email: '.$user_email.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Teléfono: '.$user_phone.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Tipo de Mensaje: '.$user_type.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Mensaje: '.$user_message.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>-------</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Dirección IP: '.$ip.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Ciudad: '.$city.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Estado: '.$state.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>País: '.$country.'</td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message .= '<tr style="'.$tr_style.'"><td>Enviado desde el Formulario de Contacto en'.get_bloginfo('url').'</td></tr>';
		$message .= '</table>';
		$message .= '</body></html>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $admin_email, $subject, $message );


		// Email to user 
		$subject_user = '[ZOI Venezuela] ¡Gracias por contactarnos!';	
		$message_user = '<html><body><br />';
		$message_user .= '<table border="0" cellpadding="10" width="600px">';
		$message_user .= '<tr style="'.$tr_style.'"><td>Hola '.$user_fullname.', Gracias por Contactarnos</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>¡Hemos recibido tu email, vamos a revisarlo y te contactaremos próximamente!</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>Equipo ZOI Venezuela</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td><a href="'.get_bloginfo('url').'">zoivenezuela.com - Vive la Aventura</a></td></tr>';
		$message_user .= '</table>';
		$message_user .= '</body></html>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $user_email, $subject_user, $message_user );
	}

}
