<?php
/**
 * Confirms an email by setting true the metadata
 * @param integer user_id 
 * @param string $token confirmation token to be checked
 * @return void
 */
function confirm_email($user_login, $token) {

    $user = get_user_by('login', $user_login);
    $user_id = $user->ID; 
    $confirmation_meta_key = 'email_confirmed';

    $confirmed = get_user_meta($user_id, $confirmation_meta_key, true); 

    // Empty confirmation means this user does not have a pending confirmation
    if (empty($confirmed)) {
        return false; 
    }
    // If it's confirmed already, do nothing
    if ($confirmed['value']) {
        return true;
    }

    // If the token matches, confirm the user, otherwise, do nothing
    if ($token === $confirmed['token']) {
        delete_user_meta( $user_id, $confirmation_meta_key, $confirmed );
        add_user_meta( $user_id, $confirmation_meta_key, array('value' => true, 'token' => $token) );
        return true;
    } else {
        return false; 
    }
}
