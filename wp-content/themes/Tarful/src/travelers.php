<?php
/**
 * Creates traveler form for specific traveler number and echoes
 * @param object $checkout
 * @param object $args
 * @return void
 */
function traveler_form($checkout, $args) {

    // Won't do anything if product_id or product_places is empty
    if ( !array_key_exists('product_id', $args) || !array_key_exists('variation_id', $args) ) {
        return; 
    }

    $product_id     = $args['product_id'];
    $variation_id = $args['variation_id'];
    $k              = ( array_key_exists('traveler', $args) ? $args['traveler'] : 0 );  

    $traveler_id = $product_id . '_' . $variation_id . '[]';
    $traveler_class = $product_id . '_' . $variation_id;

    echo '<h5 class="fieldset-subtitle"> Viajero #'.($k + 1).'</h5>';  
    echo '<div class="' . $traveler_class . '-traveler-fieldset traveler-fieldset">';

    echo '<div class="row">';
    woocommerce_form_field( "name_$traveler_id", array(
        'type'        => 'text',
        'required'    => true,
        'class'       => array('col-lg-6 col-md-6 name_' . $traveler_class),
        'label'       => __('Name'),
        'placeholder' => __('Name'),
    ), $checkout->get_value("name_$traveler_id"));
    woocommerce_form_field( "lastname_$traveler_id", array(
        'type'        => 'text',
        'required'    => true,
        'class'       => array('col-lg-6 col-md-6 lastname_' . $traveler_class),
        'label'       => __('Last Name', 'woocommerce'),
        'placeholder' => __('Last Name', 'woocommerce'),
    ), $checkout->get_value("lastname_$traveler_id"));
    echo '</div>'; 

    echo '<div class="row traveler-form">'; 
    woocommerce_form_field( "nationality_$traveler_id", array(
        'type'        => 'select',
        'options'     => array(
            'Venezolano' => 'V',
            'Extranjero' => 'E'
        ),
        'required'    => true,
        'class'       => array('col-lg-1 col-md-1 nationality_' . $traveler_class),
        'label'       => 'V/E', 
    ), $checkout->get_value("nationality_$traveler_id"));
    woocommerce_form_field( "id_$traveler_id", array(
        'type'        => 'text',
        'required'    => true,
        'class'       => array('id-traveler-field col-lg-5 col-md-5 input-number-cond id_' . $traveler_class),
        'label'       => __('Cédula de identidad'),
        'placeholder' => __('22222222'),
    ), $checkout->get_value("id_$traveler_id"));
    woocommerce_form_field( "email_$traveler_id", array(
        'type'        => 'text',
        'required'    => true,
        'class'       => array('col-lg-6 col-md-6 input-email email_' . $traveler_class),
        'label'       => __('Email'),
        'placeholder' => __('Email'),
    ), $checkout->get_value("email_$traveler_id"));
    echo "</div>";

    echo '<div class="row traveler-form">';
    woocommerce_form_field( "phone_$traveler_id", array(
        'type'        => 'text',
        'required'    => true,
        'class'       => array('col-lg-6 col-md-6 input-number phone_' . $traveler_class),
        'label'       => __('Phone', 'woocommerce'),
        'placeholder' => __('Phone', 'woocommerce'),
    ), $checkout->get_value("phone_$traveler_id"));
    woocommerce_form_field( "genre_$traveler_id", array(
        'type'        => 'select',
        'required'    => true,
        'class'       => array('form-select col-lg-6 col-md-6 genre_' . $traveler_class),
        'label'       => __('Genre'),
        'options'     => array(
            'masculino' => 'Masculino',
            'femenino'  => 'Femenino'
        ),
    ), $checkout->get_value("genre_$traveler_id"));
    echo "</div>";

    echo '<div class="row">';
    woocommerce_form_field( "city_$traveler_id", array(
        'type'        => 'text',
        'required'    => true,
        'class'       => array('col-lg-6 col-md-6 city_' . $traveler_class),
        'label'       => __('City', 'woocommerce'),
        'placeholder' => __('City', 'woocommerce'),
    ), $checkout->get_value("city_$traveler_id"));
    woocommerce_form_field( "birthdate_$traveler_id", array(
        'type'        => 'text',
        'required'    => true,
        'class'       => array('col-lg-6 col-md-6 input-date birthdate_' . $traveler_class),
        // TODO: Add date of birth translation
        'label'       => __('Fecha de nacimiento'),
        'placeholder' => __('dd/mm/yyyy'),
        'id'          => '',
    ), $checkout->get_value("birthdate_$traveler_id"));

    echo "</div>";
    echo '<div class="row">';
    woocommerce_form_field( "comments_$traveler_id", array(
        'type'        => 'textarea',
        'required'    => false,
        'class'       => array('col-lg-12 col-md-12 comments_' . $traveler_class),
        'label'       => __('Comentarios', 'woocommerce'),
        'placeholder' => __('¿Qué debemos saber de ti? Soy vegetariano, me mareo fácilmente en los botes, etc', 'woocommerce'),
    ), $checkout->get_value("comments_$traveler_id"));
    echo "</div>";

    echo "</div>";
}

add_action('woocommerce_checkout_after_customer_details', 'custom_client_fields'); 
/**
 * Adds custom client fields in the checkout form 
 * @param object checkout
 * @return void
 */
function custom_client_fields( $checkout ) {

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        
        //Will prevent loading traveler forms if an order contains only category type=producto
        $category = wp_get_post_terms( $_product->id, 'product_cat' );
        if($category[0]->name == "Producto"){
            continue;
        }

        $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
        $variation_id = $cart_item['variation_id']; 
        $product_name = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';

        if (array_key_exists('attribute_pa_grupo', $cart_item['variation'])) {
            $parts      = explode('-', $cart_item['variation']['attribute_pa_grupo']);
            $max_people = $parts[0];
        } else {
            $max_people   = CFS()->get('max_people', $product_id); 
        }

        // We only proceed if the product existst and there's in fact a product in the cart
        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {


            $traveler_id = $product_id . '_' . $variation_id;
            echo "</br>"; 
            echo '<div class="travelers-wrapper">';
            echo "<div class=\"fieldset-title mcenter\"><h4>$product_name</h4></div>";
            echo "</br>"; 
            echo '<div class="row">';
            echo '<div class="col-lg-12 col-md-12">';
            $current_user = wp_get_current_user();

            $username = get_user_meta($current_user->ID, 'billing_first_name', true); 
            // Only shows the checkbox if the user has at least first name
            if ( !empty($username) ) {
                echo '<input class="fill-data-checkbox-input fill-data-checkbox-input-' . $traveler_id . '" type="checkbox" /> <span class="fill-data-checkbox">Yo viajo en esta experiencia</span>';
            }


        ?>
        <script type="text/javascript">
            jQuery(function($){
                $('.fill-data-checkbox-input-<?=$traveler_id?>').change(function(){
                    if ( $(this).is(":checked") )  {
                        $('.name_<?=$traveler_id?> input').first().val('<?=get_user_meta($current_user->ID, 'billing_first_name', true)?>');
                        $('.lastname_<?=$traveler_id?> input').first().val('<?=get_user_meta($current_user->ID, 'billing_last_name', true)?>');
                        $('.nationality_<?=$traveler_id?> select').first().val('<?=get_user_meta($current_user->ID, 'billing_nationality', true)?>');
                        $('.id_<?=$traveler_id?> input').first().val('<?=get_user_meta($current_user->ID, 'billing_cirif', true)?>');
                        $('.email_<?=$traveler_id?> input').first().val('<?=get_user_meta($current_user->ID, 'billing_email', true)?>');
                        $('.phone_<?=$traveler_id?> input').first().val('<?=get_user_meta($current_user->ID, 'billing_phone', true)?>');
                        $('.city_<?=$traveler_id?> input').first().val('<?=get_user_meta($current_user->ID, 'billing_city', true)?>');
                        $('.genre_<?=$traveler_id?> input').first().val('<?=get_user_meta($current_user->ID, 'billing_genre', true)?>');
                        $('.birthdate_<?=$traveler_id?> input').first().val('<?=get_user_meta($current_user->ID, 'billing_birthdate', true)?>');
                    } else {
                        $('.name_<?=$traveler_id?> input').first().val('');
                        $('.lastname_<?=$traveler_id?> input').first().val('');
                        $('.nationality_<?=$traveler_id?> select').first().val($('.nationality_<?=$traveler_id?> select option:first').val());
                        $('.id_<?=$traveler_id?> input').first().val('');
                        $('.email_<?=$traveler_id?> input').first().val('');
                        $('.phone_<?=$traveler_id?> input').first().val('');
                        $('.city_<?=$traveler_id?> input').first().val('');
                        $('.birthdate_<?=$traveler_id?> input').first().val('');
                    }
                })
            })
        </script>
        <?php
            echo '</div>';
            echo '</div>';

            echo '<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 mcenter ' . $traveler_id . '-traveler-container">';


            $people_min   = get_post_meta( $variation_id, '_people_min', true );


            //$total_places = $cart_item['quantity']; 
            $total_places = $cart_item['quantity']; 
            $total_people = array_key_exists('extra_people', $cart_item) && is_int($cart_item['extra_people']) ? $cart_item['extra_people'] : 0; 


            if (!empty($people_min) && $total_people < $people_min) {
                $total_people = $people_min; 
            }

            $total_places *= $total_people > 0 ? $total_people : 1; 
            for ($k = 0; $k < $total_places; ++$k) { 
                // echo traveler form
                traveler_form($checkout, array(
                    'product_id'   => $product_id,
                    'variation_id' => $variation_id,
                    'traveler'     => $k
                ));
            }

            echo '</div>';
            if ($max_people > 1) {
                echo '<div class="col-lg-offset-8 col-md-offset-8 col-lg-4 col-xs-offset-4 col-xs-8 col-md-4">';
                echo '<button class="avalaible button add-traveler-button" href="#" data-max_people="' . ($max_people * $total_places) . '" data-traveler_id="' . $traveler_id . '">Añadir Viajero</button>';
                echo '</div>'; 
            }
            echo '</div>'; 
        }  
    }
}


/**
 * Process the checkout with the new fields
 * @return void
 */
add_action('woocommerce_checkout_process', 'custom_client_fields_process');
function custom_client_fields_process() {

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
        $product_name = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
        $variation_id = $cart_item['variation_id'];

        $variation_attrs = $_product->get_variation_attributes(); 
        // Get the variation npersonas and nninos to set the number of travelers
        $product_adults   = $variation_attrs['attribute_pa_npersonas'];
        $product_children = $variation_attrs['attribute_pa_nninos'];

        // We only proceed if the product existst and there's in fact a product in the cart
        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

            $traveler_id = $product_id . '_' . $variation_id;
            $fields = array(
                        'name',
                        'lastname',
                        'id',
                        'email',
                        'phone',
                        'genre',
                        'nationality',
                        'city',
                        'birthdate'
                        ); 


            $travelers = array(); 

            foreach ( $fields as $field ) {
                $field_id = $field . "_$traveler_id";
                $total_places = sizeof($_POST[$field_id]); 

                // Fill travelers array
                if ( empty($travelers) ) {
                    $travelers = array_fill(0, $total_places, false); 
                }

                for ($k = 0; $k < $total_places; ++$k) { 
                    if (! $_POST[$field_id][$k]) {
                        $travelers[$k] = true; 
                    }
                }
            }

            foreach($travelers as $number => $traveler) {
                if ($traveler) {
                    wc_add_notice('<strong>' . __('Debe llenar todos los campos') . '</strong> ' . __('del viajero #') . ($number + 1) . " ($product_name)",'error'); 
                }
            }
        }  
    }
}

/**
 * Updates traveler form order meta
 * @param integer $order_id
 * @param object $args
 * @return void
 */
function update_traveler_form_meta($order_id, $args) {

    // Do nothing if there isn't product_variation_id
    if ( !array_key_exists('product_variation_id', $args) || !array_key_exists('fields', $args) ) {
        return; 
    }

    $product_variation_id = $args['product_variation_id'];
    $fields               = $args['fields'];

    foreach( $fields as $f )  {
        if (!empty($_POST["$f$product_variation_id"])) {
            // Delete the post meta before adding it, to avoid duplicates
            delete_post_meta( $order_id, "$f$product_variation_id" ); 
            update_post_meta( $order_id, "$f$product_variation_id", sanitize_text_field($_POST["$f$product_variation_id"])); 
        }
    }
}

/**
 * Update the order meta with the fields value
 * @param integer $order_id
 * @return void
 */
add_action('woocommerce_checkout_update_order_meta', 'custom_client_fields_update_order_meta');
function custom_client_fields_update_order_meta( $order_id ) {

    delete_post_meta( $order_id, 'product_id' ); 

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
        $product_name = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
        $variation_id = $cart_item['variation_id'];

        $variation_attrs = $_product->get_variation_attributes(); 
        // Get the variation npersonas and nninos to set the number of travelers
        $product_adults   = $variation_attrs['attribute_pa_npersonas'];
        $product_children = $variation_attrs['attribute_pa_nninos'];
        // Get product_ids 
        $product_ids = get_post_meta( $order_id, 'product_id', true );

        if (empty($product_ids)) {
            $product_ids = array(); 
        }

        $product_ids[] = array($product_id, $product_name, $variation_id); 
        update_post_meta( $order_id, 'product_id', $product_ids); 


        // We only proceed if the product existst and there's in fact a product in the cart
        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
            // Set number of clients
            $traveler_id = $product_id . '_' . $variation_id;
            $fields = array(
                'name',
                'lastname',
                'id',
                'email',
                'phone',
                'genre',
                'nationality',
                'city',
                'birthdate',
                'comments'
            ); 


            $total_places = 1; 

            foreach ( $fields as $field ) {
                $field_id = $field . "_$traveler_id";
                $total_places = sizeof($_POST[$field_id]); 
                for ($k = 0; $k < $total_places; ++$k) { 
                    if ( !empty($_POST[$field_id][$k]) ) {
                        delete_post_meta( $order_id, $field_id . '_' . $k); 
                        update_post_meta( $order_id, $field_id . '_' . $k, sanitize_text_field($_POST[$field_id][$k])); 
                    }
                }
            }

            delete_post_meta( $order_id, "number_of_clients_$traveler_id" ); 
            update_post_meta( $order_id, "number_of_clients_$traveler_id", $total_places); 

            $experience_date = $cart_item['experience_date']; 
            if ( !empty($experience_date) ) {
                update_post_meta( $order_id, "experience_date", sanitize_text_field($experience_date)); 
            }
        }
    }
}

/**
 * Display the new fields in the order edit page
 * @param object $order 
 * @return void
 */
function display_custom_client_order_meta( $order ) {


    $product_ids        = get_post_meta( $order->id, 'product_id', true );

    echo '<div class="order_data_column" style="width:100%;">';
    foreach ($product_ids as $product) {
        $product_id        = $product[0];
        $product_name      = $product[1];
        $variation_id      = $product[2];
        $traveler_id = $product_id . '_' . $variation_id;
        $number_of_clients = get_post_meta( $order->id, "number_of_clients_$traveler_id", true );
        $category = wp_get_post_terms( $product_id, 'product_cat' );
        
        // TODO: Add translations
        if($category[0]->name != "Producto"){
            echo '<h4>'. "Viajeros de <strong>$product_name</strong>".'</h4><hr>';               
         
            for ($k = 0; $k < $number_of_clients; ++$k) { 
                $product_variation_id = "_$traveler_id"."_$k";
                echo '<h5> Viajero #'.($k + 1).'</h5>';
                echo '<p><strong>'.__('Name').' </strong>'.get_post_meta( $order->id, "name$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('Last Name',  'woocommerce').' </strong>'.get_post_meta( $order->id, "lastname$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('Cédula de identidad').' </strong>'.get_post_meta( $order->id, "id$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('Nacionalidad').' </strong>'.get_post_meta( $order->id, "nationality$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('Email').' </strong>'.get_post_meta( $order->id, "email$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('Phone', 'woocommerce').' </strong>'.get_post_meta( $order->id, "phone$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('Genre').' </strong>'.get_post_meta( $order->id, "genre$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('City', 'woocommerce').' </strong>'.get_post_meta( $order->id, "city$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('Fecha de nacimiento').' </strong>'.get_post_meta( $order->id, "birthdate$product_variation_id", true).'</p>'; 
                echo '<p><strong>'.__('Comentarios').' </strong>'.get_post_meta( $order->id, "comments$product_variation_id", true).'</p>'; 
                echo '</br>';
            }
        }
        
    }


    echo '</div>';
    $experience_date = get_post_meta( $order->id, 'experience_date', true ); 

    if ( !empty($experience_date) ) {
        // Lookup table to change month by its number
        $month_lookup = array(
            "1"  => "enero",
            "2"  => "febrero",
            "3"  => "marzo",
            "4"  => "abril",
            "5"  => "mayo",
            "6"  => "junio",
            "7"  => "julio",
            "8"  => "agosto",
            "9"  => "septiembre",
            "10" => "octubre",
            "11" => "noviembre",
            "12" => "diciembre"
        );

        $parts = explode('/', $experience_date);
        $day = $parts[0]; 
        $month = $month_lookup[$parts[1]]; 
        $year  = $parts[2]; 
        echo '<div class="order_data_column" style="width:100%;">';
        echo '<h4>Fecha solicitada:</h4>'; 
        echo "<p>El cliente solicitó la fecha <strong>$day de $month de $year</strong> para el plan <strong>$product_name";
        echo '</div>';
    }
}
add_action('woocommerce_admin_order_data_after_order_details', 'display_custom_client_order_meta'); 
