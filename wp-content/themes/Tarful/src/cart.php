<?php
/**
 * Returns an array of cart items 
 * from the current cart
 * @return $items array of cart items or false if none is retrieved
 */
function get_cart_items() {

    global $woocommerce; 

    $items = array(); 
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
        if ( $_product && $_product->exists() && 
             $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {

                 $single_item              = array();
                 $single_item['image']     = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(array(64,64), array('class' => 'media-object')), $cart_item, $cart_item_key );
                 $single_item['quantity']  = $cart_item['quantity'];
                 $single_item['data']      = WC()->cart->get_item_data( $cart_item );
                 $single_item['price']     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                 $single_item['url']       = $woocommerce->cart->get_cart_url();
                 $single_item['name']      = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );

                 $items[] = $single_item; 

        }
    }

    return ( empty($items) ? false : $items ); 
}


add_action("template_redirect", 'tarful_redirect_cart');
/**
 *  Redirect Cart if empty goes to shop. If has products goes to checkout
 *  It's called by the action template_redirect
 *  @return void
 */
function tarful_redirect_cart(){
	global $woocommerce;
	if( is_cart() && sizeof($woocommerce->cart->cart_contents) == 0){
		wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
	}

	if( is_cart() && sizeof($woocommerce->cart->cart_contents) > 0){
		wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'checkout' ) ) );
	}

}
