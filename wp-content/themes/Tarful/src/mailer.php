<?php

/**
 * Sends form experience email 
 * @return void
 */
function tarful_send_mail(){
	error_reporting(E_ALL ^ E_NOTICE);
	//If the form is submitted
	if(isset($_POST['form-experience'])) {


		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		// Get information related to visitor, location, state, city, country.
		$location_data = array();
		$location_data = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));
		
		$continent_code = $location_data["geoplugin_continentCode"];
		$country_code = $location_data["geoplugin_countryCode"];
		$latitude = $location_data["geoplugin_latitude"];
		$longitude = $location_data["geoplugin_longitude"];
		$city = $location_data["geoplugin_city"];
		$state = $location_data["geoplugin_regionName"];
		$country = $location_data["geoplugin_countryName"];


        $category    = "No especificado";
        $location    = "No especificado";
        $checkin     = "No especificado";
        $checkout    = "No especificado";
        $budget      = "No especificado";
        $email       = "No especificado";
        $description = "No especificado";
        $phone       = "No especificado";

		if(trim($_POST['category']) != '') {
			$category = trim($_POST['category']);
		}
		if(trim($_POST['location']) != '') {
			$location = trim($_POST['location']);
		}
		if(trim($_POST['checkin']) != '') {
			$checkin = trim($_POST['checkin']);
		}
		if(trim($_POST['checkout']) != '') {
			$checkout = trim($_POST['checkout']);
		}
		if(trim($_POST['budget']) != '') {
			$budget = trim($_POST['budget']);
		}
		if(trim($_POST['email']) != '') {
			$email = trim($_POST['email']);
		}
		if(trim($_POST['description']) != '') {
			$description = trim($_POST['description']);
		}
		if(trim($_POST['phone']) != '') {
			$phone = trim($_POST['phone']);
		}

        $tr_style = ""; 
        $emailTo  = get_option( 'admin_email' );
        $subject  = '[ZOI Venezuela] Solicitud de nueva experiencia';
        $message  = '<html><body><br />';
        $message .= '<table border="0" cellpadding="10" width="600px">';
        $message .= '<tr style="'.$tr_style.'"><td>Has recibido un mensaje desde el formulario de contacto</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td><strong>Ubicación:</strong> '.$location.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td><strong>Email:</strong> '.$email.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td><strong>Teléfono:</strong> '.$phone.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td><strong>Categoria:</strong> '.$category.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td><strong>Fecha Inicio:</strong> '.$checkin.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td><strong>Fecha Fin:</strong> '.$checkout.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td><strong>Presupuesto:</strong> '.$budget.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td><strong>Comentarios:</strong> '.$description.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td></td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td>-------</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td></td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td>Dirección IP: '.$ip.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td>Ciudad: '.$city.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td>Estado: '.$state.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td>País: '.$country.'</td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td></td></tr>';
        $message .= '<tr style="'.$tr_style.'"><td>Enviado desde el Formulario de crear nueva experiencia en'.get_bloginfo('url').'</td></tr>';
        $message .= '</table>';
        $message .= '</body></html>';

        $headers = 'From: ZOI VENEZUELA' .' <'.$email.'>' . "\r\n" . 'Reply-To: ' . $email;
        
        add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
        $sent = wp_mail( $emailTo, $subject, $message, $headers);
        tarful_thank_you_mail($email);

	}
}


/**
 * Sends thank you email for experience form
 * @param string $email
 * @return void
 */
function tarful_thank_you_mail($email){

		// Email to user 
        $tr_style     = ""; 
		$subject_user = '[ZOI Venezuela] ¡Gracias por contactarnos!';	 
		$message_user = '<html><body><br />';
		$message_user .= '<table border="0" cellpadding="10" width="600px">';
		$message_user .= '<tr style="'.$tr_style.'"><td>Hola '.$user_fullname.', Gracias por Contactarnos</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>¡Hemos recibido tu email, vamos a revisarlo y te contactaremos próximamente!</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td></td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td>Equipo ZOI Venezuela</td></tr>';
		$message_user .= '<tr style="'.$tr_style.'"><td><a href="'.get_bloginfo('url').'">zoivenezuela.com - Vive la Aventura</a></td></tr>';
		$message_user .= '</table>';
		$message_user .= '</body></html>';

		$from = get_option( 'admin_email' );
		$headers = 'From: ZOI VENEZUELA' .' <'.$from.'>' . "\r\n" . 'Reply-To:  <'.$from.'>';
	
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		wp_mail( $email, $subject_user, $message_user, $headers);
}
