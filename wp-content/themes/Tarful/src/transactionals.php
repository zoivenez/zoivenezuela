<?php

// For unit tests, need to load the SendGrid class
if (!class_exists('SendGrid')) {
    require_once dirname(__FILE__) . '/../../../plugins/sendgrid-email-delivery-simplified/vendor/sendgrid-php/vendor/autoload.php'; 
}
/**
 * Global sendgrid variable authenticated
 * @var Sendgrid object
 */
$sendgrid = new SendGrid(SENDGRID_USERNAME, 
                         SENDGRID_PASSWORD, 
                         array("turn_off_ssl_verification" => true));


/**
 * Sends an email using sendgrid template system
 * @param array $sender sender attributes (email, name, reply-to, subject) from email
 * @param string $to receipient email
 * @param string $templateid template ID
 * @param string $contenthtml HTML content
 * @param string $contenttext text content
 * @param date $sendat specific unix date to delay the email
 * @param array $params describe name of parameter and value, to pass dynamic data.
 * @return boolean true if email was sent, false otherwise
 */
function send_sendgrid_email($to, $sender, $template_id, $content_html = '', $content_text = '',$send_at = false, $params=array()) {
    global $sendgrid; 

    $from          = $sender['from'];
    $subject       = $sender['subject'];
    $from_name     = array_key_exists('from_name', $sender) ? $sender['from_name'] : 'Equipo ZOI';
    $reply_to      = array_key_exists('reply_to', $sender) ? $sender['reply_to'] : $from;

    $email = new SendGrid\Email();
    $email 
        ->addTo($to)
        ->setFrom($from)
        ->setReplyTo($reply_to)
        ->setSubject($subject)
        ->setFromName($from_name)
        ->setHTML($content_html)
        ->setText($content_text)
        ->setTemplateId($template_id); 

    /* If exist a delay*/
    if($send_at){
        $email->setSendAt($send_at);
    }


    /* Loop to get each name and value of params, then use Substitution to pass the data */
    foreach($params as $param) {
        $email->addSubstitution($param[0], array($param[1]));
    }

    return $sendgrid->send($email); 
}

 
/**
 * Generates a token for a user, links the user with the token 
 * generates URL and sends confirmation email to user
 * @param integer $userid , user id to be used
 * @param boolean $delay true to enable delay - false to disable delay
 * @return true if emails was sent, false otherwise
 */
function send_confirmation_email($user_id, $delay = false) {

    $user_data = get_userdata( $user_id);
    // Unexistent user, return false
    if ( !$user_data ) {
        return false; 
    }

    $user_confirmed = is_email_confirmed($user_id); 
    // If it's confirmed, return false
    if (is_array($user_confirmed) && $user_confirmed[0]) { 
        return false; 
    }



    $confirmation_meta_key = 'email_confirmed';

    // Default length: 32
    $token = generate_token();
    // Remove metadata
    delete_user_meta( $user_id, $confirmation_meta_key );
    // Add metadata
    add_user_meta( $user_id, $confirmation_meta_key, array('value' => false, 'token' => $token) );

    // Generate URL
    $confirmation_url = generate_confirmation_url($user_data->user_login, $token);
    $username         = $user_data->user_login;


    $email_content = " "; // "reset"

    $params = array
      (
      array("%username%",$username),
      array("%confirmation_url%",$confirmation_url)
      );

    //PC::debug($email_content); 
    $from    = get_option( 'admin_email' );
    $email   = $user_data->user_email;
    $subject = 'Confirma tu cuenta ZOI';
    
    

    $template_id = 'aebb9dc6-3669-489b-89d1-be8c3af08c26';
    try {
        if ($delay){
            $result = send_sendgrid_email($email, 
                                      array(
                                          'from'         => $from,
                                          'subject'      => $subject,
                                      ),
                                      $template_id, $email_content,'',strtotime($send_at),$params); 
    }else{

        $result = send_sendgrid_email($email, 
                                      array(
                                          'from'         => $from,
                                          'subject'      => $subject,
                                      ),
                                      $template_id, $email_content,'',false,$params); 

    }
        //PC::debug($result); 
    } catch (\SendGrid\Exception $e) {
        //echo $e->getCode();
        //PC::debug($e); 
        return false;
    }

    return true; 
}

/**
 * Helper function to get AJAX request to send the user confirmation
 * @return void
 */
function send_confirmation_email_helper() {
    $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : null;

    // No user id, die
    if (!$user_id) {
        echo 'request error';
        wp_die(); 
    }

    $sent = send_confirmation_email($user_id, false); 
    echo $sent ? 'success' : 'failure'; 
    wp_die(); 
}
// AJAX action hook
add_action('wp_ajax_send_confirmation_email_helper', 'send_confirmation_email_helper'); 

// Action that links sends verification email
add_action( 'user_register', 'send_welcome_and_confirmation_email_helper', 10, 1 );

/**
 * Send Welcome Email (calling Static Template)
 * @param integer $userid , user id to be used
 * @param boolean $delay true to enable delay - false to disable delay
 * @return true if emails was sent, false otherwise
 */
function send_welcome_email($user_id,$delay = false) {
     $user_data = get_userdata( $user_id);
    // Unexistent user, return false
    if ( !$user_data ) {
        return false; 
    }


    $from    = get_option( 'admin_email' );
    $email   = $user_data->user_email;
    $subject = 'Bienvenido a ZOI Venezuela!';
    $email_content = " ";
    $template_id = '54c2d796-58e2-403b-b5e0-6dced5e30d29';

    /* functions to set timezone and create the delay datetime  */
    date_default_timezone_set('America/Caracas');
    $send_at = date('Y-m-d H:i:s');
    $send_at = date('Y-m-d H:i:s', strtotime($send_at . '+60 minutes'));

    try {

        if ($delay){
            $result = send_sendgrid_email($email, 
                                      array(
                                          'from'         => $from,
                                          'subject'      => $subject,
                                      ),
                                      $template_id, $email_content,'',strtotime($send_at)); 
    }else{

        $result = send_sendgrid_email($email, 
                                      array(
                                          'from'         => $from,
                                          'subject'      => $subject,
                                      ),
                                      $template_id, $email_content); 

    }
    } catch (\SendGrid\Exception $e) {
        return false;
    }

    return true; 
}

/* 
 * Helper function that send Welcome and Confirmation (with delay) emails
 * @return void
 */

function send_welcome_and_confirmation_email_helper($user_id){

    $sent = send_confirmation_email($user_id,false);
    $sent = send_welcome_email($user_id,true);
    return $sent;
}

/**
 * Sends a notification email to admin when an experience 
 * runs out of dates 
 * @param integer $experience_id 
 * @return void
 */
function send_no_date_experience_email($experience_id) {

    $template_id = '652c16ef-36c3-47c0-8ff9-504ac8576b8e';

    $email_content = " "; // "reset"

    $experience_name = get_the_title($experience_id); 
    $experience_url  = get_permalink($experience_id);

    $params = array (
        array("%experience_name%",$experience_name),
        array("%experience_url%",$experience_url)
    );

    //PC::debug($email_content); 
    $from    = get_option( 'admin_email' );
    $email   = $from;
    $subject = '¡Aviso! Experiencia sin fechas';
    
    

    $template_id = '652c16ef-36c3-47c0-8ff9-504ac8576b8e';
    try {

        $result = send_sendgrid_email($email, 
                                      array(
                                          'from'         => $from,
                                          'subject'      => $subject,
                                          'from_name'    => 'ZOI Tecnología'
                                      ),
                                      $template_id, $email_content,'',false,$params); 

        //PC::debug($result); 
    } catch (\SendGrid\Exception $e) {
        //echo $e->getCode();
        //PC::debug($e); 
        return false;
    }

    return true; 

}

/* 
 * Function that send Invitation email
 * @param string $invite_email
 * @param object $sender object with sender information
 * @param string $token invitation token
 * @return void
 */

function send_invitation_user($invite_email, $sender, $token){
  if(!$invite_email || !$sender || !$token){
    return false;
  }

  $from    = get_option( 'admin_email' );
  $email   = $invite_email;
  $subject = 'Invitación a ZOI Venezuela!';
  $email_content = " ";
  $template_id = 'ced3345c-e63a-4ee2-bb38-1b3d33546546';

  $params = array
      (
      array("%name_sender%",$sender->user_login),
      array("%confirm_invitation%",generate_invitation_url($token))
      );

  try {

    $result = send_sendgrid_email($email, 
                                    array(
                                        'from'         => $from,
                                        'subject'      => $subject,
                                    ),
                                    $template_id, $email_content,'',false,$params);

  } catch (\SendGrid\Exception $e) {
      return false;
  }

  return true; 
}
