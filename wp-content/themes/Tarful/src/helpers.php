<?php
/**
 * Generates the corresponding URL for the email confirmation
 * @param integer $user_id 
 * @param string $token confirmation token to be compared
 * @return string $url
 */
function generate_confirmation_url($user_login, $token) {
    return get_bloginfo('url') . '/email_confirmation?user_login=' . $user_login . '&conf_token=' . $token;
}


/**
 * Generates the corresponding URL for the email invitation
 * @param string $token invitation token to be compared
 * @return string $url
 */
function generate_invitation_url($token){
    return get_bloginfo('url') . '/invitation?inv_token=' . $token;
}

/**
 * Truncate string
 * @param string $string string to truncate
 * @param int $length size of final string 
 * @return string truncated
 */
function truncate_string($string, $length = 6){
  if($string){
    $asterisk = 2;
    $length_string = strlen($string);
    $final_string;
    //$even = ($length % 2) == 0;
    if($length_string > $length){
        $length = $length - $asterisk;
        $half = floor($length / 2);
        $first_digits = substr($string, 0, $half);
        $last_digits = substr($string, -$half);
        $final_string = $first_digits.'**'.$last_digits;

    } else {
        $final_string = $string;
    }
    return $final_string;
  }
  return false;
}

/**
 * Verifies if a user email is confirmed
 * @param integer $userid id of the user to confirm
 * @return array (true,token) if it's confirmed, bool false if it isn't 
 */
function is_email_confirmed($user_id) {

    $confirmation_meta_key = 'email_confirmed';
    $confirmed = get_user_meta($user_id, $confirmation_meta_key, true); 

    if (empty($confirmed) || !$confirmed) {
        return false; 
    }

    return ($confirmed['value'] ?  $confirmed : false);
}

/**
 * Generates a token with specific length
 * @param  integer $length of the token, defaults to 32
 * @return string $token
 */
function generate_token($length = 32) {
    $allowed_chars  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $allowed_chars .= "abcdefghijklmnopqrstuvwxyz";
    $allowed_chars .= "0123456789";

    $n_allowed_chars = strlen($allowed_chars) - 1; 

    $token = '';

    for ($i = 0; $i < $length; ++$i) {
        $token .= $allowed_chars[mt_rand(0, $n_allowed_chars)];
    }

    return $token; 
}

/**
 * Return a list of experiences to be shown in the calendar
 * @param date $from begin date in milliseconds 
 * @param date $to end date in milliseconds 
 * @return string $experiences
 */
function get_experiences_between($from, $to, $all = false) {

    // Empty range case
    if ($to < $from) return array('success' => 0, 'error' => 'Empty range'); 

    // Null $form means I'm asking for dates less than $to
    if ($from) {
        $start_seconds = $from / 1000;
    }
    $end_seconds   = $to / 1000;

    $start = date('Y-m-d', $start_seconds);
    $end   = date('Y-m-d', $end_seconds);

    // Args to get ALL posts
    $qargs = array(
        'posts_per_page' => -1,
        'post_type' => 'product'
    );

    // Get all posts to filter by date
    $result = new WP_Query( $qargs ); 
    $posts = $result->get_posts(); 

    if ($all) {
        $filtered_experiences = $posts; 
    } else {
        // Get experiences with at least one date inside the range
        $filtered_experiences = array_filter($posts, function($post) use ($from, $to) {

            // Get all dates in this post
            $dates = wp_get_post_terms( $post->ID, 'pa_fechas-de-experiencia' ); 


            // Check each date
            foreach ($dates as $date) {
                $post_date = parse_spanish_date($date->name); 
                // If from is not null, check between range
                if (($from !== NULL && ( $from <= $post_date && $post_date <= $to )) || 
                    // If from is null, just check if the date is less than $to
                    ($from === NULL && $post_date <= $to) || $all ) return true;
            }
            return false; 
        });
    }

    // Array with different event classes
    $classes = array('event-important', 'event-success', 
                     'event-warning', 'event-info', 
                     'event-inverse', 'event-special'); 

    $curr_class = 0;
    
    $duplicated_experiences = array(); 
    foreach($filtered_experiences as $exp) {
        global $curr_class; 
        // how many times the experience will be added as duplicate
        $dates = wp_get_post_terms( $exp->ID, 'pa_fechas-de-experiencia' ); 
        $used_dates = array(); 
        foreach ($dates as $date) {
            $post_date = parse_spanish_date($date->name); 
            //var_dump($date->name);
            //var_dump(date('Y-m-d', $post_date));
            if (!$post_date) continue; 
            // If from is not null, check between range
            if ( !array_key_exists($post_date, $used_dates) && (($from !== null && ( $from <= $post_date && $post_date <= $to )) || 
                // If from is null, just check if the date is less than $to
                ($from === null && $post_date <= $to) || $all) ) {
                $duplicated_experiences[] = array('post'  => $exp,
                                                  //'end'  => $post_date . '000',
                                                  'begin'  => strtotime(date('Y-m-d',$post_date) .' +1 day') . '000', // Calendar set events one day after
                                                  'class' => $classes[$curr_class] );
                $used_dates[$post_date] = true; 

            }
        }
        $curr_class = ( $curr_class + 1 ) % 6; 
    }

    // Get the correct format from each experience object
    $experiences = array_map(function($post) use ($from, $to) { 
        $event_begin = $post['begin']; 
        //$event_end   = $post['end']; 
        $experience  = $post['post'];
        $class       = $post['class'];

        $experience_data = array(
            'id'    => $experience->ID,
            'title' => $experience->post_title,
            'url'   => get_permalink( $experience->ID ),
            'start' => $event_begin,
            // XXX: Use duration to highlight the whole plan
            'end'   => $event_begin,
            'class' => $class

        );

        return $experience_data; 

    }, $duplicated_experiences);

    return array('success' => 1, 'result' => $experiences); 
}

/**
 * Gets experiences with expired dates
 * @return string $experiences json string
 */
function get_experiences_with_past_dates() {

    return get_experiences_between(null, strtotime('today UTC'));
}


/**
 * Gets extra info of experience based on variation
 * @param array $variations 
 * @return array variation_id => info
 */
function get_experience_extra_info($variations) {


    $week_days = array('lunes', 'martes', 'miércoles', 
        'jueves', 'viernes', 'sábado', 'domingo'); 
    $days_map = array(
        'domingo'   => 0,
        'lunes'     => 1,
        'martes'    => 2,
        'miércoles' => 3,
        'jueves'    => 4,
        'viernes'   => 5,
        'sábado'    => 6
    );

    $extra_info = array(); 
    foreach($variations as $loop => $variation) {
        $variation_id = $variation["variation_id"]; 
        $days = array(); 
        foreach($week_days as $day) {
            $variation_availability = get_post_meta( $variation_id, 'variable_booking_day['.$loop.']['.$day.']', true );  
            if (!empty($variation_availability ) && $variation_availability == "yes") {
                $days[] = $days_map[$day]; 
            }

            $variation_duration = get_post_meta( $variation_id, 'duration', true );  
            if (empty($variation_duration)) {
                $variation_duration = 1; 
            }

            $variation_people = get_post_meta( $variation_id, 'people', true ); 
            if (empty($variation_people)) {
                $variation_people = 1; 
            }

        }

        $extra_info[$variation_id] = array($days, $variation_duration, $variation_people); 
    }

    return $extra_info; 
}

/**
 * Sets the data for a cart item by key. Similar to post meta, but based on session.
 * @param string $cart_item_key
 * @param string $key
 * @param string $value
 * @return void
 */
function lm_set_item_data( $cart_item_key, $key, $value ) {
  $data = (array) WC()->session->get('_lm_product_data');
  if ( empty($data[$cart_item_key]) ) $data[$cart_item_key] = array();
  
  $data[$cart_item_key][$key] = $value;
  
  WC()->session->set('_lm_product_data', $data);
}

/**
 * Returns cart item data for the specified cart item. If $key is provided, a single value is returned.  
 * Otherwise, an array of all cart item data is returned.
 * @param string $cart_item_key
 * @param string $key default to null
 * @param string $default default to null
 * @return void
 */
function lm_get_item_data( $cart_item_key, $key = null, $default = null ) {
  $data = (array) WC()->session->get('_lm_product_data');
  if ( empty($data[$cart_item_key]) ) $data[$cart_item_key] = array();
  
  // If no key specified, return an array of all results.
  if ( $key == null ) return $data[$cart_item_key] ? $data[$cart_item_key] : $default;
  else return empty($data[$cart_item_key][$key]) ? $default : $data[$cart_item_key][$key];
}

/**
 * Function: get_payment_gateway_by_id
 * Description: gets a payment gateway instance by id
 * @param string $gateway_id
 * @return $gateway
 */
function get_payment_gateway_by_id($gateway_id) {
    $available_gateways = WC()->payment_gateways()->get_available_payment_gateways();

    foreach($available_gateways as $gateway) {
        if ($gateway->id === $gateway_id) {
            return $gateway; 
        }
    }

    return false; 
}

/**
 * Function: sort_experiecne_dates
 * Description: Makes a chornological sorting of a set of dates 
 * with the format "day de month [ de year ] (in spanish) 
 * @param array $dates
 * @return array $sorted_dates
 *
 */
function sort_experience_dates($dates) {

    $array_to_sort = array();     // Array to be sorted
    $unsorted_elements = array(); // Array with no-dates to be added
    foreach ($dates as $date) {

        // Get the date from the term
        $spanish_date = apply_filters( 'woocommerce_variation_option_name', $date->name );

        // Parse the date and get the time object
        $date_obj = parse_spanish_date($spanish_date); 

        if (!$date_obj) {
            $unsorted_elements[] = $date;
            continue; 
        }

        // Add the key-value to be sorted
        while (array_key_exists($date_obj, $array_to_sort)) {
            ++$date_obj;
        }

        $array_to_sort[$date_obj] = $date; 
    }


    // If nothing was sorted
    if (empty($array_to_sort))
        return $dates; 

    // Sort the array in the alphanumeric way
    if (ksort($array_to_sort)) {
        return array_merge($unsorted_elements, array_values($array_to_sort)); 
    } else {
        return array(); 
    }

}

/**
 * Get the product terms sorted
 * @param integer $product_id
 * @param string $taxonomy
 * @param object $args
 * @return array $sorted_terms
 */
function wc_get_sorted_product_terms($product_id, $taxonomy, $args) {
    $terms = wc_get_product_terms( $product_id, $taxonomy, $args );

    if (array_key_exists('sort_dates', $args)) {
        return ($args['sort_dates'] ? sort_experience_dates($terms) : $terms); 
    } else {
        return $terms; 
    }
}

/**
 * Function: parse_spanish_date
 * Description: Takes a date in the format "day de month [ de year [ - extra info ]] "
 * and creates a time object
 * @param string $spanish_date
 * @return date $parsed_date or boolean false if the date couldn't be parsed
 */
function parse_spanish_date($spanish_date) {

    // Lookup table to change month by its number
    $month_lookup = array(
        "enero"      => "1",
        "febrero"    => "2",
        "marzo"      => "3",
        "abril"      => "4",
        "mayo"       => "5",
        "junio"      => "6",
        "julio"      => "7",
        "agosto"     => "8",
        "septiembre" => "9",
        "octubre"    => "10",
        "noviembre"  => "11",
        "diciembre"  => "12"
    );

    // Array of matches initialized empty
    $matches = array();

    // Pattern to get the day, the month and the year (if present)
    $pat = "/(\d+) de ([a-zA-Z]+)( de (\d{4}))?( - [a-zA-Z0-9\ \"]+)?/";

    // Convert the month in integer
    $res = preg_match($pat, $spanish_date, $matches); 

    // If there aren't any matches, return false
    if (sizeof($matches) < 3) {
        return false; 
    }

    // Invalid month
    if (!array_key_exists($matches[2], $month_lookup)) { 
        return false; 
    }


    // Get day and month
    $day   = $matches[1]; 
    $month = $month_lookup[$matches[2]];

    if (sizeof($matches) >= 5 && preg_match('/\d{4}/', $matches[4]) !== 0) {
        $year = $matches[4]; 
    } else {
        $year = date('Y'); 
    }

    $date = "$day-$month-$year";
    return strtotime($date); 
}

/**
 * Shows the group of users' avatars
 * @return integer $user_count
 */
function tarful_users_count() {

	global $wpdb;
	$user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->users" );

	echo $user_count;

}

/**
 * Shows the group of users' (team)
 * @return integer $team_user_count
 */
function tarful_team_users_count() {

	global $wpdb;
	$team_user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->usermeta WHERE meta_key='wp_capabilities' AND meta_value LIKE '%administrator%'" );

	echo $team_user_count;

}


/**
 * Shows the group of users' (gurus)
 * @return integer $gurus_user_count
 */
function tarful_gurus_users_count() {

	global $wpdb;
	$gurus_user_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->usermeta WHERE meta_key='wp_capabilities' AND meta_value LIKE '%gurú%'" );

	echo $gurus_user_count;

}

/*
 * Checks whether an email has a gravatar account or not
 * @param integer $email
 * @return boolean true if gravatar account is validated
 */
function validate_gravatar($email) {
	// Craft a potential url and test its headers
	$hash = md5(strtolower(trim($email)));
	$uri = 'http://www.gravatar.com/avatar/' . $hash . '?d=404';
	$headers = @get_headers($uri);
	if (!preg_match("|200|", $headers[0])) {
		$has_valid_avatar = FALSE;
	} else {
		$has_valid_avatar = TRUE;
	}
	return $has_valid_avatar;
}

/**
 * Checks if user has facebook avatar or not
 * @param string $url
 * @return boolean true if facebook account is validated
 */
function validate_facebook_avatar($url) {
    return (preg_match('/graph.facebook/', $url) ? true : false); 
}

/**
 * Checks if user has google+ avatar or not
 * @param string $url
 * @return boolean true if google+ account is validated
 */
function validate_google_avatar($url) {
    return (preg_match('/googleusercontent/', $url) ? true : false); 
}

/**
 * Checks if user has visible avatar
 * @param string $email 
 * @param string $url
 * @return boolean true if avatar is validated
 */
function validate_avatar($email, $url) {

    return validate_google_avatar($url) ||
            validate_facebook_avatar($url) || validate_gravatar($email); 
}
						
/** 
 * Count total registered users and show info if they like product
 * @param integer $quantity 
 * @param integer $post_id
 * @return void
 */
function tarful_networks_avatars($quantity, $post_id) { 
	
	global $wpdb;

	if ($post_id != 0) {
		
		$users_liked = $wpdb->get_results("SELECT * FROM like_post WHERE id_post = '$post_id' LIMIT $quantity");
		$array_users_like = array();
		
		foreach ($users_liked as $user_info) {
			$temp_user_id = $user_info->id_user_registered;
			$temp_user_like = $user_info->like;
			
			if ($temp_user_like == '1') {
				$current_user = get_userdata( $temp_user_id );

                // Ignore users without image
				?>
				<a href="#" class="hint  hint-top" data-hint="<?php echo $current_user->first_name . ' ' . $current_user->last_name; ?>"><?php echo get_avatar( $temp_user_id, 40 ); ?></a>
			<?php 
			}		 
		}
	
	} else {

        $showed_avatars = $quantity; 

        while ($showed_avatars > 0 ) {
            $userids = $wpdb->get_results("SELECT ID FROM $wpdb->users ORDER BY RAND() LIMIT $showed_avatars");
            foreach ($userids as $user) {
                if ( $user->ID != '') { 
                    --$showed_avatars; 
                    $current_user = get_userdata( $user->ID );			
                    $username = (empty($current_user->first_name) ? $current_user->user_login : $current_user->first_name . ' ' . $current_user->last_name ); 
                    ?>
                    <a class="hint  hint-top" data-hint="<?php echo $username; ?>"><?php echo get_avatar( $user->ID, 40 ); ?></a>
                    <?php
                }
            }
		}

	}
	
}

/** 
 * Count total registered users and show info if they like product
 * @param integer $quantity 
 * @param integer $post_id
 * @return void
 */
function tarful_networks_avatars_all($post_id) { 
	
	global $wpdb;

	if ($post_id != 0) {
		
		$users_liked = $wpdb->get_results("SELECT * FROM like_post WHERE id_post = '$post_id'");
		$array_users_like = array();
		
		foreach ($users_liked as $user_info) {
			$temp_user_id = $user_info->id_user_registered;
			$temp_user_like = $user_info->like;
			
			if ($temp_user_like == '1') {
				$current_user = get_userdata( $temp_user_id );
				?>
				<div class="row">
					<a href="#" class="hint  hint-top" data-hint="<?php echo $current_user->first_name . ' ' . $current_user->last_name; ?>"><?php echo get_avatar( $temp_user_id, 40 ); ?></a>
					<span class=""><?php echo $current_user->first_name . ' ' . $current_user->last_name; ?></span>
				</div>
			<?php
			}		 
		}
	
	} else {
		$userids = $wpdb->get_results("SELECT ID FROM $wpdb->users ORDER BY RAND()");
		foreach ($userids as $user) {
			if ($user->ID != '') { 
				$current_user = get_userdata( $user->ID );			
				?>
				<div class="row">
					<a href="#" class="hint  hint-top" data-hint="<?php echo $current_user->first_name . ' ' . $current_user->last_name; ?>"><?php echo get_avatar( $user->ID, 40 ); ?></a>
					<span class=""><?php echo $current_user->first_name . ' ' . $current_user->last_name; ?></span>
				</div>
				<?php
			}
		}

	}
	
}

/**
 * Filter experiences by search query
 * @param string $query 
 * @return array of posts or false
 */
function filter_experiences_by_query($query = '') {

    // Empty query return false
    if ($query === '') return false; 

    // Query args
    $qargs = array(
        'post_type' => 'product',
        's' => $query,
        'posts_per_page' => -1
    );

    // Create query and get posts
    $result = new WP_Query($qargs); 

    $experiences = array_map(function($post) { return $post->ID; }, $result->get_posts()); 

    // Return false if empty experiences
    return !empty($experiences) ? $experiences : false;
}

/**
 * Filter experiences by date
 * @param string $when 
 * @param string $taxonomy
 * @param string $free_date 
 * @return array of posts or false
 */
function filter_experiences_by_date($when = '', 
                                    $taxonomy = 'pa_fechas-de-experiencia', 
                                    $free_date = 'reserva') {
    // Empty date or empty posts and not first, return false
    if ($when === '') return false; 

    // Args to get ALL posts
    $qargs = array(
        'posts_per_page' => -1,
        'post_type' => 'product'
    );

    // Get all posts to filter by date
    $result = new WP_Query( $qargs ); 
    $posts = $result->get_posts(); 


    // Filter the posts without the corresponding date
    $experiences = array_map(function($post) { return $post->ID; }, 
                   array_filter($posts, function($post) use ($when, $free_date, $taxonomy) {

        // Get all dates in this post
        $dates = wp_get_post_terms( $post->ID, $taxonomy ); 

        // Check each date
        foreach ($dates as $date) {
            $date = $date->slug; 
            $parts = explode('-', $date); 

            // If the date slug is 1-de-enero, take enero and compare
            // to $when. If the date is reserva-tu-fecha, compare reserva.
            // If True, then add post to experiences
            if ( ( is_numeric($parts[0]) && $parts[2] === $when ) || 
                ( $parts[0] === $free_date ) ) {
                    return true; 
            }
        }
        return false; 
    }));

    // Return the resulting array
    return !empty($experiences) ? $experiences : false;
}


/**
 * Filter experiences by taxonomy
 * @param string $key
 * @param string $value
 * @return array of posts or false
 */
function filter_experiences_by_taxonomy($key = '', $value = '') {

    // Empty key/value or empty posts and not first, return false
    if ($key === '') return false; 
    if ($value === '') return false; 

    $tax_query = array(
        'taxonomy' => $key,
        'field'    => 'slug',
        'terms'    => $value
    );
    $qargs = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'tax_query' => array($tax_query)
    );
    // Create query and get posts
    $result = new WP_Query($qargs); 

    $experiences = array_map(function($post) { return $post->ID; }, $result->get_posts()); 

    // Return false if empty experiences
    return !empty($experiences) ? $experiences : false;
}

/**
 * Returns an array of experience filtered
 * by args
 * @param object $args object with query configuration
 * @return array of posts or false if none is found
 */
function filter_experiences_by($args = array()) {

    if (empty($args)) return false; 
    // Empty experiences array
    $results = array(); 

    global $wpdb; 
    // Hide wpdb errors
    $wpdb->hide_errors(); 

    // Handle keyword search
    if (isset($args['s']) && !empty($args['s'])) {
        // Get the experiences by seearch query
        $query = sanitize_text_field($args['s']);
        $result = filter_experiences_by_query($query); 

        // No results, return false
        if (!$result) return false;
        $results[] = $result; 

        // Remove the key 's'
        unset($args['s']); 
    }

    // Handle the 'when' filter
    if (isset($args['pa_fechas-de-experiencia']) && !empty($args['pa_fechas-de-experiencia'])) {

        // Get the experiences by date
        $when   = sanitize_text_field($args['pa_fechas-de-experiencia']);
        $result = filter_experiences_by_date($when);
        // No results, return false
        if (!$result) return false;
        $results[] = $result; 

        // Unset when taxonomy
        unset($args['pa_fechas-de-experiencia']); 
    }

    // Loop over taxonomies
    foreach($args as $key => $value) {

        if (empty($value)) continue; 

        // Get the experiences by taxonomy
        $key    = sanitize_text_field($key);
        $value  = sanitize_text_field($value);
        $result = filter_experiences_by_taxonomy($key, $value);
        // No results, return false
        if (!$result) return false;
        $results[] = $result; 
    }

    // Not empty array, intersect internal arrays
    if (!empty($results)) {
        $all_ids = array_reduce($results, function($carry, $item) {
            $carry = array_unique(array_merge($carry, $item)); 
            return $carry; 
        }, array());

        $results = array_reduce($results, function($carry, $item) {
            $carry = array_intersect($carry, $item); 
            return $carry; 
        }, $all_ids);
    }

    return $results; 
}

/**
 *  Function to sort desc 
 *  @param object $a
 *  @param object $b
 *  @return boolean if priority in $a is less then $b's
 */
function priory_sort($a,$b) {
    return $a['priority'] < $b['priority'];
}


/**
 * Function to calculate the priority of a product category. 
 * @param integer $cant
 * @param integer $weight
 * @param float $alpha
 * @return float that means the priority.
 */
function calculate_priority($cant = 0, $weight = 0, $alpha = 0.5){

    return $alpha*$cant + (1-$alpha)*$weight;
}

/**
 * Get ALL Categories including its count and weight
 * It returns an array with all categories of product where each category has id, 
 * slug, count( amount of experiences published, useful value to calculate 
 * a priority), weight (useful value to calculate a priority) 
 * @return array $categories
 */
function get_categories_by_prior(){
        $alpha = 0.7;
        $categories = array();

        /* get ALL categories (including hide)*/
        $product_categories = get_terms('product_cat', array ('hide_empty' => 0));
        
        
        foreach ($product_categories as $category) {

            /* get custom field of category */
            $cm =  get_category_meta(false,get_term_by('slug',  $category->slug,'product_cat'));

            /* check if category has a valid weight */
            $cm = ($cm && preg_match("/^(?:0|[1-9][0-9]*)$/", $cm['weight'])) ? intval($cm['weight']) : 1;
            $args = array(
            'post_type' => 'product',
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'slug',
                    'terms'    => $category->slug
                )
            )
            );

            $query = new WP_Query( $args );

            /* append id, slug, experiences amount published according to this category and weight. */
            array_push($categories, array('id' => $category->term_id, 'name'=> $category->name, 'slug' => $category->slug, 'count' => $query->post_count, 'weight' => $cm , 'priority' => calculate_priority($query->post_count,$cm,$alpha) ));
            
    
    }

       
    usort($categories, 'priory_sort');


    return $categories;
}

/**
 * Show loop of custom taxonomy (showing badges for each)
 * @param string $taxonomy 
 * @return void
 */
function tarful_custom_taxonomy_loop($taxonomy) {
?>

	<div id="main-content" class="site-main">
	
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
	
				<?php get_template_part('content','main-slider'); ?>
	
				<section>
	
					<ul class="products" style="margin: 0px;">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-main-taxonomies">
							<?php
							if ( get_query_var( 'paged' ) )
								$paged = get_query_var('paged');
							else if ( get_query_var( 'page' ) )
								$paged = get_query_var( 'page' );
							else
								$paged = 1;
							
							$per_page	 = 10;
							$count_taxonomies = count( get_terms( $taxonomy ) );
							$offset		= $per_page * ( $paged - 1) ;
							
							$args = array(
								'order'		  => 'ASC',
								'orderby'		=> 'menu_order',
								'offset'		 => $offset,
								'number'		 => $per_page,
							);
							
							$taxonomies = get_terms( $taxonomy, $args );

							switch ( $taxonomy ) {

								case 'product_cat' :
	
									foreach ($taxonomies as $category) {
										$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
										$temp_image = wp_get_attachment_url( $thumbnail_id );
										$temp_link = get_term_link($category);
										if($temp_image){
										?>
										<li class="product">
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 product-grid">
												<a class="loop_taxonomy" href="<?php echo $temp_link; ?>">
													<img src="<?php echo $temp_image; ?>" alt="">
												</a>
											</div>
										</li>
									<?php } 
										}
		
									break;
			
								case 'localidad' :
	
									foreach($taxonomies as $category) { 
										$temp_p = get_category_meta(false, get_term_by('id',  $category->term_id , $taxonomy));
										$temp_image = $temp_p['image'];
										$temp_link = get_term_link($category);
										?>
										<li class="product">
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 product-grid">
												<a class="loop_taxonomy" href="<?php echo $temp_link; ?>">
													<img src="<?php echo $temp_image; ?>" alt="">
												</a>
											</div>
										</li>
									<?php } 
									
									break;
							} ?>			
														
						</div>					
					</ul>		
					
					<div class="woocommerce-pagination">
						<?php
						echo '<div style="clear:both;"></div>';	
						echo paginate_links( array(
							'format'  => '?paged=%#%',
							'current' => $paged,
							'total'	=> ceil( $count_taxonomies / $per_page )
						) );
						?>
					</div>
				
				</section>
			</div><!-- #content -->
		</div><!-- #primary -->
	</div><!-- #main-content -->
	

<?php	
}

/**
 * Sorts dates
 * @param string $a
 * @param string $b
 * @return integer $difference
 */
function tarful_sort_dates_function( $a, $b ){
	 return strtotime($a) - strtotime($b);
}

/**
 * Helper function to call tarful_sort_dates_funcion
 * @param array $array dates
 * @return array sorted dates
 */
function tarful_sort_dates( $array ){
	 uasort($array, "tarful_sort_dates_function");
	 return $array;
}


