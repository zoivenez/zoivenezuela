<?php

/**
 * Gift
 * @param integer $user_id
 * @return boolean, true on success, false on failure
 */

function sum_point($user_id){
    /* DEFINE FUNCTION */
}


/**
 * Function to redeem a gift
 * @param string $email of recent user
 * @param string $token
 * @param function $function award function
 * @return boolean, true on success, false on failure
 */
function give_gift_user($email,$token,$function = null){
    $meta_value = '"'.$token.'"';
    $results = $GLOBALS['wpdb']->get_results( 'SELECT * FROM wp_usermeta WHERE meta_key = "invitations" and meta_value LIKE "%'.addslashes($meta_value).'%"');
    if(empty((array)$results)) return false;
    $invite_email = unserialize($results{0}->meta_value)['email'];
    if($invite_email === $email){
        $sender_id = $results{0}->user_id;
        $invitation_id = $results{0}->id;
        if(delete_metadata_by_mid('user', $results{0}->umeta_id)){
            if($function)
                $function($sender_id); 
            return true;
        }

    }
    return false;
}
 
/**
 * Helper to redeem a gift
 * @param integer $customer_id 
 * @return void
 */
// XXX: Definir logica sobre invitacion de usuarios (Beneficios)
function wooc_give_gift_user_helper( $customer_id ) {
    if(isset($_POST['token'])){
        $token = sanitize_text_field($_POST['token']);
        $email = sanitize_text_field($_POST['email']);
        give_gift_user($email,$token, 'sum_point');
    }

}

add_action( 'woocommerce_created_customer', 'wooc_give_gift_user_helper');


/**
 * Function to create invite user form 
 * @return void
 */
function form_invite_user(){ ?>

    <form onsubmit=" return false;">
        <div class="form-group">
            <label for="inviteUserInput">Email del invitado</label>
            <input type="text" class="form-control" id="inviteUserInput" placeholder="leonardo@ejemplo.com">
        </div>
        <div>
            <a id="inviteUser" href="#" class="login-next-tab login-footer-btn" rel="1">Invitar</a>
        </div>
    </form>

    <script type="text/javascript">
        jQuery(function( $ ) {
            // On submit, recheck 
            $(document).on('click', '#inviteUser', function(event){
                //event.preventDefault(); 
                var email  = $('#inviteUserInput').val();
                if (email) {

                 //var user_login = email.substring(0, email.lastIndexOf("@"));

                    var ajaxUrl = "<?=admin_url('admin-ajax.php')?>";
                    
                    // Ajax call to server
                    $.ajax({
                        'type' : 'post',
                        'url'  : "<?=admin_url('admin-ajax.php')?>",
                        'data' : {
                            'action'          : 'manage_sendgrid_invite_user_helper',
                            'email'           : email
                        },
                        beforeSend: function(data, textStatus, jqXHR ){
                            var parentButton = $('#inviteUser').parent();
                            parentButton.html('<img id="inviteUser" src="<?=get_stylesheet_directory_uri()?>/images/loader.gif" alt="Enviando...">');
                        },
                        sucesss: function(data, textStatus, jqXHR ){
     
                        },
                        complete: function(data, dataStatus, jqXHR) {

                            var json = $.parseJSON(data.responseText);
                            var parentButton = $('#inviteUser').parent();
                            parentButton.html('<a id="inviteUser" href="#" class="login-next-tab login-footer-btn" rel="1">Invitar</a>');
                            parentButton.append('<span>'+ json.message + '<span>');

                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            var parentButton = $('#inviteUser').parent();
                            parentButton.html('<a id="inviteUser" href="#" class="login-next-tab login-footer-btn" rel="1">Invitar</a>');
                            parentButton.append('<span>Oh, ha ocurrido un error<span>');

                        }

                      });

                } else {

                    /* Email vacío. */
                }
            });
        });
    </script>
<?php   
}
