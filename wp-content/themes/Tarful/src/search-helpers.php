<?php 
/**
 * Performs a custom search setting wp_query and rendering results template
 * Hooked to action pre_get_posts
 * @param object $query previous query object
 * @return void
 */
function main_search( $query ) {

    global $wpdb, $wp_query; 

    // The query must come from the home page
    if (!$query->is_main_query()) return $query; 
    // Cannot be a query in the admin panel
    if (is_admin()) return $query; 
    // It has to be a search query
    if (!$query->is_search) return $query; 
    // Need at least another parameter in GET

    $post_ids = filter_experiences_by($_GET); 

    $qargs = array(
        'post_type' => 'product',
        'post__in'  => $post_ids
    );

    // Set post_ids
    $query->set('post__in', $post_ids);  
    
    return $query; 
}

add_action( 'pre_get_posts', 'main_search' ); 

/**
 * Suggest experiences by product and location
 * @param integer num_of_suggestions number of suggestion to show, default 3
 * @return $query
 */

function posts_suggestion($num_of_suggestions = 3){
    /* GET value of query */
    $product_ids = filter_experiences_by(array('product_cat' => get_query_var('product_cat'))); 
    $localidad_ids = filter_experiences_by(array('localidad' => get_query_var('localidad'))); 

    /* UNION both results */
    $union = array_unique(array_merge($localidad_ids , $product_ids));

    /* Extract only $num_of_suggestion products randomly */
    $num_of_suggestions = min(sizeof($union),$num_of_suggestions);
    $rand_suggestion = array_rand(array_flip($union), $num_of_suggestions);


   return (new WP_Query( array(
        'post_type' => 'product',
        'posts_per_page' => $num_of_suggestions,
        'post__in'  => $rand_suggestion
    )));
}

/**
 * Parses tax queries
 * @param object $query
 * @return void
 */
function tax_query_search( $query ) {

    if (!is_tax()) return $query; 

    $tax_obj = $query->get_queried_object(); 

    $tax_query = array(
        'taxonomy' => $tax_obj->taxonomy,
        'field' => 'slug',
        'terms' => $tax_obj->slug,
    );

    $query->tax_query->queries[] = $tax_query;
    $query->query_vars['tax_query'] = $query->tax_query->queries; 
}
