<?php

// Travelers setup
require_once('travelers.php'); 


/**
 * Validates whether another shipping method 
 * than 'reo' (recoger en oficina) exists in the cart
 * @return $exists_other boolean true if there's another
 */
function validate_shipping_type()
{
    $exists_other = false;
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $category = wp_get_post_terms( $_product->id, 'product_cat' );
        if($category[0]->name == "Producto"){
            $shipping_type = ($cart_item['variation']['attribute_pa_tipo_envio']);
            if($shipping_type !== "reo"){
                $exists_other = true;
            }
        }
    }

    return $exists_other;
}

// Hook checkout fields override
add_filter( 'woocommerce_checkout_fields' , 'tarful_custom_override_checkout_fields' );

// Override checkout fields 
// First unset the default ones and then create the new ones.
// We're including:
// - First name
// - Last name
// - Nationality
// - CI/RIF
// - Email
// - Phone
// - Address
// - City
// - State
// - Country
// - ZIP code
// - Genre
// - Birthdate
// - Comments
// @param object $fields
// @return void
function tarful_custom_override_checkout_fields( $fields ) {

    global $wpdb;

    header ('Content-type: text/html; charset=UTF-8');

    // Delete the deafult fields to create new ones for BILLING
    unset($fields['billing']['billing_first_name']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_nationality']);
    unset($fields['billing']['billing_cirif']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_email']);
    unset($fields['billing']['billing_phone']);
    unset($fields['billing']['shipping_address_1']);

    // Create new fields
    $fields['billing']['billing_first_name'] = array(
        'label'	  => __('Nombre', 'woocommerce'),
        'placeholder'	=> _x('Nombre', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6'),
        'clear'	  => false
    );

    $fields['billing']['billing_last_name'] = array(
        'label'	  => __('Apellido', 'woocommerce'),
        'placeholder'	=> _x('Apellido', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6'),
        'clear'	  => true
    ); 

    $fields['billing']['billing_nationality'] = array(
        'label'	  => __('V/E', 'woocommerce'),
        'required' => true,
        'class'	  => array('col-lg-1 col-md-1 billing-form'),
        'type' => 'select',
        'options' => array(
            'Venezolano' => 'V',
            'Extranjero' => 'E'
        ),

    );

    $fields['billing']['billing_cirif'] = array(
        'label'	  => __('Cédula', 'woocommerce'),
        'placeholder'	=> _x('Cédula o RIF', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-5 col-md-5 input-number-cond'),
        'clear'	  => false
    );

    $fields['billing']['billing_email'] = array(
        'label'	  => __('Email', 'woocommerce'),
        'placeholder'	=> _x('Email', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6 input-email'),
        'clear'	  => true
    );

    $fields['billing']['billing_phone']= array(
        'label'	  => __('Teléfono', 'woocommerce'),
        'placeholder'	=> _x('Teléfono móvil', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6 input-number'),
        'clear'	  => false
    );

    $fields['billing']['billing_phone_o']= array(
        'label'	  => __('Teléfono 2', 'woocommerce'),
        'placeholder'	=> _x('Téléfono fijo (Opcional)', 'placeholder', 'woocommerce'),
        'required'  => false,
        'class'	  => array('col-lg-6 col-md-6 input-number'),
        'clear'	  => true
    );

    $fields['billing']['billing_address_1']= array(
        'label'	  => __('Dirección 1', 'woocommerce'),
        'placeholder'	=> _x('Dirección 1', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6 '),
        'clear'	  => false
    );

    $fields['billing']['billing_address_2']= array(
        'label'	  => __('Dirección', 'woocommerce'),
        'placeholder'	=> _x('Dirección 2 (opcional)', 'placeholder', 'woocommerce'),
        'required'  => false,
        'class'	  => array('col-lg-6 col-md-6 '),
        'clear'	  => true
    );

    $fields['billing']['billing_city'] = array(
        'label'	  => __('Ciudad', 'woocommerce'),
        'placeholder'	=> _x('Ciudad', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6 '),
        'clear'	  => false
    );

    $fields['billing']['billing_state'] = array(
        'label'	  => __('Estado', 'woocommerce'),
        'placeholder'	=> _x('Estado', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6 '),
        'clear'	  => true
    );

    $fields['billing']['billing_country'] = array(
        'label'	  => __('País', 'woocommerce'),
        'placeholder'	=> _x('Venezuela', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6 '),
        'clear'	  => false
    );

    $fields['billing']['billing_postcode'] = array(
        'label'	  => __('Código postal', 'woocommerce'),
        'placeholder'	=> _x('código postal', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'	  => array('col-lg-6 col-md-6 '),
        'clear'	  => true
    );

    $fields['billing']['billing_genre'] = array(
        'label'	  => __('Genre'),
        'required'  => true,
        'class'	  => array('billing-form col-lg-6 col-md-6 '),
        'type' => 'select',
        'options' => array(
            'masculino' => 'Masculino',
            'femenino' => 'Femenino'
        ),
    );

    $fields['billing']['billing_birthdate'] = array(
        'label'	  => __('Fecha de nacimiento', 'woocommerce'),
        'required'  => true,
        'class'	  => array('regular-date-picker input-date col-lg-6 col-md-6 '),
        'clear' => true ,
        'placeholder' => 'dd/mm/yyyy'
    );

    if(validate_shipping_type()){

        $fields['billing']['shipping_address_1'] = array(
        'type'          => 'textarea',
        'label'         => __('Direccion de Envio - Tienes que llenarlo si algún producto no es retirado en oficina', 'woocommerce'),
        'required'      => true,
        'class'         => array('col-lg-12 col-md-12   '),
        'clear'         => true ,
        'placeholder'   => 'direccion de envio'
        );
    }
    

    return $fields;
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'tarful_custom_checkout_field_display_admin_order_meta', 10, 1 );

/**
 * Display custom checkout fields on the order edit page
 * Hooked to action woocommerce_admin_order_data_after_billing_address
 * @param object $order
 * @return void
 */
function tarful_custom_checkout_field_display_admin_order_meta($order){
	echo '<p><strong>'.__('Cédula o RIF').':</strong><br />' . get_post_meta( $order->id, '_billing_cirif', true ) . '</p>';
}


add_action('woocommerce_checkout_update_order_meta', 'update_shipping_order_address'); 
/**
 * Updates the orders information to add the shipping address to its meta data.
 * Hooked to action woocommerce_checkout_update_order_meta
 * @param integer $order_id
 * @return void
 */
function update_shipping_order_address($order_id){
    update_post_meta($order_id, 'shipping_address',  sanitize_text_field($_POST['shipping_address_1'])); 
}


add_action('woocommerce_admin_order_data_after_shipping_address', 'display_custom_client_order_shipping_address');

/**
 * Display the buyers shipping address in the admins view.
 * @param object $order
 * @return void
 */
function display_custom_client_order_shipping_address( $order ){
    $shipping_address = get_post_meta( $order->id, 'shipping_address', true );
    echo '<div class="order_data_column" style="width:100%;">';
        echo '<h4>Direccion de envio</h4><hr>';
        echo ($shipping_address === "") ? "Retiro en oficina" : $shipping_address;               
    echo '</div>';

}

