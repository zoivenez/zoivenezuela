<?php
/**
 * Update database with values for my account forms.
 * This function checks the $_POST variable to store all the personal info
 * in the database or make validations
 * @return void
 */
function tarful_update_user_personal_info() {

	global $wpdb, $current_user;
	
	if (isset($_POST['updateuser'])) {
		
		if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
			if ( $_POST['pass1'] == $_POST['pass2'] ) {
				wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] )));
			}	
		}
	
		if ( !empty( $_POST['first-name'] )) {
			update_user_meta( $current_user->ID, 'billing_first_name', esc_attr( $_POST['first-name'] ) );
			update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
		}
		if ( !empty( $_POST['last-name'] )) {
			update_user_meta( $current_user->ID, 'billing_last_name', esc_attr( $_POST['last-name'] ) );
			update_user_meta( $current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
		}
		if ( !empty( $_POST['billing_nationality'] )) {
			update_user_meta( $current_user->ID, 'billing_nationality', esc_attr( $_POST['billing_nationality'] ) );
		}
		if ( !empty( $_POST['cedula'] )) {
			update_user_meta( $current_user->ID, 'billing_cirif', esc_attr( $_POST['cedula'] ) );
		}
		if ( !empty( $_POST['email'] )) {
			update_user_meta( $current_user->ID, 'billing_email', esc_attr( $_POST['email'] ) );
			wp_update_user( array( 'ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
		}
		if ( !empty( $_POST['phone1'] )) {
			update_user_meta( $current_user->ID, 'billing_phone', esc_attr( $_POST['phone1'] ) );
		}
		if ( !empty( $_POST['phone2'] )) {
			update_user_meta( $current_user->ID, 'billing_phone_o', esc_attr( $_POST['phone2'] ) );
		}
		if ( !empty( $_POST['address1'] )) {
			update_user_meta( $current_user->ID, 'billing_address_1', esc_attr( $_POST['address1'] ) );
		}
		if ( !empty( $_POST['address2'] )) {
			update_user_meta( $current_user->ID, 'billing_address_2', esc_attr( $_POST['address2'] ) );
		}
		if ( !empty( $_POST['city'] )) {
			update_user_meta( $current_user->ID, 'billing_city', esc_attr( $_POST['city'] ) );
		}
		if ( !empty( $_POST['state'] )) {
			update_user_meta( $current_user->ID, 'billing_state', esc_attr( $_POST['state'] ) );
		}
		if ( !empty( $_POST['country'] )) {
			update_user_meta( $current_user->ID, 'billing_country', esc_attr( $_POST['country'] ) );
		}
		if ( !empty( $_POST['postcode'] )) {
			update_user_meta( $current_user->ID, 'billing_postcode', esc_attr( $_POST['postcode'] ) );
		}
		if ( !empty( $_POST['billing_birthdate'] ) ) {
			update_user_meta( $current_user->ID, 'billing_birthdate', esc_attr( $_POST['billing_birthdate'] ) );
		}
		if ( !empty( $_POST['billing_genre'] ) ) {
			update_user_meta( $current_user->ID, 'billing_genre', esc_attr( $_POST['billing_genre'] ) );
		}
		
		do_action('edit_user_profile_update', $current_user->ID);
	}
		
}

add_filter('show_admin_bar', '__return_false');
add_filter('woocommerce_customer_meta_fields','tarful_customer_meta_fields');
/**
 * This function adds new customer meta fields for woocommerce.
 * It's called by woocommerce_customer_meta_fields hook
 * @return void
 */
function tarful_customer_meta_fields(){
	return array(
			'billing' => array(
				'title' => __( 'Customer Billing Address', 'woocommerce' ),
				'fields' => array(
					'billing_first_name' => array(
						'label'       => __( 'First name', 'woocommerce' ),
						'description' => ''
					),
					'billing_last_name' => array(
						'label'       => __( 'Last name', 'woocommerce' ),
						'description' => ''
					),
					'billing_cirif' => array(
						'label' => 'Cedula',
						'description' => ''
					),
					'billing_company' => array(
						'label'       => __( 'Company', 'woocommerce' ),
						'description' => ''
					),
					'billing_address_1' => array(
						'label'       => __( 'Address 1', 'woocommerce' ),
						'description' => ''
					),
					'billing_address_2' => array(
						'label'       => __( 'Address 2', 'woocommerce' ),
						'description' => ''
					),
					'billing_city' => array(
						'label'       => __( 'City', 'woocommerce' ),
						'description' => ''
					),
					'billing_postcode' => array(
						'label'       => __( 'Postcode', 'woocommerce' ),
						'description' => ''
					),
					'billing_country' => array(
						'label'       => __( 'Country', 'woocommerce' ),
						'description' => '',
						'class'       => 'js_field-country',
						'type'        => 'select',
						'options'     => array( '' => __( 'Select a country&hellip;', 'woocommerce' ) ) + WC()->countries->get_allowed_countries()
					),
					'billing_state' => array(
						'label'       => __( 'State/County', 'woocommerce' ),
						'description' => __( 'State/County or state code', 'woocommerce' ),
						'class'       => 'js_field-state'
					),
					'billing_phone' => array(
						'label'       => __( 'Telephone', 'woocommerce' ),
						'description' => ''
					),
					'billing_email' => array(
						'label'       => __( 'Email', 'woocommerce' ),
						'description' => ''
					),
					'billing_edad' => array(
						'label'       => __( 'Edad', 'woocommerce' ),
						'description' => ''
					),
					'billing_fechnac_dia' => array(
						'label'       => __( 'Fecha de Nacimiento', 'woocommerce' ),
						'description' => ''
					),
					'billing_fechnac_mes' => array(
						'label'       => __( 'Fecha de Nacimiento', 'woocommerce' ),
						'description' => ''
					),
					'billing_fechnac_anno' => array(
						'label'       => __( 'Fecha de Nacimiento', 'woocommerce' ),
						'description' => ''
					)
				)
			),
			'shipping' => array(
				'title' => __( 'Customer Shipping Address', 'woocommerce' ),
				'fields' => array(
					'shipping_first_name' => array(
						'label'       => __( 'First name', 'woocommerce' ),
						'description' => ''
					),
					'shipping_last_name' => array(
						'label'       => __( 'Last name', 'woocommerce' ),
						'description' => ''
					),
					'billing_cirif' => array(
						'label' => 'Cedula',
						'description' => ''
					),
					'shipping_company' => array(
						'label'       => __( 'Company', 'woocommerce' ),
						'description' => ''
					),
					'shipping_address_1' => array(
						'label'       => __( 'Address 1', 'woocommerce' ),
						'description' => ''
					),
					'shipping_address_2' => array(
						'label'       => __( 'Address 2', 'woocommerce' ),
						'description' => ''
					),
					'shipping_city' => array(
						'label'       => __( 'City', 'woocommerce' ),
						'description' => ''
					),
					'shipping_postcode' => array(
						'label'       => __( 'Postcode', 'woocommerce' ),
						'description' => ''
					),
					'shipping_country' => array(
						'label'       => __( 'Country', 'woocommerce' ),
						'description' => '',
						'class'       => 'js_field-country',
						'type'        => 'select',
						'options'     => array( '' => __( 'Select a country&hellip;', 'woocommerce' ) ) + WC()->countries->get_allowed_countries()
					),
					'shipping_state' => array(
						'label'       => __( 'State/County', 'woocommerce' ),
						'description' => __( 'State/County or state code', 'woocommerce' ),
						'class'       => 'js_field-state'
					)
				)
			)
		);
}

/**
 * This function updates the user's meta profile fields
 * checking the $_POST variable when the request is made.
 * It's called by the following actions 
 * - show_user_profile
 * - edit_user_profile
 * - personal_options_update
 * - edit_user_profile_update
 *
 * @param integer $user_id
 * @return void
 */
function tarful_save_custom_user_profile_fields( $user_id ) {
	
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
	update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
	update_user_meta( $user_id, 'instagram', $_POST['instagram'] );
	delete_user_meta( $user_id, 'description');
	add_user_meta( $user_id, 'description' ,$_POST['tf_description'] );
}

add_action( 'show_user_profile', 'tarful_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'tarful_custom_user_profile_fields' );

add_action( 'personal_options_update', 'tarful_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'tarful_save_custom_user_profile_fields' );
