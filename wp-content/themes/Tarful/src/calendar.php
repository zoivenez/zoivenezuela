<?php
add_filter('wp_footer', 'fullpage_calendar_setup'); 
/**
 * Adds fullpage calendar js and stylesheet
 * @return void
 */
function fullpage_calendar_setup() {
    if (is_product()) :
?>
<script type="text/javascript">
    jQuery(function($){

        // Call to return
        $(document).on('click', '.ui-datepicker-month', function(){
            window.location.href = '<?=get_bloginfo('url'). '/calendar'?>'
        });

        $(document).on('click', '.ui-datepicker-year', function(){
            window.location.href = '<?=get_bloginfo('url'). '/calendar'?>'
        });
    })
</script>
<?php endif; 
}
