<?php
/**
 * The template for displaying Author bios
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

$redes_sociales = get_user_meta(get_the_author_meta( 'ID'));

?>

<div class="author-info ">
	<div class="author-avatar col-lg-12 no-padding">

		<?php echo get_avatar( get_the_author_meta( 'user_email' ), 50 );?>

		<div class="col-xs-8 no-padding">
			<h4 class="author-title"><?php echo get_the_author(); ?></h4> 
			<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
			<p><?php echo count_user_posts(get_the_author_meta( 'ID' )); ?> artículos</p>
			</a>
		
			<div class="blog-social">
				<a href="<?php if($redes_sociales['facebook'][0]) echo $redes_sociales['facebook'][0]; else echo '#'; ?>">
					<img width="15" src="<?php  echo bloginfo('stylesheet_directory') ?>/images/fb.png" alt="">
				</a>
				<a href="<?php if($redes_sociales['twitter'][0]) echo $redes_sociales['twitter'][0]; else echo '#'; ?>">
					<img width="15" src="<?php  echo bloginfo('stylesheet_directory') ?>/images/tw.png" alt="">
				</a>
				<a href="<?php if($redes_sociales['instagram'][0]) echo $redes_sociales['instagram'][0]; else echo '#'; ?>">
					<img width="15" src="<?php  echo bloginfo('stylesheet_directory') ?>/images/in.png" alt="">
				</a>
			</div>
		</div>


	</div><!-- .author-avatar -->

	<div class="author-description col-xs-12 col-lg-12 no-padding">
		<p class="author-bio">
			<?php the_author_meta( 'description' ); ?>
		</p><!-- .author-bio -->
	</div><!-- .author-info -->
</div>
