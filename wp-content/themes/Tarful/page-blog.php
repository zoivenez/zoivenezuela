<?php
/**
 * Template Name:Fullwidth blog
 *
 * by Tarful
 *
 */

get_header();
$cat_border_color;
$j=0;
?>

<div id="main-content" class="site-main page-blog">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<section>
				<div class="archive-banner archive-banner-blog" style="width:100%; height: auto;">
					<img src="<?php echo CFS()->get('blog_main',33); ?>" alt="">
					<div class="shadow"></div>
					<div class="contenedor-caption">
						<div class="ajustador-caption contenedor-centrado-vertical" style="text-align: center;">
							<div class="centrado-vertical">
								<h1><?php echo CFS()->get('blog_tagline',33);?></h1>
							</div>
						</div>
					</div>
					<?php category_menu(); ?>
				</div>

				<div class="limit">

					<div class="list-blog">
					<?php
						$args = array(
							'posts_per_page'   => 8,
							'offset'           => 0,
							'paged' => get_query_var('paged') ? get_query_var('paged') : 1,
							'orderby'          => 'date',
							'order'            => 'DESC',
							'post_type'        => 'post',
							'post_status'      => 'publish',
						);
						$posts_array = new WP_Query( $args );
						$num_item = 0;

					    while ( $posts_array->have_posts() ) : $posts_array->the_post(); 

							$float = "";
							switch ($num_item) {
								case '0':
									$col = 7;
									if($args["paged"] % 2){ $float = 'float: right;'; }
									$item_size = 'item-size-larga';
								break;

								case '1':
								case '2':
									$col = 5;
									$item_size = 'item-size-corta';
								 break;

								case '3':
								case '4':
									$col = 6;
									$item_size = 'item-size-medio';
								break;

								case '5':
								case '6':
								case '7':
									$col = 4;
									$item_size = 'item-size-tercio';
									if ($num_item == 7){ $num_item = 0; }
								break;
								
								default:
									echo "entro en default";
							 	break;
							}
							blog_post_replace( $col, $float, $item_size);
							$num_item ++;
						endwhile;
						?>
						<div class="blogs-pagination"> 
							<?php next_posts_link( 'Older Entries', $posts_array->max_num_pages ); ?>
						</div>
						<?php wp_reset_postdata();?>
					</div>
				</div>
			</section>
		</div><!-- .site-main -->
	</div><!-- .content-area -->
</div>
<?php get_footer(); ?>
