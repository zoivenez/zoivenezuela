<?php
/**
 * Template Name: Full Width Gurus
 *
 * by Tarful
 *
 */
get_header(); ?>

	<div id="page-gurus" class="row page-gurus">
		<div class="overlay" style="display:none;">
			<div class="modal_perfil">
				<div class="contenedor_guru">
					<div class="caption_banner_guru">
						<div class="caption_guru">
							<div class="content-guru">
								<div class="contenedor_avatar"></div>
								<div class="guru-name"></div>
								<div class="list-category-guru"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="content_guru">
					<div class="contenido"></div>
					<div class="prefooter-modal_guru row">
						<div class="col-xs-12">
							<div class="firma"></div>
							<div class="social-gurus"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="footer-modal_guru">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 imgs_full_width contenedor_banner_principal" style="margin-bottom:7%;">
				<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
				<div class="caption_banner_guru">
					<div class="caption_guru" style="max-width:20%;">
						<div class="content-guru">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/zoiguru.png" >
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="site-content">
			<div class="row listado_gurus">
				<?php
					$gurus = get_users( array('role' => 'gurú') );
				?>
				<script type="text/javascript">
					var perfil = {};
				</script>
				<?php foreach ($gurus as $guru) {
						$perfiles = new WP_Query( array('post_type' => 'post_perfil', 'meta_query' => array( array( 'key' => 'guru', 'value' => $guru->ID, 'compare' => '=') ) ) );
						//buscar productos relacionados a cada GURÚ
						$products = new WP_Query( array('post_type' => 'product', 'meta_query' => array( array( 'key' => 'guru', 'value' => $guru->ID, 'compare' => '=') ) ) );
						
					    if ( $perfiles->have_posts() ) : $perfiles->the_post();
							$single_cat = get_the_category();

							if(isset($single_cat) && is_array($single_cat)){
								foreach ($single_cat as $key => $value) {
									$single_cat[$key] = $value->slug;
								}
							}
					?>
					<div class="col-xs-12 col-sm-6">
						<div class="contenedor_guru" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>);">
							<div class="caption_banner_guru">
								<div class="caption_guru">
									<div class="content-guru">
										<div class="contenedor_avatar">
										<?php
											$avatar = get_avatar( $guru->ID, 100, get_bloginfo('stylesheet_directory').'/images/img_user.png', $guru->display_name );
											echo $avatar;
										?>
										</div>
										<div class="guru-name"><?php echo $guru->display_name;  ?></div>
										<a class="btn_avalaible perfil_link perfil_link-<?php echo $guru->ID?>" href="#" data-perfilid="<?php echo $guru->ID?>"><?php echo _('Ver perfil');?></a>
										<script type="text/javascript">
											perfil["<?php echo $guru->ID; ?>"] = {
												'contenido' : <?php echo json_encode(get_the_content()); ?>,
												'nombre' : "<?php echo $guru->display_name; ?>",
												'categoria' : <?php echo json_encode($single_cat); ?>,
												'firma' : "<?php echo $guru->first_name; ?>",
												'avatar' : "<?php echo $avatar; ?>",
												'link_selector' : ".perfil_link-<?php echo $guru->ID?>",
												'products' : '<?php echo $products->found_posts; ?>'
											};
										</script>
									</div>
									<div class="guru-products">
										<?php if(isset($products->found_posts) && ($products->found_posts)){ ?>
										<a href="<?php echo get_bloginfo('url'); ?>/experiencias/?by_guru=<?php echo $guru->ID;?>">
											TIENE <?php echo $products->found_posts; ?> EXPERIENCIA(S) DISPONIBLES. ¡CONóCELAS
										</a>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<?php } ?>
			</div>
			<div class="row" style="margin-top: 9%;">
				<div class="hidden-xs col-sm-1"></div>
				<div class="col-xs-6 col-sm-2">
					<div class="item-estadistica">
						<div class="dato-seccion-guru"><?php tarful_team_users_count(); ?></div>
						<div class="info-seccion-guru"><?php _e('EQUIPO ZOI'); ?></div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-2">
					<div class="item-estadistica">
						<?php $count_experiences = wp_count_posts('product'); ?>
						<div class="dato-seccion-guru"><?php echo $count_experiences->publish; ?></div>
						<div class="info-seccion-guru"><?php _e('EXPERIENCIAS ZOI'); ?></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-2">
					<div class="item-estadistica">
						<img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/zoi-logo-light.png">
					</div>
				</div>
				<div class="col-xs-6 col-sm-2">
					<div class="item-estadistica">
						<div class="dato-seccion-guru"><?php echo count($gurus); ?></div>
						<div class="info-seccion-guru"><?php _e('GURÚS ZOI'); ?></div>
					</div>
				</div>
				<div class="col-xs-6 col-sm-2">
					<div class="item-estadistica">
						<div class="dato-seccion-guru"><?php tarful_users_count(); ?></div>
						<div class="info-seccion-guru">COMUNIDAD ZOI</div>
					</div>
				</div>
				<div class="hidden-xs col-sm-1"></div>
			</div>
			<div class="row" style="margin: 0px -34px;">
				<div class="col-xs-12 testimonial-featured">
					<?php get_template_part('content','testimonial-featured'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<?php tarful_zoi_prefooter(); ?>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function () {
			enderizeTarfulToHeaderCall($('.page-gurus'));
			//enderizeTarfulHeightCall('.page-gurus .listado_gurus .contenedor_guru', 175);
			//enderizeTarfulHeightCall('.page-gurus .overlay .contenedor_guru',230);
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function () {
			$("a.perfil_link").click(function(e){
				e.preventDefault();
				var link = $(this);
				$(".modal_perfil .contenedor_guru").css("background-image",link.parents(".contenedor_guru").first().css("background-image"));
				$(".modal_perfil .contenedor_guru .guru-name").text(perfil[link.data("perfilid")]["nombre"]);
				$(".modal_perfil .contenedor_avatar").html(perfil[link.data("perfilid")]["avatar"]);

				if( perfil[link.data("perfilid")]["categoria"].length ){
					var categorias = "";
					for (var i = 0; i < perfil[link.data("perfilid")]["categoria"].length; i++) {
						categorias += "<div class=\"row\"><div class=\"category\">" + perfil[link.data("perfilid")]["categoria"][i] + "</div></div>";
					}
				}
				$(".modal_perfil .list-category-guru").html(categorias);
				$(".modal_perfil .contenido").html(perfil[link.data("perfilid")]["contenido"]);
				$(".modal_perfil .firma").html("- "+perfil[link.data("perfilid")]["firma"]);
				if(perfil[link.data("perfilid")]["products"]){
					$(".modal_perfil .footer-modal_guru").html("<a class='avalaible' href=\"<?php echo get_bloginfo('url'); ?>/experiencias/?by_guru="+link.data("perfilid")+"\">TIENE "+perfil[link.data("perfilid")]["products"]+" EXPERIENCIA(S) DISPONIBLES</a>");
				}else{
					$(".modal_perfil .footer-modal_guru").empty();
				}
				$(".overlay").fadeIn();
			});
			$(".overlay").on("click",function(e){
				if($(e.target).hasClass("overlay")){
					$(".overlay").fadeOut();
				}
			});
			<?php if(isset($_GET['id_guru'])){ ?>
				$("a.perfil_link-<?php echo $_GET['id_guru']; ?>").trigger("click");
			<?php } ?>
		});
	</script>
<?php get_footer(); ?>
