<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
	<div id="primary" class="content-area site-content pagenotfound">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyfifteen' ); ?></h1>
                    <center>
                        <a href="<?=get_bloginfo('url')?>" class="btn-danger btn btn-zoi gobacklink" style="">Ir a Experiencias</a>
                    </center>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <center>
                        <img src="<?=bloginfo('stylesheet_directory');?>/images/404.png"/>
                    </center>
                </div>
            </div>
	</div><!-- .content-area -->

<?php get_footer(); ?>
