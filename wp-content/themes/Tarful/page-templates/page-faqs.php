<?php
/**
 * Template Name: Content + Sidebar - FAQ's
 *
 * by Tarful
 *
 */

get_header(); 

$current_featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
$current_featured_img = $current_featured_img[0];
?>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-page-header" style="background: url('<?php echo $current_featured_img; ?>') no-repeat center top">

		<h1><?php the_title(); ?></h1>
		
	</div>

	<div style="clear:both;"></div>

	<div id="main-content" class="site-main limit">
	
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

				<section>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mcenter limit">				
						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 box-page-content">
							<?php
							while ( have_posts() ) : the_post();
								the_content();
							endwhile;
							?>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 faqs-box">
								<ul class="accordion">
									<?php
									$faqs_loop = CFS()->get('faqs_loop');
									foreach ($faqs_loop as $faq) { ?>
										<li>
										<input type="checkbox" checked>
										<h3><?php echo $faq['faqs_title']; ?></h3>
										<i></i>
										
										<div class="content-accordion"><?php echo $faq['faqs_content']; ?></div>
										</li>
									<?php } ?>
								</ul>
							</div>		
						</div>
										
						<?php get_template_part( 'sidebar' ); ?>
					</div>
	
					<div style="clear:both;"></div>

				</section>
			</div><!-- #content -->
		</div><!-- #primary -->
	</div><!-- #main-content -->

	<script>
	$(document).ready(function(){
		var aux; 
		var tit; 
		$('.faqs-box li').each(function(){

			aux = $(this).find("input");

			$(aux).click(function(){
				var aux2 = $(this).next("h3");
				
				if ( $(aux2).hasClass('colored') ) {
					$(this).next("h3").removeClass("colored");
				}else{
					$(this).next("h3").addClass("colored");
				};
				 
			});
		});


	});

	</script>

<?php get_footer(); ?>
