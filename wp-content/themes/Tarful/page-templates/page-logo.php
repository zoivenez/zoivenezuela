<?php
/**
 * Template Name: Logo Template
 * @author Juan Carlos Arocha
 * @description page template to show the company's logos
 */

get_header();

$logos = CFS()->get('logos');
$data_images = array(); 
foreach($logos as $logo) {
    $data_logo = array(); 

    $data_logo['small']  = $logo['small-logo'];
    $data_logo['medium'] = $logo['medium-logo'];
    $data_logo['big']    = $logo['big-logo'];

    $data_images[] = $data_logo; 
}


?>

<div class="site-content">
<div class="row" id="logo-grid" data-size="<?=esc_attr(sizeof($data_images))?>" data-images="<?=esc_attr(json_encode($data_images))?>">
<?php 
    $logos = CFS()->get('logos');
    foreach($logos as $pos => $logo): ?>
        <div class="col-xs-12 col-sm-4">
            <div class="thumbnail">
                <img src="<?=$logo['medium-logo']?>"/>
                <div class="caption">
                    <h3>Logo #<?=$pos + 1?></h3>
                    <div class="form-group form-inline">
                        <label for="size" style="display:block">Tamaño</label>
                        <select  class="download-logo-select form-control" data-pos=<?=$pos?>>
                            <option value="small">Pequeño ( <?=CFS()->get('small-dimmensions')?> )</option>
                            <option value="medium">Mediano ( <?=CFS()->get('medium-dimmensions')?> )</option>
                            <option value="big">Grande ( <?=CFS()->get('big-dimmensions')?> )</option>
                        </select>
                        <a download="logo-zoivenezuela.png" href="<?=$logo['small-logo']?>" target="_blank" id="download-logo-link-<?=$pos?>" class="btn btn-danger btn-zoi download-logo">Descargar</a>
                    </div>
                </div>
            </div>
        </div>
<?php endforeach; ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(function( $ ) {

        var $buttonHeight = $('.download-logo').first().height(); 
        $('.download-logo-select').height($buttonHeight); 
        var $nLogos    = $('#logo-grid').data('size');
        var $imageUrls = $('#logo-grid').data('images');

        $(document).on('change', '.download-logo-select', function(event){
            var $position   = $( this ).data('pos');
            var $size       = $( this ).val();
            // Get chosen image url
            var $imageUrl  = $imageUrls[parseInt($position)][$size];
            $('#download-logo-link-' + $position).attr('download', 'logo-zoivenezuela.png'); 
            $('#download-logo-link-' + $position).attr('href', $imageUrl); 
            $('#download-logo-link-' + $position).attr('target', '_blank'); 
        });
    }); 
</script>
