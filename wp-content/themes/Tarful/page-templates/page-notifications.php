<?php
/**
 * Template Name: Notifications
 *
 * by Tarful
 *
 */

/*  If user is not logged, redirect to register page  */
$user = wp_get_current_user();

	if(0 == $user->ID){
		
		wp_redirect(get_bloginfo('url').'/mi-cuenta'); exit;
	}


get_header(); ?>



<div id="main-content" class="site-main limit">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<section>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mcenter limit ">				
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 box-page-content">
						<div class="container">
							<div class="jumbotron unsubscribe">
													<!-- Nav tabs -->

								<ul class="nav nav-tabs col-sm-9" id="notificationTabs" role="tablist">
									<li role="presentation" class="active"><a href="#frequency" aria-controls="frequency" role="tab" data-toggle="tab">Frecuencia</a></li>
									<li role="presentation"><a href="#interests" aria-controls="interests" role="tab" data-toggle="tab">Intereses</a></li>
								</ul>

								<a  href="<?=get_bloginfo('url').'/mi-cuenta'?>" style="float: right" class="col-sm-3">Ir a mi cuenta</a>

								<br>
								<!-- Tab panes -->
								<div class="tab-content">

								<!-- FREQUENCY -->
									<div role="tabpanel" class="tab-pane fade in active" id="frequency">
									<h1> Deseo recibir correos :</h1>
									<form onsubmit=" return false;">
										<div class="row">
											<div class="options col-xs-12 col-sm-6 col-md-6">
											<!--
												<div class="radio">
												  <label>
												    <input type="radio" value="daily"  name="frequency" id="option2" checked>
												    Diariamente
												  </label>
												</div>-->
												<div class="radio">
												  <label>
												    <input type="radio" value="weekly"  name="frequency" id="option3" checked>
												    Semanalmente
												  </label>
												</div>
												<div class="radio">
												  <label>
												    <input type="radio" value="monthly"  name="frequency" id="option4">
												    Mensualmente
												  </label>
												</div>
												<div class="radio">
												  <label>
												    <input type="radio" value="never"  name="frequency" id="option1">
												    Nunca
												  </label>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-6 textarea">
												<h5>¿Por qué no deseas recibir notificaciones?</h5>
												<textarea placeholder="Comentario" id="comment" name="comment"></textarea>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-6 description">
												<h5></h5>
												<p class="description-frequency" >
												</p>
											</div>
										</div>
										<div>
								            <a id="updateFrequency" style="float:none;" href="#" class="btn btn-notification btn_avalaible" >Aplicar</a>
								        </div>
									</form>

									</div>
									<!-- END FREQUENCY -->

									<!-- INTERESTS -->
									<div role="tabpanel" class="tab-pane fade" id="interests">
										<h1> Deseo recibir correos sobre :</h1>
												<form onsubmit=" return false;">
												<div class="container">
													<div class="row">
													<div class="options col-xs-12" style="text-align: left; margin-bottom: 1.4em;">
													  <label>
													    <input type="checkbox" value="all" name="allCategory"  id="allCategory">
													   <strong>Seleccionar Todos</strong>
													  </label>
													</div>


														<?php


														$product_categories = get_categories_by_prior();

														foreach ($product_categories as $category) {
															
														?>

															<div class="options col-xs-6 col-md-4 col-lg-4">
															  <label>
															    <input type="checkbox" value="<?php echo $category['slug'] ?>" name="category">
															    <?php echo $category['name']?>
															    
															  </label>
															</div>
															
													
													<?php } ?>

													</div>
												</div>
												<div>
												<a id="updateInterest" href="#" style="float:none;" class="btn btn-notification btn_avalaible" >Aplicar</a>
												</div>

											</form>

									</div>
									<!-- END INTERESTS -->
								</div>
								<script type="text/javascript">
									$('#notificationTabs a').click(function (e) {
										  e.preventDefault()
										  $(this).tab('show')
										});

									$("#allCategory").click(function(){

									    $('input[type=checkbox][name="category"]').prop('checked', this.checked);   
									});

									$(document).ready(function() {
										/* LOAD FREQUENCY SETTINGS */
										<? $frequency = get_meta_notifications( wp_get_current_user()->ID,'frequency');
										 ?>
										$('input[type=radio][name="frequency"][value="<?php if($frequency) echo $frequency[0];?>"]').prop('checked', true);
										$('#comment').text("<?php if(sizeof($frequency)>1) echo $frequency[1]; ?>");

										/* LOAD INTERESTS SETTINGS */
										<? $interests = get_meta_notifications( wp_get_current_user()->ID,'interest');
											foreach($interests as $interest) {
												?>
												$('input[type=checkbox][name="category"][value="<?php echo $interest ?>"]').prop('checked', true);
												<?php
											}
										 ?>
									});

								</script>

								<!-- Frequency settings -->
								<script type="text/javascript">
									$(document).on('click', '#updateFrequency', function(event){
						                event.preventDefault(); 
						                var radio  = $('input[name=frequency]:checked').val();
						                if (radio) {

						                 //var user_login = email.substring(0, email.lastIndexOf("@"));
						                   var comment = $('#comment').val();
						                    

						                    var ajaxUrl = "<?=admin_url('admin-ajax.php')?>";
						                    
						                    // Ajax call to server
						                    $.ajax({
						                        'type' : 'post',
						                        'url'  : "<?=admin_url('admin-ajax.php')?>",
						                        'data' : {
						                            'action'          : 'add_meta_frequency_notifications_helper',
						                            'frequency'           : radio,
						                            'comment'			: comment
						                        },
						                        beforeSend: function(data, textStatus, jqXHR ){
						                            var parentButton = $('#updateFrequency').parent();
						                            parentButton.html('<img id="updateFrequency" style="float:none;" src="<?=get_stylesheet_directory_uri()?>/images/loader.gif" alt="Actualizando...">');
						                        },
						                        success: function(data, textStatus, jqXHR ){
						     
						                        },
						                        complete: function(data, dataStatus, jqXHR) {

						                            var json = $.parseJSON(data.responseText);
						                            
						                            var parentButton = $('#updateFrequency').parent();
						                            parentButton.html('<a id="updateFrequency" style="float:none;" href="#" class="btn btn-notification btn_avalaible col-xs-6" >Aplicar</a>');
						                            if(json.response)
						                            	parentButton.append('<span  style="float:none;" class="message-notification-sucess "><br>'+ json.message + '<span>');
						                            else
						                            	parentButton.append('<span style="float:none;" class="message-notification-error "><br>Oh, ha ocurrido un error<span>');

						                        },

						                        error: function(jqXHR, textStatus, errorThrown) {
						                            var parentButton = $('#inviteUser').parent();
						                            parentButton.html('<a id="updateFrequency" style="float:none;" href="#" class="btn btn-notification btn_avalaible" >Aplicar</a>');
						                            parentButton.append('<span  class="message-notification-error "><br> Oh, ha ocurrido un error<span>');

						                        }

						                      });

						                    
						                } else {

						                   
						                }

						            });
								</script>
								<!-- END Frequency settings -->

								<!-- Interest settings -->
								<script type="text/javascript">
									$(document).on('click', '#updateInterest', function(event){
						                event.preventDefault(); 
						                var checkboxes  = $('input[name=category]:checked').map(function() {
											    return this.value;
											}).get();
						                
						                if (checkboxes) {
						                 
						                 var jsonString = JSON.stringify(checkboxes);
						                    

						                    var ajaxUrl = "<?=admin_url('admin-ajax.php')?>";
						                    
						                    // Ajax call to server
						                    $.ajax({
						                        'type' : 'post',
						                        'url'  : "<?=admin_url('admin-ajax.php')?>",
						                        'data' : {
						                            'action'          : 'add_meta_interest_notifications_helper',
						                            'category'           : jsonString
						                        },
						                        beforeSend: function(data, textStatus, jqXHR ){
						                            var parentButton = $('#updateInterest').parent();
						                            parentButton.html('<img id="updateInterest" src="<?=get_stylesheet_directory_uri()?>/images/loader.gif" alt="Actualizando...">');
						                        },
						                        success: function(data, textStatus, jqXHR ){
						     
						                        },
						                        complete: function(data, dataStatus, jqXHR) {

						                            var json = $.parseJSON(data.responseText);
						                            var parentButton = $('#updateInterest').parent();
						                            parentButton.html('<a id="updateInterest" style="float:none;" href="#" class="btn btn-notification btn_avalaible" >Aplicar</a>');
						                            if(json.response)
						                            	parentButton.append('<span style="float:none;" class="message-notification-sucess "><br>'+ json.message + '<span>');
						                            else
						                            	parentButton.append('<span style="float:none;" class="message-notification-error "><br>Oh, ha ocurrido un error<span>');

						                        },

						                        error: function(jqXHR, textStatus, errorThrown) {
						                            var parentButton = $('#updateInterest').parent();
						                            parentButton.html('<a id="updateInterest" style="float:none;" href="#" class="btn btn-notification btn_avalaible" >Aplicar</a>');
						                            parentButton.append('<span class="message-notification-error "><br>Oh, ha ocurrido un error<span>');

						                        }

						                      });

						                    
						                } else {

						                   
						                }

						            });
								</script>
								<!-- END Frequency settings -->

							</div>

						</div>
						
					</div>
					

				</div>

				<div style="clear:both;"></div>
			</section>
		</div>
	</div>
</div>

<!-- This script changes the side content when radio input is selected -->

<script type="text/javascript">
		$(document).ready(function() {
	        var radios = document.querySelectorAll('input[type=radio][name="frequency"]');
	        var description = $('.description');
			var textarea = $('.textarea');

	        changeContent($('input[name=frequency]:checked').val());

			function changeHandler(event) {
			   	changeContent(this.value);
			}

			function changeContent(value){

				if (value === 'never'){

					textarea.show();
					$( "#comment" ).focus();
				    description.hide();

				}else if ( value === 'daily') {

					textarea.hide();
					description.children('h5').text("Notificaciones Diariamente");
			      	description.children('p.description-frequency').text("Será notificado diariamente al correo siendo informado sobre nuevos planes, noticias, eventos, productos entre otras cosas.");
			      	description.show();

				}else if( value === 'weekly'){

					textarea.hide();
			      	description.children('h5').text("Notificaciones Semanalmente");
			      	description.children('p.description-frequency').text("Será notificado semanalmente al correo siendo informado sobre nuevos planes, noticias, eventos, productos entre otras cosas.");
			      	description.show();

				}else if(value === 'monthly'){

					textarea.hide();
			      	description.children('h5').text("Notificaciones Mensualmente");
			      	description.children('p.description-frequency').text("Será notificado mensualmente al correo siendo informado sobre nuevos planes, noticias, eventos, productos entre otras cosas.");
			      	description.show();
				}
			}

			Array.prototype.forEach.call(radios, function(radio) {
			   radio.addEventListener('change', changeHandler);
			});
		});
</script>

<?php

get_footer();