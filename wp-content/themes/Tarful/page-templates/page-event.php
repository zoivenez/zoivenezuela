<?php
/**
 * Template Name: Event template
 */

get_header(); ?>
<link rel="stylesheet" href="<?= get_stylesheet_directory_uri()?>/js/blueimp-gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="<?= get_stylesheet_directory_uri()?>/js/bootstrap-image-gallery/css/bootstrap-image-gallery.min.css">
<div id="main-content" style="padding-bottom:0px !important;" class="site-main content">
<?php
global $cfs;
$j=0; $i=0;	 
?>
<div class="row">
<section>
		<?php 
            $slider_image = CFS()->get('slider_image'); 
        ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 archive-banner-event" style="background: url('<?php echo $slider_image; ?>') no-repeat;">
        <div>
            <div class="row">
                <span class="col-xs-12 col-sm-offset-1 col-sm-5 archive-banner-content">
                    <center>
                        <h1><?php the_title(); ?></h1>
                    </center>
               </span>
                <span class="col-xs-12 col-sm-5 archive-banner-content">
                    <button class="btn-zoi slider-button btn-event">Quiero participar</button>
                </span>
            </div>
            <div class="row">
                <span class="clock text-center hidden-xs col-sm-12">
<?php 
    $today = new DateTime(); 
    $eventDate = new DateTime(CFS()->get('date')); 

    if ($today <= $eventDate): ?>
        <span class="clock-item days">días</span>
        <span class="clock-item hours">horas</span>
        <span class="clock-item minutes">minutos</span>
        <span class="clock-item seconds">seg</span>
<?php
    endif;
?>
                </span>
            </div>
        </div> 
    </div> 
</section>
</div> 
<div class="row">
    <div class="event-description col-xs-12 col-sm-10 col-sm-offset-1">
        <?=the_content()?>
    </div>
</div>
<!-- Booking info -->
<div class="row event-booking-info">
<?php 
    $days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
    $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $date = strtotime(CFS()->get('date')); 
     
    $formatted_date = $days[date('w', $date)]." ".date('d', $date)." de ".$months[date('n', $date)-1]. " del ".date('Y', $date) ;
?>
    <div class="col-xs-12 col-sm-6 event-info-item">
        <span style="background-color:#3D95BE;" class="hidden-xs section-header">Información del evento</span>
            <span class="event-info-item-content"><span class="glyphicon glyphicon-calendar"></span> <?=$formatted_date?></span>
    </div>
    <div class="col-xs-12 col-sm-6 event-info-item">
            <span class="event-info-item-content"><span class="glyphicon glyphicon-map-marker"></span> <?=CFS()->get('place')?></span>
    </div>
</div>

<!-- Video -->
<div class="row">
<?php if (CFS()->get('video')): ?>
    <div class="col-xs-12 col-sm-7 event-video">
        <iframe width="760" height="430" src="<?=CFS()->get('video')?>" frameborder="0" allowfullscreen></iframe>
    </div>
<?php endif; ?>
    <div class="hidden-xs col-sm-5 event-map">
        <h1>¿Cómo llegar?</h1>
        <div id="map-canvas"></div>
    </div>
</div>
<div class="row">
<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="links" class="gallery-links">
<?php 
    $gallery = CFS()->get('gallery');
    foreach($gallery as $image) { ?>
        <div class="col-xs-12 col-sm-3">
            <a href="<?=$image['image']?>" title="<?php the_title() ?>" data-gallery>
                <img src="<?=$image['image']?>" alt="<?php the_title() ?>">
            </a>
        </div>
<?php } ?>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 event-participate" style="background: url('<?=CFS()->get('image-bottom')?>') no-repeat">
        <div>
            <h1>¡No te lo pierdas!</h1>
            <button class="btn-zoi slider-button btn-event">Quiero participar</button>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript"> 
    // For event clock
    jQuery(function($){
          function initialize() {
            var mapCanvas = document.getElementById('map-canvas');
            
            var myLatlng = new google.maps.LatLng(<?=CFS()->get('coordinates')?>);
            
            var mapOptions = {
              center: myLatlng,
              zoom: 8,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(mapCanvas, mapOptions)
            
            var marker = new google.maps.Marker({
              position: myLatlng,
              map: map
            });
          }
          google.maps.event.addDomListener(window, 'load', initialize);
	       
        $('.slider-button').click(function(event){
            event.preventDefault(); 
            window.open('<?=CFS()->get('form')?>', '_blank');
        });
        updateClock();
        var run = setInterval(updateClock, 1000); 

        // Updates the clock
        function updateClock() {

            var $days = $('.days'); 
            var $hours = $('.hours'); 
            var $minutes = $('.minutes'); 
            var $seconds = $('.seconds'); 
            

            // Added 1 day to date because the php to js conversion substracts 1
            var dateStr = '<?=date('Y-m-d', strtotime(CFS()->get('date') . '+1 day'))?>';
            var date = new Date(dateStr); 
            var today = new Date();
            if (today < date) {
                var timespan = countdown(today, date, countdown.DAYS | countdown.HOURS | countdown.MINUTES | countdown.SECONDS);
                $days.html(timespan.days + ' días');
                $hours.html(timespan.hours + ' horas');
                $minutes.html(timespan.minutes + ' min');
                $seconds.html(timespan.seconds + ' seg');
            } else {
                $days.html('0 días');
                $hours.html('0 horas');
                $minutes.html('0 minutos');
                $seconds.html('0 segundos');
                clearInterval(run); 
            }
        }

    });
</script>
<script src="<?= get_stylesheet_directory_uri()?>/js/bootstrap-image-gallery/js/bootstrap-image-gallery.min.js"></script>
<script src="<?= get_stylesheet_directory_uri()?>/js/blueimp-gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="<?= get_stylesheet_directory_uri()?>/js/countdown.min.js"></script>
</div>
<!-- The modal dialog, which will be used to wrap the lightbox content -->
<div id="form-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">¡Llena tus datos y vive la experiencia!</h4>
            </div>
            <div class="modal-body next">
            <iframe src="<?=CFS()->get('form')?>" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<?php 
    get_footer();
?>


