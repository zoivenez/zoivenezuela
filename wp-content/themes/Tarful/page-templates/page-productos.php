
<?php
	/**
	 * Template Name: Productos
	 *
	**/
global $cfs; 

get_header();
 ?>
	
	<div id="main-content" class="site-main">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<?php get_template_part('content','main-slider'); ?>
					<section>
						<div class="col-xs-12 col-sm-11 col-md-11 col-lg-11 mcenter recent-products">
							<?php
							$query = new WP_Query( array( 'post_type' => 'product', 'posts_per_page' => -1, 'orderby' =>'date','order' => 'DESC'));
							if ( $query->have_posts() ){
							while ( $query->have_posts() ) : $query->the_post(); 
								$product = get_product( $query->post->ID );  
								$product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
								$single_cat = array_shift( $product_cats );	
								$product_place = wp_get_post_terms( get_the_ID(), 'localidad');
								$single_place = array_shift( $product_place);

								if($single_cat->name == "Producto")
								{
							?>
									<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 product grid">
										<a href="<?php the_permalink(); ?>">
										<?php if(!has_post_thumbnail()){ ?>
											  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/placeholder_grid.png" alt="<?php the_title(); ?>">
										<?php
										} 	else { 
											the_post_thumbnail('small'); 
										} ?>		
										<div class="carousel-caption">
								            <?php if ($single_cat): ?>
											<div class="category"><a href="<?php echo get_term_link( $single_cat ); ?>"><?php  echo (!empty($single_cat) ? $single_cat->name : "Turismo");  ?></a></div>
								            <?php else: ?>
											<div class="category"><a href="">Turismo</a></div>
								            <?php endif; ?>
											<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
										</div>

										<?php
										$guru = get_post_meta ( get_the_ID(), 'guru', true );
										if (isset($guru) && $guru) { 
											$guru_info = get_userdata($guru);
										?>
										<a href="<?php echo get_site_url(); ?>/zoi-gurus/?id_guru=<?php echo $guru_info->ID; ?>">
											<div class="box_guru_grid">
												<div class="guru_info">
													<?php echo get_avatar( $guru, 40 ); ?>
													<h5><?php echo $guru_info->display_name; ?></br><?php echo _e('ZOI GURÚ'); ?></h5>
												</div>
											</div>
										</a>
										<?php } ?>

										<div class="hover">			
											<center>
												<a class="btn_avalaible main-cta" href="<?php the_permalink(); ?>"><?php _e('VEr MÁS', 'tarful') ?></a></center>
											</center>
											<?php tarful_facebook_share(get_the_ID() , apply_filters( 'woocommerce_short_description', $post->post_content )); ?>
											<?php tarful_twitter_share(); ?>
										</div>

										</a>
									</div>
						<?php  	} 
							endwhile;
							wp_reset_postdata(); 
							}else {
								?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
								<?php 
								}?>
						</div>
					</section>
					<div style="clear: both;"></div>
			</div>
		</div>
	</div>
					 
<?php get_footer();
