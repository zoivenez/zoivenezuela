<?php
/**
 * Template Name: Content + Sidebar - Contacto
 *
 * by Tarful
 *
 */

get_header(); ?>


<div id="main-content" class="site-main limit">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<section>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mcenter limit ">				
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 box-page-content">
			
						<?php
						while ( have_posts() ) : the_post();
							get_template_part( 'content', 'page' );
						endwhile;
						?>
					
						<div style="clear:both;"></div> 
						
						<div class="box_email_suupport">
							<a href="mailto:<?php echo CFS()->get('email', 22); ?>?subject=Contacto Email Soporte"><?php echo CFS()->get('email', 22); ?></a>
						</div>
						
						<div class="box_contact_form">
                        <h4>¡Contáctanos!</h4>
                        <hr>
						
						<?php tarful_box_contact_form(); ?>
						
						</div>
					</div>
					
					<?php get_template_part( 'sidebar' ); ?>

				</div>

				<div style="clear:both;"></div>
			</section>
		</div><!-- .site-main -->
	</div><!-- .content-area -->
</div>




<?php get_footer(); ?>
