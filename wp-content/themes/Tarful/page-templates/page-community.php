<?php
/**
 * Template Name: Community
 *
 *
 */
get_header();

/**
 * Generates a simple bxslider without main content nor modal on image click
 * @title string section title
 * @title_slug string section title slug
 * @post_type string with post type
 * @category string with category 
 */
function slider_without_title($title, $title_slug, $post_type, $category, $color='#19202e') { 
    $category_key = 'category_name'; 
    if ($post_type === 'product') {
        $category_key = 'product_cat';
    }

    $args = array(
        'post_type' => $post_type,
        $category_key => $category
    );

    $query = new WP_Query($args); 
    $posts = $query->get_posts(); 

    if (is_array($posts) && !empty($posts)) : 
?>
<div class="container-fluid <?=$title_slug?>" style="padding-top: 30px; padding-bottom:30px;">
    <div class="row">
        <div class="col-xs-12 col-sm-offset-1 col-sm-10" style="margin-bottom:10px;color:#EBEBEB">
        <span style="font-size:30px;"><strong><?=$title?></strong></span>
        </div>
    </div> <!-- Section Header -->
    <div class="container-fluid">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <ul class="<?=$title_slug?>-slider">
            <?php foreach ($posts as $i => $post) : 
            ?>
                <li>
                    <a href="<?=get_permalink($post->ID)?>">
                    <?php if (has_post_thumbnail($post->ID)): 
                        $post_args = array(
                            'title' => esc_attr(get_the_title($post->ID))
                        );
                    ?>
                        <?=get_the_post_thumbnail($post->ID, 'small', $post_args)?>
                    <?php else: ?>
                        <img title="<?=esc_attr(get_the_title($post->ID))?>" src="<?=array_key_exists('image', $object) ? $object['image'] : 'http://placehold.it/300x300'?>" />
                    <?php endif; ?>
                    </a>
                </li>
            <?php endforeach; ?>
            </ul>
        </div> <!-- /.col-xs-12 col-sm-12 col-sm-offset-1 -->
    </div> <!-- Section carousel -->

    <!-- Begin styling and JS for carousel -->
    <script type="text/javascript">
        $(document).ready(function(){
            $('.<?=$title_slug?>-slider').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                slideMargin: 5,
                captions: true,
                hideControlOnEnd: true,
                infiniteLoop: false,
                slideWidth: 300
            });
        }); 
    </script>
    <style>
        .<?=$title_slug?>-slider li {
            cursor: pointer; 
        }
        .<?=$title_slug?>-slider li img {
            box-shadow: 0px 0px 5px #000; 
            -webkit-box-shadow: 0px 0px 5px #000; 
            -moz-box-shadow: 0px 0px 5px #000; 
        }
        .<?=$title_slug?> .bx-wrapper {
            margin: 0 !important; 
        }
        .<?=$title_slug?> .bx-viewport {
            height: auto !important; 
            padding: 5px; 
        }
        
        .<?=$title_slug?> {
            background-color: <?=$color?>; 
        }
        .<?=$title_slug?> img {
        }

        .<?=$title_slug?> .bx-controls a {
            margin-top: 134px; 
        }
    </style>
    <!-- End styling and JS for carousel -->
<?php endif; ?>
</div>


<?php }

/**
 * Generates a simple bxslider with title, main image and description and modal on image click
 * @title string slider title
 * @image string url to image
 * @description string slider description
 * @slider_objects array with objects (title, description, video_url and image)
 */
function slider_with_title($title, $title_slug, $image, $description, $slider_objects, $color = '#FFFFFF', $font_color = '#000000') { ?>

<div class="container-fluid" style="padding-top:30px; padding-bottom:10px;background-color:<?=$color?>; color:<?=$font_color?>">
<div class="row" style="color: <?=$font_color?>">
        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center" style="padding-bottom:55px;">
            <span style="font-size:30px;"><strong><?=$title?></strong></span>
        </div>
    </div> <!-- Section Header -->
    <div class="row" style="margin-bottom: 45px;">
        <div style="padding:0px;margin-right:30px;" class="col-xs-12 col-sm-3 col-sm-offset-1 main-image text-center">
                <p style="color:<?=$font_color?>; text-align:left"><?= !empty($description) ? $description : "Seguimos organizando el próximo <strong>$title</strong>. Pronto estaremos informando"?></p> 
                <a id="<?=$title_slug?>-form-link" 
                    data-description="LLena tus datos para asistir" 
                    data-form="true" 
                    data-title="¡Asiste!" 
                    data-toggle="modal" data-target="#<?=$title_slug?>-modal"
                    data-url="<?=CFS()->get('form_url') ? CFS()->get('form_url') : 'https://docs.google.com/forms/d/1hLcm6jjANc4FzsSCbGJXpnK0uAVfyK3_8fXlzfOj6PM/viewform?embedded=true'?>" href="#" class="btn btn-danger btn-zoi"><span style="font-size:24px;color:#EBEBEB">Asiste</span></a>
        </div>
<?php
    // Make sure $slider_objects is an array
    if ( is_array($slider_objects) ) : 
        $opt_size = sizeof($slider_objects);
?>
        <div class="<?=$title_slug?>">
            <div class="col-xs-12 col-sm-7">
                <ul class="<?=$title_slug?>-slider">
                <li>
                    <img width="300" src="<?=$image?>" />
                </li>
                <?php foreach ($slider_objects as $i => $object) : ?>
                    <li id="<?=$title_slug?>-slider-<?=$i?>" 
                        data-url="<?=esc_attr($object['video_url'])?>" 
                        data-description="<?=esc_attr($object['description'])?>" 
                        data-toggle="modal" data-target="#<?=$title_slug?>-modal">

                        <img title="<?=esc_attr($object['title'])?>" src="<?=array_key_exists('image', $object) ? $object['image'] : 'http://placehold.it/300x300'?>" />
                    </li>
                <?php endforeach; ?>
                </ul>
            </div> <!-- /.col-xs-12 col-sm-12 col-sm-offset-1 -->
        </div> <!-- Section carousel -->
</div>

        <div class="modal fade" id="<?=$title_slug?>-modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body" style="text-align:center">
                        <iframe width="420" height="315" allowfullscreen></iframe>
                        <p style="color:#000;text-align:justify"></p> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danget btn-zoi" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- Begin styling and JS for carousel -->
        <script type="text/javascript">
            $(document).ready(function(){
                $('.<?=$title_slug?>-slider').bxSlider({
                    minSlides: 1,
                    maxSlides: 1,
                    slideMargin: 5,
                    captions: false,
                    hideControlOnEnd: true,
                    infiniteLoop: false,
                    slideWidth: 300
                });

                <?php 
                      for ($i = 0; $i <= $opt_size; ++$i) : ?>
                        // Show modal onclick and add youtube src
                        $(document).on('click', '#<?=$title_slug?>-slider-<?=$i?>', function(event) {
                            $('#<?=$title_slug?>-modal').find('h4').first().html($(this).find('img').first().attr('title')); // Header
                            $('#<?=$title_slug?>-modal').find('.modal-body p').first().html($(this).data('description')); // Description
                            $('#<?=$title_slug?>-modal iframe').attr('src', $(this).data('url'));  // YT URL
                            $('#<?=$title_slug?>-modal iframe').attr('width', 420);  // YT URL
                            $('#<?=$title_slug?>-modal iframe').attr('height', 315);  // YT URL
                        });
                        // Show modal form onclick link
                        $(document).on('click', '#<?=$title_slug?>-form-link', function(event) {
                            event.preventDefault(); 
                            $('#<?=$title_slug?>-modal').find('h4').first().html($(this).find('img').first().attr('title')); // Header
                            $('#<?=$title_slug?>-modal').find('.modal-body p').first().html($(this).data('description')); // Description
                            $('#<?=$title_slug?>-modal iframe').attr('src', $(this).data('url'));  // Form URL
                            $('#<?=$title_slug?>-modal iframe').attr('width', 760);  // YT URL
                            $('#<?=$title_slug?>-modal iframe').attr('height', 500);  // YT URL
                        });
                        // Remove src attr
                        $(document).on('click', '#<?=$title_slug?>-modal button', function(event){ 
                            $('#<?=$title_slug?>-modal iframe').removeAttr('src'); 
                            $('#<?=$title_slug?>-modal iframe').removeAttr('width'); 
                            $('#<?=$title_slug?>-modal iframe').removeAttr('height'); 
                        });
                <?php endfor;
                ?>
            }); 
        </script>
        <style>
            .main-image img {
                box-shadow: 0px 0px 5px #000; 
                -webkit-box-shadow: 0px 0px 5px #000; 
                -moz-box-shadow: 0px 0px 5px #000; 
                border-radius: 3px; 
                max-width: 400px;
            }
            .<?=$title_slug?>-slider li {
                cursor: pointer; 
            }
            .<?=$title_slug?> .bx-wrapper {
                margin: 0 !important; 
            }
            .<?=$title_slug?> .bx-viewport {
                height: auto !important; 
                padding: 5px; 
            }
            
            .<?=$title_slug?> {
            }
            .<?=$title_slug?> img {
                box-shadow: 0px 0px 5px #000; 
                -webkit-box-shadow: 0px 0px 5px #000; 
                -moz-box-shadow: 0px 0px 5px #000; 
            }

            .<?=$title_slug?> .bx-controls a {
                margin-top: 134px; 
            }

        </style>
        <!-- End styling and JS for carousel -->
<?php endif; ?>

</div> 

<?php }


/**
 * CFS sections:
 * title: section title
 * description: Will appear in the description banner 
 * image: Will appear in the descrition banner
 * has_main_slide: true if the slider has a main image with description
 * main_image: url to main image
 * main_description: description of main image
 * slider_type: 
 * * "Contenido fijo" means it's all through CFS
 * * "Entrada del blog" means it's a blog post
 * * "Experiencia" means it's a woocommerce product
 * slider_category: post or product category
 * slider_objects: array with all images if the slider_type is "Contenido fijo"
 * * title: Image title
 * * description: Image description
 * * video_url: Video URL
 * * image: Image file
 */
$sections = CFS()->get('sections');
$number_of_sections = is_array($sections) ? sizeof($sections) : 0; 
?>
            <style>
                    .archive-banner-aboutus img {
                        height: auto !important; 
                        width: 1000px !important; 
                    }

                    @media screen and (min-width: 1240px) {
                        .archive-banner-aboutus img {
                            position: absolute !important; 
                            left: 30%; 
                            margin-top: 5% !important; 
                        }
                    }
                    @media screen and (max-width: 1240px) {
                        .archive-banner-aboutus img {
                            position: absolute !important; 
                            left: 10%; 
                            margin-top: 5% !important; 
                        }

                    }

                    @media screen and (max-width: 768px) {
                        .archive-banner-aboutus img {
                            position: absolute !important; 
                            left: 10%; 
                            margin-top: 5% !important; 
                        }
                    }
            </style>
			<section class="about-us">
				<div class="row">
					<?php $url = wp_get_attachment_url (get_post_thumbnail_id ($post->ID)); ?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 archive-banner archive-banner-event" style="width:100%; height: 700px; overflow:hidden; padding:0; background: url('<?php echo $url; ?>') no-repeat; background-size:cover;background-position:center center;">
                        <img class="img-responsive" src="<?=bloginfo('stylesheet_directory')?>/images/zoi-comunidad-titulo.png" />
                        
					</div>
				</div> <!-- /.row Main banner image -->
				<div class="about-us-content">
					<div class="container-fluid">
						<div class="col-xs-12 col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-sm-4 col-lg-4 col-md-4" style="background: url('<?php bloginfo('url'); ?>/wp-content/themes/Tarful/images/stamp.png') no-repeat;background-position:right bottom; padding:0;">
							<span style="text-transform:none"><?php the_content(); ?></span>
						</div>
						<div class="col-xs-12 col-sm-4 col-lg-4 col-md-4 about-us-user-info text-center">
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<span><?= empty(CFS()->get('ongs')) ? '0' : CFS()->get('ongs') ?></span>
								<p><?php _e('ONGs'); ?></p>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<?php $count_experiences = wp_count_posts('product'); ?>
								<span><?= empty(CFS()->get('volunteers')) ? '0' : CFS()->get('volunteers') ?></span>
								<p><?php _e('Voluntarios'); ?></p>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<span><?= empty(CFS()->get('allies')) ? '0' : CFS()->get('allies') ?></span>
								<p><?php _e('Aliados turísticos'); ?></p>
							</div>
							<!--<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<span><?= empty(CFS()->get('community')) ? '0' : CFS()->get('community') ?></span>
								<p><?php _e('COMUNIDAD'); ?></p>
							</div>-->
						</div>
					</div>
				</div> <!-- /.about-us-content page content and counters -->

                <div class="container-fluid" style="padding-top:45px;padding-bottom:55px;">
                    <div class="row" >
                        <div class="col-xs-12 col-sm-6">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-4 text-center" style="padding-bottom:55px;">
                                <span style="color:#000;font-size:30px;"><strong>¿QUÉ HACEMOS?</strong></span>
                            </div>
                            <div class="col-xs-12 col-sm-offset-3 col-sm-8 ">
                                <p style="font-size:20px;color:#000;text-align:justify;text-transform:none"><?=CFS()->get('whatwedo')?></p> 
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="col-sm-12 text-center">
                                <img src="<?= CFS()->get('whatwedo-img') ? CFS()->get('whatwedo-img') : 'http://placehold.it/350x350'?>">
                            </div>
                        </div>
                    </div>
                </div> <!-- . que hacemos content -->

                <div class="container-fluid container-division" style="background-color:#22A8A0; padding-top:45px; padding-bottom:55px;">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                        <?php foreach($sections as $section): 
                            // Hidden sections are skipped
                            if (array_key_exists('hide', $section) && $section['hide']) continue; ?>
                            <div class="col-xs-12 col-sm-4 text-center">
                                <img width="200" src="<?= array_key_exists('image', $section) ? $section['image'] : 'http://placehold.it/200x200'?>">
                                <p style="text-align:justify; padding: 0px 50px 0px 50px;"> <?= array_key_exists('description', $section) ? $section['description']  : ' '?></p>
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div> <!-- image and descriptions of each sections -->

<?php

    // PAGE CONTENT 
    // Loop on sections
    foreach($sections as $section) {
        // Hidden sections are skipped
        if (array_key_exists('hide', $section) && $section['hide']) continue; 

        // Get attributes 
        $title = $section['title'];
        $title_slug = $section['title_slug'];
        $image = $section['main_image'];
        $description = $section['main_description'];

        // If has main image, show slider with title and objects
        if ($section['has_main_slide']) {
            // Contenido fijo means it will be filled in the comunity page and 
            // it won't crawl content from the website.
            if (array_key_exists('Contenido fijo', $section['slider_type'])) {
                $slider_objects = $section['slider_objects']; 
                slider_with_title($title, $title_slug, $image, $description, $slider_objects, '#FFFFFF', '#000000');
            } else {
                $category = $section['slider_category'];
                $color = array_key_exists('color', $section) ? $section['color'] : '#19202e';
                if (array_key_exists('Entrada del blog', $section['slider_type'])) {
                    slider_without_title($title, $title_slug, 'post', $category, $color);
                } elseif (array_key_exists('Experiencia', $section['slider_type'])) {
                    slider_without_title($title, $title_slug, 'product', $category, $color);
                }
            }
        } else {
            // Two types of crawling: 
            // * Entrada del blog (Blog entry)
            // * Experiencia (Product)
            $category = $section['slider_category'];
            $color = array_key_exists('color', $section) ? $section['color'] : '#19202e';
            if (array_key_exists('Entrada del blog', $section['slider_type'])) {
                slider_without_title($title, $title_slug, 'post', $category, $color);
            } elseif (array_key_exists('Experiencia', $section['slider_type'])) {
                slider_without_title($title, $title_slug, 'product', $category, $color);
            }
        
        }
    }

?>
            <!-- Section values -->
            <div class="container-fluid" style="background: url(<?=bloginfo('stylesheet_directory')?>/images/zoi-comunidad-nuestros-mandamientos.jpg) no-repeat center center;background-size:cover; color:#EBEBEB;padding-left:0px;padding-right:0px">
            <div style="width:100%;background-color:rgba(0,0,0,0.5); background-size:cover;padding-bottom:55px">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center" style="padding-top:45px;padding-bottom:55px;">
                        <span style="font-size:30px;"><strong>Nuestros Mandamientos</strong></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center" style="padding-bottom:55px;">
                        <span style="font-size:26px;"><strong>Gente</strong></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1" style="padding:30px">
                        <span style="">
                            <?=CFS()->get('personas_1')?>
                        </span>
                    </div>
                    <div class="col-sm-4" style="padding:30px">
                        <span style="">
                            <?=CFS()->get('personas_2')?>
                        </span>
                    </div>
                    <div class="col-sm-3" style="padding:30px">
                        <span style="">
                            <?=CFS()->get('personas_3')?>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center" style="padding-top:55px;padding-bottom:55px;">
                        <span style="font-size:26px;"><strong>Medio ambiente</strong></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1" style="padding:30px">
                        <span style="">
                            <?=CFS()->get('medio_ambiente_1')?>
                        </span>
                    </div>
                    <div class="col-sm-4" style="padding:30px">
                        <span style="">
                            <?=CFS()->get('medio_ambiente_2')?>
                        </span>
                    </div>
                    <div class="col-sm-3" style="padding:30px">
                        <span style="">
                            <?=CFS()->get('medio_ambiente_3')?>
                        </span>
                    </div>
                </div>
            </div>

            </div> <!-- ./ Section Values -->

            <!-- Section join us -->
            <div class="container-fluid" style="padding:0px;background: url(<?=bloginfo('stylesheet_directory')?>/images/zoi-comunidad-unete-al-equipo.jpg) no-repeat center center; background-size:cover; color:#EBEBE;">

                <div style="width:100%; background-color:rgba(0,0,0,0.5); background-size:cover;">
                    <div class="container" >
                        <div class="row" style="">
                            <div class="col-xs-12 col-sm-6" style="height:349px;padding-top: 150px;">
                                <span style="font-size:30px;margin:auto">Únete a nuestra red de voluntarios</span>
                            </div>
                            <div class="col-xs-12 col-sm-6" style="height:349px;padding-top: 150px;">
                                <a href="<?=CFS()->get('join_form_url')?>" class="btn btn-danger btn-zoi"><span style="font-size:24px;color:#EBEBEB">Únete</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- ./ Section join us -->
            <!-- Section allies -->
            <div class="container-fluid" style="background: #FFF; color:#000;">
                    <div class="container" >
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center" style="padding-top:45px;padding-bottom:55px;">
                                <span style="font-size:30px;"><strong>Nuestra familia</strong></span>
                            </div>
                        </div>
                        <div class="row">
                        <?php 
                            $companies = CFS()->get('companies'); 
                            foreach($companies as $company): ?>
                            <div class="col-xs-6 col-sm-3" style="margin-bottom:30px">
                            <img width="200" src="<?=array_key_exists('image', $company) ? $company['image'] : 'https://placehold.it/200'?>" />
                            </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
            </div> <!-- ./ Section allies -->
       </section>
<?php get_footer(); ?>
