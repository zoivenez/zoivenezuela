<?php
/**
 * Template Name: Full width calendar
 * @author: Juan Carlos Arocha
 * @description: Full-width Calendar to have a global view of the experiences schedule
 */

get_header(); 
$events = json_encode(get_experiences_between(0, 0, true)); 
?>

<div class="site-content">
    <div id="fullpage-calendar">
        <div class="row">
            <div class="calendar-title">Calendario de Experiencias</div>
            <div class="open-date-info">Existen experiencias con fecha programada y con fecha <strong>abierta <span id="open-date-tooltip" class="glyphicon glyphicon-question-sign" rel="tooltip" title="Son experiencias donde tú eliges la fecha cuando quieras"></span></strong>. En este calendario sólo se muestran aquellas con fecha programada. Para ver todas las experiencias, incluyendo aquellas con fecha abierta, visita la sección de <a href="<?=get_bloginfo('url') . '/experiencias';?>" class="btn btn-danger btn-zoi">Experiencias</a></div>

            <div class="calendar-nav form-inline">
                <div class="btn-group">
                    <button class="btn-nav-calendar btn btn-primary" data-calendar-nav="prev"><span class="glyphicon glyphicon-arrow-left"></span></button>
                    <button class="btn-nav-calendar btn btn-default" data-calendar-nav="today">Hoy</button>
                    <button class="btn-nav-calendar btn btn-primary" data-calendar-nav="next"><span class="glyphicon glyphicon-arrow-right"></span></button>
                </div>
            </div>
            <div class="calendar-view form-inline">
                <div class="btn-group">
                    <button class="btn-view-calendar btn btn-warning" data-calendar-view="year">Año</button>
                    <button class="btn-view-calendar btn btn-warning active" data-calendar-view="month">Mes</button>
                    <button class="btn-view-calendar btn btn-warning" data-calendar-view="week">Semana</button>
                </div>
            </div>
            <div id="calendar-info"></div>
            <div id="calendar"></div>

            <script type="text/javascript" src="<?=get_bloginfo('stylesheet_directory').'/bootstrap/js/bootstrap.min.js'?>"></script>
            <script type="text/javascript" src="<?=get_bloginfo('stylesheet_directory').'/bootstrap-fullpage-calendar/js/language/es-ES.js'?>"></script>
            <script type="text/javascript" src="<?=get_bloginfo('stylesheet_directory').'/bootstrap-fullpage-calendar/js/calendar.min.js'?>"></script>
            <script type="text/javascript" src="<?=get_bloginfo('stylesheet_directory').'/underscore/underscore-min.js'?>"></script>
            <link rel="stylesheet" href="<?=get_bloginfo('stylesheet_directory').'/bootstrap-fullpage-calendar/css/calendar.min.css'?>" type="text/css" media="all">
            <script type="text/javascript">
                jQuery(function( $ ){
                    var events  = $.parseJSON('<?=$events?>');
                    var calendar = $('#calendar').calendar({
                        language: 'es-ES',
                        events_source: events['result'],
                        tmpl_path: "<?=get_bloginfo('stylesheet_directory')?>/bootstrap-fullpage-calendar/tmpls/",
                        display_week_numbers: false,
                        week_box: false,
                        views: {
                            year: {
                                slide_events: 1,
                                enable: 1
                            },
                            month: {
                                slide_events: 1,
                                enable: 1
                            },
                            week: {
                                enable: 1
                            },
                            day: {
                                enable: 0
                            }
                        },
                        onAfterViewLoad: function(view) {
                            $('#calendar-info').html(this.getTitle()); 
                        },
                        onAfterEventsLoad: function(view) {
                            $('#calendar-info').html(this.getTitle()); 
                            console.log("hola"); 
                        },
                    });

                    // Remove link of color circles
                    $('a.event').attr('href', '#'); 

                    $(document).on('click', '.btn-nav-calendar', function(){
                        $where = $( this ).data('calendar-nav'); 
                        $( '.btn-nav-calendar' ).removeClass('active');
                        $( this ).addClass('active');
                        calendar.navigate($where); 
                    }); 

                    $(document).on('click', '.btn-view-calendar', function(){
                        $view = $( this ).data('calendar-view'); 
                        $( '.btn-view-calendar' ).removeClass('active');
                        $( this ).addClass('active');
                        calendar.view($view);
                    }); 

                    $(document).on('click', 'a.event', function(event){
                        event.preventDefault(); 
                    });

                    $('#open-date-tooltip').tooltip(); 
                });
            </script>
        </div>
        <div class="row">
        </div>
    </div>
</div>
<?php get_footer(); ?>
