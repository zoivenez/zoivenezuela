<?php
/**
 * Template Name: Content + Sidebar - Terms
 *
 * by Tarful
 *
 */

get_header(); 

$current_featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
$current_featured_img = $current_featured_img[0];
?>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-page-header" style="background: url('<?php echo $current_featured_img; ?>') no-repeat center top">

		<h1><?php the_title(); ?></h1>
		
	</div>

	<div style="clear:both;"></div>

	<div id="main-content" class="site-main limit">
	
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

				<section>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mcenter limit">				
						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 box-page-content">
							<?php
							while ( have_posts() ) : the_post();
								the_content();
							endwhile;
							?>
						</div>
										
						<?php get_template_part( 'sidebar' ); ?>
	
					</div>
	
					<div style="clear:both;"></div>

				</section>
			</div><!-- #content -->
		</div><!-- #primary -->
	</div><!-- #main-content -->

<?php get_footer(); ?>
