<?php
/**
 * Template Name: Full Width Experiencias Page
 *
 * by Tarful
 *
 */

get_header(); ?>

<div id="main-content" class="site-main">


	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php get_template_part('content','main-slider'); ?>

			<section>

				<div class="col-xs-12 col-sm-11 col-md-11 col-lg-11 mcenter recent-products">

					<?php get_template_part('content', 'newest-products' ); ?>


					<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8  product featured-location">

						<?php 
						$args = array('taxonomy' => 'localidad', 'hide_empty' => 0);
						$categories_p = get_categories($args);
						
						 foreach ($categories_p as $category) {
						 							 	$temp_p = get_category_meta(false, get_term_by('id',  $category->term_id , 'localidad'));
							
						 	if ((!empty($temp_p)) and ($temp_p['featured'])) {
								$temp_image = $temp_p['image'];
								$temp_link = get_term_link($category);
						 		break;
						 	}
						 }
						 ?>
						 <a href="<?php echo $temp_link; ?>">
						 	<img src="<?php echo $temp_image; ?>" alt="">
						</a>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 block_separator"></div>
					
				</div>
			</section>
			
			<div style="clear: both;"></div>

			<?php get_template_part('content','testimonial-featured'); ?>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 block_network text-center">
				<div class="col-xs-16 col-sm-16 col-md-10 col-lg-10 col-centered text-center">
					<center>

						<h2>Ya somos una comunidad de <u><?php tarful_users_count(); ?></u> aventureros</h2>

						<?php tarful_networks_avatars(9); // Cambiar 2 por 9 ?>
						<div style="clear:both;"></div>

						<a href="" class="btn avalaible main_btn">Crear tu cuenta ahora</a>
						<a href="" class="btn avalaible fb_btn"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/fb_btn.png" alt="Calidad ZOI"/> Iniciar sesión con Facebook</a>
			
						<div class="box_stamp">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/stamp.png" alt="Calidad ZOI"/>
						</div>
	
					</center>
						
				</div>
			</div>
			
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php

get_footer();
