<?php
/**
 * Template Name: Full Width Home Page
 *
 * by Tarful
 *
 */

get_header(); ?>

<div id="main-content" class="site-main content">


	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php get_template_part('content','how-it-works'); ?>
			<?php get_template_part('content','main-slider'); ?>
			<div class="row">
		        <div id="main_slider_btn_div" class="col-xs-12 col-sm-12 col-md-12 main-slider-floating-div text-center">
			        <a id="btn_slider" style="text-transform: uppercase; background-color: #22A8A0;" class="how-it-works-button btn-main-slider">Cómo Funciona</a>
			    </div>
		        <div  style="clear: both;"></div> 
		    </div>
            <?php get_search_form()?>
			<section>

				<div class="col-xs-12 col-sm-11 col-md-11 col-lg-11 mcenter recent-products">

					<?php get_template_part('content', 'newest-experiences' ); ?>

					<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8  product featured-location">

						<?php 
						$args = array('taxonomy' => 'localidad', 'hide_empty' => 0);
						$categories_p = get_categories($args);

						 foreach ($categories_p as $category) {
						 	$temp_p = get_category_meta(false, get_term_by('id',  $category->term_id , 'localidad'));			 	
							 	if ((!empty($temp_p)) and (array_key_exists('featured',$temp_p) and $temp_p['featured'])) {
									$temp_image = $temp_p['image'];
									$temp_link = get_term_link($category);
							 		break;
						 		}
						 }
						 ?>
						 <a href="<?php echo $temp_link; ?>">
						 	<img src="<?php echo $temp_image; ?>" alt="<?= $category->name;?>">
						</a>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 product featured-zoi-promise">
						<a href="<?php echo get_permalink(977); ?>">
						<?php echo get_the_post_thumbnail( 977, 'small'); ?>
						</a>
					</div>

					<div class="clear-style" style="clear: both;"></div>

					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 featured-guru product">
						
						<a id="create_experiencie">
							<div>
								<img class="attachment-small wp-post-image" width="370" height="370" alt="Crea tu experiencia" src="<?php echo CFS()->get('imagen_para_listado', 4); ?>">	 
							</div>
						</a>
					</div>


					<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 featured-category product">

						<?php 
						$args = array('taxonomy' => 'product_cat');
						$categories = get_categories($args);
						 foreach ($categories as $category) {
						 
						 	$temp = get_category_meta(false, get_term_by('id',  $category->term_id , 'product_cat'));
						 	if (array_key_exists('featured',$temp) && $temp['featured']) {
						 		$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
								$image = wp_get_attachment_url( $thumbnail_id );
								$cat_link = get_term_link($category);
						 		break;
						 	}
						 }
						?>
						<a href="<?php echo $cat_link ?>">
							<img src="<?php echo $image; ?>" alt="<?= $category->name; ?>">
						</a>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-button">
						<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 mcenter">
							<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' )); ?>">
							<button type="button" class="btn btn-block avalaible" style="padding:1.5%">CONOCE MÁS EXPERIENCIAS</button>
							</a>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 block_separator"></div>
					<div  style="clear: both;"></div>

				</div>
			</section>
			
			<div  style="clear: both;"></div> 

			<?php get_template_part('content','testimonial-featured'); ?>

			<?php taful_comunidad_zoi(); ?>
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->
<script type="text/javascript">
$(document).ready(function(){
	enderizeTarfulHeightCall(".grid img, .featured-zoi-promise img, .featured-guru img",96);
	enderizeTarfulHeightCall($(".featured-location img, .featured-category img"),199.956);
});
</script>
<?php

get_footer();
