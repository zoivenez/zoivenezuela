<?php
/**
 * Template Name: Email Confirmation template
 * @author: Juan Carlos Arocha
 * @description: Template to handle email confirmation
 */
get_header(); ?>

<div class="site-content email-confirmation">
    <div class="row">
        <div class="col-xs-12 col-lg-8 col-lg-offset-2">
<?php
if ( !isset($_GET['user_login']) || !isset($_GET['conf_token']) ) { 
    wp_redirect( home_url() ); 
    exit;
} else { 
    $user_login = sanitize_text_field($_GET['user_login']);
    $token      = sanitize_text_field($_GET['conf_token']);
    $user       = get_user_by('login', $user_login);
    $confirmed  = confirm_email($user_login, $token);

    // If email was confirmed, then show corresponding view
    if ($confirmed) {
?>
    <h3>¡Hola, <?=$user_login?>!</h3>
    <p>Gracias por confirmar tu cuenta ZOI. ¡Ya estás listo para aventurarte en cualquiera de nuestras experiencias!</p>
    <div class="col-xs-12 col-lg-2 col-lg-offset-5">
        <a class="btn btn-danger btn-zoi" href="<?=get_bloginfo('url')?>/experiencias">Ir a Experiencias</a>
    </div>
     
<?php
    } else { 
        wp_redirect( home_url() ); 
        exit;
    }
} ?>
       
        </div>
    </div>
</div>
