<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * Template Name: Export Report
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

// Obtains "from" and "to" variables from url and uses them as date range
$from = (isset($_GET['from']) ? parse_date_exp_2(sanitize_text_field($_GET["from"])) : null);
$to   = (isset($_GET['to']) ? parse_date_exp_2(sanitize_text_field($_GET["to"])) : null);

// Control if in case page is accessed without the right parameters 
if( $from != null) {
    
    //assign the time variable to the date range. This is used to obtain records that contain timestamps. 
    $from = $from ." 00:00:00";
    $to = $to . " 23:59:59";

    // converts date ranges to time variables.
    $from = strtotime($from); 
    $to   = strtotime($to);
    
    // get the orders specified by the date range input given by the uer
    $final = get_orders_by_date_range($from,$to, sanitize_text_field($_GET['searchByExperience']));

    // if no orders are obtained then you get redirected to the inital page

    if(count($final) > 0) {
        createxls($final);    
    } else {
        header( "Location: ".get_bloginfo('url')."/wp-admin/index.php") ; 
    }
}
else{
    echo "Wrong parameter specification, contact admin.";
} 
