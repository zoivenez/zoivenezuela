<?php
/**
 * Template Name: Form Template
 * @author: Juan Carlos Arocha
 * @description: Redirects to the specified URL
 */

wp_redirect(CFS()->get('form-url')); 
 
