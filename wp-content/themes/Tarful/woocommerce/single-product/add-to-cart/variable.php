<?php
/**
 * Variable product add to cart
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $post;


$dates = wp_get_object_terms($post->ID, 'pa_fechas-de-experiencia'); 
$product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
$validation_category = array_shift( $product_cats );
$dates = wp_get_object_terms($post->ID, 'pa_fechas-de-experiencia', true); 
// Get product to verify it's bookable
$product = wc_get_product( $product->id );
// Is product bookable ?
$is_bookable = get_post_meta( $product->id, '_booking_option', true );
$childrens_age = CFS()->get('childrens-age') ? CFS()->get('childrens-age') : 'hasta 8 años';
$out_of_stock = false;
?>


<script type="text/javascript">
	
	$('.testeo').each(function() {
  $(this).before($('<div>').text("prueba"));
});
	
</script>

<?php do_action( 'woocommerce_before_add_to_cart_form' ); 
// XXX: this is a workaround to show the public name but it suppose to be
// handled from server
$title_map = array(
    'pa_npersonas' => 'Número de personas',
    'pa_fechas-de-experiencia' => 'Elige tu fecha',
    'pa_extras' => 'Extras',
    'pa_duracion' => 'Duración',
    'pa_transporte' => 'Transporte',
    'pa_grupo' => 'Grupo',
    'pa_habitacion' => 'Tipo de habitación',
    'pa_ninos-adicionales' => "Niños adicionales ($childrens_age)",
    'pa_adultos-adicionales' => 'Adultos adicionales',
    'pa_color' => 'Color',
    'pa_talla' => 'Talla',
    'pa_tipo_envio' => 'Tipo de Envio',
    'pa_habitacion' => 'Tipo de habitación',
    'pa_plan' => 'Tipo de plan',
    'pa_viaje' => 'Tipo de viaje'

);
?>

<form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo $post->ID; ?>" data-product_variations="<?php echo esc_attr( json_encode( $available_variations ) ) ?>" data-product_bookeabledays="<?php echo esc_attr( json_encode( get_experience_extra_info($available_variations) ) ) ?>">
	<?php if ( ! empty( $available_variations ) ) : ?>

<?php
    $min_people = CFS()->get('min_people'); 
    $max_people = CFS()->get('max_people'); 
    $price_grid_size = 8; 
    if ($max_people > 1) {
        $price_grid_size = 9; 
    }

    if($validation_category->name === "Producto"){
    	foreach ($available_variations as $variation) {
	    	$var_product = wc_get_product($variation["variation_id"]);
	    	if($var_product->get_stock_quantity() < 1){
		    	$out_of_stock = true;
		    }
    	}	
    }
    else{
    	if($product->get_stock_quantity() < 1){
		    	$out_of_stock = true;
		    }
    }
    
    
?>
    <div class="price-fieldset">
        <div class="row">
            <div class="col-lg-12 col-xs-12 price-caption">
<?php
			if($validation_category->name != "Producto") {
		        if (empty($dates) && !$is_bookable): ?>
		                <h3 style="padding:10px;"> Nuevas fechas próximamente</h3>
                <?php elseif ($is_bookable): ?>
                        <h4>Precio <span class="people-price">por reserva</span></h4>
			<?php else: ?>
		                <h4>Precio <span class="people-price"><?=($max_people > 1) ? "por grupos de $max_people" : "por persona"?></span></h4>
			<?php endif;
			 } else{
				  if ($out_of_stock):  ?>
		                <h3 style="padding:10px;"> No hay existencia del producto</h3>
			<?php else: ?>
		                <h4>Precio por unidad <span class="people-price"></span></h4>
			<?php endif;
	  		}	?> 	
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 price">
                <p class="single_variation booking_price"></p> 
               <!--  ADDED FOR SEO PURPOSE -->
                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" style="display: none;">
                    <span itemprop="price"><?php if(!$out_of_stock){echo $product->get_price();}else{echo "Precio no disponible";} ?></span>
				</div>    
            </div>
        </div> <!-- Price -->
    </div> <!-- Price -->


    <div class="row location-fieldset">
<?php
		$product_place = wp_get_post_terms( get_the_ID(), 'localidad');
		$single_place = array_shift( $product_place);
	    if ( !empty($single_place) && $validation_category->name != "Producto") :
?>
            <div class="col-lg-12 col-xs-12 location-info">
                <p><span class="glyphicon glyphicon-map-marker"></span> <a href="#mapbox"><?=$single_place->name?></a></p>
            </div> <!-- Location -->
<?php 	endif; 
		$duration = CFS()->get('lenght'); 
		if (!empty($duration) && $validation_category->name != "Producto") : 
?>
            <div class="col-lg-12 col-xs-12 location-info">
                <p><span class="glyphicon glyphicon-time"></span> <?php echo CFS()->get('lenght'); ?></p>
            </div>
<?php 	endif; 
		if ($validation_category->name != "Producto") :
?>
            <div class="col-lg-12 col-xs-12 location-info">
                <p><span class="glyphicon glyphicon-tent"></span> <?php echo CFS()->get('nombre_aliado'); ?></p>
            </div> <!-- Ally -->
<?php 	endif; ?>
        <div class="col-lg-12 col-xs-12 location-info">
            <p>
<?php 
	    $categories = get_the_terms($product->ID, 'product_cat'); 
	    foreach ($categories as $category) : 
	        $cm = get_category_meta(false, get_term_by('slug', $category->slug, 'category'));
	    	if (!empty($cm['cat_img'])):
?>
<!--<img src="<?php echo $cm['cat_img']; ?>" style="background-color: <?=$cm['cat_color']?>" class="category_image" alt="<?=$category->name?>" title="<?=$category->name?>">-->
    <?php 
        	endif;
        endforeach; ?>
    		</p>
        </div> <!-- Categories -->
    </div>

		<div class="date">
			<table class="variations" cellspacing="0">
				<tbody>


            <?php 
                    $loop = 0; 
                    foreach ( $attributes as $name => $options ) : 
                        $loop++;
                        if($validation_category->name != "Producto" && $name == "pa_tipo_envio"){
                        	continue;
                        } ?>
                        <tr> <td> 
                            <h4 class="pull-left"><?=$title_map[$name]?> </h4>
                            <?php if ( $loop === 1 ): ?> 
                                 <span class="single_variation_availability pull-right"></span>
                            <?php
                                  endif; 
                            ?>
                        </td> </tr>
						<tr>
							<td class="value">
								<?php 
			                    if (!empty($dates) || $is_bookable || $validation_category->name == "Producto"):
								?>
									<select class="experience-date tarful_select variation_select" id="<?php echo esc_attr( sanitize_title( $name ) ); ?>" name="attribute_<?php echo sanitize_title( $name ); ?>" data-attribute_name="attribute_<?php echo sanitize_title( $name ); ?>">
										<?php

											if ( is_array( $options ) ) {

												if ( isset( $_REQUEST[ 'attribute_' . sanitize_title( $name ) ] ) ) {
													$selected_value = $_REQUEST[ 'attribute_' . sanitize_title( $name ) ];
												} elseif ( isset( $selected_attributes[ sanitize_title( $name ) ] ) ) {
													$selected_value = $selected_attributes[ sanitize_title( $name ) ];
												} else {
													$selected_value = '';
												}

												// Get terms if this is a taxonomy - ordered
												if ( taxonomy_exists( $name ) ) {

			                                        // Get sorted dates
			                                        $terms = wc_get_sorted_product_terms( $post->ID, $name, array( 'fields' => 'all', 'sort_dates' => true ) );
													//$terms = wc_get_product_terms( $post->ID, $name, array( 'fields' => 'all' ) );
			    
			                                        foreach ( $terms as $term ) {
			                                            if ( ! in_array( $term->slug, $options ) ) {
			                                                continue;
			                                            }
			                                            echo '<option value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $selected_value ), sanitize_title( $term->slug ), false ) . '>' . apply_filters( 'woocommerce_variation_option_name', $term->name ) . '</option>';
			                                        }

												} else {

													foreach ( $options as $option ) {
														echo '<option value="' . esc_attr( sanitize_title( $option ) ) . '" ' . selected( sanitize_title( $selected_value ), sanitize_title( $option ), false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '</option>';
													}

												}
											}
										?>
									</select> 
						<?php   endif; ?>
							</td>
						</tr>
			        <?php endforeach;?>
                    <tr><td class="hidden-xs hidden-sm col-lg-12">
                    	<?php if($validation_category->name != "Producto"):?>
                        		<div class="experience-calendar"></div>
                    	<?php endif;?>
                    </td></tr>

<?php
    // If it's bookable, close table 
    if (!$is_bookable) : 
?>
				</tbody>
			</table>
		</div>
<?php endif; ?>

<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<div class="single_variation_wrap text-center">
			<?php do_action( 'woocommerce_before_single_variation' ); ?>

			<div class="row book-button">
				<div class="col-xs-12 col-lg-4 cantidad-product">
					<h5>cantidad</h5>
				 	<?php
				 		if ( ! $product->is_sold_individually() )
				 			woocommerce_quantity_input( array(
				 				'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
				 				'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
				 			) );
				 	?>
					<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
				</div>

				<div class="col-lg-8 col-xs-12 col-md-12" style="top:20px">
                <button style="top: 10px" type="submit" class="avalaible buy-button" <?=empty($dates)&& $validation_category->name != "Producto" && !$is_bookable ? 'disabled' : ''?>><h4><?php if($validation_category->name == "Producto"): echo "añadir a carrito"; else: echo "reservar"; endif; ?></h4></button>
			</div>

			<input type="hidden" name="add-to-cart" value="<?php echo $product->id; ?>" />
			<input type="hidden" name="product_id" value="<?php echo esc_attr( $post->ID ); ?>" />
			<input type="hidden" name="variation_id" class="variation_id" value="" />
			<input type="hidden" id="experience_date" name="experience_date" value="" />
			<input type="hidden" id="experience_duration" name="experience_duration" value="<?=CFS()->get('dias'); ?>" />

			<?php do_action( 'woocommerce_after_single_variation' ); ?>
		</div>
		
		<div style="clear: both;"></div>

		<div class="box-available">
<!-- 			<div class="single_variation"></div> -->
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<?php else : ?>

		<p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>

	<?php endif; ?>

</form>
<!-- The modal dialog, which will be used to wrap the lightbox content -->
<div id="message-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">¡Nos fuimos de viaje!</h3>
            </div>
            <div class="modal-body next">
                <p>Estamos viajando por toda Venezuela para seguir afiliando los mejores operadores turísticos. Estaremos de vuelta en enero. </p>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
