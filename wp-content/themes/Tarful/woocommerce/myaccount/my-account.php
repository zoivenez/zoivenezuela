<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$translate = array('monthly' => 'Mensualmente','never' => 'Nunca','daily' => 'Diariamente','weekly' => 'Semanalmente');
$current_featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
$current_featured_img = $current_featured_img[0];

tarful_update_user_personal_info();
do_action( 'edit_user_profile_update' );
wc_print_notices(); ?>

	<?php do_action( 'woocommerce_before_my_account' ); ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box_centered box_form_login box-myaccount">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-page-header" style="background: url('<?php echo $current_featured_img; ?>') no-repeat center top">
		<?php echo get_avatar( $current_user->ID, 140 ); ?> 		
	</div>


	<script type="text/javascript">
		$(document).ready( function() {
			$('#userbox-container').easytabs();
		});
	</script>

	<div id="userbox-container" class='tab-container'>
		<ul class='col-xs-12 col-sm-12 col-md-12 col-lg-12 myaccount-menu etabs'>
		<li class='tab'><a href="#userinfo">Datos de usuario</a></li>
		<li class='tab'><a href="#personalinfo">Datos personales</a></li>
		<li class='tab'><a href="#purchases">Histórico de compras</a></li>
		<li class='tab'><a href="#likeexperiences">Mis experiencias favoritas</a></li>
	</ul>

	<div class='panel-container'>
		<?php if(isset($_POST['updateuser'])) { ?>
		<div class="woocommerce-message">Gracias, por actualizar sus datos personales</div>
		<?php } ?>
		<div id="userinfo">

			<div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 box_centered box-form-userupdate">
		
				<form method="post" id="adduser" action="<?php the_permalink(); ?>" <?php do_action( 'user_edit_form_tag' ); ?>>
					
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>Usuario</span>
							<input class="text-input" name="user-name" type="text" id="user-name" value="<?php the_author_meta( 'user_login', $current_user->ID ); ?>" disabled />
						 </div>
						 <span class="small_notice">Tu nombre de usuario no puede ser cambiado</span>
					</div>

				   <div class="box_profile_extrainfo form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Foto de perfil</span>
							<input class="text-input" name="" type="text" id="trigger_foto" value="cambiar foto" />
						 </div>
						 <div  id="form_avatar">
							<?php
							//action hook for plugin and extra fields
								do_action('edit_user_profile',$current_user);
							?>
						 </div>
				   </div>
					<div class="clear"></div>

                    <div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						<div class="row-box">
							<span>Email principal</span>
                            <input type="text" class="text-input" value="<?=$current_user->user_email?>" disabled="disabled"/>
                        </div>

                        <!-- Check if the current user email is confirmed. If it isn't, show button to resend confirmation -->
                        <?php $confirmed = is_email_confirmed($current_user->ID);
                            if ( !is_array($confirmed) && !$confirmed ) { ?>
                                <a href="#" style="color:#EBEBEB" id="resend-confirmation" class="btn btn-danger btn-zoi">Verificar mi cuenta</a>
                                <script type="text/javascript">
                                    jQuery(function( $ ) {

                                        // Click event for sending confirmation emails
                                        $('#resend-confirmation').click(function(event) {
                                            event.preventDefault(); 

                                            // AJAX to trigger send_confirmation_email function
                                            // @user_id int with current user_id
                                            $.ajax({
                                                type: "post", 
                                                url: "<?=admin_url('admin-ajax.php')?>",
                                                data : {
                                                    'action' : 'send_confirmation_email_helper',
                                                    'user_id' : '<?=$current_user->ID?>'
                                                },
                                                complete: function (jqxhr, textStatus) {
                                                },
                                                success: function(data){
                                                    if (data === 'success') {
                                                        alert("Correo enviado"); 
                                                    } else {
                                                        alert("Hay problemas con el servidor. Intente de nuevo más tarde"); 
                                                    }
                                                },
                                                error: function(errorThrown){
                                                    alert("Hay problemas con el servidor. Intente de nuevo más tarde"); 
                                                    console.log(errorThrown); 
                                                }
                                            }); 
                                        }); 
                                    }); 
                                </script>
                            <?php }
                            ?>
					</div>
					
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Notificaciones</span>
								<input value="<?php $noti = get_meta_notifications( wp_get_current_user()->ID,'frequency'); echo $noti? $translate[$noti[0]]: 'Sin establecer'?>" disabled>
							<span />
						 </div>
						 <a href="<?=get_bloginfo('url').'/notificaciones'?>" style="color:#EBEBEB" class="btn btn-danger btn-zoi">Cambiar Notificaciones</a>
					</div>
					
					<div class="clear"></div>
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>Contraseña <b>*</b></span>
							<input class="text-input" name="pass1" type="password" id="pass1" />
						 </div>
					</div>
					
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Repetir Contraseña <b>*</b></span>
							<input class="text-input" name="pass2" type="password" id="pass2" />
						 </div>
					</div>
					
					<div class="clear"></div>
					<br />
				   <p>Deja en blanco la contraseña si no deseas cambiarla</p>
				   <br />
   
					 <p class="form-submit">
						 <?php echo $referer; ?>
						 <input name="updateuser" type="submit" id="updateuser" class="submit button" value="Guardar" />
						 <?php wp_nonce_field( 'update-user' ) ?>
						 <input name="action" type="hidden" id="action" value="update-user" />
					 </p><!-- .form-submit -->
				</form><!-- #adduser -->
		
			</div>
		
		</div>
		
		<div id="personalinfo">
	
			<div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 box_centered box-form-userupdate">

				<form method="post" id="adduser" action="<?php the_permalink(); ?>">

					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>Nombre <b>*</b></span>
							<input class="text-input requerido" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'billing_first_name', $current_user->ID ); ?>" />
						 </div>
					</div>
					
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Apellido <b>*</b></span>
							<input class="text-input requerido" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'billing_last_name', $current_user->ID ); ?>" />
						 </div>
					 </div> 

					<div class="clear"></div>

					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>Cédula <b>*</b></span>
							<input class="text-input numerico requerido" name="cedula" type="text" id="cedula" value="<?php the_author_meta( 'billing_cirif', $current_user->ID ); ?>" />
						 </div>
					</div>

					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Nacionalidad <b>*</b></span>
							<div class="text-input">
                                <div class="select_div">
									<select name="billing_nationality" class="requerido">
										<option value=""> </option>
										<option value="Venezolano" <?php if(get_the_author_meta( 'billing_nationality', $current_user->ID ) == "Venezolano") echo "selected"; ?> >Venezolano</option>
										<option value="Extranjero" <?php if(get_the_author_meta( 'billing_nationality', $current_user->ID ) == "Extranjero") echo "selected"; ?> >Extranjero</option>
									</select>
								</div>
                            </div>
						 </div>
					</div>
					
						<div class="clear"></div>
                    <div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Email de facturación<b>*</b></span>
                            <?php 
                                $billing_email = get_the_author_meta( 'billing_email', $current_user->ID ); 
                                $user_email = !empty($billing_email) ? $billing_email : $current_user->user_email; ?>
							<input class="text-input requerido" name="email" type="text" id="email" value="<?=$user_email?>" />
						 </div>
                     </div> 


					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>Teléfono Móvil <b>*</b></span>
							<input class="text-input numerico requerido" name="phone1" type="text" id="phone1" value="<?php the_author_meta( 'billing_phone', $current_user->ID ); ?>" />
						 </div>
					</div>
					
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Teléfono Fijo</span>
							<input class="text-input numerico" name="phone2" type="text" id="phone2" value="<?php the_author_meta( 'billing_phone_o', $current_user->ID ); ?>" />
						 </div>
					 </div> 

					<div class="clear"></div>

					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>Dirección 1 <b>*</b></span>
							<input class="text-input requerido" name="address1" type="text" id="address1" value="<?php the_author_meta( 'billing_address_1', $current_user->ID ); ?>" />
						 </div>
					</div>
					
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Dirección 2</span>
							<input class="text-input" name="address2" type="text" id="address2" value="<?php the_author_meta( 'billing_address_2', $current_user->ID ); ?>" />
						 </div>
					 </div> 

					<div class="clear"></div>

					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>Ciudad <b>*</b></span>
							<input class="text-input requerido" name="city" type="text" id="city" value="<?php the_author_meta( 'billing_city', $current_user->ID ); ?>" />
						 </div>
					</div>
					
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Estado <b>*</b></span>
							<input class="text-input requerido" name="state" type="text" id="state" value="<?php the_author_meta( 'billing_state', $current_user->ID ); ?>" />
						 </div>
					 </div> 

					<div class="clear"></div>

					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>País <b>*</b></span>
							<input class="text-input requerido" name="coutry" type="text" id="country" value="<?php the_author_meta( 'billing_country', $current_user->ID ); ?>" />
						 </div>
					</div>
					
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right">
						 <div class="row-box">
							<span>Código Postal <b>*</b></span>
							<input class="text-input numerico requerido" name="postcode" type="text" id="postcode" value="<?php the_author_meta( 'billing_postcode', $current_user->ID ); ?>" />
						 </div>
					</div>

					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 right regular-date-picker">
						 <div class="row-box">
							<span>Fecha de nacimiento <b>*</b></span>
                            <input type="text" class="text-input requerido" name="billing_birthdate" id="billing_birthdate" value="<?= the_author_meta( 'billing_birthdate', $current_user->ID )?>" />
                        </div>
                    </div>

					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
						 <div class="row-box">
							<span>Sexo <b>*</b></span>
							<div class="text-input">
								<div class="select_div">
									<select name="billing_genre" class="requerido">
										<option value=""> </option>
										<option value="femenino" <?php if(get_the_author_meta( 'billing_genre', $current_user->ID ) == "femenino") echo "selected"; ?> >Femenino</option>
										<option value="masculino" <?php if(get_the_author_meta( 'billing_genre', $current_user->ID ) == "masculino") echo "selected"; ?> >Masculino</option>
									</select>
								</div>
							</div>
						 </div>
					</div>
					<div class="clear"></div>
					<div class="form-row col-xs-12 col-sm-6 col-md-6 col-lg-6 left">
                        <div class="newsletter-my-account">
                        <?php 
                            $subscription = get_user_meta( $current_user->ID, 'newsletter', true ); 
                        ?>
                        <input id="newsletter" type="checkbox" name="newsletter" <?php echo (is_array($subscription) && $subscription['subscribed'] ? "checked" : ""); ?> ><span>Deseo recibir ofertas de experiencias y eventos de ZOI Venezuela</span>
                        </div>
					</div>
                    <div class="clear"></div>
					
					 <p class="form-submit">
						 <?php echo $referer; ?>
						 <input name="updateuser" type="submit" id="updateuser" class="submit button" value="Guardar" />
						 <?php wp_nonce_field( 'update-user' ) ?>
						 <input name="action" type="hidden" id="action" value="update-user" />
					 </p><!-- .form-submit -->
				</form>

			</div>

		</div>
	
	
		<div id="purchases" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mcenter">
		
			<?php wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
		
		</div>
	
		<div id="likeexperiences">
			
			<section class="grid-container">
				<div class="grid">
	
					<?php
					global $wpdb;
					
					$userid = $current_user->ID;
					$posts_likes = $wpdb->get_results("SELECT * FROM like_post WHERE id_user_registered = $userid");
		
					$cat_count = sizeof( get_the_terms( 465, 'product_cat' ) );
						
					$cat_array = array();
					$product_array = array();
		
					foreach ( $posts_likes as $current_like ) {
						if ($current_like->like == 1) {
							$current_product = $current_like->id_post;
							array_push($product_array, $current_product);
							
							$current_categories = get_the_terms( $current_product, 'product_cat' );
		
							foreach ( $current_categories as $current_cat ) {
								$cat_id = $current_cat->term_id;
								$cat_name = $current_cat->name;
								$cat_count = sizeof( get_the_terms( $cat_id, 'product_cat' ));
								array_push($cat_array, $cat_id);  
							}	
						}
					}
					$cat_array = array_unique($cat_array); // Remove duplicates categories
		
					foreach($cat_array as $cat) {
						wp_reset_query();
						$args = array('post_type' => 'product',
							'tax_query' => array(
								array(
									'taxonomy' => 'product_cat',
									'field' => 'term_id',
									'terms' => $cat,
								),
							),
						 );
					
						 $loop = new WP_Query($args);
						 if($loop->have_posts()) {
							 
							$thumbnail_id = get_woocommerce_term_meta( $cat, 'thumbnail_id', true );
							$temp_image = wp_get_attachment_url( $thumbnail_id );
							$temp_link = get_term_link($category);
		
							?>
							<div class="item">				
							
								<div class="box_header" style="background:url(<?php echo $temp_image; ?>) center center no-repeat;">
																
								</div>					
							   <?php
								while($loop->have_posts()) : $loop->the_post();
					
									$temp_post_id = get_the_id();
			
									if (in_array($temp_post_id, $product_array)) { ?>
										<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
										
									<?php }
			
								endwhile;
								?>
							</div>					
						<?php
						}
					}?>
				</div>
			</section>
			<script src='http://masonry.desandro.com/masonry.pkgd.js'></script>
			
			<script type="text/javascript">
				jQuery(document).ready(function ($) {

				  var $container = $('.grid').masonry({
					columnWidth: 500,
					itemSelector: '.item',
					isFitWidth: true
				  });
				  
				  $('input').click(function () {
					$('.item').removeClass('expand');
					  //element.show();
					  $container.masonry();
				  });
				  $(this).on("click","#trigger_foto",function(e){
					e.preventDefault();
					$("#simple-local-avatar").trigger("click");
				  })
				});
			</script>
            <div class="clear"></div>

		</div>
	</div>
</div>

<div style="clear:both"></div>
	
<?php do_action( 'woocommerce_after_my_account' ); ?>

</div>
