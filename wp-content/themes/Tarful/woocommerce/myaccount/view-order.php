<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page
 *
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version   2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php	
$order_number = $order->get_order_number();
$order_url = str_replace('?order='.$order->id,'',esc_url( add_query_arg('order', $order->id, get_permalink( woocommerce_get_page_id( 'view_order' ) ))));
$current_user = get_current_user_id();

?>
<?php

if( isset($_POST['submit'])) {
	
	
	// Create post object
	$new_payment_support_post = array(
	  'post_title'    => 'Soporte Pago Orden ' .$order_number,
	  'post_content'  => '',
	  'post_type'  => 'payment_support',
	  'post_status'   => 'publish',
	  'post_author'   => $current_user,
	  'post_parent'   => $order_number,
	);

	// Insert the post into the database
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );

	$temp_file1 = $_FILES['payment_support']["tmp_name"];

	if (( $temp_file1 ) && !($_FILES['userfile']['error'])){
		$payment_support_id = wp_insert_post( $new_payment_support_post );

		$attachment_id = media_handle_upload( 'payment_support', $payment_support_id ); // Upload image
		add_post_meta($post_id, 'payment_support_attach', $attachment_id, true);
		add_post_meta($post_id, 'product_related_id', $payment_support_id, true);
		do_action('wp_insert_post', 'wp_insert_post');
		tarful_send_payment_support($order_number, $current_user);
	}
}
if(isset($_POST['submit'])){ 
	if(( $temp_file1 ) && !($_FILES['userfile']['error'])) { $user_info = get_userdata($current_user);?>
		<div class="woocommerce-message">Gracias <?php echo $user_info->first_name; ?>, Hemos recibido tu soporte de pago satisfactoriamente.</div>
	<?php }else{ ?>
		<div class="woocommerce-error">Disculpe, no hemos recibido su sopoerte de pago, por favor intente de nuevo</div>		
	<?php } ?>
<?php } ?>
<br /><br /><br />
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box_centered box_form_login box-myaccount">

<div id="box-order-detail" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mcenter box-my-order">

	<center>
		<a class="link_bak_to" href="<?php echo get_permalink(7); ?>#purchases">Regresar a mi histórico de compras</a>
	</center>
	
	<h2>Detalle de Compra</h2>
<center>
<p class="order-info"><?php printf( __( 'Order #<mark class="order-number">%s</mark> was placed on <mark class="order-date">%s</mark> and is currently <mark class="order-status">%s</mark>.', 'woocommerce' ), $order->get_order_number(), date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ), wc_get_order_status_name( $order->get_status() ) ); ?></p>
</center>


<?php
if ( in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_payment', array( 'pending', 'on-hold' ), $order ) ) ) { ?>

<div class="box_upload_payment_support">
<p class="order-info">Si realizaste tu compra con Déposito Bancario o Transferencia, por favor carga tu comprobante de pago para completar la orden</p>

	<form id="upload_posts" name="upload_posts" method="post" action="" enctype="multipart/form-data">
		<?php if(!(isset($_POST['submit'])) || !( $temp_file1 ) || ($_FILES['userfile']['error']) ) { ?>

			<input type="file" id="payment_support" name="payment_support"/>

			<input type="submit" name="submit" id="submit"  value="Enviar Soporte de pago" tabindex="1" id="submit" name="submit"/>
			<input type="hidden" name="uniqid" value="<?php echo uniqid();?>" />
			<input type="hidden" name="action" value="post_new_payment_support" />

		<?php } ?>

		<?php global $post; ?>
		<input type="hidden" name="page" id="page" value="<?php echo $post->ID; ?>"/>
		
		<?php wp_nonce_field( 'upload_posts' ); ?>

	</form>

</div>


<?php } ?>



<?php if ( $notes = $order->get_customer_order_notes() ) :
	?>
	<h2><?php _e( 'Order Updates', 'woocommerce' ); ?></h2>
	<ol class="commentlist notes">
		<?php foreach ( $notes as $note ) : ?>
		<li class="comment note">
			<div class="comment_container">
				<div class="comment-text">
					<p class="meta"><?php echo date_i18n( __( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?></p>
					<div class="description">
						<?php echo wpautop( wptexturize( $note->comment_content ) ); ?>
					</div>
	  				<div class="clear"></div>
	  			</div>
				<div class="clear"></div>
			</div>
		</li>
		<?php endforeach; ?>
	</ol>
	<?php
endif;

do_action( 'woocommerce_view_order', $order_id );
?>

</div>






</div>
