<?php
/**
 * Order details
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wpdb;
$order = wc_get_order( $order_id );
$order_numer = $order->get_order_number();

?>
<?php if (is_wc_endpoint_url( 'order-received' )) { ?>

	<h2 style="text-align:center"><?php _e( 'Order Details', 'woocommerce' ); ?></h2><h2><span class="box-type-payment">Tipo de Pago: <strong><?php echo $order->payment_method_title; ?></strong></span></h2>

<?php } ?>

<div class="clear"></div>

<div class="order_details_responsive">
		<?php
		if ( sizeof( $order->get_items() ) > 0 ) {?>
			<div class="row hidden-xs">
				<div class="col-sm-offset-6 col-sm-2 cantidad_pedido"><strong>Cantidad</strong> </div>
				<div class="col-sm-2"><strong>Precio Unit.</strong></div>
				<div class="col-sm-2"><strong>Sub-total</strong></div>
			</div>

			<?php
			foreach( $order->get_items() as $item_id => $item ) {
				$_product  = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
				$item_meta = new WC_Order_Item_Meta( $item['item_meta'], $_product );

				if ( apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
					?>
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<strong>
							<?php
								if ( $_product && ! $_product->is_visible() ) {
									echo apply_filters( 'woocommerce_order_item_name', $item['name'], $item );
								} else {
									echo apply_filters( 'woocommerce_order_item_name', sprintf( '<a href="%s">%s</a>', get_permalink( $item['product_id'] ), $item['name'] ), $item );
								}
							?>
							</strong>
							<?php
								$item_meta->display();
								echo '<p class="lenght"><span>'.CFS()->get('lenght', $item['product_id']).'</span></p>';
							?>
						</div>
						<div class="col-xs-6 hidden-sm hidden-md hidden-lg">Cantidad</div>
						<div class="col-xs-6 col-sm-2 right_xs cantidad_pedido">
							<?php

								echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '%s', $item['qty'] ) . '</strong>', $item );


								if ( $_product && $_product->exists() && $_product->is_downloadable() && $order->is_download_permitted() ) {

									$download_files = $order->get_item_downloads( $item );
									$i              = 0;
									$links          = array();

									foreach ( $download_files as $download_id => $file ) {
										$i++;

										$links[] = '<small><a href="' . esc_url( $file['download_url'] ) . '">' . sprintf( __( 'Download file%s', 'woocommerce' ), ( count( $download_files ) > 1 ? ' ' . $i . ': ' : ': ' ) ) . esc_html( $file['name'] ) . '</a></small>';
									}

									echo '<br/>' . implode( '<br/>', $links );
								}
							?>
						</div>
						<div class="col-xs-6 hidden-sm hidden-md hidden-lg">
							<?php echo "Precio Unitario"; ?>
						</div>
						<div class="col-xs-6 col-sm-2 right_xs">
							<?php echo $_product->get_price_html(); ?>
						</div>
						<div class="col-xs-6 hidden-sm hidden-md hidden-lg">
							<?php echo "Sub-total Unit."; ?>
						</div>
						<div class="col-xs-6 col-sm-2 right_xs">
							<?php echo $order->get_formatted_line_subtotal( $item ); ?>
						</div>
					</div>
					<?php
				}

				if ( $order->has_status( array( 'completed', 'processing' ) ) && ( $purchase_note = get_post_meta( $_product->id, '_purchase_note', true ) ) ) {
					?>
					<div class="row">
						<div style="col-xs-12"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></div>
					</div>
					<?php
				}
			}
		}

		$has_refund = false;

		if ( $total_refunded = $order->get_total_refunded() ) {
			$has_refund = true;
		}

		if ( $totals = $order->get_order_item_totals() ) {
			?>
			<div class="row">
			<?php
			foreach ( $totals as $key => $total ) {
				$value = $total['value'];

				// Check for refund
				if ( $has_refund && $key === 'order_total' ) {
					$refunded_tax_del = '';
					$refunded_tax_ins = '';

					// Tax for inclusive prices
					if ( wc_tax_enabled() && 'incl' == $order->tax_display_cart ) {

						$tax_del_array = array();
						$tax_ins_array = array();

						if ( 'itemized' == get_option( 'woocommerce_tax_total_display' ) ) {

							foreach ( $order->get_tax_totals() as $code => $tax ) {
								$tax_del_array[] = sprintf( '%s %s', $tax->formatted_amount, $tax->label );
								$tax_ins_array[] = sprintf( '%s %s', wc_price( $tax->amount - $order->get_total_tax_refunded_by_rate_id( $tax->rate_id ), array( 'currency' => $order->get_order_currency() ) ), $tax->label );
							}

						} else {
							$tax_del_array[] = sprintf( '%s %s', wc_price( $order->get_total_tax(), array( 'currency' => $order->get_order_currency() ) ), WC()->countries->tax_or_vat() );
							$tax_ins_array[] = sprintf( '%s %s', wc_price( $order->get_total_tax() - $order->get_total_tax_refunded(), array( 'currency' => $order->get_order_currency() ) ), WC()->countries->tax_or_vat() );
						}

						if ( ! empty( $tax_del_array ) ) {
							$refunded_tax_del .= ' ' . sprintf( __( '(Includes %s)', 'woocommerce' ), implode( ', ', $tax_del_array ) );
						}

						if ( ! empty( $tax_ins_array ) ) {
							$refunded_tax_ins .= ' ' . sprintf( __( '(Includes %s)', 'woocommerce' ), implode( ', ', $tax_ins_array ) );
						}
					}

					$value = '<del>' . strip_tags( $order->get_formatted_order_total() ) . $refunded_tax_del . '</del> <ins>' . wc_price( $order->get_total() - $total_refunded, array( 'currency' => $order->get_order_currency() ) ) . $refunded_tax_ins . '</ins>';
				}
				if($total['label'] == "Total:"){
				?>
				<div class="col-sm-3 hidden-xs">
					<a href="<?php echo $order->get_view_order_url(); ?>" class="btn_order_detail">Ver Detalles del Pedido</a>
				</div>
				<div class="col-sm-3 hidden-xs">
					<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' )); ?>" class="link_order_detail">Regresar a Experiencias</a>
				</div>
				<div class="col-sm-2 hidden-xs"></div>
				<div class="col-xs-6 col-sm-2"> <?php echo $total['label']; ?></div>
				<div class="col-xs-6 col-sm-2 right_xs"> <?php echo $value; ?></div>
				<?php
				}else{
				?>
				<div class="col-xs-6 col-sm-offset-8 col-sm-2"> <?php echo $total['label']; ?></div>
				<div class="col-xs-6 col-sm-2 right_xs"> <?php echo $value; ?></div>
				<?php
				}
			}
			?>
			</div>
			<?php
		}
		// Check for refund
		if ( $has_refund ) { ?>
			?>
			<div class="row">
				<?php _e( 'Refunded:', 'woocommerce' ); ?>
				-<?php echo wc_price( $total_refunded, array( 'currency' => $order->get_order_currency() ) ); ?>
			?>
			</div>
		<?php
		}
		// Check for customer note
		if ( '' != $order->customer_note ) { ?>
			?>
			<div class="row">
			<?php _e( 'Note:', 'woocommerce' ); ?>
			<?php echo wptexturize( $order->customer_note ); ?>
			</div>
		<?php } ?>
</div>

