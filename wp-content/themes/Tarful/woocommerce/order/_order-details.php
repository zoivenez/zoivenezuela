<?php
/**
 * Order details
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$order = wc_get_order( $order_id );

?>
<?php if (!is_wc_endpoint_url( 'order-received' )) { ?>

	<h2>Detalle de Compra</h2>

<?php } else { ?>

	<h2><?php _e( 'Order Details', 'woocommerce' ); ?><span class="box-type-payment">Tipo de Pago: <strong><?php echo $order->payment_method_title; ?></strong></span></h2>

<?php } ?>

<table class="shop_table order_details">
	<tbody>
		<?php
		if ( sizeof( $order->get_items() ) > 0 ) {

			foreach( $order->get_items() as $item_id => $item ) {
				$_product  = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
				$item_meta = new WC_Order_Item_Meta( $item['item_meta'], $_product );

				if ( apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
					?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
						<td>
							photo
							
						</td>
						
						
						
						<td colspan="2" class="product-name">
							<?php
								if ( $_product && ! $_product->is_visible() ) {
									echo apply_filters( 'woocommerce_order_item_name', $item['name'], $item );
								} else {
									echo apply_filters( 'woocommerce_order_item_name', sprintf( '<a href="%s">%s</a>', get_permalink( $item['product_id'] ), $item['name'] ), $item );
								}

								echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );

								echo '<div style="clear:both;"></div>';

								// Allow other plugins to add additional product information here
								do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

								$item_meta->display();

								// Allow other plugins to add additional product information here
								do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
							?>
							
								<?php echo '<p class="lenght"><span>'.CFS()->get('lenght', $item['product_id']).'</span></p>'; ?>
							
						</td>
						
						<?php if (is_wc_endpoint_url( 'order-received' )) { ?>
							<td class="thx-product-qty">
								<?php echo $item['qty']; ?>
							</td>
							<td class="product-unit">
								<?php echo $_product->get_price_html(); ?>
							</td>
						<?php } ?>

						<td class="product-total">
							<?php echo $order->get_formatted_line_subtotal( $item ); ?>
						</td>
					</tr>
					<?php
				}

				if ( $order->has_status( array( 'completed', 'processing' ) ) && ( $purchase_note = get_post_meta( $_product->id, '_purchase_note', true ) ) ) {
					?>
					<tr class="product-purchase-note">
						<td colspan="3"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></td>
					</tr>
					<?php
				}
			}
		}

		do_action( 'woocommerce_order_items_table', $order );
		?>
	</tbody>
	<tfoot>


	<?php	
		if ( $totals = $order->get_order_item_totals() ) {
			foreach ( $totals as $key => $total ) {
								
				$value = $total['value'];
				?>

				<?php if ($total['label'] != 'Forma de pago:') { ?>

					<tr>
						<td colspan="3" style="text-align: right;" class="td-footer-total"><?php echo $total['label']; ?></td>
						<td style="text-align: right;"><?php echo $value; ?></td>
					</tr>
				<?php } ?>
				<?php
			}
		}

		// Check for customer note
		if ( '' != $order->customer_note ) { ?>
			<tr>
				<th scope="row"><?php _e( 'Note:', 'woocommerce' ); ?></th>
				<td><?php echo wptexturize( $order->customer_note ); ?></td>
			</tr>
		<?php } ?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>



<?php if (!is_wc_endpoint_url( 'order-received' )) { ?>

	<header>
		<h2><?php _e( 'Customer details', 'woocommerce' ); ?></h2>
	</header>
	<table class="shop_table shop_table_responsive customer_details">
	<?php
		if ( $order->billing_email ) {
			echo '<tr><th>' . __( 'Email:', 'woocommerce' ) . '</th><td data-title="' . __( 'Email', 'woocommerce' ) . '">' . $order->billing_email . '</td></tr>';
		}
	
		if ( $order->billing_phone ) {
			echo '<tr><th>' . __( 'Telephone:', 'woocommerce' ) . '</th><td data-title="' . __( 'Telephone', 'woocommerce' ) . '">' . $order->billing_phone . '</td></tr>';
		}
	
		// Additional customer details hook
		do_action( 'woocommerce_order_details_after_customer_details', $order );
	?>
	</table>
	
<?php } ?>

<?php if (!is_wc_endpoint_url( 'order-received' )) { ?>

	<?php if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) : ?>
	
	<div class="col2-set addresses">
	
		<div class="col-1">
	
	<?php endif; ?>
	
			<header class="title">
				<h3><?php _e( 'Billing Address', 'woocommerce' ); ?></h3>
			</header>
			<address>
				<?php
					if ( ! $order->get_formatted_billing_address() ) {
						_e( 'N/A', 'woocommerce' );
					} else {
						echo $order->get_formatted_billing_address();
					}
				?>
			</address>
	
	<?php if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) : ?>
	
		</div><!-- /.col-1 -->
	
		<div class="col-2">
	
			<header class="title">
				<h3><?php _e( 'Shipping Address', 'woocommerce' ); ?></h3>
			</header>
			<address>
				<?php
					if ( ! $order->get_formatted_shipping_address() ) {
						_e( 'N/A', 'woocommerce' );
					} else {
						echo $order->get_formatted_shipping_address();
					}
				?>
			</address>
	
		</div><!-- /.col-2 -->
	
	</div><!-- /.col2-set -->
	
	<?php endif; ?>

<?php } ?>

<div class="clear"></div>
