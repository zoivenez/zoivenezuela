<?php
/**
 * Cart item data (when outputting non-flat)
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version 	2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<dl class="variation">
	<?php
		foreach ( $item_data as $data ) :
			$key = sanitize_text_field( $data['key'] );
            if ( (($key === 'Fechas de Experiencia' && $data['value'] !== "Reserva tu fecha") || $key !== 'Fechas de Experiencia') 
                  && $key !== 'Transporte' && $key !== 'Grupo')  : 

            $completer = ''; 
            if ($key === "Habitación") {
                $completer = 'Habitación: ';
            } elseif ($key === 'Número de personas') {
                $completer = 'Número de personas: ';
            }
	?>
		<dt class="variation-<?php echo sanitize_html_class( $key ); ?>"><?php echo wp_kses_post( $data['key'] ); ?>:</dt>
		<dd class="variation-<?php echo sanitize_html_class( $key ); ?>"><?php echo wp_kses_post(wpautop( $completer . $data['value'] )); ?></dd> </br>
    <?php endif;
        endforeach; ?>
</dl>
