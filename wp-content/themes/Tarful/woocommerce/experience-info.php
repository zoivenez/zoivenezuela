<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $wodb, $post, $woocommerce, $product;
$testimoniales = CFS()->get('product_testimonials');
$i = 0;
$x = 0;

 
$product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
$category = array_shift( $product_cats );

?>
<?php if($category->name != "Producto"): ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-includes">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <h3>¿POR QUÉ NOS ENCANTA? &nbsp;  </h3>
            <?php echo CFS()->get('loving_reasons'); ?>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 que-incluye">
            <h3>¿QUÉ INCLUYE? &nbsp;  </h3>
            <?php echo CFS()->get('includes'); ?>
        </div>
    </div>
</div>

<div style="clear:both;"></div>
<?php else:?>
    <br>
    <div style="clear:both;"></div>
<?php endif; ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-desc">
        <?php if($category->name != "Producto"): ?>
            <h3>¿Cómo será la experiencia?</h3>

            <?php if ( $post->post_content ) { ?>
                <div itemprop="description">
                    <?php echo apply_filters( 'woocommerce_short_description', $post->post_content ); ?>
                </div>
            <?php } ?>

            <?php
            $guru = get_post_meta ( get_the_ID(), 'guru', true );
            if (isset($guru) && $guru) { 
                $guru_info = get_userdata($guru);
                if ($guru_info): ?>
            <a href="<?php echo get_site_url(); ?>/zoi-gurus/?id_guru=<?php echo $guru_info->ID; ?>">
                <div class="box_guru_grid">
                    <div class="guru_info">
                        <?php echo get_avatar( $guru, 40 ); ?>
                        <h5 class="detail-product-guru-name"><?php echo $guru_info->display_name; ?></br><?php echo _e('ZOI GURÚ'); ?></h5>
                    </div>
                </div>
            </a>
            <?php 
                endif;
            } ?>
        <?php else: ?>
            <h3>Descripcion del Producto</h3>
            <?php if ( $post->post_content ) { ?>
                <div itemprop="description">
                    <?php echo apply_filters( 'woocommerce_short_description', $post->post_content ); ?>
                </div>
            <?php } ?>
        <?php endif; ?>

    </div>
</div>

<?php if($category->name != "Producto"): ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 intinerary">
        <ul class="accordion">
          <li>
            <input type="checkbox" checked>
            <i></i>
            <h3>ITINERARIO</h3>
                <div class="content-accordion"><?php echo CFS()->get('intinerary'); ?></div>
          </li>
        </ul>
    </div>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 faqs">
        <ul class="accordion">
          <li>
            <input type="checkbox" checked>
            <i></i>
            <h3>PREGUNTAS FRECUENTES</h3>
                <div class="content-accordion"><?php echo CFS()->get('faqs'); ?></div>
          </li>
        </ul>
    </div>
</div>
<!--<div class="row">
    <div id="review" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 faqs">
        
            <h3>COMENTARIOS (<?php echo $product->get_review_count()?>)</h3>
                <div class="content-accordion"><?php comments_template()?></div>
    </div>
</div> --> 

<script type="text/javascript">
     $(document).ready(function(){
            $('#review-link').click(function(event){
                 event.preventDefault();
                $('html,body').animate({scrollTop:$('#reviews').offset().top-100}, 800); 
            });
    
});
</script>

<div class="row">
<?php get_template_part('content','terms' ); ?>
</div>
