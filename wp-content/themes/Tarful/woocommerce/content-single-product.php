<?php
/**
 * @author 		Tarful
 * @package 	Tarful/woocommerce
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $wodb, $post, $woocommerce, $product;
$testimoniales = CFS()->get('product_testimonials');
$i = 0;
$x = 0;
 ?>

<?php
	/* woocommerce_before_single_product hook	 * @hooked wc_print_notices - 10 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }

	$product_place = wp_get_post_terms( get_the_ID(), 'localidad');
	$single_place = array_shift( $product_place);
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row-fluid mcenter limit">
		<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
            <?php get_template_part('woocommerce/experience-slider') ?>
        </div> <!-- main slider -->
		<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 pull-right product-info">
            <?php get_template_part('woocommerce/experience-booking-info') ?>
        </div> <!-- .product-info -->
		<div class="col-xs-hidden col-sm-hidden col-md-hidden col-lg-1 pull-right"></div>
    </div>
    <div class="row-fluid mcenter limit">
        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
            <?php get_template_part('woocommerce/experience-info') ?>
        </div> 
		<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 pull-right product-info promise-wrapper">
            <?php get_template_part('content','zoi' ); ?>
        </div> <!-- ZOI promise -->
		<div class="col-xs-hidden col-sm-hidden col-md-hidden col-lg-1 pull-right"></div>
    </div> <!-- experience details -->
    <div class="row-fluid mcenter limit">
		<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3 pull-right product-info contact-wrapper">
            <?php get_template_part('woocommerce/experience-contact-info') ?>
        </div> <!--  extra info -->
    </div> <!--  extra info -->
	<div class="row-fluid mcenter limit">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php
            /**
             * woocommerce_after_single_product_summary hook
             *
             * @hooked woocommerce_output_product_data_tabs - 10
             * @hooked woocommerce_upsell_display - 15
             * @hooked woocommerce_output_related_products - 20
             */
            do_action( 'woocommerce_after_single_product_summary' );
        ?>
    </div> <!-- related experiences -->



<?php do_action( 'woocommerce_after_single_product' ); ?>




<!-- MODAL REVIEW -->

<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->id ) ) : ?>
<?
	$arg = array (
		'user_id' => wp_get_current_user()->ID,
		'post_id' => get_the_ID()
		);
	$already_commented = get_comments($arg);
	if($already_commented)
		$rating = get_comment_meta( $already_commented['0']->{'comment_ID'}, 'rating', true );
	?> 

<? if ($already_commented):?>
<div id="review-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="social-login">

                    <div id="review_form_wrapper">
			<div id="review_form">

				<?php
					$commenter = wp_get_current_commenter();

					$comment_form = array(
						'title_reply'          => '<h4 style="text-align: center;">Editar Comentario</h4>',
						'title_reply_to'       => __( 'Leave a Reply to %s', 'woocommerce' ),
						'comment_notes_before' => '',
						'comment_notes_after'  => '',
						'fields'               => array(
							'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
							            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" /></p>',
							'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
							            '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" /></p>',
						),
						'label_submit'  => __( 'Submit', 'woocommerce' ),
						'logged_in_as'  => '',
						'comment_field' => ''
					);

					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<p class="comment-form-rating"><label for="rating">' . __( 'Your Rating', 'woocommerce' ) .'</label>
												<div class="stars">
													<input type="radio" name="rating" class="star-1" id="star-1" value="1" />
													<label class="star-1" for="star-1" data-toggle="tooltip" data-placement="top" title="No me gustó">1</label>
													<input type="radio" name="rating" class="star-2" id="star-2" value="2" />
													<label class="star-2" for="star-2"  data-toggle="tooltip" data-placement="top" title="Regular">2</label>
													<input type="radio" name="rating" class="star-3" id="star-3" value="3" />
													<label class="star-3" for="star-3" data-toggle="tooltip" data-placement="top" title="Bueno">3</label>
													<input type="radio" name="rating" class="star-4" id="star-4" value="4"/>
													<label class="star-4" for="star-4" data-toggle="tooltip" data-placement="top" title="Muy bueno">4</label>
													<input type="radio" name="rating" class="star-5" id="star-5" value="5" />
													<label class="star-5" for="star-5" data-toggle="tooltip" data-placement="top" title="Me encantó">5</label>
													<span></span>
												</div>
						</p>';
					}

					$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . __( 'Your Review', 'woocommerce' ) . '</label><textarea id="comment" name="comment" cols="45" rows="4" aria-required="true" autofocus>'.$already_commented["0"]->{"comment_content"}.'</textarea></p>';

					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>


                    </div> <!-- tab panel --> 
                </div> <!-- tab content -->
            </div> <!-- /.modal-body -->
            <div class="modal-footer" style="text-align: center;">
            	<a id="delete-review" href="javascript:;"> Eliminar Comentario</a>
                
            </div> <!-- /.modal-footer -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">

$('document').ready(function(){
	

	$('.stars #star-<?=$rating?>').attr('checked', 'checked');
	/* edit review */
	$('#submit').on('click',function(e){
    	e.preventDefault();

    	$.ajax({
                        'type' : 'post',
                        'url'  : "<?=admin_url('admin-ajax.php')?>",
                        'data' : {
                            'action'    : 'review_helper',
                            'comment_id' : '<?php echo $already_commented["0"]->{"comment_ID"}?>',
                            'act'    : 'edit',
                            'comment'	: $('#comment').val(),
                            'rating'	: $('input[name=rating]:checked').val()
                        },
                        beforeSend: function(data, textStatus, jqXHR ){
                        	$('.modal-footer').empty();
                        	$('.modal-footer').append('<a id="delete-review" href="javascript:;"> Eliminar Comentario</a>');
                            
                        },
                        sucesss: function(data, textStatus, jqXHR ){
     	
     						
                        },
                        complete: function(data, dataStatus, jqXHR) {
                        	

                            var json = $.parseJSON(data.responseText);
                            
                            if(json.response == '1'){
                            	$('.modal-footer').append('<h5>'+ json.message + '</h5>');
                            	setTimeout(function(){
			                        window.location.reload();
			                    }, 2000);  
                            }else{
                            	$('.modal-footer').append('<h5>'+ json.message + '</h5>');
                            }

                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            

                        }

                      });

	});

	
	/* delete review*/

	$('#delete-review').on('click',function(e){
    	e.preventDefault();

    	$.ajax({
                        'type' : 'post',
                        'url'  : "<?=admin_url('admin-ajax.php')?>",
                        'data' : {
                            'action'    : 'review_helper',
                            'comment_id' : '<?php echo $already_commented["0"]->{"comment_ID"}?>',
                            'act'    : 'delete'
                        },
                        beforeSend: function(data, textStatus, jqXHR ){
                        	$('.modal-footer').empty();
                        	$('.modal-footer').append('<a id="delete-review" href="javascript:;"> Eliminar Comentario</a>');
                            
                        },
                        sucesss: function(data, textStatus, jqXHR ){
     	
     						
                        },
                        complete: function(data, dataStatus, jqXHR) {
                        	

                            var json = $.parseJSON(data.responseText);
                            
                            if(json.response == '1'){
                            	$('.modal-footer').append('<h5>'+ json.message + '</h5>');
                            	setTimeout(function(){
			                        window.location.reload();
			                    }, 2000);  
                            }else{
                            	$('.modal-footer').append('<h5>'+ json.message + '</h5>');
                            }

                        },

                        error: function(jqXHR, textStatus, errorThrown) {
                            

                        }

                      });

	});



});

</script>
<? else: ?>
	<div id="review-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="social-login">

                    <div id="review_form_wrapper">
			<div id="review_form">

				<?php
					$commenter = wp_get_current_commenter();

					$comment_form = array(
						'title_reply'          => have_comments() ? '<h4 style="text-align: center;"> Agregar Comentario</h4>' : __( 'Be the first to review', 'woocommerce' ) . ' &ldquo;' . get_the_title() . '&rdquo;',
						'title_reply_to'       => __( 'Leave a Reply to %s', 'woocommerce' ),
						'comment_notes_before' => '',
						'comment_notes_after'  => '',
						'fields'               => array(
							'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
							            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" /></p>',
							'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
							            '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" /></p>',
						),
						'label_submit'  => __( 'Submit', 'woocommerce' ),
						'logged_in_as'  => '',
						'comment_field' => ''
					);

					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<p class="comment-form-rating"><label for="rating">' . __( 'Your Rating', 'woocommerce' ) .'</label>
												<div class="stars">
													<input type="radio" name="rating" class="star-1" id="star-1" value="1" />
													<label class="star-1" for="star-1" data-toggle="tooltip" data-placement="top" title="No me gustó">1</label>
													<input type="radio" name="rating" class="star-2" id="star-2" value="2" />
													<label class="star-2" for="star-2"  data-toggle="tooltip" data-placement="top" title="Regular">2</label>
													<input type="radio" name="rating" class="star-3" id="star-3" value="3" />
													<label class="star-3" for="star-3" data-toggle="tooltip" data-placement="top" title="Bueno">3</label>
													<input type="radio" name="rating" class="star-4" id="star-4" value="4"/>
													<label class="star-4" for="star-4" data-toggle="tooltip" data-placement="top" title="Muy bueno">4</label>
													<input type="radio" name="rating" class="star-5" id="star-5" value="5" />
													<label class="star-5" for="star-5" data-toggle="tooltip" data-placement="top" title="Me encantó">5</label>
													<span></span>
												</div>
						</p>';
					}

					$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . __( 'Your Review', 'woocommerce' ) . '</label><textarea id="comment" name="comment" cols="45" rows="6" aria-required="true"></textarea></p>';

					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>


                    </div> <!-- tab panel --> 
                </div> <!-- tab content -->
            </div> <!-- /.modal-body -->
            <div class="modal-footer">
                
            </div> <!-- /.modal-footer -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>
<?php endif; ?>

<!-- END MODAL REVIEW -->






<script type="text/javascript">
	var id_post= <?php the_ID(); ?>;
	$(document).ready(function(){
		$('.slider1').bxSlider({
            speed: 100,
			slideWidth: 200,
			minSlides: 2,
			maxSlides: 3,
			slideMargin: 10
		});
		var retardo = 200;
		$(this).on("click",".linkOverlayUsuarios",function(e){
			e.preventDefault();
			if(!$('.uers_likes_all').is(':empty')){
				$(".overlayFondo").fadeIn(retardo);
				$(".modalUsuarios").fadeIn(retardo);
				enderizarModalUsuarios();
			}
		});
		$(this).on("click",".overlayFondo",function(){
			$(".overlayFondo").fadeOut(retardo);
			$(".modalUsuarios").fadeOut(retardo);
		});
		var modalUsuario = document.getElementsByClassName("modalUsuarios")[0];
		function enderizarModalUsuarios(){
			modalUsuario.style.left = ((document.getElementsByClassName("overlayFondo")[0].offsetWidth - modalUsuario.offsetWidth)/2)+"px";
		}
		$(window).resize(function() {
			enderizarModalUsuarios();
		});
		$(this).on("click",".chat",function(e){
			e.preventDefault();
			if($("#habla_window_state_div").hasClass("olrk-state-compressed")){
				$("#habla_topbar_div").trigger("click");
			}
			$("#habla_name_input").focus();
		})
            
    });

</script>

<script type="text/javascript">
$(document).ready(function(){
	$('.form-submit').css({'text-align' : 'center'});
	$('.form-submit input#submit').css({
	'width' : '40%',
	'background-color':' #e74c3c',
    'color': '#fff',
    'font-size': '14px',
    'font-family': "open_sansregular",
    'border-radius': '3px',
   'margin-bottom': '10px',
    '-webkit-box-shadow': '0px 3px 0px 0px #b32e20',
    '-moz-box-shadow': '0px 3px 0px 0px #b32e20',
    'box-shadow': '0px 3px 0px 0px #b32e20'
	});


});
</script>