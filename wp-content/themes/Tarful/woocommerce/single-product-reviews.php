<?php
/**
 * Display single product reviews (comments)
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.2
 */
global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews">
	<div id="comments">
		<h2><?php
			if ( !get_option( 'woocommerce_enable_review_rating' ) === 'yes' && !( $count = $product->get_review_count() ) )
				//printf( _n( '%s review for %s', '%s reviews for %s', $count, 'woocommerce' ), $count, get_the_title() );
			
				_e( 'Reviews', 'woocommerce' );
		?></h2><br>

		<?php if ( have_comments() ) : ?>

			<ol class="commentlist">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="woocommerce-pagination">';
				paginate_comments_links( apply_filters( 'woocommerce_comment_pagination_args', array(
					'prev_text' => '&larr;',
					'next_text' => '&rarr;',
					'type'      => 'list',
				) ) );
				echo '</nav>';
			endif; ?>

		<?php else : ?>

			<p class="woocommerce-noreviews"><?php _e( 'No hay comentarios todavía', 'woocommerce' ); ?></p>

		<?php endif; ?>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->id ) ) : ?>
	<?
	$arg = array (
		'user_id' => wp_get_current_user()->ID,
		'post_id' => get_the_ID()
		);
	$already_commented = get_comments($arg);
		?>
		<?php if ($already_commented) :?>
			
			 <a id="add-review" style="white-space: nowrap;" class="button-review col-xs-5 col-md-3 cols-lg-3 center-block" href="javascript:;">Editar comentario</a>		
		<?php else :?>	

			<?php if (is_user_logged_in()) :?>
			 	<a id="add-review" style="white-space: nowrap;" class="button-review col-xs-5 col-md-3 cols-lg-3 center-block" href="javascript:;">Añadir comentario</a>
			 <?else:?>
			 	<p class="woocommerce-noreviews"><?php _e( 'Para dejar un comentario debes <a style="color: #e74c3c;" href='.get_bloginfo("url")."/mi-cuenta".'>iniciar sesión<a>', 'woocommerce' ); ?></p>
			 <?endif;?>
		<?php endif; ?>
		<br>
	<?php else : ?>

		<p class="woocommerce-verification-required"><?php _e( 'Only logged in customers who have purchased this product may leave a review.', 'woocommerce' ); ?></p>

	<?php endif; ?>

	<div class="clear"></div>
</div>

<script type="text/javascript">
 jQuery(function($) {
 	$('#add-review').on('click',function(){
		$('#review-modal').attr('style', ''); 
	    $('#review-modal').modal('show');
	});
});
</script>


