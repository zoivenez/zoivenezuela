<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

?>


		<?php if ( $product->is_purchasable() ) {
			// Availability
			$availability      = $product->get_availability();
			$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';
			
			if ( $product->is_in_stock() ) {

				 do_action( 'woocommerce_before_add_to_cart_form' ); ?>
					
				<form class="cart" method="post" enctype='multipart/form-data'>

				 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

						<div class="price">
							<h4>Precio por persona</h4> <?php echo $product->get_price_html()?>
						</div>					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-3 cant no-padding">

						<h5>cantidad</h5>
					 	<?php
					 		if ( ! $product->is_sold_individually() )
					 			woocommerce_quantity_input( array(
					 				'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
					 				'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
					 			) );
					 	?>
						<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 no-padding" style="padding-left:10px">
						<button type="submit" class="avalaible buy-button"><h4>comprar</h4></button>
					</div>

					<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
				</form>
				
				<p class="av">Quedan <?php echo $product->get_total_stock() ?> cupos </p> 

		
				<?php  do_action( 'woocommerce_after_add_to_cart_form' );
			 } else {
				echo '<p class="stock out-of-stock">Experiencia Agotada</p>';
			 }
			
		} ?>
		




