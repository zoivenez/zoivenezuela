<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $wodb, $post, $woocommerce, $product;
$testimoniales = CFS()->get('product_testimonials');
$i = 0;
$x = 0;

?>
<h1 itemprop="name" class="col-md-12 col-md-12 col-md-10 col-lg-10 product_title"><?php the_title(); ?></h1>

			<?php $like = likeThis(get_the_ID()); ?>
        <div class="like like-top like-<?php echo get_the_ID(); ?> <?php if($like && $like->like){echo "done";} ?>">
            <a href="#" class="likeThis " data-post-id="<?php echo get_the_ID(); ?>">ME GUSTA</a>
        </div>

        <div class="images box-product-slider">
            <?php 
                    $attachment_ids = $product->get_gallery_attachment_ids();
                    $number = count($attachment_ids);

                if ( !empty($attachment_ids)) {?>

                <div id="carousel-product" class="carousel slide main-slider-product" data-ride="carousel">
                      <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php { $k = 0;
                            foreach( $attachment_ids as $attachment_id ) {
                             $image_link = wp_get_attachment_url( $attachment_id ); ?>
                                
                                <div class="item <?php if ($k == 0){ echo 'active';} ?>">
                                    <img src="<?php  echo $image_link; ?>" alt="<?=get_the_title()?>">
                                </div>
                            
                        <?php $k=1; } }?>
                    </div>
                    <a class="left carousel-control" href="#carousel-product" role="button" data-slide="prev">
                        <img src="<?php bloginfo('stylesheet_directory');?>/images/nav_tarful_left.png" class="left_icon carousel_icon">
                    </a>
                    <a class="right carousel-control" href="#carousel-product" role="button" data-slide="next">
                        <img src="<?php bloginfo('stylesheet_directory');?>/images/nav_tarful_right.png" class="right_icon carousel_icon">
                    </a>
                </div> 
                <?php }?>
        
            <div class="slider1">

                <?php

                if (!empty($attachment_ids)) {
                $key = 0;
                 foreach( $attachment_ids as $attachment_id ) {
                    $image_link = wp_get_attachment_url( $attachment_id ); ?>
                    <div class="slide" data-target="#carousel-product" data-slide-to="<?php echo $key ?>"><img src="<?php echo $image_link ?>"></div>
                    <?php ++$key; ?>
                <?php  } 
                }

                if ( (empty($attachment_id)) || (sizeof($attachment_ids) < 7) ) { ?>
                <style>
                    .bx-clone{ display : none !important;}
                </style>
                
            <?php }	?>
              
            </div>

        </div>
