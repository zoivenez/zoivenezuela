	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 extra-info">
        <div class="hidden-xs hidden-sm hidden-md col-lg-12">
        </div>
	
		<div id="shrink-right" >
			<?php if (!empty($testimoniales)) { ?>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-testimonials no-padding">
					<h4 id="test-tittle">Testimoniales de la experiencia</h4>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product-testimonials no-padding">
					<div style="clear: both;"></div>
					<?php
					$number = sizeof($testimoniales);
					$once = 0;
					$j =0;
					?>
					<div id="carousel-example-generic-2" class="carousel slide testimonials-slider" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<?php foreach ($testimoniales as $testimonial) { ?>	
								<div class="item <?php if ($once==0) { echo 'active'; } ?>  ">
									<div class="carousel-caption">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
											<div class="box_avatar_block"><img class="user" src="<?php echo $testimonial['prod_testimonial_avatar']; ?>" alt="<?php echo $testimonial['prod_testimonial_name']; ?>"></div>
											<h4><?php echo $testimonial['prod_testimonial_name']; ?></h4>
											<p><?php tarful_fixed_lenght_notags($testimonial['prod_testimonial_frase'], 100); ?></p>
										</div>
									</div>
								</div>
	
								<?php $once=1;?>
							<?php }?>
						</div>	<!-- carousel-inner -->
						<ol class="carousel-indicators">
							<?php while ($j < $number ) { 
								if ($j==0){
									echo '<li data-target="#carousel-example-generic-2" data-slide-to="0" class="active"></li>';
								} 
								else{
									echo '<li data-target="#carousel-example-generic-2" data-slide-to="'.$j.'"></li>';
								} 
								$j++; 
							}?>
						</ol> <!-- carousel-indicators -->
					</div> <!-- carousel-example-generic-2 -->
                </div> <!-- product-testimonials -->
			<?php } ?>		

			<div style="clear:both;"></div>	

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding network">
				<div class="col-xs-4 col-sm-12 col-md-12 col-lg-4">
					<div class="followers mcenter cantidad_likes-<?php echo get_the_ID(); ?>">
						<?php echo tarful_likes(get_the_ID()); ?>
					</div>
					<button class="avalaible like" type="submit">ME GUSTA</button>
				</div>
                <div class="hidden-xs col-sm-12 hidden-lg col-md-12" style="clear:both"></div>
				<div class="col-xs-8 col-sm-12 col-md-12 col-lg-8 ">
					<div class="uers_likes">
					<?php tarful_networks_avatars(4, $post->ID);  ?>
					</div>
					<a class="av linkOverlayUsuarios" href="#">ver todos los usuarios</a>
					<div style="clear:both;"></div>
				</div>
			</div> <!-- wells network -->

			<div style="clear:both;"></div>

			<?php tarful_facebook_share(get_the_ID() , apply_filters( 'woocommerce_short_description', $post->post_content )); ?>
			<?php tarful_twitter_share(); ?>

			<?php get_template_part('content','finals' ); ?>
		</div>
	<?php $product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
		$validation_category = array_shift( $product_cats );
		if($validation_category->name != "Producto") :
		?>
			<div id="mapbox"></div>
			<div style="clear:both;"></div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 map-box">
				<h3>Estado <?php if (!empty($single_place)) { echo $single_place->name;}  ?></h3>
			</div>
			<?php $locator = CFS()->get('mapa_experiencia'); ?>

			<script src="https://maps.googleapis.com/maps/api/js"></script>
		    <script>
		      function initialize() {
		        var mapCanvas = document.getElementById('map-canvas');
		        
		        var myLatlng = new google.maps.LatLng(<?php echo $locator; ?>);
		        
		        var mapOptions = {
		          center: myLatlng,
		          zoom: 8,
		          mapTypeId: google.maps.MapTypeId.ROADMAP
		        }
		        var map = new google.maps.Map(mapCanvas, mapOptions)
		        
		        var marker = new google.maps.Marker({
				  position: myLatlng,
				  map: map
				});

		      }
		      google.maps.event.addDomListener(window, 'load', initialize);
	        </script>
				
			<div id="map-canvas"></div>
  <?php endif;?>
			
    </div>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />
</div>

</div><!-- #product-<?php the_ID(); ?> -->

<div class="modalUsuarios" style="display:none;">
	<div class="contenedor uers_likes_all"><?php tarful_networks_avatars_all($post->ID); ?></div>
</div>
