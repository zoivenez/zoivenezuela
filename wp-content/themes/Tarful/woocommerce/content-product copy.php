<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );

// Ensure visibility
if ( ! $product || ! $product->is_visible() )
	return;



$product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
$single_cat = array_shift( $product_cats );	
$product_place = wp_get_post_terms( get_the_ID(), 'localidad');
$single_place = array_shift( $product_place);

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>
<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<a href="<?php the_permalink(); ?>">

	
	<div class="product">
		<?php the_post_thumbnail('small'); ?>
		<div class="carousel-caption">
			<div class="category"><a href=""><?php  echo $single_cat->name  ?></a></div>
			<div class="location"><a href=""><?php if (!empty($single_place)) { echo $single_place->name;}  ?></a></div>
			<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
		</div>
		<div class="avalaible"> <?php echo $product->get_price_html() ;?>| <span> <?php echo $product->get_total_stock() ;?> CUPOS </span></div>
	</div>

	</a>

	<?php

		/*
		 * woocommerce_after_shop_loop_item hook
		 *
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 * do_action( 'woocommerce_after_shop_loop_item' ); 
		*/
	?>

</li>
