<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$current_user = wp_get_current_user();
?>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thx-page thankyou">

	<?php
	if ( $order ) : ?>
	
		<?php if ( $order->has_status( 'failed' ) ) : ?>
	
			<p><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'woocommerce' ); ?></p>
	
			<p><?php
				if ( is_user_logged_in() )
					_e( 'Please attempt your purchase again or go to your account page.', 'woocommerce' );
				else
					_e( 'Please attempt your purchase again.', 'woocommerce' );
			?></p>
	
			<p>
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
				<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>
	
		<?php else : ?>

			<p class="thx_title">¡ <?= property_exists($current_user, "user_firstname") ? $current_user->user_firstname : $current_user->user_login ?>, Gracias por tu Compra !</p>

			<div class="thx_content_header"><?php echo CFS()->get('encabezado_thankyou', 6); ?></div>
			<div class="clear"></div>
	
		<?php endif; ?>

		<div class="box_thx_meta">

			<?php do_action( 'woocommerce_thankyou', $order->id ); ?>

		</div>

		<div class="footer_thx_meta">
        <?php if ($order->payment_method_title == "Tarjeta de credito"): ?>
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <h4>¿Realizaste pago con tarjeta de crédito?</h4>
                    <p>Revisa tu comprobante de compra</p>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <a href="" class="cargar_comprobante_buton" data-toggle="modal" data-target="#receipt-modal">Ver recibo de compra</a>
                </div>
                <div style="clear:both;"></div>
            </div>

        <?php else: ?>
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <h4>¿Realizaste pago con depósito o transferencia?</h4>
                    <p><?php echo CFS()->get('paso_para_pago', 6); ?></p>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <a href="<?php echo $order->get_view_order_url(); ?>" class="cargar_comprobante_buton">Cargar comprobante de pago</a>
                </div>
                <div style="clear:both;"></div>
            </div>
        <?php endif ?>


	<?php else : ?>
	
		<p class="thx-thx_title"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>
	
	<?php endif; ?>

</div>

<div id="receipt-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Recibo de compra </h4>
            </div>
            <div class="modal-body">
                <?php instapago_load_receipt( $order ); ?>
            </div>
            <div class="modal-footer">
                <button id="print-voucher" onclick="printVoucher()">Imprimir</button>
                <button data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script>

    function printVoucher() {
        //var width = $("#voucher").css('width') + 10; 
        //var height = $("#voucher").css('height') + 10; 
        var voucherWindow = window.open('', 'Voucher', 'height=1000, width=600'); 
        voucherWindow.document.write('<html><head><title>Voucher</title>'); 
        voucherWindow.document.write('</head><body>');
        voucherWindow.document.write($("#voucher").html()); 
        voucherWindow.document.write('</body></html>'); 
        voucherWindow.document.close(); 
        voucherWindow.print();
        voucherWindow.close(); 
    }
</script>
