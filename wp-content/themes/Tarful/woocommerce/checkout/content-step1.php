<?php do_action( 'woocommerce_before_cart' ); ?>

<form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">

<?php do_action( 'woocommerce_before_cart_table' ); ?>

<table class="shop_table cart" cellspacing="0">
	<thead>
		<tr>
			<th class="product-thumbnail">&nbsp;</th>
			<th class="product-name">&nbsp;</th>
			<th class="product-quantity">Cantidad</th>
			<th class="product-price">Precio unit.</th>
			<th class="product-subtotal">Sub-total</th>
		</tr>
	</thead>
	<tbody>
		<?php do_action( 'woocommerce_before_cart_contents' ); ?>

		<?php
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

					<td class="product-thumbnail">
						<?php
							$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

							if ( ! $_product->is_visible() )
								echo $thumbnail;
							else
								printf( '<a href="%s">%s</a>', $_product->get_permalink( $cart_item ), $thumbnail );
						?>
					</td>

					<td class="product-name">
						<?php
							if ( ! $_product->is_visible() )
								echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
							else
								echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', $_product->get_permalink( $cart_item ), $_product->get_title() ), $cart_item, $cart_item_key );

							// Meta data

							echo '<div style="clear:both;"></div>';
							echo WC()->cart->get_item_data( $cart_item );
							echo '<div style="clear:both;"></div>';
                            $experience_date = lm_get_item_data($cart_item_key, 'experience_date'); 

                            if (array_key_exists('additional_adult', $cart_item)) { ?>
                                    <dt></dt>
                                    <dd><p><span>Adultos adicionales: <?=$cart_item['additional_adult']?> </span></p></dd>
                        <?php }

                            if (array_key_exists('additional_children', $cart_item)) { ?>
                                    <dt></dt>
                                    <dd><p><span>Niños adicionales: <?=$cart_item['additional_children']?> </span></p></dd>
                        <?php }


                            $hint_message = "En un máximo de 2 días hábiles te confirmaremos la disponibilidad."; 

                            if (array_key_exists('_start_date', $cart_item) && $cart_item['_start_date'] != "Array"
                                && array_key_exists('_end_date', $cart_item) && $cart_item['_end_date'] != "Array") { ?>
                                    <dt></dt>
                                    <dd><p><span><u>Checkin:</u> <?=$cart_item['_start_date']?> </span></p></dd>
                                    <dt></dt>
                                    <dd><p><span><u>Checkout:</u> <?=$cart_item['_end_date']?> </span></p></dd>
                                    <dt></dt>
                                    <dd><div class="checkout-disponibility">
                                        <p style="font-style:italic !important; color: black; font-weight:bold"><span>Sujeto a disponibilidad</span></p>
                                        <a href="#" class="hint hint-top" data-hint="<?=$hint_message?>" >
                                        <img src="<?=get_bloginfo('url')?>/wp-content/plugins/woocommerce/assets/images/help.png" style="width:20px; height:20px;"></a>
                                    </div></dd>

                        <?php }

                            
                            if (!is_null( $experience_date )) : ?>
                                    <dt></dt>
                                    <dd><p><span> Fecha: <?=$experience_date?> </span></p></dd>
                                    <dt></dt>
                                    <dd><div class="checkout-disponibility">
                                        <p style="font-style:italic !important; color: black; font-weight:bold"><span>Sujeto a disponibilidad</span></p>
                                        <a href="#" class="hint hint-top" data-hint="<?=$hint_message?>" >
                                        <img src="<?=get_bloginfo('url')?>/wp-content/plugins/woocommerce/assets/images/help.png" style="width:20px; height:20px;"></a>
                                    </div></dd>
					<?php   endif; 
							echo '<div style="clear:both;"></div>';
							echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s">Remover</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );

               				// Backorder notification
               				if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
               					echo '<p class="backorder_notification">' . __( 'Available on backorder', 'woocommerce' ) . '</p>';


						?>
					</td>

					<td class="product-quantity">
						<?php
							if ( $_product->is_sold_individually() ) {
								$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
							} else {
								$product_quantity = woocommerce_quantity_input( array(
									'input_name'  => "cart[{$cart_item_key}][qty]",
									'input_value' => $cart_item['quantity'],
									'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
									'min_value'   => '0'
								), $_product, false );
							}

							echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
						?>
					</td>

					<td class="product-price">
						<?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
						?>
					</td>

					<td class="product-subtotal">
						<?php
							echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
						?>
					</td>
				</tr>

				<?php
			}
		}

		do_action( 'woocommerce_cart_contents' );
		?>


        <!-- [Leonardo] Show Subtotal of cart-->
		<tr class="cart-subtotal">
			<td colspan="3"></td>
			<td><?php _e( 'Sub-total', 'woocommerce' ); ?></td>
			<td><?php wc_cart_totals_subtotal_html(); ?></td>
		</tr>

		
        <!-- [Leonardo] Show Title of Coupon-->
		<?php if ( sizeof(WC()->cart->get_coupons()) === 1  )  : ?>

			<tr class="cart-discount">
				<td colspan="3"></td>	
				<td> <?=esc_html( __( 'Cupón:', 'woocommerce' ) )?></td>
			</tr>
		<?php elseif  (WC()->cart->get_coupons()) :?>
			<tr class="cart-discount">	
				<td colspan="3"></td>
				<td> <?=esc_html( __( 'Cupones:', 'woocommerce' ) )?></td>
			</tr>
		<?php endif; ?>


        <!-- [Leonardo] Show Code and Price (Discount) of Coupon -->
		<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
			<tr class="cart-discount coupon-<?php echo esc_attr( $code ); ?>">
				<td colspan="2"></td>
				<td colspan="2" style="text-align: right;"><?=  esc_html(truncate_string ($coupon->code,8) )?></td>
				<td colspan="1">
				<?  if($coupon->is_type(( array( 'percent_product', 'percent'))) || $coupon->is_type(( array( 'percent_product', 'fixed_product'))) ) {

					echo '<span class="amount">-'.$coupon->coupon_amount.'%</span>'; ?></td>
				<?
				}else{
					echo wc_price(-$coupon->coupon_amount);
				}?>
			</tr>
		<?php endforeach; ?>

        <!--[Leonardo] Show Tax  -->
		<?php if ( WC()->cart->tax_display_cart === 'excl' ) : ?>
			<?php if ( get_option( 'woocommerce_tax_total_display' ) === 'itemized' ) : ?>
				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
					<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
						<td colspan="3"></td>
						<td><?php echo esc_html( $tax->label ); ?></td>
						<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php else : ?>
				<tr class="tax-total">
					<td colspan="3"></td>
					<td><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></td>
					<td><?php echo wc_price( WC()->cart->get_taxes_total() ); ?></td>
				</tr>
			<?php endif; ?>
		<?php endif; ?>	

        <!-- [Leonardo] Show Total of cart -->
		<tr class="order-total">
			<td colspan="3"></td>
			<td><?php _e( 'Total', 'woocommerce' ); ?></td>
			<td><?php wc_cart_totals_order_total_html(); ?></td>
		</tr>

		<tr>
			<td colspan="6" class="actions">
				<!-- [Leonardo] Show Input and Button to submit a coupon -->
				<?php if ( WC()->cart->coupons_enabled() ) { ?>
					<div class="coupon">

						<label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php _e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button avalaible" name="apply_coupon" value="<?php _e( 'Apply Coupon', 'woocommerce' ); ?>" />

						<?php do_action( 'woocommerce_cart_coupon' ); ?>

					</div>
				<?php } ?>

				
                
				<input type="submit" class="button avalaible update-cart-button" name="update_cart" value="<?php _e( 'Update Cart', 'woocommerce' ); ?>" />
				
				<!-- updateCart
					 Function: update the Cart to refresh coupons when it is removed-->
				<script type="text/javascript">
                    
				    function updateCart () {
				        var upd_cart_btn = $(".update-cart-button");
				        upd_cart_btn.trigger("click");
				    }
				</script>

				<?php do_action( 'woocommerce_cart_actions' ); ?>

				<?php wp_nonce_field( 'woocommerce-cart' ); ?>
			</td>
		</tr>

		<?php do_action( 'woocommerce_after_cart_contents' ); ?>

		</tbody>
	

</table>



<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<?php do_action( 'woocommerce_after_cart' ); ?>
