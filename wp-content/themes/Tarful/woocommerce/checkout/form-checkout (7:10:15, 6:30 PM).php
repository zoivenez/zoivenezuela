<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();
global $woocommerce;

// do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

function bullets($number){
	echo '<ol class="carousel-indicators indicators-checkout">';
		for ($i=1; $i < 5 ; $i++) { 
			if ($i == $number) {
				echo '<li class="active"></li>';
			}else{
				echo '<li class=""></li>';
			}	
		}
	echo '</ol>';	
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 mcenter checkout-steps">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkout-step-one">
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
			<p class="number">01</p>
			<h4>Revisa tu compra</h4>
			<?php bullets(1); ?>
		</div>
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 checkout-box-right">
			<?php include('content-step1.php') ?>
		</div>
	</div>

	<?php if ( ! is_user_logged_in() ) { ?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkout-step-one box-stage-login">		
			<?php do_action( 'woocommerce_before_checkout_form', $checkout );?> 		
			<?php do_action( 'wordpress_social_login' ); // only shows when user is not logged-in ?> 
		</div>
	<?php } ?>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkout-step-one">
<form name="checkout" method="post" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data" style="display:inline;">
		<div class="row">
			<div class="col-xs-12 col-sm-3 step-container">
				<p class="number">02</p>
				<h4>Datos de tu cuenta</h4>
				<?php bullets(2); ?>
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
				<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

					<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

					<div class="col2-set" id="customer_details">
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
					</div>

					<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

				<?php endif; ?>
			</div>
		</div>
		<div class="col-xs-12 separator"></div>
		<div class="row">
			<div class="col-xs-12 col-sm-3 step-container">
				<p class="number">03</p>
				<h4>Selecciona tipo de pago</h4>
				<?php bullets(3); ?>
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
				<div class="row">
					<div class="col-xs-12 step-container payment-methods">
					
						<div class="col-xs-12 col-md-offset-1 col-sm-10">
							<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>		
							<div id="order_review" class="woocommerce-checkout-review-order">
								<?php do_action( 'woocommerce_checkout_order_review' ); ?>
							</div>
							<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-12 separator"></div>

		<div class="row">
			<div class="col-xs-12 col-sm-3 step-container">
				<p class="number">04</p>
				<h4>Finalizar compra</h4>
				<?php bullets(4); ?>
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 step-container checkout-totals">

					<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

						<div id="order_review" class="woocommerce-checkout-review-order">
						<?php do_action( 'woocommerce_checkout_order_review' ); ?>
						</div>

					<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
					</div>
				</div>
			</div>
		</div>
</form>

	</div>
		
</div>



