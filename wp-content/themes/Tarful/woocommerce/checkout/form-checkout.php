<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();
global $woocommerce;

// do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

function bullets($number){
	echo '<ol class="carousel-indicators indicators-checkout">';
		for ($i=1; $i < 5 ; $i++) { 
			if ($i == $number) {
				echo '<li class="active"></li>';
			}else{
				echo '<li class=""></li>';
			}	
		}
	echo '</ol>';	
}

// {Ramon} verifies if cart contains only producto type to modify view fields.
function validate_only_producto()
{
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $category = wp_get_post_terms( $_product->id, 'product_cat' );
        if($category[0]->name != "Producto"){
            return false;
        }
    }

    return true;
}

$verified_only_product = validate_only_producto(); 
// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 mcenter checkout-steps">

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkout-step-one">
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 ">
			<p class="number">01</p>
			<h4>Revisa tu compra</h4>
			<?php bullets(1); ?>
		</div>
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 checkout-box-right">
			<?php include('content-step1.php') ?>
		</div>
	</div>

	<?php if ( ! is_user_logged_in() ) { ?>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkout-step-one box-stage-login">		
			<?php do_action( 'woocommerce_before_checkout_form', $checkout );?> 		
			<?php do_action( 'wordpress_social_login' ); // only shows when user is not logged-in ?> 
		</div>
	<?php } ?>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkout-step-one">
<form name="checkout" class="checkout woocommerce-checkout" method="post" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data" style="display:inline;">
		<div class="row">
			<div class="col-xs-12 col-sm-3 step-container">
				<p class="number">02</p>
				<h4>Datos de tu cuenta</h4>
				<?php bullets(2); ?>
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
				<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

					<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

					<div class="col2-set" id="customer_details">
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
					</div>


				<?php endif; ?>
			</div>
		</div>
        <div class="col-xs-12 separator"></div>
        <?php if (!$verified_only_product) : ?>
		<div class="row">
			<div class="col-xs-12 col-sm-3 step-container">
				<p class="number">03</p>
				<h4>Datos viajero(s)</h4>
				<?php bullets(3); ?>
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
				<div class="row">
					<div class="col-xs-12 step-container">
					
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div id="order_review" class="woocommerce-checkout-review-order">
                                <?php do_action( 'woocommerce_checkout_after_customer_details', $checkout ); ?>
							</div>
							<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 separator"></div>
		<?php endif;?>

	
		<div class="row">
			<div class="col-xs-12 col-sm-3 step-container">
				<?php if (!$verified_only_product) : ?>
				<p class="number">04</p>
				<h4>Selecciona tipo de pago</h4>
				<?php bullets(4); ?>
				<?php else : ?>
				<p class="number">03</p>
				<h4>Selecciona tipo de pago</h4>
				<?php bullets(3); ?>
				<?php endif;?>
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
				<div class="row">
					<div class="col-xs-12 step-container payment-methods">
					
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>		
							<div id="order_review" class="woocommerce-checkout-review-order">
								<?php do_action( 'woocommerce_checkout_order_review' ); ?>
							</div>
							<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="row transfer-details" style="display:none">
            <div class="col-xs-12 col-sm-3 step-container vcenter">
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
                <div class="col2-set">
                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 mcenter" style="border-top: 1px solid #efefef;">		
                        <br>
                        <h4>Datos para realizar transferencia</h4>
                        <br>
                        <?php 
                            if ( WC()->cart->needs_payment() ) {
                                $gateway = get_payment_gateway_by_id("bacs"); 
                                if ( $gateway ) : ?>
                                    <dl class="dl-horizontal account-info">
                                        <?php foreach ($gateway->account_details as $account): ?>
                                            <dt>Banco</dt>
                                            <dd><?=$account['bank_name']?></dd>
                                            <dt>Beneficiario</dt>
                                            <dd><?=$account['account_name']?></dd>
                                            <dt>N° de cuenta</dt>
                                            <dd><?=$account['account_number']?></dd>
                                            <br>
                                        <?php endforeach ?>
                                    </dl>
                                <?php endif; 
                            } ?>
                   </div>
                </div>
            </div>
        </div>

		<div class="row credit-card-details" style="display:none">
            <div class="col-xs-12 col-sm-3 step-container vcenter">
				<h4>Ingresa los datos de tu tarjeta</h4>
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
                <div class="col2-set">
                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 mcenter credit-card-form" style="border-top: 1px solid #efefef;">		
                        <br>

                        <?php 
                            if ( WC()->cart->needs_payment() ) {
                                $gateway = get_payment_gateway_by_id("instapago"); 
                            if ( $gateway && $gateway->has_fields() ) : ?>
                                    <?= $gateway->payment_fields() ?>
                            <?php endif; 
                            } ?>
                    </div>
                </div>
            </div>
        </div>

		<div class="col-xs-12 separator"></div>

		<div class="row">
			<div class="col-xs-12 col-sm-3 step-container">
				<?php if (!$verified_only_product) : ?>
				<p class="number">05</p>
				<h4>Selecciona tipo de pago</h4>
				<?php bullets(5); ?>
				<?php else : ?>
				<p class="number">04</p>
				<h4>Selecciona tipo de pago</h4>
				<?php bullets(4); ?>
				<?php endif;?>
			</div>
			<div class="col-xs-12 col-sm-9 checkout-box-right">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 step-container checkout-totals">

					<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

						<div id="order_review" class="woocommerce-checkout-review-order">
						<?php do_action( 'woocommerce_checkout_order_review' ); ?>
						</div>

					<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
					</div>
				</div>
			</div>
		</div>
</form>
			<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>	

	</div>
		
</div>

<script type="text/javascript">
	$(document).ready(function(){
		enderizeTarfulHeightCall(".payment_methods li > label",135);
	});
</script>
<div id="warningModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p style="font-size: 24px"><strong>¡Alerta!</strong></p>
            </div>
            <div id="travelerWarning" class="modal-body">
            </div>
            <div class="modal-footer warning-modal-footer">
                <button type="button" class="button avalaible" id="continue-checkout" data-dismiss="modal"> Continuar con la compra </button>
                <button type="button" class="button avalaible" data-dismiss="modal"> Cancelar </button>
            </div>
        </div>
    </div>
</div>
