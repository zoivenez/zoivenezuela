<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $wodb, $post, $woocommerce, $product;
$testimoniales = CFS()->get('product_testimonials');
$i = 0;
$x = 0;
$product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
$validation_category = array_shift( $product_cats );
?>
<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 buy-info">
    <?php do_action( 'woocommerce_single_product_summary' ); ?>
</div> <!--buy-info -->
<?php if ($validation_category->name != "Producto") :?>
    <div style="clear: both;"></div>
    <div class="row skills">
        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-7 col-xs-offset-3 col-lg-offset-3 exp-measurement no-padding">
            <h4>dificultad</h4>
                <?php
                $values = CFS()->get('difficulty');
                echo '<ol class="carousel-indicators">';
                if (!empty($values)) {
                    foreach ($values as $value => $label) {
                        for ($i=0; $i < $value ; $i++) {  ?>
                            <li class="active"><img style="float:left;" src="<?php bloginfo('stylesheet_directory');?>/images/img_difficulty.png" width="20" height="20" alt=""></li>
                         <?php
                         }
                    }	
                }else{
                    $value =0;
                }	
                 for ($i=0; $i < (5-$value) ; $i++) { ?>
                    <li class=""><img style="float:left;" src="<?php bloginfo('stylesheet_directory');?>/images/img_difficulty_normal.png" width="20" height="20" alt=""></li>
                <?php
                }
                echo '</ol>';	
            ?>
        </div>
    </div>
<?php endif; ?>
<?php if (CFS()->get('telefono', 22)): ?>
<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 contact-info <?php if($validation_category->name != "Producto"): echo "date-place"; endif; ?>">
    <span class="glyphicon glyphicon-earphone"></span>
        <div class="place">
            <h4><?=CFS()->get('telefono', 22)?></h4>
        </div>
    </div>
<?php endif; ?>
<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 contact-info date-place">
    <div class="place">
        <span class="glyphicon glyphicon-envelope"></span>
        <h4><a href="<?php echo get_permalink(22); ?>">hola@zoivenezuela.com</a></h4>
    </div>
</div>
<!-- widget de valoración -->
<!--
<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 contact-info date-place">
    <div class="place rate row">
        <h4 class="col-xs-12">valoración</h4><br>
        <?php $rating = $product->get_average_rating();
              ?>
        <span class='average <?php echo $rating? : "none" ?>'><?php echo $rating? $rating : 'No hay calificaciones'?></span>
        <?php if($rating) :?><span class="glyphicon glyphicon-star star"></span><?php endif;?>
        <a id="review-link" class="col-xs-12" href="javascript:;"><?php echo $rating? 'ver comentarios' : 'sé el primero en calificar'?></a>
    </div>
</div>
-->
