<?php
/**
 * Template Name: Full Width Experiencias Page
 *
 * by Tarful
 *
 */

get_header(); ?>

<div id="main-content" class="site-main limit">


	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php get_template_part('content','main-slider'); ?>

			<section>

				<div class="col-xs-12 col-sm-11 col-md-11 col-lg-11 mcenter recent-products">

					<?php get_template_part('content', 'all-products' ); ?>
										
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 block_separator"></div>
					
				</div>
			</section>
			
			<div style="clear: both;"></div>

			<?php get_template_part('content','testimonial-featured'); ?>
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 block_network text-center">
				<div class="col-xs-16 col-sm-16 col-md-10 col-lg-10 col-centered text-center">
					<center>

						<h2>Ya somos una comunidad de <u><?php tarful_users_count(); ?></u> aventureros</h2>

						<?php tarful_networks_avatars(9); // Cambiar 2 por 9 ?>
						<div style="clear:both;"></div>

						<a href="" class="btn avalaible main_btn">Crear tu cuenta ahora</a>
						<a href="" class="btn avalaible fb_btn"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/fb_btn.png" alt="Calidad ZOI"/> Iniciar sesión con Facebook</a>
			
						<div class="box_stamp">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/stamp.png" alt="Calidad ZOI"/>
						</div>
	
					</center>
						
				</div>
			</div>
			
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php

get_footer();
