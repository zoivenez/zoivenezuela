<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<div id="main-content" class="site-main">
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
			<?php 
			if (is_tax( 'localidad' )) {
				$temp_p = get_category_meta(false, get_term_by('slug',  get_query_var('localidad') , 'localidad'));		
				
				if ((!empty($temp_p)) && (!empty($temp_p['imagen_ancha']))) { 
					$temp_image = $temp_p['imagen_ancha'];
					?>
					<div class="archive-banner" style="width:100%; height: auto;">
						<img src="<?php echo $temp_image; ?>" alt="">
					</div>				
				<?php } ?>
			<?php
			} else if (is_tax( 'product_cat' )) {
				$temp_p = get_category_meta(false, get_term_by('slug',  get_query_var('product_cat') , 'product_cat'));		
				
				if ((!empty($temp_p)) && (!empty($temp_p['imagen_ancha']))) { 
					$temp_image = $temp_p['imagen_ancha'];
					?>
					<div class="archive-banner" style="width:100%; height: auto;">
						<img src="<?php echo $temp_image; ?>" alt="">
					</div>				
				<?php } ?>
			<?php
			} else {
				get_template_part('content','main-slider');
			}
			?>

			<?php
				/**
				 * woocommerce_before_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action( 'woocommerce_before_main_content' );
			?>

			<?php if ( have_posts() ) : ?>
		
				<?php
					/**
					 * woocommerce_before_shop_loop hook
					 *
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					//do_action( 'woocommerce_before_shop_loop' );
				?>
		
		
				<div class="col-xs-12 col-sm-11 col-md-11 col-lg-11 mcenter recent-products">
					<?php woocommerce_product_loop_start(); 

					woocommerce_product_subcategories();

						while ( have_posts() ) : the_post();

						 wc_get_template_part( 'content', 'product' );

						endwhile; // end of the loop. 
						global $wp_query;
						if(max( 1, get_query_var( 'paged' ) ) >= $wp_query->max_num_pages){
							tarful_box_create_experience();
						}
					 woocommerce_product_loop_end(); ?>
				</div>
				
					
				
				<?php

					/**
					 * woocommerce_after_shop_loop hook
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				?>			
		
			<?php endif; ?>


			<?php
				/**
				 * woocommerce_after_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' );
			?>
		
			<?php
				/**
				 * woocommerce_sidebar hook
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				tarful_box_guru();

			?>
	
	    </div>
    </div>
</div>

<!-- INIT No products found -->
<?php if ( !have_posts() && ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
		
	<?php wc_get_template( 'loop/no-products-found.php' );?>
		
<?php endif; ?>
<!-- END No products found -->

<script type="text/javascript">
$(document).ready(function(){
	enderizeTarfulHeightCall(".products li .product img",96);
});
</script>
<?php get_footer( 'shop' ); ?>
