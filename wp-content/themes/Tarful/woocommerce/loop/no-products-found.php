<?php
/**
 * Displayed when no products are found matching the current query.
 *
 * Override this template by copying it to yourtheme/woocommerce/loop/no-products-found.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<h2 class="woocommerce-info" style="text-transform: none; ">Lo sentimos, no se encontraron resultados para tu búsqueda</h2>
<!-- We need at least product category and location to show product suggestions -->
<?php if(isset($_GET['product_cat']) && isset($_GET['localidad'])): 

 $query = posts_suggestion(4);?>

<div class="row-fluid mcenter limit">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="related products">
			<p class="woocommerce-info" style="float: left; padding: 0 !important; margin: 0 0 3% 0 !important; text-transform: none; "> Sin embargo, tenemos sugerencias</p>

			<!-- Loop to show products suggested -->
			 <?php woocommerce_product_loop_start(); 

					while ( $query->have_posts() ) : $query->the_post();

						wc_get_template_part( 'content', 'product' );

					endwhile; 
							
				woocommerce_product_loop_end(); ?>
		</div>
	</div>
</div>
 
 <!-- Script to remove and add classes to styling -->
<script type="text/javascript">
	$('body').removeClass('archive search search-no-results').addClass( "single single-product" );
</script>

<?php endif; ?>
<center>
	<a class="btn-no-result" style="display: inline-block;" href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' )); ?>">Volver a Experiencias</a>
</center>
<br />
