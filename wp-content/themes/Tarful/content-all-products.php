<?php
/**
 * The template used for displaying the 9 most recent products in the homepage
 *
*/
?>
<?php global $cfs; ?>

<!-- 'meta_key'=>'_featured', 'meta_value'=>'no', -->

	<?php 
	$query = new WP_Query( array( 'post_type' => 'product', 'posts_per_page' => -1));

	if ( $query->have_posts() ){
	while ( $query->have_posts() ) : $query->the_post(); 
		$product = get_product( $query->post->ID );  
		$product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
		$single_cat = array_shift( $product_cats );	
		$product_place = wp_get_post_terms( get_the_ID(), 'localidad');
		$single_place = array_shift( $product_place);
	?>

	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 product grid">
		<?php the_post_thumbnail('small'); ?>
		<div class="carousel-caption">
			<div class="category"><a href="<?php echo get_site_url();?>/localidad-producto/<?php echo $single_cat->slug; ?>"><?php  echo $single_cat->name  ?></a></div>
			<div class="location"><a href="<?php echo get_site_url();?>/localidad/<?php echo $single_place->slug; ?>"><?php if (!empty($single_place)) { echo $single_place->name;}  ?></a></div>
			<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
		</div>
		<div class="avalaible"> <?php echo $product->get_price_html() ;?>| 
									<span>
									<?php if($validation_category->name != "Producto"):
											echo $product->get_total_stock()." cupos";
										   else: 
										   	echo $product->get_total_stock()." en stock";
										   endif:?>
									</span>
		</div>
	
		<div class="hover">			
			<a  href="<?php the_permalink(); ?>" class="btn btn_avalaible"><?php _e('VEr MAS', 'tarful') ?></a>
			<!--<a  href="<?php the_permalink(); ?>" class="info">MAS INFORMACIÓN</a>-->
			<?php tarful_facebook_share(get_the_ID() , apply_filters( 'woocommerce_short_description', $post->post_content )); ?>
			<?php tarful_twitter_share(); ?>
		</div>


	</div>

	<?php  endwhile;
	wp_reset_postdata(); 

	}else {?>
		<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php } ?>


