<?php
/**
 * Sidebar for general pages
 *
 * by Tarful
 *
 */
?>

	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box_contact_meta">
			<div class="meta-content">
				<a href="http://www.twitter.com/<?php echo CFS()->get('account_instagram', 22); ?>" target="_blank" class="tw_icon_sidebar">ZOI en Twitter</a>
				<a href="https://instagram.com/<?php echo CFS()->get('account_instagram', 22); ?>" target="_blank" class="in_icon_sidebar">ZOI en Instagram</a>
				<a href="http://www.facebook.com/<?php echo CFS()->get('account_facebook', 22); ?>" target="_blank" class="fb_icon_sidebar">ZOI en Facebook</a>
			</div>
			<div class="meta-footer">
				<h3>Enlaces de Interés</h3>
				<ul>
					
					<li><a href="<?php echo get_bloginfo('url') . '/experiencias/'; ?>">Qué ofrecemos</a></li>
					<?php if (is_page( 22 ) ) { // 22 Page Cotnacto  ?>
						<li><a href="<?php echo get_permalink(14); ?>">Preguntas Frecuentes</a></li>
						<li><a href="<?php echo get_permalink(16); ?>">Términos y Condiciones</a></li>
					<?php } ?>
					<?php if (is_page( 16 ) ) { // 22 Page Terms and Conditions  ?>
						<li><a href="<?php echo get_permalink(14); ?>">Preguntas Frecuentes</a></li>
						<li><a href="<?php echo get_permalink(22); ?>">Contacto</a></li>
					<?php } ?>
					<?php if (is_page( 14 ) ) { // 22 Page FAQs  ?>
						<li><a href="<?php echo get_permalink(16); ?>">Términos y Condiciones</a></li>
						<li><a href="<?php echo get_permalink(22); ?>">Contacto</a></li>
					<?php } ?>

				</ul>
			</div>
		</div>

		<?php if (is_page( 22 ) ) { // 22 Page Cotnacto  ?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box_contact_meta office_info">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 map-box">
					<h3>¿Dónde estamos ubicados?</h3>
					<p><?php echo CFS()->get('office_address', 22); ?></p>
				</div>
				<?php $locator = CFS()->get('office_map', 22); ?>
		
				<script src="https://maps.googleapis.com/maps/api/js"></script>

				<script>
				      function initialize() {
				        var mapCanvas = document.getElementById('map-canvas');
				        
				        var myLatlng = new google.maps.LatLng(<?php echo $locator; ?>);
				        
				        var mapOptions = {
				          center: myLatlng,
				          zoom: 8,
				          mapTypeId: google.maps.MapTypeId.ROADMAP
				        }
				        var map = new google.maps.Map(mapCanvas, mapOptions)
				        
				        var marker = new google.maps.Marker({
						  position: myLatlng,
						  map: map
						});

				      }
				      google.maps.event.addDomListener(window, 'load', initialize);
				</script>
					
				<div id="map-canvas"></div>
			</div>		
		<?php } ?>
		
	</div>