<?php
/**
 * The template used for displaying the 9 most recent products in the homepage
 *
*/
?>
<?php global $cfs; ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 terms-flyer">

	<h3><?php echo get_the_title(16);?></h3>

	<p>Visita nuestras secciones de <a href="<?php echo get_permalink(16); ?>">Términos y Condiciones</a> y <a href="<?php echo get_permalink(14); ?>">Preguntas Frecuentes</a> </p>

</div>