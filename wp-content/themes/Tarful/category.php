<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

$aux_loop = 0;
get_header(); ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<div class="archive-banner" style="width:100%; height: auto;">
				<img src="<?php echo CFS()->get('blog_main',33); ?>" alt="">
				<div class="contenedor-caption ">
					<div class="ajustador-caption contenedor-centrado-vertical"style="text-align: center;">
						<div class="centrado-vertical">
							<h1><?php echo single_cat_title( '', true );  ?></h1>
						</div>
					</div>
				</div>
				<div class="shadow" style="bottom: 0"></div>
				<?php category_menu(); ?>
			</div>

			<div class="limit">

				<div class="list-blog">
				<?php
                        $cur_cat = get_the_category()[0]; 
						$args = array(
                            'cat'         => $cur_cat->cat_ID, 
							'posts_per_page'   => 8,
							'offset'           => 0,
							'paged' => get_query_var('paged') ? get_query_var('paged') : 1,
							'orderby'          => 'date',
							'order'            => 'DESC',
							'post_type'        => 'post',
							'post_status'      => 'publish',
						);
						$posts_array = new WP_Query( $args );
						$num_item = 0;

					    while ( $posts_array->have_posts() ) : $posts_array->the_post(); 

							$float = "";
							switch ($num_item) {
								case '0':
									$col = 7;
									if($args["paged"] % 2){ $float = 'float: right;'; }
									$item_size = 'item-size-larga';
								break;

								case '1':
								case '2':
									$col = 5;
									$item_size = 'item-size-corta';
								 break;

								case '3':
								case '4':
									$col = 6;
									$item_size = 'item-size-medio';
								break;

								case '5':
								case '6':
								case '7':
									$col = 4;
									$item_size = 'item-size-tercio';
									if ($num_item == 7){ $num_item = 0; }
								break;
								
								default:
									echo "entro en default";
							 	break;
							}
							blog_post_replace( $col, $float, $item_size);
							$num_item ++;
						endwhile;
						?>
						<div class="blogs-pagination"> 
							<?php next_posts_link( 'Older Entries', $posts_array->max_num_pages ); ?>
						</div>
						<?php wp_reset_postdata();?>
				</div>
			</div>
		</div><!-- #content -->
	</section><!-- #primary -->

<?php
get_footer();
