<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * Template Name: Exportar Excel
 *
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */
global $wpdb;
$users = $wpdb->get_results("SELECT wp_postmeta.meta_value as ID, wp_woocommerce_order_items.order_id as pedido FROM wp_woocommerce_order_itemmeta
	INNER JOIN wp_woocommerce_order_items on wp_woocommerce_order_itemmeta.order_item_id = wp_woocommerce_order_items.order_item_id
	INNER JOIN wp_posts on wp_posts.ID = wp_woocommerce_order_items.order_id
	INNER JOIN wp_postmeta on wp_postmeta.post_id = wp_posts.ID
	WHERE wp_woocommerce_order_itemmeta.meta_key = '_product_id' 
	AND wp_woocommerce_order_itemmeta.meta_value = ".$_GET['post_id']."
	AND wp_woocommerce_order_items.order_item_type = 'line_item'
	AND wp_postmeta.meta_key = '_customer_user' ");

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/PHPExcel_1.8.0_doc/Classes/PHPExcel.php';

// var_dump(get_userdata( 2 ));
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("ZOI")
							 ->setLastModifiedBy("ZOI")
							 ->setTitle("Office 2007 XLSX Traveler List")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


// Add some data
$current_user = wp_get_current_user();
$usuario = get_userdata( $current_user->ID );
$i=2;
$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Experiencia')
			->setCellValue('B1', 'Aliado')
			->setCellValue('C1', 'Status')
			->setCellValue('D1', '#')
			->setCellValue('E1', 'Nombres')
			->setCellValue('F1', 'Apellidos')
			->setCellValue('G1', 'Email')
			->setCellValue('H1', 'Fecha de Salida')
			->setCellValue('I1', 'Fecha de Nacimiento')
			->setCellValue('J1', 'Sexo')
			->setCellValue('K1', 'Cédula')
			->setCellValue('L1', 'Teléfono')
			->setCellValue('M1', 'Ciudad')
			->setCellValue('N1', 'Método de Pago')
			->setCellValue('O1', '#')
			->setCellValue('P1', 'Pago 1')
			->setCellValue('Q1', 'Pago 2')
			->setCellValue('R1', 'Pago Final')
			->setCellValue('S1', 'Confirmación Aliado')
			->setCellValue('T1', 'Comentarios')
			->setCellValue('U1', 'Encuesta');


$related_orders = array(); 
$current_product_id = $_GET['post_id'];
$orders = get_posts( array(
            'post_type'   => 'shop_order',
            'post_status' => array( 'wc-processing', 'wc-on-hold' ),
            'numberposts' => -1
        ));

foreach($orders as $post_order) {    
    $order_id = $post_order->ID;
    $order = new WC_Order($order_id);



    $items = $order->get_items(); 

    foreach ( $items as $item ) {
        $product_id = $item['product_id'];
        // Save order id if the current product was bought on it
        if ($product_id === $current_product_id) {
            $variation_id = $item['variation_id'];
            $traveler_id = $product_id . '_' . $variation_id;
            $bookable = false; 
            // Add reserva tu fecha date only if it's confirmed
            if (array_key_exists('pa_fechas-de-experiencia', $item)) {
                $date = $item['pa_fechas-de-experiencia'];
            } else {
                if (array_key_exists('ebs_start_format', $item)) {
                    $start = date('d-m-Y', strtotime($item['ebs_start_format']));
                } else {
                    $start = "NE"; 
                }
                if (array_key_exists('ebs_start_format', $item)) {
                    $end = date('d-m-Y', strtotime($item['ebs_end_format']));
                } else {
                    $end = "NE"; 
                }
                $date  = "$start/$end";

                $bookable = true; 
            }

            $related_orders[] = array(
                'payment_method' => get_post_meta($order->id, '_payment_method', true),
                'order_id'       => $order_id,
                'traveler_id'    => $traveler_id,
                'price'          => $item['line_total'] + $item['line_tax'],
                'product_id'     => $item['product_id'],
                'date'           => $date,
                'bookable'       => $bookable
            );
        }
    }
}

// Traveler fields
$fields = array(
    'E' => 'name',
    'F' => 'lastname',
    'G' => 'email',
    'I' => 'birthdate',
    'J' => 'genre',
    'K' => 'id',
    'L' => 'phone',
    'M' => 'city',
    'T' => 'comments'
); 


// Month Lookup
$month_lookup = array(
    "1"  => "enero",
    "2"  => "febrero",
    "3"  => "marzo",
    "4"  => "abril",
    "5"  => "mayo",
    "6"  => "junio",
    "7"  => "julio",
    "8"  => "agosto",
    "9"  => "septiembre",
    "10" => "octubre",
    "11" => "noviembre",
    "12" => "diciembre"
);

// Month Lookup: month -> number
$month_inv_lookup = array(
    "enero"      => "1",
    "febrero"    => "2",   
    "marzo"      => "3",   
    "abril"      => "4",   
    "mayo"       => "5",   
    "junio"      => "6",   
    "julio"      => "7",   
    "agosto"     => "8",   
    "septiembre" => "9",   
    "octubre"    => "10",  
    "noviembre"  => "11",  
    "diciembre"  => "12"  
);




$row = 2; 
// Iterate over related orders 
foreach ( $related_orders as $order_info ) {
    $order_id          = $order_info['order_id'];
    $traveler_id       = $order_info['traveler_id'];
    $date              = $order_info['date'];
    $payment_method    = $order_info['payment_method'];
    $price             = $order_info['price'];
    $bookable          = $order_info['bookable'];
    $ally              = CFS()->get('nombre_aliado', $order_info['product_id']); 
    $number_of_clients = get_post_meta( $order_id, "number_of_clients_$traveler_id", true );

    $experience_date = get_post_meta( $order_id, 'experience_date', true ); 

    // Show payzzed price only if credit card
    $show_price = $payment_method !== "bacs"; 
    // Reformat payment_method
    $payment_method = ( $payment_method === "bacs" ? "TXF/DEP" : "TDC");

    // 0 -> day
    // 1 -> de
    // 2 -> month 
    // 3 -> de 
    // 4 -> year
    $parts = explode('-', $date);

    if (!$bookable) {
        // Change date format 
        if ( sizeof($parts) >= 3 && is_numeric($parts[0]) ) {
            $day   = $parts[0];
            $month = $month_inv_lookup[$parts[2]];
            $year  = ( sizeof($parts) === 5 ? $parts[4] : date("Y") );
            $date  = "$day/$month/$year"; 
        } else {
            $date = ucfirst(implode(" ", $parts)); 
        }
    }

    // Loop travelers
    for ($k = 0; $k < $number_of_clients; ++$k) { 
        $traveler_fields_id = "_$traveler_id"."_$k";
        // Loop fields
        foreach ( $fields as $pos => $field ) {
            $traveler_field_id = "$field$traveler_fields_id";
            $field_value = get_post_meta( $order_id, $traveler_field_id, true); 
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit($pos.($k + $row), $field_value, PHPExcel_Cell_DataType::TYPE_STRING);
        }

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A".($k + $row),  ' ')
            ->setCellValue("B".($k + $row),  $ally)
            ->setCellValue("C".($k + $row),  'Pendiente')
            ->setCellValue("D".($k + $row),  ' ')
            ->setCellValue("H".($k + $row),  $date . ( !empty($experience_date) ? " $experience_date": ' '))
            ->setCellValue("N".($k + $row),  $payment_method)
            ->setCellValue("O".($k + $row),  $order_id)
            ->setCellValue("P".($k + $row),  $show_price ? $price : ' ')
            ->setCellValue("Q".($k + $row),  ' ')
            ->setCellValue("R".($k + $row),  ' ')
            ->setCellValue("S".($k + $row),  ' ')
            ->setCellValue("U".($k + $row),  ' ');

    }

    $row += $number_of_clients; 

}

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Users Data');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.get_the_title( $_GET['post_id'] ).'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//ob_end_clean();
$objWriter->save('php://output');
exit;
