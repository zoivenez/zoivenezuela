<?php
/**
 * Admin class of WP Extened Search
 *
 * @author 5um17
 */
class WP_ES_admin {
    
    /* Defaults Variable */
    public $text_domain = '';

    /**
     * Default Constructor
     * @since 1.0
     */
    public function __construct(){
        global $WP_ES;
        
        $this->text_domain = $WP_ES->text_domain;

        add_action('admin_menu', array($this, 'WP_ES_admin_add_page'));
        add_action('admin_init', array($this, 'WP_ES_admin_init'));
        
        add_action('admin_enqueue_scripts', array($this, 'WP_ES_admin_scripts'));
    }

    /**
     * Add Admin page
     * @since 1.0
     */
    public function WP_ES_admin_add_page(){
        add_options_page('WP Extended Search Settings', 'Extended Search', 'manage_options', 'wp-es', array($this, 'wp_es_page'));
    }

    /**
     * Print admin page content
     * @since 1.0
     */
    public function wp_es_page(){ ?>
        <div class="wrap">
            
            <h2>WP Extended Search <?php _e('Settings', $this->text_domain); ?></h2>

            <form method="post" action="options.php"><?php
                settings_fields('wp_es_option_group');	
                do_settings_sections('wp-es');
                submit_button(__('Save Changes'), 'primary', 'submit', false);
                echo '&nbsp;&nbsp;';
                submit_button(__('Reset to WP default'), 'secondary', 'reset', false); ?>
            </form>
            
        </div><?php
    }

    /**
     * Add Section settings and settings fields
     * @since 1.0
     */
    public function WP_ES_admin_init(){

        /* Register Settings */
        register_setting('wp_es_option_group', 'wp_es_options', array($this, 'wp_es_save'));

        /* Add Section */
        add_settings_section( 'wp_es_section_1', __('Select Fields to include in WordPress default Search', $this->text_domain ), array($this, 'wp_es_section_content'), 'wp-es' );	

        /* Add fields */
        add_settings_field( 'wp_es_title_and_post_content', __('General Search Setting', $this->text_domain), array($this, 'wp_es_title_content_checkbox'), 'wp-es', 'wp_es_section_1' );
        add_settings_field( 'wp_es_list_custom_fields', __('Select Meta Key Names' , $this->text_domain), array($this, 'wp_es_custom_field_name_list'), 'wp-es', 'wp_es_section_1' );
        add_settings_field( 'wp_es_list_taxonomies', __('Select Taxonomies' , $this->text_domain), array($this, 'wp_es_taxonomies_settings'), 'wp-es', 'wp_es_section_1' );
        add_settings_field( 'wp_es_list_post_types', __('Select Post Types' , $this->text_domain), array($this, 'wp_es_post_types_settings'), 'wp-es', 'wp_es_section_1' );
        add_settings_field( 'wp_es_exclude_older_results', __('Select date to exclude older results' , $this->text_domain), array($this, 'wp_es_exclude_results'), 'wp-es', 'wp_es_section_1', array('label_for' => 'es_exclude_date') );
        
    }
    
    /**
     * enqueue admin style and scripts
     * @since 1.0
     */
    public function WP_ES_admin_scripts($hook) {
        if ($hook == 'settings_page_wp-es') {
            wp_enqueue_script('jquery-ui-datepicker');
            wp_enqueue_style('wpes_jquery_ui', WP_ES_URL . 'assets/css/jQueryUI/jquery-ui.min.css');
            wp_enqueue_style('wpes_jquery_ui_theme', WP_ES_URL . 'assets/css/jQueryUI/jquery-ui.theme.min.css');
            wp_enqueue_style('wpes_admin_css', WP_ES_URL . 'assets/css/wp-es-admin.css');
        }
    }

    /**
     * Get all meta keys
     * @since 1.0
     * @global Object $wpdb WPDB object
     * @return Array array of meta keys
     */
    public function wp_es_fields() {
        global $wpdb;
        /**
         * Filter query for meta keys in admin options
         * @since 1.0.1
         * @param string SQL query
         */
        $wp_es_fields = $wpdb->get_results(apply_filters('wpes_meta_keys_query', "select DISTINCT meta_key from $wpdb->postmeta where meta_key NOT LIKE '\_%' ORDER BY meta_key ASC"));
        $meta_keys = array();

        if (is_array($wp_es_fields) && !empty($wp_es_fields)) {
            foreach ($wp_es_fields as $field){
                if (isset($field->meta_key)) {
                    $meta_keys[] = $field->meta_key;
                }
            }
        }
        
        return $meta_keys;
    }

    /**
     * Validate input settings
     * @since 1.0
     * @global object $WP_ES Main class object
     * @param array $input input array by user
     * @return array validated input for saving
     */
    public function wp_es_save($input){
        global $WP_ES;
        $settings = $WP_ES->WP_ES_settings;
        
        if (isset($_POST['reset'])) {
            add_settings_error('wp_es_error', 'wp_es_error_reset', __('Your settings has been changed to WordPress default search setting.', $this->text_domain), 'updated');
            return $WP_ES->default_options();
        }
        
        if (!isset($input['post_types']) || empty($input['post_types'])) {
            add_settings_error('wp_es_error', 'wp_es_error_post_type', __('Select atleast one post type!', $this->text_domain));
            return $settings;
        }
        
        if (empty($input['title']) && empty($input['content']) && (!isset($input['meta_keys']) || empty($input['meta_keys'])) && (!isset($input['taxonomies']) || empty($input['taxonomies']))) {
            add_settings_error('wp_es_error', 'wp_es_error_all_empty', __('Select atleast one setting to search!', $this->text_domain));
            return $settings;   
        }
        
        if (!empty($input['exclude_date']) && !strtotime($input['exclude_date'])) {
            add_settings_error('wp_es_error', 'wp_es_error_invalid_date', __('Date seems to be in invalid format!', $this->text_domain));
            return $settings;
        }
        
        if (empty($input['title']) || empty($input['content'])) {
            add_settings_error('wp_es_error', 'wp_es_error_settings_saved', __('Settings saved.', $this->text_domain), 'updated');
            add_settings_error('wp_es_error', 'wp_es_error_default_settings', __('You have made changes to WordPress default search settings!', $this->text_domain), 'updated attention');
        }
        
        return $input;
    }

    /**
     * Section content before display fields
     * @since 1.0
     */
    public function wp_es_section_content(){ ?>
        <em><?php _e('Every field have OR relation with each other. e.g. if someone search for "5um17" then search results will show those items which have "5um17" as meta value or taxonomy\'s term or in title or in content, whatever option is selected.', $this->text_domain); ?></em><?php
    }

    /**
     * Default settings checkbox
     * @since 1.0
     * @global object $WP_ES
     */
    public function wp_es_title_content_checkbox(){ 
        global $WP_ES;
        $settings = $WP_ES->WP_ES_settings; ?>

        <input type="hidden" name="wp_es_options[title]" value="0" />
        <input <?php checked($settings['title']); ?> type="checkbox" id="estitle" name="wp_es_options[title]" value="1" />&nbsp;
        <label for="estitle"><?php _e('Search in Title', $this->text_domain); ?></label>
        <br />
        <input type="hidden" name="wp_es_options[content]" value="0" />
        <input <?php checked($settings['content']); ?> type="checkbox" id="escontent" name="wp_es_options[content]" value="1" />&nbsp;
        <label for="escontent"><?php _e('Search in Content', $this->text_domain); ?></label><?php
    }

    /**
     * Meta keys checkboxes
     * @since 1.0
     * @global object $WP_ES
     */
    public function wp_es_custom_field_name_list() {
        global $WP_ES;

        $meta_keys = $this->wp_es_fields();
        if (!empty($meta_keys)) { ?>
            <div class="wpes-meta-keys-wrapper"><?php
                foreach ((array)$meta_keys as $meta_key) { ?>
                    <p>
                        <input <?php echo $this->wp_es_checked($meta_key, $WP_ES->WP_ES_settings['meta_keys']); ?> type="checkbox" id="<?php echo $meta_key; ?>" name="wp_es_options[meta_keys][]" value="<?php echo $meta_key; ?>" />
                        <label for="<?php echo $meta_key; ?>"><?php echo $meta_key; ?></label>&nbsp;&nbsp;&nbsp;
                    </p><?php
                } ?>
            </div><?php
        } else { ?>
            <em><?php _e('No meta key found!', $this->text_domain); ?></em><?php
        }

    }
    
    /**
     * Taxonomies checboxes
     * @since 1.0
     * @global object $WP_ES
     */
    public function wp_es_taxonomies_settings() {
        global $WP_ES;
        
        /**
         * Filter taxonomies arguments
         * @since 1.0.1
         * @param array arguments array
         */
        $all_taxonomies = get_taxonomies(apply_filters('wpes_tax_args', array(
            'show_ui' => TRUE,
            'public' => TRUE
        )), 'objects');
        
        if (is_array($all_taxonomies) && !empty($all_taxonomies)) {
            foreach ($all_taxonomies as $tax_name => $tax_obj) { ?>
                <input <?php echo $this->wp_es_checked($tax_name, $WP_ES->WP_ES_settings['taxonomies']); ?> type="checkbox" value="<?php echo $tax_name; ?>" id="<?php echo 'wp_es_' . $tax_name; ?>" name="wp_es_options[taxonomies][]" />&nbsp;
                <label for="<?php echo 'wp_es_' . $tax_name; ?>"><?php echo isset($tax_obj->labels->name) ? $tax_obj->labels->name : $tax_name; ?></label><br /><?php
            }
        } else { ?>
            <em><?php _e('No public taxonomy found!', $this->text_domain); ?></em><?php
        }
    }
    
    /**
     * Post type checkboexes
     * @since 1.0
     * @global object $WP_ES
     */
    public function wp_es_post_types_settings() {
        global $WP_ES;

        /**
         * Filter post type arguments
         * @since 1.0.1
         * @param array arguments array
         */
        $all_post_types = get_post_types(apply_filters('wpes_post_types_args', array(
            'show_ui' => TRUE,
            'public' => TRUE
        )), 'objects');
        
        if (is_array($all_post_types) && !empty($all_post_types)) {
            foreach ($all_post_types as $post_name => $post_obj) { ?>
                <input <?php echo $this->wp_es_checked($post_name, $WP_ES->WP_ES_settings['post_types']); ?> type="checkbox" value="<?php echo $post_name; ?>" id="<?php echo 'wp_es_' . $post_name; ?>" name="wp_es_options[post_types][]" />&nbsp;
                <label for="<?php echo 'wp_es_' . $post_name; ?>"><?php echo isset($post_obj->labels->name) ? $post_obj->labels->name : $post_name; ?></label><br /><?php
            }
        } else { ?>
            <em><?php _e('No public post type found!', $this->text_domain); ?></em><?php
        }
    }
    
    /**
     * Exclude older results
     * @since 1.0.2
     * @global object $WP_ES
     */
    public function wp_es_exclude_results() {
        global $WP_ES; ?>
        <script type="text/javascript">jQuery(document).ready(function (){ jQuery('#es_exclude_date').datepicker({ maxDate: new Date(), changeYear: true, dateFormat: "MM dd, yy" }); });</script>
        <input class="regular-text" type="text" value="<?php echo esc_attr($WP_ES->WP_ES_settings['exclude_date']); ?>" name="wp_es_options[exclude_date]" id="es_exclude_date" />
        <p class="description"><?php _e('Contents will not appear in search results older than this date OR leave blank to disable this feature.', $this->text_domain); ?></p><?php
    }

    /**
     * return checked if value exist in array
     * @since 1.0
     * @param mixed $value value to check against array
     * @param array $array haystack array
     * @return string checked="checked" or blank string
     */
    public function wp_es_checked($value = false, $array = array()) {
        if (in_array($value, $array, true)) {
            $checked = 'checked="checked"';
        } else {
            $checked = '';
        }
        
        return $checked;
    }
}