<?php
/*
Plugin Name: Tarful Likes
Plugin URI: http://tarful.com
Description: Integrates a "Like This" option for posts.
Version: 1.0
Author: Tarful Team
Author URI: http://tarful.com
License: GPL2

Copyright 2015	Tarful Team	(email : hello@tarful.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA	02110-1301	USA
*/
include(WP_PLUGIN_DIR . "/tarful-like/options.php");
include(WP_PLUGIN_DIR . "/tarful-like/widget.php");
include(WP_PLUGIN_DIR . "/tarful-like/manage_posts.php");

function likeThis($post_id, $action = 'get', $direction = 1) {
	global $wpdb;

	$current_user_id =	get_current_user_id(); 
	$current_user = '_tarful_user_likes_' . get_current_user_id(); 
	

	if (!is_numeric($post_id)) {
	error_log("Error: Value submitted for post_id was not numeric");
	return;
	} //if

	switch ($action) {

		case 'get':
			if($current_user_id){
				$likeThis = $wpdb->get_results( "SELECT * FROM like_post WHERE id_user_registered = ".$current_user_id." AND id_post = $post_id LIMIT 1" );
				return $likeThis[0];
			/*}else{
				$likeThis = $wpdb->get_results( "SELECT * FROM like_post WHERE user_cookie = '".$_COOKIE["PHPSESSID"]."' AND id_post = $post_id LIMIT 1" );
				return $likeThis[0];*/
			}
			break;

		case 'update':

			$insert = array('like' => 1, 'id_post' => $post_id);
			$registered=null;

			if($current_user_id){
				$currentValue = $wpdb->get_results( "SELECT * FROM like_post where id_user_registered = ".$current_user_id." AND id_post = $post_id limit 1" );
				$currentValue = $currentValue[0];
				$insert['id_user_registered'] = $current_user_id;
			/*}else{
				$currentValue = $wpdb->get_results( "SELECT * FROM like_post where user_cookie = '".$_COOKIE["PHPSESSID"]."' AND id_post = $post_id limit 1" );
				$currentValue = $currentValue[0];*/

				if ($currentValue) {
					if($currentValue->like){
						$insert['like'] = 0;
					}
					$registered = $wpdb->update( "like_post", $insert, array("id" => $currentValue->id));
				}else{
					$wpdb->insert( "like_post", $insert );
					$registered = $wpdb->insert_id;
				} //if
			}

			//$insert['user_cookie'] = $_COOKIE["PHPSESSID"];
			return array("registered" => $registered, "like" => $insert['like']);
			break;

	} //switch

} //likeThis

function tarful_likes($post_id) {
	global $wpdb;
	return $wpdb->get_var( "SELECT COUNT(*) FROM like_post where `id_post` = ".$post_id." AND `like` = '1'" );
}

function generateLikeString($post_id, $value) {
	$likes = likeThis($post_id);

	$who = str_replace("%", $likes, get_option('some_likes'));

	if ($likes == 1) {
	$who = str_replace("%", $likes, get_option('one_like'));
	} //if

	if ($likes == 0) {
	$who = str_replace("%", $likes, get_option('no_likes'));
	}

	if ($value) {
	return '<a href="#" class="likeThis done" id="like-' . $post_id . '"	data-post-id="' . $post_id . '">' . $who . '</a>';
	} //if

	return '<a href="#" class="likeThis" id="like-' . $post_id . '" data-post-id="' . $post_id . '">' . $who . '</a>';
}

function likeThisSetUpPostLikes($post_id) {
	if (!is_numeric($post_id)) {
	error_log("Error: Value submitted for post_id was not numeric");
	return;
	} //if

	add_post_meta($post_id, '_likes', '0', true);
} //setUpPost

function likeThisCheckHeaders() {
	if (isset($_POST["likepost"])) {
		$id = $_POST["likepost"];

		$respuesta_php = likeThis($id, 'update');
		ob_start();
		tarful_networks_avatars(4, $id);
		$avatars = ob_get_clean();
		ob_start();
		tarful_networks_avatars_all($id);
		$avatars_all = ob_get_clean();


		$respuesta_js = array(".cantidad_likes-".$id => tarful_likes($id),".uers_likes"=>$avatars,".uers_likes_all"=>$avatars_all);
		if($respuesta_php["like"]){
			//$respuesta_js[".like-".$id] = "<img src=".bloginfo('stylesheet_directory')."/images/like_btn.png' alt='ME GUSTA ZOI'/><a href='#' class='likeThis done' data-post-id=".$id.">ME GUSTA</a>";
		}else{
			//$respuesta_js[".like-".$id] = "<img src=".bloginfo('stylesheet_directory')."/images/like_btn.png' alt='ME GUSTA ZOI'/><a href='#' class='likeThis' data-post-id=".$id.">ME GUSTA</a>";
		}
		$register = array(
			"register" => $respuesta_php['registered'],
			"fragments" => $respuesta_js,
			"like" => $respuesta_php["like"]
		);
		header('Content-type: application/json');
		$out = json_encode($register);
		die(print($out));
	} //if
} //checkHeaders


function likeThisJsIncludes() {
	wp_enqueue_script('jquery');
	if(get_current_user_id()){
		wp_register_script('likesScript', WP_PLUGIN_URL . '/tarful-like/likesScript.js');
	}else{
		wp_register_script('likesScript', WP_PLUGIN_URL . '/tarful-like/likesScriptloger.js');
	}
	wp_localize_script('likesScript', 'like_this_ajax_object', array(
	'ajax_url' => admin_url('admin-ajax.php')
	));
	wp_enqueue_script('likesScript', array(
	'jquery'
	));
} //jsIncludes

add_action('publish_post', 'likeThisSetUpPostLikes');
add_action('wp_enqueue_scripts', 'likeThisJsIncludes');

if (is_admin()) {
	add_action('wp_ajax_like_this_like_post', 'likeThisCheckHeaders');
	add_action('wp_ajax_nopriv_like_this_like_post', 'likeThisCheckHeaders');
}
?>
