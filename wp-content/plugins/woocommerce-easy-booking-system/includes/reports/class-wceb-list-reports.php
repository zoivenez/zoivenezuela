<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Easy_Booking_List_Reports extends WP_List_Table {

	protected $max_items;
	public $start_date_text;
	public $end_date_text;

	public function __construct() {

		$wceb_settings = get_option('easy_booking_settings');
		$start_date_text = $wceb_settings['easy_booking_start_date_text'];
        $end_date_text = $wceb_settings['easy_booking_end_date_text'];

		parent::__construct( array(
			'singular'  => __( 'Report', 'woocommerce' ),
			'plural'    => __( 'Reports', 'woocommerce' ),
			'ajax'		=> true
		) );

	}

	protected function display_tablenav( $which ) {

        $filter_id = isset( $_GET['wceb_report_product_ids'] ) ? stripslashes( $_GET['wceb_report_product_ids'] ) : '';
		$filter_start_date = isset( $_GET['wceb_report_start_date_submit'] ) ? stripslashes( $_GET['wceb_report_start_date_submit'] ) : '';
		$filter_end_date = isset( $_GET['wceb_report_end_date_submit'] ) ? stripslashes( $_GET['wceb_report_end_date_submit'] ) : '';
		
		if ( ! empty( $filter_id ) )
			$_product = wc_get_product( $filter_id );

		$product = isset( $_product ) && is_object( $_product ) ? $_product->get_formatted_name() : '';

		?>
		<div class="wceb-reports tablenav <?php echo esc_attr( $which ); ?>">

			<div class="alignleft actions bulkactions">
				<form id="bookings-filter" method="get">
					<input type="hidden" name="page" value="easy-booking-reports">
					<div class="reports-filter filter-id">
						<input type="hidden" id="reports_search" name="wceb_report_product_ids" value='<?php echo absint( $filter_id ) ? absint( $filter_id ) : ''; ?>' data-selected='<?php echo $product; ?>' class="wc-product-search" style="width: 100%;" data-action="wceb_reports_product_id" data-placeholder="<?php _e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-allow_clear="true" data-multiple="false" />
					</div>
					<div class="reports-filter filter-date">
						<input type="text" name="wceb_report_start_date" data-value="<?php echo $filter_start_date; ?>" class="datepicker" placeholder="<?php echo isset( $start_date_text ) ? $start_date_text : __( 'Start', 'easy_booking' ); ?>">
					</div>
					<div class="reports-filter filter-date">
						<input type="text" name="wceb_report_end_date" data-value="<?php echo $filter_end_date; ?>" class="datepicker" placeholder="<?php echo isset( $end_date_text ) ? $end_date_text : __( 'End', 'easy_booking' ); ?>">
					</div>
					<div class="reports-filter-submit">
						<input type="submit" id="post-query-submit" class="button" value="Filtrer">
					</div>
					<br class="clear">
				</form>
				<?php $this->bulk_actions( $which ); ?>
			</div>

			<?php
				$this->extra_tablenav( $which );
				$this->pagination( $which );
			?>

			<br class="clear">
		</div>
		<?php
	}

	public function get_columns() {

		$columns = apply_filters( 'easy_booking_reports_columns', array(
			'order_status' => '<span class="status_head tips" data-tip="' . esc_attr__( 'Status', 'woocommerce' ) . '">' . esc_attr__( 'Status', 'woocommerce' ) . '</span>',
			'order_id' => __( 'Order', 'woocommerce' ),
			'product' => __( 'Product', 'woocommerce' ),
			'start_date' => isset( $start_date_text ) ? $start_date_text : __( 'Start', 'easy_booking' ),
			'end_date'  => isset( $end_date_text ) ? $end_date_text : __( 'End', 'easy_booking' ),
			'qty_booked' => __( 'Quantity booked', 'easy_booking' )
		) );

		return $columns;
	}

	protected function get_sortable_columns() {

		$sortable_columns = array(
			'order_id' => array( 'order_id', true ),
			'product' => array( 'product_id', true ),
			'start_date' => array( 'start_date', false ),
			'end_date' => array( 'end_date', false )
		);

		return $sortable_columns;
	}

	public function column_default( $item, $column_name ) {

		if ( ! empty( $item['order_id'] ) ) {
			$order = wc_get_order( $item['order_id'] );
		}

		if ( ! $order )
			return;

		$product = wc_get_product( $item['product_id'] );

		if ( ! $product )
			return;

		switch ( $column_name ) {

			case 'order_status' :
				printf( '<mark class="%s tips" data-tip="%s">%s</mark>', sanitize_title( $order->get_status() ), wc_get_order_status_name( $order->get_status() ), wc_get_order_status_name( $order->get_status() ) );
			break;

			case 'order_id' :
				$customer_tip = array();

					if ( $address = $order->get_formatted_billing_address() ) {
						$customer_tip[] = __( 'Billing:', 'woocommerce' ) . ' ' . $address . '<br/><br/>';
					}

					if ( $order->billing_phone ) {
						$customer_tip[] = __( 'Tel:', 'woocommerce' ) . ' ' . $order->billing_phone;
					}

					echo '<div class="tips" data-tip="' . wc_sanitize_tooltip( implode( "<br/>", $customer_tip ) ) . '">';

					if ( $order->user_id ) {
						$user_info = get_userdata( $order->user_id );
					}

					if ( ! empty( $user_info ) ) {

						$username = '<a href="user-edit.php?user_id=' . absint( $user_info->ID ) . '">';

						if ( $user_info->first_name || $user_info->last_name ) {
							$username .= esc_html( ucfirst( $user_info->first_name ) . ' ' . ucfirst( $user_info->last_name ) );
						} else {
							$username .= esc_html( ucfirst( $user_info->display_name ) );
						}

						$username .= '</a>';

					} else {
						if ( $order->billing_first_name || $order->billing_last_name ) {
							$username = trim( $order->billing_first_name . ' ' . $order->billing_last_name );
						} else {
							$username = __( 'Guest', 'woocommerce' );
						}
					}

					printf( _x( '%s by %s', 'Order number by X', 'woocommerce' ), '<a href="' . admin_url( 'post.php?post=' . absint( $item['order_id'] ) . '&action=edit' ) . '"><strong>#' . esc_attr( $order->get_order_number() ) . '</strong></a>', $username );

					if ( $order->billing_email ) {
						echo '<small class="meta email"><a href="' . esc_url( 'mailto:' . $order->billing_email ) . '">' . esc_html( $order->billing_email ) . '</a></small>';
					}

					echo '</div>';
			break;

			case 'product' :
				$product_name = $product->get_formatted_name();
				echo $product_name;
			break;

			case 'start_date' :
				echo '<input type="text" data-value="' . $item['start'] . '" disabled="true" class="datepicker datepicker_start">';
			break;

			case 'end_date' :
				echo '<input type="text" data-value="' . $item['end'] . '" disabled="true" class="datepicker datepicker_end">';
			break;

			case 'qty_booked' :
				echo $item['qty'];
			break;

		}
		
	}

	function usort_reorder( $a, $b ) {

		// If no sort, default to product ID
		$orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'product_id';
		// If no order, default to asc
		$order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';

		// Determine sort order
		switch( $orderby ) {

			case 'order_id' :
				$result = ( $a['order_id'] > $b['order_id'] ) ? -1 : 1;
			break;

			case 'product_id' :
				$result = ( $a['product_id'] > $b['product_id'] ) ? -1 : 1;
			break;

			case 'start_date' :
				$a_start_date = strtotime( $a['start'] );
				$b_start_date = strtotime( $b['start'] );

				$result = ( $a_start_date > $b_start_date ) ? -1 : 1;
			break;

			case 'end_date' :
				$a_end_date = strtotime( $a['end'] );
				$b_end_date = strtotime( $b['end'] );

				$result = ( $a_end_date > $b_end_date ) ? -1 : 1;
			break;

		}
		
		// Send final sort direction to usort
		return ( $order === 'asc' ) ? $result : -$result;
	}

	public function prepare_items() {

		$this->_column_headers = array( $this->get_columns(), array(), $this->get_sortable_columns() );
		$current_page          = absint( $this->get_pagenum() );
		$per_page              = 20;

		$this->get_items( $current_page, $per_page );

		/**
		 * Pagination
		 */
		$this->set_pagination_args( array(
			'total_items' => $this->max_items,
			'per_page'    => $per_page,
			'total_pages' => ceil( $this->max_items / $per_page )
		) );
	}

	public function get_items( $current_page, $per_page ) {
		global $wpdb;

		$this->max_items = 0;
		$this->items     = array();

		$booked_products = WCEB()->easy_booking_get_booked_items_from_orders();
		$booked_dates = array();

		if ( $booked_products ) :
			foreach ( $booked_products as $booked_product_id => $booked_product_dates ) {
				foreach ( $booked_product_dates as $date ) {
					$date['product_id'] = $booked_product_id;
					$booked_dates[] = $date;
				}
			}
		endif;

		$filter_id = isset( $_GET['wceb_report_product_ids'] ) ? stripslashes( $_GET['wceb_report_product_ids'] ) : '';
		$filter_start_date = isset( $_GET['wceb_report_start_date_submit'] ) ? stripslashes( $_GET['wceb_report_start_date_submit'] ) : '';
		$filter_end_date = isset( $_GET['wceb_report_end_date_submit'] ) ? stripslashes( $_GET['wceb_report_end_date_submit'] ) : '';

		$filters = array(
			'start' => $filter_start_date,
			'end' => $filter_end_date
		);

		if ( ! empty( $filter_id ) ) {
			foreach ( $booked_dates as $index => $booked_date ) {
				if ( $booked_date['product_id'] != $filter_id ) {
					unset( $booked_dates[$index] );
					continue;
				}
			}
		}

		if ( ! empty( $filter_start_date ) && ! empty( $filter_end_date ) ) {
			foreach ( $booked_dates as $index => $booked_date ) {
				$start = strtotime( $booked_date['start'] );
				$end = strtotime( $booked_date['end'] );

				$start_filter = strtotime( $filter_start_date );
				$end_filter = strtotime( $filter_end_date );

				if ( $start < $start_filter || $end > $end_filter ) {
					unset( $booked_dates[$index] );
					continue;
				}
			}
		} else {
			foreach( $filters as $filter => $filtered ) {
				if ( ! empty( $filtered ) ) {
					foreach ( $booked_dates as $index => $booked_date ) {
						if ( $booked_date[$filter] != $filtered ) {
							unset( $booked_dates[$index] );
							continue;
						}
					}
				}
			}
		}

		usort( $booked_dates, array($this, 'usort_reorder') );

		$total_items = count($booked_dates);
		$min = ( $current_page - 1 ) * $per_page;
		$max = $min + $per_page;

		$set_max = $total_items < $max ? $total_items : $max;

		$items = array();
		for ( $i = $min; $i < $set_max; $i++ ) {
			$items[] = $booked_dates[$i];
		}

		$this->items     = $items;
		$this->max_items = $total_items;
	}

}