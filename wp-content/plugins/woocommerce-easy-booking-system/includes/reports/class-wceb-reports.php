<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class WCEB_Reports {

	public function __construct() {

		if ( is_admin() ) {
			add_action( 'admin_menu', array($this, 'easy_booking_add_reports_pages'), 10 );
		}

	}

	public function easy_booking_add_reports_pages() {
		$reports_page = add_submenu_page( 'easy-booking', __('Reports', 'easy_booking'), __('Reports', 'easy_booking'), 'manage_options', 'easy-booking-reports', array($this, 'easy_booking_reports_page') );

		add_action( 'admin_print_scripts-'. $reports_page, array($this, 'easy_booking_load_admin_reports_scripts') );
		add_action( 'easy_booking_reports_bookings', array($this, 'easy_booking_reports_content') );
	}

	public function easy_booking_load_admin_reports_scripts() {
		wp_enqueue_script( 'select2' );
		wp_enqueue_script( 'wc-enhanced-select' );
		wp_enqueue_script( 'jquery-tiptip' );
		wp_enqueue_script( 'wc-admin-meta-boxes' );
		wp_enqueue_style( 'woocommerce_admin_styles', WC()->plugin_url() . '/assets/css/admin.css', array(), WC_VERSION );
		wp_enqueue_script( 'pickadate' );
		// wp_enqueue_script('easy_booking_reports', plugins_url('assets/js/admin/wceb-reports-functions.min.js', WCEB_PLUGIN_FILE), array('jquery'), '1.0', true );
		wp_enqueue_script('easy_booking_reports', plugins_url('/assets/js/admin/dev/wceb-reports-functions.js', WCEB_PLUGIN_FILE), array('jquery'), '1.0', true );
		wp_enqueue_style('easy_booking_reports_styles', plugins_url('/assets/css/admin/dev/wceb-reports.css', WCEB_PLUGIN_FILE), array(), 1.0);
		
		wp_enqueue_script( 'datepicker.language' );
		wp_enqueue_style( 'picker' );

		do_action( 'easy_booking_load_report_scripts' );
	}

	public function easy_booking_reports_page() {

		?><div class="wrap">

			<div id="wceb-settings-container">

				<?php $wceb_tabs = apply_filters( 'easy_booking_reports_tabs', array(
					'bookings' => __('Bookings', 'easy_booking')
				));

				$current_tab = empty( $_GET['tab'] ) ? 'bookings' : sanitize_title( $_GET['tab'] ); ?>

				<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
					<?php foreach ( $wceb_tabs as $tab => $label ) { ?>
						<a href="<?php echo admin_url( 'admin.php?page=easy-booking-reports&tab=' . $tab ); ?>" class="nav-tab <?php echo ( $current_tab == $tab ? 'nav-tab-active' : '' ) ?>"><?php echo $label; ?></a>
					<?php } ?>
				</h2>

				<?php do_action( 'easy_booking_reports_' . $current_tab ); ?>

			</div>
		</div>

		<?php

	}

	public function easy_booking_reports_content() {

		$wceb_report_list = new Easy_Booking_List_Reports();
		$wceb_report_list->prepare_items();
		$wceb_report_list->display();

	}
}

return new WCEB_Reports();