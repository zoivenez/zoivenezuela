<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCEB_Admin_Assets' ) ) :

class WCEB_Admin_Assets {

	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'easy_booking_enqueue_admin_scripts' ));
	}

	public function easy_booking_enqueue_admin_scripts() {
        global $post;

        $screen = get_current_screen();
        $this->options = get_option('easy_booking_settings');

        // Concatenated and minified script including picker.js, picker.date.js and legacy.js
        wp_register_script( 'pickadate', plugins_url( 'assets/js/pickadate.min.js', WCEB_PLUGIN_FILE ), array('jquery'), '1.0', true );
        // wp_register_script( 'picker', plugins_url( 'assets/js/dev/picker.js', WCEB_PLUGIN_FILE  ), array('jquery'), '1.0', true );
        // wp_register_script( 'picker.date', plugins_url( 'assets/js/dev/picker.date.js', WCEB_PLUGIN_FILE  ), array('jquery'), '1.0', true );
        // wp_register_script( 'legacy', plugins_url( 'assets/js/dev/legacy.js', WCEB_PLUGIN_FILE  ), array('jquery'), '1.0', true );

        // Calendar theme
        $theme = $this->options['easy_booking_calendar_theme'];

        if ( function_exists( 'is_multisite' ) && is_multisite() ) {
            $blog_id = get_current_blog_id();
            wp_register_style( 'picker', plugins_url('assets/css/' . $theme . '.' . $blog_id . '.min.css', WCEB_PLUGIN_FILE), true);
            wp_register_style( 'default-picker', plugins_url('assets/css/default.' . $blog_id . '.min.css', WCEB_PLUGIN_FILE), true);
        } else {
            wp_register_style( 'picker', plugins_url('assets/css/' . $theme . '.min.css', WCEB_PLUGIN_FILE), true);
            wp_register_style( 'default-picker', plugins_url('assets/css/default.min.css', WCEB_PLUGIN_FILE), true);
        }

        if ( in_array( $screen->id, array( 'product' ) ) ) {

            $product = wc_get_product( $post->ID );

            //wp_enqueue_script( 'wceb-admin-product', plugins_url('assets/js/admin/' . WCEB_PATH . 'wceb-admin-product' . WCEB_SUFFIX . '.js', WCEB_PLUGIN_FILE), array('jquery'), '1.0', true );
            wp_enqueue_script( 'wceb-admin-product', plugins_url('assets/js/admin/dev/wceb-admin-product.js', WCEB_PLUGIN_FILE), array('jquery'), '1.0', true );

            wp_register_style( 'static-picker', plugins_url( 'assets/css/admin/' . WCEB_PATH . 'static-picker' . WCEB_SUFFIX . '.css', WCEB_PLUGIN_FILE ), true);
            wp_enqueue_style( 'static-picker' );
            
        }

        if ( in_array( $screen->id, array( 'shop_order' ) ) ) {

            // Calculation mode (Days or Nights)
            $calc_mode = $this->options['easy_booking_calc_mode'];

            wp_enqueue_script( 'pickadate-custom-admin', plugins_url( 'assets/js/admin/' . WCEB_PATH . 'pickadate-custom-admin' . WCEB_SUFFIX . '.js', WCEB_PLUGIN_FILE ), array('jquery', 'pickadate'), '1.0', true);
            wp_enqueue_style( 'picker' );

            wp_localize_script( 'pickadate-custom-admin', 'order_ajax_info',
                array( 
                    'ajax_url' => esc_url( admin_url( 'admin-ajax.php' ) ),
                    'order_id' => $post->ID,
                    'calc_mode' => esc_html( $calc_mode )
                )
            );

        }

        wp_register_script( 'datepicker.language', plugins_url( 'assets/js/translations/' . WCEB_LANG . '.js', WCEB_PLUGIN_FILE ), array('jquery', 'pickadate'), '1.0', true);

        if ( in_array( $screen->id, array( 'product' ) ) || in_array( $screen->id, array( 'shop_order' ) ) ) {

            wp_enqueue_script( 'pickadate' );

            if ( is_rtl() ) {
                // Load Right to left CSS file
                wp_register_style( 'rtl-style', plugins_url( 'assets/css/' . WCEB_PATH . 'rtl' . WCEB_SUFFIX . '.css', WCEB_PLUGIN_FILE ), true );
                wp_enqueue_style( 'rtl-style' );  
            }
            
            wp_enqueue_script( 'datepicker.language' );
        }

        // JS for admin notices
        wp_enqueue_script( 'easy_booking_functions', plugins_url( 'assets/js/admin/' . WCEB_PATH . 'wceb-admin-functions' . WCEB_SUFFIX . '.js', WCEB_PLUGIN_FILE ), array('jquery'), '1.0', true);
        
        // CSS for admin notices
        wp_enqueue_style( 'easy_booking_notices', plugins_url(  'assets/css/admin/' . WCEB_PATH . 'wceb-notices' . WCEB_SUFFIX . '.css', WCEB_PLUGIN_FILE ) );

        wp_localize_script( 'easy_booking_functions', 'ajax_object',
            array( 
                'ajax_url' => esc_url( admin_url( 'admin-ajax.php' ) )
            )
        );

    }
}

return new WCEB_Admin_Assets();

endif;
