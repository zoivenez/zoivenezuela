<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<div id="booking_variation_data" class="show_if_variation_bookable">

    <p class="form-row form-row-first">
        <label for="_var_booking_min[<?php echo $loop; ?>]">
            <?php _e( 'Minimum booking duration', 'easy_booking' ); ?>
            <span class="tips" data-tip="<?php _e( 'Enter zero to set no duration limit or leave blank to use the parent product\'s booking options or the global settings.', 'easy_booking' ); ?>">[?]</span></label>
        <input type="number" class="input_text" min="0" name="_var_booking_min[<?php echo $loop; ?>]" value="<?php if ( isset( $booking_min ) ) echo esc_attr( $booking_min ); ?>" />
    </p>

    <p class="form-row form-row-last">
        <label for="_var_booking_max[<?php echo $loop; ?>]">
            <?php _e( 'Maximum booking duration', 'easy_booking' ); ?>
            <span class="tips" data-tip="<?php _e( 'Enter zero to set no duration limit or leave blank to use the parent product\'s booking options or the global settings.', 'easy_booking' ); ?>">[?]</span></label>
        <input type="number" class="input_text" min="0" name="_var_booking_max[<?php echo $loop; ?>]" value="<?php if ( isset( $booking_max ) ) echo esc_attr( $booking_max ); ?>" />
    </p>

    <p class="form-row form-row-first">
        <label for="_var_first_available_date[<?php echo $loop; ?>]">
            <?php _e( 'First available date', 'easy_booking' ); ?>
            <span class="tips" data-tip="<?php _e( 'First available date, relative to today. I.e. : today + 5 days. Enter 0 for today or leave blank to use the parent product\'s booking options or the global settings.', 'easy_booking' ); ?>">[?]</span></label>
        <input type="number" class="input_text" min="0" name="_var_first_available_date[<?php echo $loop; ?>]" value="<?php if ( isset( $first_available_date ) ) echo esc_attr( $first_available_date ); ?>" />
    </p>

    <p class="form-row form-row-first">
        <label for="_var_last_available_date[<?php echo $loop; ?>]">
            <?php _e( 'Last available date', 'easy_booking' ); ?>
            <span class="tips" data-tip="<?php _e( 'Last available date, this will disable dates after in the datepicker.', 'easy_booking' ); ?>">[?]</span>
            <a href="" class="clear_av_date">clear field</a>

</label>
        <input type="text" id="last_available_date_<?=$loop?>" class="input_text input_date" name="_var_last_available_date[<?php echo $loop; ?>]" value="<?php if ( isset( $last_available_date ) ) echo esc_attr( $last_available_date ); ?>" />
    </p>

    <p class="form-row form-row-first">
        <label for="_var_people_min[<?php echo $loop; ?>]">
            <?php _e( 'People included in room price', 'easy_booking' ); ?>
            <span class="tips" data-tip="<?php _e( 'Maximum number of people included in the room price.', 'easy_booking' ); ?>">[?]</span></label>
        <input type="number" class="input_text" min="0" name="_var_people_min[<?php echo $loop; ?>]" value="<?php if ( isset( $people_min ) ) echo esc_attr( $people_min ); ?>" />
    </p>
    <p class="form-row form-row-first">
        <label for="_var_people_max[<?php echo $loop; ?>]">
            <?php _e( 'Max people per room', 'easy_booking' ); ?>
            <span class="tips" data-tip="<?php _e( 'Maximum number of people a room accepts included and not included in the price.', 'easy_booking' ); ?>">[?]</span></label>
        <input type="number" class="input_text" min="0" name="_var_people_max[<?php echo $loop; ?>]" value="<?php if ( isset( $people_max ) ) echo esc_attr( $people_max ); ?>" />
    </p>

    <p class="form-row form-row-first">
        <label for="_var_adult_price[<?php echo $loop; ?>]">
            <?php _e( 'Price per additional adult', 'easy_booking' ); ?>
            <span class="tips" data-tip="<?php _e( 'Price per additional adult above the minimum number of persons per room.', 'easy_booking' ); ?>">[?]</span></label>
        <input type="text" class="input_text" min="0" name="_var_adult_price[<?php echo $loop; ?>]" value="<?php if ( isset( $adult_price ) ) echo esc_attr( $adult_price ); ?>" />
    </p>

    <p class="form-row form-row-first">
        <label for="_var_accept_children[<?php echo $loop; ?>]">
            <?php _e( 'Accepts childrens', 'easy_booking' ); ?>  
           <span class="tips" data-tip="<?php _e( 'Check this if the room accepts childrens. This will show a dropdown for additional childrens.', 'easy_booking' ); ?>">[?]</span></label>
            <input type="checkbox" class=""  name="_var_accept_children[<?php echo $loop; ?>]" value="1" <?php checked( $accept_children, '1' ) ?> />
    </p>

    <p class="form-row form-row-first">
        <label for="_var_children_price[<?php echo $loop; ?>]">
            <?php _e( 'Price per additional children', 'easy_booking' ); ?>
            <span class="tips" data-tip="<?php _e( 'Price per additional children above the minimum number of persons per room.', 'easy_booking' ); ?>">[?]</span></label>
        <input type="text" class="input_text" min="0" name="_var_children_price[<?php echo $loop; ?>]" value="<?php if ( isset( $children_price ) ) echo esc_attr( $children_price ); ?>" />
    </p>



    <span class="form-row form-row-first">
        <div class="opening-seasons">
    <?php
        $weekdays = array(
            'monday',
            'thuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday'
        ); 
        if (isset($opening_seasons) && !empty($opening_seasons)): 
            foreach($opening_seasons as $pos => $opening_season): ?>

            <fieldset class="form-row form-row-first" style="width:100%">
                <legend class="form-row-first"><strong>Opening Season</strong>                
                    <span class="form-row-fist remove-opening-season" style="cursor:pointer">x</span>
                </legend>

                <p class="form-row form-row-first">
                    <label for="_var_season_start[<?php echo $loop; ?>][]">
                        <?php _e( 'Start date', 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( 'Starting date for an opening season', 'easy_booking' ); ?>">[?]</span></label>
                            <input type="text" class="input_text input_date" min="0" name="_var_season_start[<?php echo $loop; ?>][]" value="<?=$opening_season['start']?>"/>
                </p>
                <p class="form-row form-row-first">
                    <label for="_var_season_end[<?php echo $loop; ?>][]">
                        <?php _e( 'End date', 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( 'End date for an opening season', 'easy_booking' ); ?>">[?]</span></label>
                        <input type="text" class="input_text input_date" min="0" name="_var_season_end[<?php echo $loop; ?>][]" value="<?=$opening_season['end']?>"/>
                </p>

                <p class="form-row form-row-first">
                    <label for="_var_season_price[<?php echo $loop; ?>][]">
                        <?php _e( 'Default price', 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( 'Season price if the day price is not set', 'easy_booking' ); ?>">[?]</span></label>
                            <input type="text" class="input_text" min="0" name="_var_season_price[<?php echo $loop; ?>][]" value="<?=$opening_season['price']?>"/>
                </p>

                <p class="form-row form-row-first">
                    <label for="_var_season_price[<?php echo $loop; ?>][]">
                        <?php _e( 'Default Extra Adult price', 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( 'Season adult price if the day price is not set', 'easy_booking' ); ?>">[?]</span></label>
                            <input type="text" class="input_text" min="0" name="_var_season_adult_price[<?php echo $loop; ?>][]" value="<?=$opening_season['adult_price']?>"/>
                </p>

                <p class="form-row form-row-first">
                    <label for="_var_season_price[<?php echo $loop; ?>][]">
                        <?php _e( 'Default Extra Children price', 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( 'Season children price if the day price is not set', 'easy_booking' ); ?>">[?]</span></label>
                            <input type="text" class="input_text" min="0" name="_var_season_children_price[<?php echo $loop; ?>][]" value="<?=$opening_season['children_price']?>"/>
                </p>




                <?php
                 foreach($weekdays as $pos => $weekday):?>
                 <p class="form-row form-row-first">
                    <label for="_var_season_price_monday[<?php echo $loop; ?>][]">
                        <?php _e( ucfirst($weekday), 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( "Prices for $weekday in this season in this order: price, children price, adult price.", 'easy_booking' ); ?>">[?]</span></label>
                            <input type="text" class="input_text" placeholder="Price" name="_var_season_price_<?=$weekday?>[<?php echo $loop; ?>][]" value="<?=$opening_season['daily_prices'][$pos]['price']?>" />
                    <input type="text" class="input_text" placeholder="+ Children" name="_var_season_children_price_<?=$weekday?>[<?php echo $loop; ?>][]" value="<?=$opening_season['daily_prices'][$pos]['adult_price']?>"/>
                    <input type="text" class="input_text" placeholder="+ Adult" name="_var_season_adult_price_<?=$weekday?>[<?php echo $loop; ?>][]" value="<?=$opening_season['daily_prices'][$pos]['children_price']?>"/>
                </p>   
                <?php endforeach; ?>
           </fieldset>

    
    <?php endforeach; 
        else:  ?>
            <fieldset class="form-row form-row-first" style="width:100%">
                <legend class="form-row-first"><strong>Opening Season</strong>
                    <span class="form-row-fist remove-opening-season" style="cursor:pointer">x</span>
                </legend>
                <p class="form-row form-row-first">
                    <label for="_var_season_start[<?php echo $loop; ?>][]">
                        <?php _e( 'Start date', 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( 'Starting date for an opening season ', 'easy_booking' ); ?>">[?]</span></label>
                        <input type="text" class="input_text input_date" min="0" name="_var_season_start[<?php echo $loop; ?>][]" />
                </p>
                <p class="form-row form-row-first">
                    <label for="_var_season_end[<?php echo $loop; ?>][]">
                        <?php _e( 'End date', 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( 'End date for an opening season', 'easy_booking' ); ?>">[?]</span></label>
                        <input type="text" class="input_text input_date" min="0" name="_var_season_end[<?php echo $loop; ?>][]" />
                </p>

                <p class="form-row form-row-first">
                    <label for="_var_season_price[<?php echo $loop; ?>][]">
                        <?php _e( 'Default price', 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( 'Season price if the day price is not set', 'easy_booking' ); ?>">[?]</span></label>
                            <input type="text" class="input_text" min="0" name="_var_season_price[<?php echo $loop; ?>][]" />
                </p>
                <?php 

                foreach($weekdays as $pos => $weekday):?>
                 <p class="form-row form-row-first">
                    <label for="_var_season_price_monday[<?php echo $loop; ?>][]">
                        <?php _e( ucfirst($weekday), 'easy_booking' ); ?>
                        <span class="tips" data-tip="<?php _e( "Price for $weekday in this season in this order: price, children price, adult price.", 'easy_booking' ); ?>">[?]</span></label>
                    <input type="text" class="input_text" placeholder="Price" name="_var_season_price_<?=$weekday?>[<?php echo $loop; ?>][]" />
                    <input type="text" class="input_text" placeholder="+ Children" name="_var_season_children_price_<?=$weekday?>[<?php echo $loop; ?>][]" />
                    <input type="text" class="input_text" placeholder="+ Adult" name="_var_season_adult_price_<?=$weekday?>[<?php echo $loop; ?>][]" />
                </p>   
                <?php endforeach; ?>
 
            </fieldset>
    <?php endif; ?>
        </div>
        <span class="form-row-first">
            <button class="add-opening-season button button-primary">Add Opening Season</button>
        </span>
    </span>


    <?php do_action('easy_booking_after_variation_booking_options', $product, $variation ); ?>

</div>
