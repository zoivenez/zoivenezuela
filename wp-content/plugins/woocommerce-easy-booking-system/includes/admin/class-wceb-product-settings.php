<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCEB_Product_Settings' ) ) :

class WCEB_Product_Settings {

	public function __construct() {

        // get plugin options values
        $wceb_settings = get_option('easy_booking_settings');

        $this->all_bookable = isset( $wceb_settings['easy_booking_all_bookable'] ) ? $wceb_settings['easy_booking_all_bookable'] : '';
        $this->global_booking_min = isset( $wceb_settings['easy_booking_booking_min'] ) ? absint( $wceb_settings['easy_booking_booking_min'] ) : '';
        $this->global_booking_max = isset( $wceb_settings['easy_booking_booking_max'] ) ? absint( $wceb_settings['easy_booking_booking_max'] ) : '';
        $this->global_first_available_date = isset( $wceb_settings['easy_booking_first_available_date'] ) ? absint( $wceb_settings['easy_booking_first_available_date'] ) : '';

		add_action( 'product_type_options', array( $this, 'easy_booking_add_product_option_pricing' ));
        add_action( 'woocommerce_variation_options', array( $this, 'easy_booking_set_variation_booking_option' ), 10, 3);
        add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'easy_booking_add_variation_booking_options' ), 10, 3);
        add_filter( 'woocommerce_product_data_tabs', array( $this, 'easy_booking_add_booking_tab' ), 10, 1);
        add_action( 'woocommerce_product_data_panels', array($this, 'easy_booking_add_booking_data_panel'));
        add_filter( 'woocommerce_process_product_meta_simple', array( $this, 'easy_booking_save_booking_options' ));
        add_filter( 'woocommerce_process_product_meta_grouped', array( $this, 'easy_booking_save_booking_options' ));
        add_filter( 'woocommerce_process_product_meta_variable', array( $this, 'easy_booking_save_variable_booking_options' ));
        add_action( 'woocommerce_save_product_variation', array( $this, 'easy_booking_save_variation_booking_options' ), 10, 2);
	}

    /**
    *
    * Adds a checkbox to the product admin page to set the product as bookable
    *
    * @param array $product_type_options
    * @return array $product_type_options
    *
    **/
    public function easy_booking_add_product_option_pricing( $product_type_options ) {
        global $post;

        // Backward compatibility
        $is_bookable = get_post_meta( $post->ID, '_booking_option', true );
        
        $product_type_options['booking_option'] = array(
            'id'            => '_booking_option',
            'wrapper_class' => 'show_if_simple show_if_variable show_if_grouped',
            'label'         => __( 'Bookable', 'easy_booking' ),
            'description'   => __( 'Bookable products can be rent or booked on a daily schedule', 'easy_booking' ),
            'default'       => $is_bookable === 'yes' ? 'yes' : 'no'
        );

        return $product_type_options;
    }

    /**
    *
    * Adds a checkbox to the product variation to set it as bookable
    *
    * @param int $loop
    * @param array $variation_data
    * @param obj $variation
    *
    **/
    public function easy_booking_set_variation_booking_option( $loop, $variation_data, $variation ) {
        global $post;
        
        $is_bookable = get_post_meta( $variation->ID, '_booking_option', true ); ?>
        
            <label class="show_if_bookable"><input type="checkbox" class="checkbox variable_is_bookable" name="_var_booking_option[<?php echo $loop; ?>]" <?php checked( $is_bookable, 'yes' ) ?> /> <?php _e( 'Bookable', 'woocommerce' ); ?></label>
        
        <?php
    }

    public function easy_booking_add_variation_booking_options( $loop, $variation_data, $variation ) {
        $variation_id = $variation->ID;
        $product = wc_get_product( $variation_id );
        
        $booking_min          = get_post_meta( $variation_id, '_booking_min', true );
        $booking_max          = get_post_meta( $variation_id, '_booking_max', true );
        $first_available_date = get_post_meta( $variation_id, '_first_available_date', true );
        $last_available_date  = get_post_meta( $variation_id, '_last_available_date', true );
        $people_min           = get_post_meta( $variation_id, '_people_min', true );
        $people_max           = get_post_meta( $variation_id, '_people_max', true );
        $accept_children      = get_post_meta( $variation_id, '_accept_children', true );
        $children_price       = get_post_meta( $variation_id, '_children_price', true );
        $adult_price          = get_post_meta( $variation_id, '_adult_price', true );
        $opening_seasons      = get_post_meta( $variation_id, '_opening_seasons', true );

        include('views/wceb-html-variation-booking-options.php');
        
    }

    /**
    *
    * Adds a booking tab to the product admin page for booking options
    *
    * @param array $product_data_tabs
    * @return array $product_data_tabs
    *
    **/
    public function easy_booking_add_booking_tab( $product_data_tabs ) {

        $product_data_tabs['WCEB'] = array(
                'label'  => __( 'Bookings', 'easy_booking' ),
                'target' => 'booking_product_data',
                'class'  => array( 'show_if_simple show_if_variable show if_grouped show_if_bookable' ),
        );

        return $product_data_tabs;
    }

    /**
    *
    * Adds booking options in the booking tab
    *
    **/
    public function easy_booking_add_booking_data_panel() {
        global $post;

        $product = wc_get_product( $post->ID );
        include('views/wceb-html-product-booking-options.php');

    }

    /**
    *
    * Saves checkbox value and booking options for the product
    *
    * @param int $post_id
    *
    **/
    public function easy_booking_save_booking_options( $post_id ) {
        $is_bookable = isset( $_POST['_booking_option'] ) ? 'yes' : '';
        
        if ( ! empty( $this->all_bookable ) )
            $is_bookable = 'yes';

        $data = array(
            'booking_min' => $_POST['_booking_min'],
            'booking_max' => $_POST['_booking_max'],
            'first_available_date' => $_POST['_first_available_date']
        );

        foreach ( $data as $name => $value ) {
            $global = 'global_'.$name;

            switch ( $value ) {
                case '' :
                    $$name = ! empty( $this->{$global} ) ? $this->{$global} : '';
                break;

                case 0 :
                    $$name = '0';
                break;

                default :
                    $$name = absint( $value );
                break;
            }
        }

        if ( $booking_min != 0 && $booking_max != 0 && $booking_min > $booking_max ) {
            WC_Admin_Meta_Boxes::add_error( __( 'Minimum booking duration must be inferior to maximum booking duration', 'easy_booking' ) );
        } else {
            update_post_meta( $post_id, '_booking_min', $booking_min );
            update_post_meta( $post_id, '_booking_max', $booking_max );
        }

        update_post_meta( $post_id, '_first_available_date', $first_available_date );
        update_post_meta( $post_id, '_booking_option', $is_bookable );

    }

    public function easy_booking_save_variable_booking_options( $post_id ) {
        $is_bookable = isset( $_POST['_booking_option'] ) ? 'yes' : '';

        if ( ! empty( $this->all_bookable ) )
            $is_bookable = 'yes';

        $manage_bookings = isset( $_POST['_manage_bookings'] ) ? 'yes' : '';

        $data = array(
            'booking_min' => $_POST['_booking_min'],
            'booking_max' => $_POST['_booking_max'],
            'first_available_date' => $_POST['_first_available_date']
        );

        foreach ( $data as $name => $value ) {
            $global = 'global_'.$name;

            switch ( $value ) {
                case '' :
                    $$name = ! empty( $this->{$global} ) ? $this->{$global} : '';
                break;

                case 0 :
                    $$name = '0';
                break;

                default :
                    $$name = absint( $value );
                break;
            }
        }

        if ( $booking_min != 0 && $booking_max != 0 && $booking_min > $booking_max ) {
            WC_Admin_Meta_Boxes::add_error( __( 'Minimum booking duration must be inferior to maximum booking duration', 'easy_booking' ) );
        } else {
            update_post_meta( $post_id, '_booking_min', $booking_min );
            update_post_meta( $post_id, '_booking_max', $booking_max ); 
        }
        
        update_post_meta( $post_id, '_first_available_date', $first_available_date );
        update_post_meta( $post_id, '_booking_option', $is_bookable );
        update_post_meta( $post_id, '_manage_bookings', $manage_bookings );


    }

    public function easy_booking_save_variation_booking_options( $variation_id , $i ) {
        $is_bookable = isset( $_POST['_var_booking_option'][$i] ) ? 'yes' : '';

        if ( ! empty( $this->all_bookable ) )
            $is_bookable = 'yes';

        $data = array(
            'booking_min'                      => $_POST['_var_booking_min'][$i],
            'booking_max'                      => $_POST['_var_booking_max'][$i],
            'people_min'                       => $_POST['_var_people_min'][$i],
            'people_max'                       => $_POST['_var_people_max'][$i],
            'children_price'                   => $_POST['_var_children_price'][$i],
            'accept_children'                  => isset($_POST['_var_accept_children'][$i]) ? $_POST['_var_accept_children'][$i] : false,
            'adult_price'                      => $_POST['_var_adult_price'][$i],
            'first_available_date'             => $_POST['_var_first_available_date'][$i],
            'last_available_date'              => $_POST['_var_last_available_date'][$i],
            'season_starts'                    => (array)$_POST['_var_season_start'][$i],
            'season_ends'                      => (array)$_POST['_var_season_end'][$i],
            'season_prices'                    => (array)$_POST['_var_season_price'][$i],
            'season_adult_prices'              => (array)$_POST['_var_season_adult_price'][$i],
            'season_children_prices'           => (array)$_POST['_var_season_children_price'][$i],
            'season_monday_prices'             => (array)$_POST['_var_season_price_monday'][$i],
            'season_monday_adult_prices'       => (array)$_POST['_var_season_adult_price_monday'][$i],
            'season_monday_children_prices'    => (array)$_POST['_var_season_children_price_monday'][$i],
            'season_thuesday_prices'           => (array)$_POST['_var_season_price_thuesday'][$i],
            'season_thuesday_adult_prices'     => (array)$_POST['_var_season_adult_price_thuesday'][$i],
            'season_thuesday_children_prices'  => (array)$_POST['_var_season_children_price_thuesday'][$i],
            'season_wednesday_prices'          => (array)$_POST['_var_season_price_wednesday'][$i],
            'season_wednesday_adult_prices'    => (array)$_POST['_var_season_adult_price_wednesday'][$i],
            'season_wednesday_children_prices' => (array)$_POST['_var_season_children_price_wednesday'][$i],
            'season_thursday_prices'           => (array)$_POST['_var_season_price_thursday'][$i],
            'season_thursday_adult_prices'     => (array)$_POST['_var_season_adult_price_thursday'][$i],
            'season_thursday_children_prices'  => (array)$_POST['_var_season_children_price_thursday'][$i],
            'season_friday_prices'             => (array)$_POST['_var_season_price_friday'][$i],
            'season_friday_adult_prices'       => (array)$_POST['_var_season_adult_price_friday'][$i],
            'season_friday_children_prices'    => (array)$_POST['_var_season_children_price_friday'][$i],
            'season_saturday_prices'           => (array)$_POST['_var_season_price_saturday'][$i],
            'season_saturday_adult_prices'     => (array)$_POST['_var_season_adult_price_saturday'][$i],
            'season_saturday_children_prices'  => (array)$_POST['_var_season_children_price_saturday'][$i],
            'season_sunday_prices'             => (array)$_POST['_var_season_price_sunday'][$i],
            'season_sunday_adult_prices'       => (array)$_POST['_var_season_adult_price_sunday'][$i],
            'season_sunday_children_prices'    => (array)$_POST['_var_season_children_price_sunday'][$i]
        );

        foreach ( $data as $name => $value ) {
            
            switch ( $value ) {
                case '' :
                    $$name = '';
                break;

                case 0 :
                    $$name = '0';
                break;

                default :
                    
                    if (!is_array($value)) {
                        $$name = is_int($value) ? absint( $value ) : sanitize_text_field($value);
                    } else {
                        $$name = $value; 
                    }
                break;
            }
        }

        if ( $booking_min != 0 && $booking_max != 0 && $booking_min > $booking_max ) {
            WC_Admin_Meta_Boxes::add_error( __( 'Minimum booking duration must be inferior to maximum booking duration', 'easy_booking' ) );
        } else {
            update_post_meta( $variation_id, '_booking_min', $booking_min );
            update_post_meta( $variation_id, '_booking_max', $booking_max );
            
        }

        $opening_seasons = array(); 
        // Create the opening seasons
        $opening_seasons_size = sizeof($season_ends); 
        for($k = 0; $k < $opening_seasons_size; ++$k) {
            // Any of the 3 attributes empty, none of them is taken
            if (empty($season_starts[$k]) 
                || empty($season_ends[$k])
                || empty($season_prices[$k])) continue; 

            $opening_seasons[] = array(
                'start'          => $season_starts[$k],
                'end'            => $season_ends[$k],
                'price'          => $season_prices[$k],
                'adult_price'    => $season_adult_prices[$k],
                'children_price' => $season_children_prices[$k],
                'daily_prices'   => array(
                    0 => array(
                        'price'          => $season_monday_prices[$k],
                        'adult_price'    => $season_monday_adult_prices[$k],
                        'children_price' => $season_monday_children_prices[$k],
                    ),
                    1 => array(
                        'price'          => $season_thuesday_prices[$k],
                        'adult_price'    => $season_thuesday_adult_prices[$k],
                        'children_price' => $season_thuesday_children_prices[$k],
                    ),
                    2 => array(
                        'price'          => $season_wednesday_prices[$k],
                        'adult_price'    => $season_wednesday_adult_prices[$k],
                        'children_price' => $season_wednesday_children_prices[$k],
                    ),
                    3 => array(
                        'price'           => $season_thursday_prices[$k],
                        'adult_price'     => $season_thursday_adult_prices[$k],
                        'children_price'  => $season_thursday_children_prices[$k],
                    ),
                    4 => array(
                        'price'          => $season_friday_prices[$k],
                        'adult_price'    => $season_friday_adult_prices[$k],
                        'children_price' => $season_friday_children_prices[$k],
                    ),
                    5 => array(
                        'price'          => $season_saturday_prices[$k],
                        'adult_price'    => $season_saturday_adult_prices[$k],
                        'children_price' => $season_saturday_children_prices[$k],
                    ),
                    6 => array(
                        'price'           => $season_sunday_prices[$k],
                        'adult_price'     => $season_sunday_adult_prices[$k],
                        'children_price'  => $season_sunday_children_prices[$k]
                    )
                )
            );
        }

        //PC::debug($opening_seasons); 

        update_post_meta( $variation_id, '_first_available_date', $first_available_date );
        update_post_meta( $variation_id, '_last_available_date', $last_available_date );
        update_post_meta( $variation_id, '_booking_option', $is_bookable );
        update_post_meta( $variation_id, '_people_min', $people_min );
        update_post_meta( $variation_id, '_people_max', $people_max );
        update_post_meta( $variation_id, '_accept_children', $accept_children );
        update_post_meta( $variation_id, '_children_price', $children_price );
        update_post_meta( $variation_id, '_adult_price', $adult_price );
        update_post_meta( $variation_id, '_opening_seasons', $opening_seasons );
    }

}

return new WCEB_Product_Settings();

endif;
