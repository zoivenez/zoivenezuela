<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCEB_Ajax' ) ) :

class WCEB_Ajax {

	public function __construct() {
        // Get plugin options values
        $this->options = get_option('easy_booking_settings');
        
		add_action( 'wp_ajax_add_new_price', array( $this, 'easy_booking_get_new_price' ));
        add_action( 'wp_ajax_nopriv_add_new_price', array( $this, 'easy_booking_get_new_price' ));
        add_action( 'wp_ajax_clear_booking_session', array( $this, 'easy_booking_clear_booking_session' ));
        add_action( 'wp_ajax_nopriv_clear_booking_session', array( $this, 'easy_booking_clear_booking_session' ));
        add_action( 'wp_ajax_woocommerce_get_refreshed_fragments', array( $this, 'easy_booking_new_price_fragment' ));
        add_action( 'wp_ajax_nopriv_woocommerce_get_refreshed_fragments',  array( $this, 'easy_booking_new_price_fragment' ));
        add_action( 'wp_ajax_easy_booking_hide_notice', array($this, 'easy_booking_hide_notice') );
        add_action( 'wp_ajax_easy_booking_send_room_info', array($this, 'easy_booking_send_room_info') );


        add_action( 'wp_ajax_wceb_reports_product_id', array($this, 'wceb_reports_product_id') );
	}

    /**
    *
    * Calculates new price, update product meta and refresh fragments
    *
    **/
    public function easy_booking_get_new_price() {
        $product_id = isset( $_POST['product_id'] ) ? absint( $_POST['product_id'] ) : ''; // Product ID
        $variation_id = isset( $_POST['variation_id'] ) ? absint( $_POST['variation_id'] ) : ''; // Variation ID
        $children = isset( $_POST['children'] ) ? array_map( 'absint', $_POST['children'] ) : array();


        $calc_mode = $this->options['easy_booking_calc_mode']; // Calculation mode (Days or Nights)

        $start_date = isset( $_POST['start'] ) ? sanitize_text_field( $_POST['start'] ) : ''; // Booking start date
        $end_date = isset( $_POST['end'] ) ? sanitize_text_field( $_POST['end'] ) : ''; // Booking end date

        // If one date is empty
        if ( empty( $start_date ) || empty( $end_date ) )
            $error_code = 2;

        $start = isset( $_POST['start_format'] ) ? sanitize_text_field( $_POST['start_format'] ) : ''; // Booking start date 'yyyy-mm-dd'
        $end = isset( $_POST['end_format'] ) ? sanitize_text_field( $_POST['end_format'] ) : ''; // Booking end date 'yyyy-mm-dd'

        $additional_cost = isset( $_POST['additional_cost'] ) ? wc_format_decimal( $_POST['additional_cost'], 2 ) : 0;

        // Get total people
        $children = isset( $_POST['additional_children'] ) ? absint( $_POST['additional_children'] ) : 0; // extra children
        $adult    = isset( $_POST['additional_adult'] ) ? absint( $_POST['additional_adult'] ) : 0; // extra adults

        $people_min = get_post_meta( $variation_id, '_people_min', true );

        $total_people = $adult + $children; 

        // We don't have extra people
        if ($total_people <= $people_min) {
            $additional_adult    = 0;
            $additional_children = 0;
        // We have extra people
        } else {
            $rest = 0; 
            // If we have more adults than the people included
            // in the price, then additional adults are the 
            // total number of adults substracting the min people. The
            // additional children variable takes the value of
            // the total children.
            if ($adult >= $people_min) {
                $additional_adult = $adult - $people_min; 
            // If we have less adults than the people included
            // in the price, then we don't have additional adults 
            // and then we put all possible children inside the 
            // price, meaning that the difference between people_min
            // and adults is substracted to children
            } else {
                $rest = $people_min - $adult; 
                $additional_adult = 0; 
            }

            $additional_children = $children - $rest; 
        }

        // If total_people is less than 

        // Get booking duration
        $start_diff = strtotime( $start );
        $end_diff = strtotime( $end );

        $diff = absint( $start_diff - $end_diff ) * 1000;

        $days = $diff / 86400000;

        if ( $days == 0 )
            $days = 1;

        // If calculation mode is set to "Days", add one day
        if ( $calc_mode === 'days' && ( $start != $end ) ) {
            $duration = absint( $days + 1 );
        } elseif ( ( $calc_mode === 'days' ) && ( $start === $end ) ) {
            $duration = absint( $days );
        } else {
            $duration = absint( $days );
        }

        // If number of days is inferior to 0
        if ( $duration <= 0 )
            $error_code = 1;

        $start_datetime = new DateTime($start); 
        $end_datetime = new DateTime($end); 
        
        $seasons_prices = array(); 
        // Get all opening seasons for this variation
        $opening_seasons = get_post_meta( $variation_id, '_opening_seasons', true );

        $opening_seasons = $this->handle_overlapping_seasons($opening_seasons); 

        foreach ($opening_seasons as $pos => $opening_season) {
            $season_start = new DateTime( $opening_season['start'] ); 
            $season_end = new DateTime( $opening_season['end'] ); 

            // Not overlapping ranges, continue
            if ($season_start > $end_datetime || $season_end < $start_datetime ) { continue; }


            $season_duration = min($season_end, $end_datetime)->diff(max($season_start, $start_datetime))->days;
            $season_duration += $calc_mode === 'days'? 1 : 0; 


            $seasons_prices[] = array(
                'duration' => $season_duration,
                'price' => $opening_season['price'],
                'children_price' => $opening_season['children_price'],
                'adult_price' => $opening_season['adult_price']
            ); 
        }

        $id = ! empty( $variation_id ) ? $variation_id : $product_id; // Product or variation id

        $product = wc_get_product( $product_id ); // Product object
        $_product = wc_get_product( $id ); // Product or variation object
        
        $prices_include_tax = get_option('woocommerce_prices_include_tax');
        $tax_display_mode = get_option('woocommerce_tax_display_shop');
        
        // If product is variable, get variation price
        if ( $product->is_type( 'variable' ) && empty( $variation_id ) ) // If no variation was selected
            $error_code = 3;

        if ( $product->is_type( 'grouped' ) && empty( $children ) ) // If no quantity was selected for grouped products
            $error_code = 4;

        // Show error message
        if ( isset( $error_code ) ) {

            $error_message = $this->easy_booking_get_date_error( $error_code );
            wc_add_notice( $error_message, 'error' );

            $this->easy_booking_error_fragment( $error_message );
            $this->easy_booking_clear_booking_session();
            die();

        }

        // XXX: Commented this so there's no extra taxes in price
        /*if ( $additional_cost > 0 ) {*/

            //$rates = WC_Tax::get_base_tax_rates( $_product->get_tax_class() );

            //if ( $tax_display_mode === 'excl' && $prices_include_tax === 'yes' ) {
                //$taxes = WC_Tax::calc_exclusive_tax( $additional_cost, $rates );

                //if ( $taxes ) foreach ( $taxes as $tax ) {
                    //$additional_cost += $tax;
                //}

            //} else if ( $tax_display_mode === 'incl' && $prices_include_tax === 'no' ) {
               //$taxes = WC_Tax::calc_inclusive_tax( $additional_cost, $rates );

               //if ( $taxes ) foreach ( $taxes as $tax ) {
                    //$additional_cost -= $tax;
                //}

            //}
            
        /*}*/

        $data = array(
            'duration' => $duration,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'start' => $start,
            'end' => $end
        );

        $booking_data = array();
        $booking_price = 0;

        if ( $product->is_type( 'grouped' ) ) {
            $product_children = $product->get_children();

            foreach ( $product_children as $child_id ) {
                $child = wc_get_product( $child_id );
                $quantity = isset( $children[$child_id] ) ? $children[$child_id] : 0;

                $children_price[$child_id] = $prices_include_tax === 'yes' ? $child->get_price_including_tax() : $child->get_price_excluding_tax(); // Product price (Regular or sale)

                $child_booking_price = wc_format_decimal( $children_price[$child_id] * $duration ); // Price for x days

                $child_new_price = apply_filters( 'easy_booking_get_new_grouped_item_price', $child_booking_price, $product, $child, $duration );

                $data['new_price'] = $child_new_price;
                $booking_data[$child_id] = $data;

                $booking_price += ( $child_new_price * $quantity );
            }

            // No additionnal costs as WooCommerce Product Add-ons doens't work with grouped products.

        } else {
            $price = $prices_include_tax === 'yes' ? $_product->get_price_including_tax() : $_product->get_price_excluding_tax(); // Product price (Regular or sale) 

            $booking_price = 0; 
            // Reduce the duration and add prices
            foreach ($seasons_prices as $sp) {
                $booking_price += wc_format_decimal( ( $sp['price'] + 
                    $sp['adult_price'] * $additional_adult + 
                    $sp['children_price'] * $additional_children + 
                    $additional_cost ) * $sp['duration'] );
                $duration -= $sp['duration']; 
            }
            $children_price = get_post_meta( $variation_id, '_children_price', true );
            $adult_price    = get_post_meta( $variation_id, '_adult_price', true );

            $booking_price += wc_format_decimal( ( $price + 
                ($children_price * $additional_children) +
                ($adult_price * $additional_adult) +
                $additional_cost ) * $duration );
        }

        $new_price = apply_filters( 'easy_booking_get_new_item_price', $booking_price, $product, $_product, $duration ); // Price for x days 
        
        $data['new_price'] = $new_price;
        $booking_data[$id] = $data;

        // Update session data
        WC()->session->set_customer_session_cookie(true);
        WC()->session->set( 'booking', $booking_data );

        // Return fragments
        $this->easy_booking_new_price_fragment();

        die();

    }

    /**
     * Removes overlapping ranges
     */

    public function handle_overlapping_seasons($seasons) {

        $seasons_size = sizeof($seasons); 

        for ($i = 0; $i < $seasons_size; ++$i) {
            if (!array_key_exists($i, $seasons)) continue; 

            $cur_start = new DateTime( $seasons[$i]['start'] ); 
            $cur_end = new DateTime( $seasons[$i]['end'] ); 

            for ($j = 0; $j < $seasons_size; ++$j) {
                if (!array_key_exists($j, $seasons) || $i === $j) continue; 

                $adj_start = new DateTime( $seasons[$j]['start'] ); 
                $adj_end = new DateTime( $seasons[$j]['end'] ); 

                // The dates overlapp
                if ($adj_start <= $cur_end && $adj_end >= $cur_start) {

                    // Case current overlaps from the left
                    if ($cur_end > $adj_start && 
                        $cur_start < $adj_start && 
                        $cur_end < $adj_end) {

                        $cur_end_tmp = clone $cur_end; 
                        $seasons[$j]['start'] = $cur_end_tmp->format('Y-m-d'); 
                    // Case current overlaps from the right
                    } elseif ($cur_start < $adj_end && 
                              $cur_end > $adj_end && 
                              $cur_start > $adj_start) {

                        $cur_start_tmp = clone $cur_start; 
                        $seasons[$j]['end'] = $cur_start_tmp->format('Y-m-d'); 
                    // Case where current overlaps whole range
                    } elseif ($cur_start <= $adj_start && $cur_end >= $adj_end) {
                        // Remove from array
                        unset($seasons[$j]); 
                    }
                }
            }
        }

        return $seasons; 
    }

    /**
    *
    * Clears session if "Clear" button is clicked on the calendar
    *
    **/
    public function easy_booking_clear_booking_session() {
        $session = WC()->session->get( 'booking' );

        if ( ! empty( $session ) ) {
            WC()->session->set( 'booking', '' );
        }

        $session_set = false;

        die($session_set);
    }

    /**
    *
    * Gets error messages
    *
    * @param int $error_code
    * @return str $err - Error message
    *
    **/
    public function easy_booking_get_date_error( $error_code ) {

        switch ( $error_code ) {
            case 1:
                $err = __( 'Please choose valid dates', 'easy_booking' );
            break;
            case 2:
                $err = __( 'Please choose two dates', 'easy_booking' );
            break;
            case 3:
                $err = __( 'Please select product option', 'easy_booking' );
            break;
            case 4:
                $err = __( 'Please choose the quantity of items you wish to add to your cart&hellip;', 'woocommerce' );
            break;
            default:
                $err = '';
            break;
        }

        return $err;
    }

    /**
    *
    * Updates error messages with Ajax
    *
    * @param str $error_message
    *
    **/
    public function easy_booking_error_fragment( $error_message ) {

        header( 'Content-Type: application/json; charset=utf-8' );

        ob_start();
        wc_print_notices();
        $error_message = ob_get_clean();

            $data = array(
                'errors' => array(
                    'div.wc_ebs_errors' => '<div class="wc_ebs_errors">' . $error_message . '</div>'
                )
            );

        wp_send_json( $data );

        die();

    }

    /**
    *
    * Updates price fragment
    *
    **/
    public function easy_booking_new_price_fragment() {

        header( 'Content-Type: application/json; charset=utf-8' );

        $session = false;
        
        $product_id = isset( $_POST['product_id'] ) ? absint( $_POST['product_id'] ) : '';
        $variation_id = isset( $_POST['variation_id'] ) ? absint( $_POST['variation_id'] ) : '';

        $id = empty( $variation_id ) ? $product_id : $variation_id;
        $product = wc_get_product( $id );

        if ( ! $product )
            return;

        $booking_session = WC()->session->get( 'booking' );
        $new_price = $booking_session[$id]['new_price']; // New booking price

        $tax_display_mode = get_option('woocommerce_tax_display_shop');
        $new_price = $tax_display_mode === 'incl' ? $product->get_price_including_tax( 1, $new_price ) : $product->get_price_excluding_tax( 1, $new_price ); // Product price (Regular or sale)

        $currency = apply_filters( 'easy_booking_currency', get_woocommerce_currency_symbol() ); // Currency
        $args = apply_filters( 'easy_booking_new_price_args', array() );
        
        if ( ! empty( $booking_session[$id]['duration'] ) ) {

            $session = true;

            ob_start();
            $fragments = ob_get_clean();

                $data = array(
                    'fragments' => apply_filters( 'easy_booking_fragments', array(
                        'p.booking_price>span.price' => '<p class="booking_price"><span class="price">' . wc_price( $new_price, $args ) . '</span></p>',
                        'session' => $session
                        )
                    )
                );

            wp_send_json( $data );
            die();
        }

    }

    public function easy_booking_hide_notice() {
        $notice = isset( $_POST['notice'] ) ? $_POST['notice'] : '';

        if ( get_option( 'easy_booking_display_notice_' . $notice ) !== '1' )
            update_option( 'easy_booking_display_notice_' . $notice, '1' );

        die();
    }

    /*
     * Sends the current variation min,max people and additional children, adult prices
     */
    public function easy_booking_send_room_info() {

        $variation_id = $_POST['variation_id'] or die(); 

        $people_min          = get_post_meta( $variation_id, '_people_min', true );
        $people_max          = get_post_meta( $variation_id, '_people_max', true );
        $accept_children     = get_post_meta( $variation_id, '_accept_children', true );
        $children_price      = get_post_meta( $variation_id, '_children_price', true );
        $adult_price         = get_post_meta( $variation_id, '_adult_price', true );
        $last_available_date = get_post_meta( $variation_id, '_last_available_date', true );

        $data = array(
            'people_min' => $people_min,
            'people_max' => $people_max,
            'accept_children' => $accept_children,
            'children_price' => $children_price,
            'adult_price' => $adult_price,
            'last_available_date' => $last_available_date
        ); 


        wp_send_json( $data ); 
        die(); 
    }

    public static function wceb_reports_product_id() {
        ob_start();

        $term = (string) wc_clean( stripslashes( $_GET['term'] ) );

        $booked_products = WCEB()->easy_booking_get_booked_items_from_orders();

        $found_products = array();
        if ( $booked_products ) foreach ( $booked_products as $booked_product_id => $booked_product ) {
            $product = wc_get_product( $booked_product_id );
            $product_name = get_the_title( $booked_product_id );

            if ( stristr( $product_name, $term ) !== FALSE) {
                $found_products[ $booked_product_id ] = $product->get_formatted_name();
            }
        }

        wp_send_json( $found_products );

    }
}
return new WCEB_Ajax();

endif;
