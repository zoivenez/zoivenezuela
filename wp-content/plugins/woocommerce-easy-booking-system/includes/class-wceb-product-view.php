<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCEB_Product_View' ) ) :

class WCEB_Product_View {

	public function __construct() {
        // Get plugin options values
        $this->options = get_option('easy_booking_settings');
        
        add_filter( 'woocommerce_available_variation', array( $this, 'easy_booking_add_variation_bookable_attribute' ), 10, 3);
        add_filter( 'woocommerce_available_variation', array( $this, 'easy_booking_add_variation_booking_info' ), 10, 3);
		add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'easy_booking_before_add_to_cart_button' ), 20);
        add_filter( 'woocommerce_show_variation_price', array($this, 'easy_booking_variation_price_html'), 10, 3 );
        add_filter( 'woocommerce_get_price_html', array( $this, 'easy_booking_add_price_html' ), 10, 1 );
	}

    public function easy_booking_add_variation_bookable_attribute( $available_variations, $product, $variation ) {
        $is_bookable = get_post_meta( $variation->variation_id, '_booking_option', true );
        
        if ( empty( $is_bookable ) )
            $is_bookable = false;

        $available_variations['is_bookable'] = $is_bookable;

        return $available_variations;
    }

    public function easy_booking_add_variation_booking_info( $available_variations, $product, $variation ) {

            $people_min          = get_post_meta( $variation->variation_id, '_people_min', true );
            $people_max          = get_post_meta( $variation->variation_id, '_people_max', true );
            $accept_children     = get_post_meta( $variation->variation_id, '_accept_children', true );
            $children_price      = get_post_meta( $variation->variation_id, '_children_price', true );
            $adult_price         = get_post_meta( $variation->variation_id, '_adult_price', true );
            $last_available_date = get_post_meta( $variation->variation_id, '_last_available_date', true );
            $opening_seasons     = get_post_meta( $variation->variation_id, '_opening_seasons', true );

            $available_variations['people_min']          = !empty($people_min) ? $people_min : 1;
            $available_variations['people_max']          = !empty($people_max) ? $people_max : 1;
            $available_variations['accept_children']     = !empty($accept_children);
            $available_variations['children_price']      = !empty($children_price) ? $children_price : 0;
            $available_variations['adult_price']         = !empty($adult_price) ? $adult_price : 0;
            $available_variations['last_available_date'] = !empty($last_available_date) ? $last_available_date : false;
            $available_variations['opening_seasons']     = !empty($opening_seasons) ? $opening_seasons : array();

            return $available_variations; 
    }

    /**
    *
    * Adds a custom form to the product page.
    *
    **/
    public function easy_booking_before_add_to_cart_button() {
        global $post, $product;

        $product = wc_get_product( $product->id );
        
        // Is product bookable ?
        $is_bookable = get_post_meta( $product->id, '_booking_option', true );
        
        $info_text = $this->options['easy_booking_info_text'];
        $start_date_text = $this->options['easy_booking_start_date_text'];
        $end_date_text = $this->options['easy_booking_end_date_text'];

        $tax_display_mode = get_option( 'woocommerce_tax_display_shop' );
        $product_price = $tax_display_mode === 'incl' ? $product->get_price_including_tax() : $product->get_price_excluding_tax(); // Product price (Regular or sale)

        $adults_price    = 5000;
        $childrens_price = 5000;


        $args = apply_filters( 'easy_booking_new_price_args', array() );
        
        // Product is bookable
        if ( isset( $is_bookable ) && $is_bookable === 'yes' ) {

            // Display info text
            if ( isset( $info_text ) && ! empty ( $info_text ) ) {
                echo apply_filters( 'easy_booking_before_picker_form',
                    '<div class="woocommerce-info">' . wpautop( esc_textarea( $info_text ) ) . '</div>', $info_text );
            }

            echo '<div class="wc_ebs_errors">' . wc_print_notices() . '</div>';

            // Please do not remove inputs' attributes (classes, ids, etc.)
            echo apply_filters( 'easy_booking_picker_form',
                '<tr class="wceb_picker_wrap">
                    <td>
                        <h4 class="pull-left">' . esc_html( $start_date_text ) . '</h4>
                    </td>
                </tr>
                <tr class="wceb_picker_wrap">
                    <td>
                        <input type="hidden" id="variation_id" name="variation_id" data-product_id="' . absint ( $product->id ) . '" value="">
                        <input type="text" id="start_date" class="datepicker datepicker_start" data-value="" placeholder="' . esc_html( $start_date_text ) . '">
                    </td>
                </tr>
                <tr class="wceb_picker_wrap">
                    <td>
                        <h4 class="pull-left">' . esc_html( $end_date_text ) . '</h4>
                    </td>
                </tr>
                <tr class="wceb_picker_wrap">
                    <td>
                    <input type="text" id="end_date" class="datepicker datepicker_end" data-value="" placeholder="' . esc_html( $end_date_text ) . '">
                    </td>
                </tr>', $start_date_text, $end_date_text, $product );

            $childrens_age = CFS()->get('childrens-age') ? CFS()->get('childrens-age') : 'hasta 8 años';
            echo '<tr class="wceb_picker_wrap room_people" style="display:none">
                    <td>
                        <h4 class="pull-left">Adultos</h4>
                    </td>
                  </tr>
                  <tr class="wceb_picker_wrap room_people" style="display:none">
                    <td >
                        <span class="room_adult_select"></span>
                        <span class="pull-right"><p class="adult_price"></p></span>
                    </td>
                </tr>
                <tr class="wceb_picker_wrap room_people children_row" style="display:none">
                    <td>
                        <h4 class="pull-left">Niños ('. $childrens_age .') </h4>
                    </td>
                </tr>
                <tr class="wceb_picker_wrap room_people children_row" style="display:none">
                    <td>
                        <span class="room_children_select"></span>
                        <span class="pull-right"><p class="children_price"></p></span>
                    </td>
                </tr>
                <tr class="wceb_picker_wrap room_people additional_people_info" style="display:none">
                    <td>
                        <h4 class="pull-left">Información Tarifas </h4>
                    </td>
                </tr>
                <tr class="wceb_picker_wrap room_people additional_people_info" style="display:none">
                    <td>
                    <p style="font-size:13px">Los precios por personas adicionales varían dependiendo de la noche. Para más información haz click <a href="#" id="prices-info">aquí</a></p>
                    </td>
               </tr>'; 
            echo '</tbody>';
            echo '</table>';
            echo '</div>';
            
            //echo '<p class="booking_price">';
            //echo $product->is_type( 'variable' ) ? '' : '<span class="price">' . wc_price( $product_price, $args ) . '</span>';
            //echo '</p>';
           
        }
    }

    public function easy_booking_variation_price_html( $return, $product, $variation ) {
        return false;
    }

    /**
    *
    * Displays a custom price if the product is bookable on the product page
    *
    * @param str $content - Product price
    * @return str $content - Custom or base price
    *
    **/
    public function easy_booking_add_price_html( $content ) {
        global $post;
        
        $product_id = isset( $_POST['product_id'] ) ? absint( $_POST['product_id'] ) : $post->ID; // Product ID
        
        $is_bookable = get_post_meta( $product_id, '_booking_option', true ); // Is it bookable ?
        $calc_mode = $this->options['easy_booking_calc_mode'];

        // If bookable, return a price / day. If not, return normal price
        if ( isset( $is_bookable ) && $is_bookable === 'yes' ) {

            if ( $calc_mode === 'nights' ) {
                $price_text = __(' / night', 'easy_booking');
            } else {
                $price_text = __(' / day', 'easy_booking');
            }
            
            $display_price = $content . '<span class="wceb-price-format">' . $price_text . '</span>';

            return apply_filters( 'easy_booking_display_price', $display_price, $content );
        } else {
            return $content;
        }

    }
}

return new WCEB_Product_View();

endif;
