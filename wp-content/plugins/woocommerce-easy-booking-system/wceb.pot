msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Easy Booking\n"
"POT-Creation-Date: 2015-10-24 11:38+0100\n"
"PO-Revision-Date: 2015-10-24 11:38+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: .\n"

#: woocommerce-easy-booking.php:140
#: includes/settings/class-wceb-settings.php:60
#: includes/settings/class-wceb-settings.php:61
msgid "Settings"
msgstr ""

#: includes/class-wceb-ajax.php:205
msgid "Please choose valid dates"
msgstr ""

#: includes/class-wceb-ajax.php:208 includes/class-wceb-cart.php:53
msgid "Please choose two dates"
msgstr ""

#: includes/class-wceb-ajax.php:211
msgid "Please select product option"
msgstr ""

#: includes/class-wceb-ajax.php:214
msgid ""
"Please choose the quantity of items you wish to add to your cart&hellip;"
msgstr ""

#: includes/class-wceb-cart.php:138 includes/class-wceb-checkout.php:30
#: includes/class-wceb-checkout.php:60
#: includes/reports/class-wceb-list-reports.php:52
#: includes/reports/class-wceb-list-reports.php:81
#: includes/settings/class-wceb-settings.php:24
msgid "Start"
msgstr ""

#: includes/class-wceb-cart.php:139 includes/class-wceb-checkout.php:31
#: includes/class-wceb-checkout.php:61
#: includes/reports/class-wceb-list-reports.php:55
#: includes/reports/class-wceb-list-reports.php:82
#: includes/settings/class-wceb-settings.php:25
msgid "End"
msgstr ""

#: includes/class-wceb-product-archive-view.php:36
msgid "Select dates"
msgstr ""

#: includes/class-wceb-product-view.php:110
msgid " / night"
msgstr ""

#: includes/class-wceb-product-view.php:112
msgid " / day"
msgstr ""

#: includes/admin/class-wceb-product-settings.php:49
#: includes/admin/class-wceb-product-settings.php:71
msgid "Bookable"
msgstr ""

#: includes/admin/class-wceb-product-settings.php:50
msgid "Bookable products can be rent or booked on a daily schedule"
msgstr ""

#: includes/admin/class-wceb-product-settings.php:99
#: includes/reports/class-wceb-reports.php:48
msgid "Bookings"
msgstr ""

#: includes/admin/class-wceb-product-settings.php:158
#: includes/admin/class-wceb-product-settings.php:202
#: includes/admin/class-wceb-product-settings.php:244
msgid "Minimum booking duration must be inferior to maximum booking duration"
msgstr ""

#: includes/admin/views/wceb-html-notice-addons.php:12
msgid "Want more features for WooCommerce Easy Booking?"
msgstr ""

#: includes/admin/views/wceb-html-notice-addons.php:13
msgid " Check the add-ons!"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:16
msgid "Manage bookings?"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:17
msgid "Check this box to manage bookings at product level."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:27
#: includes/admin/views/wceb-html-variation-booking-options.php:13
#: includes/settings/class-wceb-settings.php:171
msgid "Minimum booking duration"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:29
#: includes/admin/views/wceb-html-product-booking-options.php:41
msgid ""
"Leave zero to set no duration limit. Leave empty to use the global settings."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:39
#: includes/admin/views/wceb-html-variation-booking-options.php:20
#: includes/settings/class-wceb-settings.php:179
msgid "Maximum booking duration"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:51
#: includes/admin/views/wceb-html-variation-booking-options.php:27
#: includes/settings/class-wceb-settings.php:187
msgid "First available date"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:53
msgid ""
"First available date, relative to the current day. I.e. : today + 5 days. "
"Leave zero for the current day. Leave empty to use the global settings."
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:14
#: includes/admin/views/wceb-html-variation-booking-options.php:21
msgid ""
"Enter zero to set no duration limit or leave blank to use the parent "
"product's booking options or the global settings."
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:28
msgid ""
"First available date, relative to today. I.e. : today + 5 days. Enter 0 for "
"today or leave blank to use the parent product's booking options or the "
"global settings."
msgstr ""

#: includes/reports/class-wceb-list-reports.php:24
msgid "Report"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:25
#: includes/reports/class-wceb-reports.php:18
msgid "Reports"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:49
msgid "Search for a product&hellip;"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:79
msgid "Order"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:80
msgid "Product"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:83
msgid "Quantity booked"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:125
msgid "Billing:"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:129
msgid "Tel:"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:154
msgid "Guest"
msgstr ""

#: includes/settings/class-wceb-settings.php:69
#: includes/settings/class-wceb-settings.php:70
msgid "Add-ons"
msgstr ""

#: includes/settings/class-wceb-settings.php:148
msgid "General settings"
msgstr ""

#: includes/settings/class-wceb-settings.php:155
msgid "Calculation mode"
msgstr ""

#: includes/settings/class-wceb-settings.php:163
msgid "Make all products bookable?"
msgstr ""

#: includes/settings/class-wceb-settings.php:195
msgid "Booking limit"
msgstr ""

#: includes/settings/class-wceb-settings.php:203
msgid "Text settings"
msgstr ""

#: includes/settings/class-wceb-settings.php:210
msgid "Information text"
msgstr ""

#: includes/settings/class-wceb-settings.php:218
msgid "First date title"
msgstr ""

#: includes/settings/class-wceb-settings.php:226
msgid "Second date title"
msgstr ""

#: includes/settings/class-wceb-settings.php:234
msgid "Appearance"
msgstr ""

#: includes/settings/class-wceb-settings.php:241
msgid "Calendar theme"
msgstr ""

#: includes/settings/class-wceb-settings.php:249
msgid "Background color"
msgstr ""

#: includes/settings/class-wceb-settings.php:257
msgid "Main color"
msgstr ""

#: includes/settings/class-wceb-settings.php:265
msgid "Text color"
msgstr ""

#: includes/settings/class-wceb-settings.php:279
msgid "WooCommerce Easy Booking settings"
msgstr ""

#: includes/settings/class-wceb-settings.php:306
msgid "Days"
msgstr ""

#: includes/settings/class-wceb-settings.php:307
msgid "Nights"
msgstr ""

#: includes/settings/class-wceb-settings.php:309
msgid ""
"Choose whether to calculate the final price depending on number of days or "
"number of nights (i.e. 5 days = 4 nights)."
msgstr ""

#: includes/settings/class-wceb-settings.php:315
msgid ""
"Check to make all your products bookable. Any new or modified product will "
"be automatically bookable."
msgstr ""

#: includes/settings/class-wceb-settings.php:321
msgid ""
"Set a minimum booking duration for all your products. You can individually "
"change it on your product settings. Leave 0 or empty to set no duration "
"limit."
msgstr ""

#: includes/settings/class-wceb-settings.php:327
msgid ""
"Set a maximum booking duration for all your products. You can individually "
"change it on your product settings. Leave 0 or empty to set no duration "
"limit."
msgstr ""

#: includes/settings/class-wceb-settings.php:333
msgid ""
"Set the first available date for all your products, relative to the current "
"day. You can individually change it on your product settings. Leave 0 or "
"empty to keep the current day."
msgstr ""

#: includes/settings/class-wceb-settings.php:341
msgid ""
"Set the limit to allow bookings (December 31 of year x). Min: current year."
msgstr ""

#: includes/settings/class-wceb-settings.php:345
msgid ""
"Make this plugin yours by choosing the different texts you want to display !"
msgstr ""

#: includes/settings/class-wceb-settings.php:350
msgid ""
"Displays an information text before date inputs. Leave empty if you don't "
"want the information text."
msgstr ""

#: includes/settings/class-wceb-settings.php:355
msgid "Text displayed before the first date"
msgstr ""

#: includes/settings/class-wceb-settings.php:360
msgid "Text displayed before the second date"
msgstr ""

#: includes/settings/class-wceb-settings.php:364
msgid "Customize the calendar so it looks great with your theme !"
msgstr ""

#: includes/settings/class-wceb-settings.php:364
msgid "Prefer a light background and a dark text color, for better rendering."
msgstr ""

#: includes/settings/class-wceb-settings.php:371
msgid "Default"
msgstr ""

#: includes/settings/class-wceb-settings.php:372
msgid "Classic"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:8
msgid "WooCommerce Easy Booking Add-ons"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:14
msgid ""
"This add-on will allow you to manage stocks and availabilties when renting "
"or booking your products."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:17
msgid ""
"Keeps track of every booked date and quantity booked for each product (keeps "
"only future dates)."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:18
msgid "Updates availabilities when an order is made, modified or cancelled."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:19
msgid "Prevents users to order unavailable quantity or out-of-stock products."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:20
msgid ""
"Allows you to easily check which dates and quantities are available in the "
"administration panel."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:26
msgid ""
"This add-on will allow you to add discounts to your products depending on "
"the duration booked by your clients."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:29
msgid "Choose custom discount amounts."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:30
#, php-format
msgid ""
"Choose between \"Product % discount\", \"Product discount\", \"Total "
"% discount\" or \"Total discount\"."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:31
msgid "Define booked duration to add your discount (E.g : from 10 to 20 days)."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:32
msgid "Add as many discounts as you want per product or variation."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:39
msgid ""
"This add-on allows you to disable days or date when renting products with "
"WooCommerce Easy Booking."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:41
msgid "You will be able to disable :"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:43
msgid "Weekdays"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:44
msgid "Specific dates (for example: public holidays)"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:45
msgid "Dateranges"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:67
msgid "Learn more"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:71
msgid "Installed"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:74
msgid "Documentation"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:77
msgid "Support"
msgstr ""
