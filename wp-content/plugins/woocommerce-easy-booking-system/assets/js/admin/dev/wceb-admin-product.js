(function($) {
	$(document).ready(function() {

		$('input#_booking_option').change( function() {

			if ( $(this).is(':checked') ) {
				$('.show_if_bookable').show();
			} else {
				$('.show_if_bookable').hide();
				$('input.variable_is_bookable').attr('checked', false).change();
			}

			if ( $('.WCEB_tab').is('.active') ) {
				$( 'ul.wc-tabs li:visible' ).eq(0).find( 'a' ).click();
			}

		}).change();

		$('input#_manage_bookings').change( function() {
			if ( $(this).is(':checked') ) {
				$('.show_if_manage_bookings').show();
			} else {
				$('.show_if_manage_bookings').hide();
			}
		}).change();

		$( '#variable_product_options' ).on( 'change', 'input.variable_is_bookable', function () {
			$( this ).closest( '.woocommerce_variation' ).find( '.show_if_variation_bookable' ).hide();

			if ( $( this ).is( ':checked' ) ) {
				$( this ).closest( '.woocommerce_variation' ).find( '.show_if_variation_bookable' ).show();
			}
		}).change();

		$( '#woocommerce-product-data' ).on( 'woocommerce_variations_loaded', function() {
			$(this).find('input.variable_is_bookable').change();
		});

        var monthNamesArray  =  [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", 
                              "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ]; 

        var dayNamesMinArray =  [ "Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab" ]; 
        // Datepicker for last available date
        $(document).on('focusin', '.input_date', function(){
            $(this).datepicker({
                monthNames : monthNamesArray,
                dayNamesMin: dayNamesMinArray,
                dateFormat: 'yy-mm-dd',
                showOtherMonths: true,
                selectOtherMonths: true,
                firstDay : 1,
                minDate : new Date(),
                nextText: '',
                prevText: '',
            }); 
        })

        $(document).on('click', '.clear_av_date', function(event){
            event.preventDefault(); 
            $inputDate = $( this ).parent('label').parent('p').find('input.input_date'); 
            $inputDate.val(' '); 
            //$('#last_available_date').val(' '); 
        });

	});

  $(document).on('click', '.add-opening-season', function(event) {
    event.preventDefault(); 

    $openingSeasonContainer = $(this).closest('.form-row').find('.opening-seasons').first(); 

    $openingSeasonHTML = '<fieldset class="form-row form-row-first" style="width:100%">';
    $openingSeasonHTML += $openingSeasonContainer.find('fieldset').first().html(); 
    $openingSeasonHTML += '</fieldset>';

    $openingSeasonObject = $($openingSeasonHTML).hide(); 
    $openingSeasonObject.find('input[type="text"]').each(function(index, value) {
      $(this).val(''); 
      // Removing hasDatepicker to render datepickers 
      // and id to avoid duplicated dates
      if ( $(this).hasClass('hasDatepicker') ) {
        $(this).removeClass('hasDatepicker'); 
        $(this).attr('id', ''); 
      }
    });
    $openingSeasonContainer.append($openingSeasonObject); 
    $openingSeasonObject.show(); 
  }); 

  $(document).on('click', '.remove-opening-season', function(event) {
    remove = confirm('Are you sure you want to remove this opening season?');
    if (remove) { 
      openingSeasonsLeft = $(this).closest('.opening-seasons').find('fieldset').length; 
      if (openingSeasonsLeft === 1) {
        alert("Yo can't remove all Opening Seasons!");
      } else { 
        $(this).closest('fieldset').remove(); 
      }
    }
  }); 

})(jQuery);
