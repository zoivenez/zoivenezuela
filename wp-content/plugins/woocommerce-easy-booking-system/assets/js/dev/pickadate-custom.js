(function($) {
  $(document).ready(function() {

    $input = $('.datepicker').pickadate({
      formatSubmit: 'yyyy-mm-dd'
    });

    // $input = $('.datepicker').pickadate();
    $inputStart = $('.datepicker_start').pickadate();
    $inputEnd = $('.datepicker_end').pickadate();

    // Use the picker object directly.
    pickerStart = $inputStart.pickadate('picker');
    pickerEnd = $inputEnd.pickadate('picker');

    var pickerStartItem = pickerStart.component.item,
      pickerEndItem = pickerEnd.component.item;

    var productType = ajax_object.product_type,
      calc_mode = ajax_object.calc_mode, // Days or Nights
      bookingMin, bookingMax, firstDate,
    maxYear = parseInt( ajax_object.max_year )
    maxOption = new Date( maxYear, 11, 31 ),
      session = false;

    var $pickerWrap = $('.wceb_picker_wrap');

    var $embeddedCalendar = $('.experience-calendar').parent('td').parent('tr');


    var peopleMin = 1, peopleMax = 1, 
      childrenPrice = 0, adultPrice = 0, lastAvailableDate = '', dateObj = null; 
    var currentVariation = null; 



    $(document).on('click', '#prices-info', function(event){
      event.preventDefault(); 

      var openingSeasons = currentVariation.opening_seasons;
      var acceptChildren = currentVariation.accept_children;
      var minPeople      = currentVariation.people_min;

      var infoTable = '<table class="table table-striped" style="font-family:open_sansregular">';
      infoTable += '<thead><tr>';
      infoTable += '<th>Temporada</th>';
      infoTable += '<th>Habitación <br>(' + minPeople +' persona' + (minPeople > 1 ? 's' : '') +')</th>';
      infoTable += '<th>+Adultos</th>';
      infoTable += '<th>+Niños</th>';
      infoTable += '</tr></thead>';
      infoTable += '<tbody>';

      infoTable += '<tr><td> * </td>';
      infoTable += '<td>'+ currentVariation.display_price +'</td>';
      infoTable += '<td>'+ currentVariation.adult_price +'</td>';
      infoTable += '<td>'+ ( acceptChildren ? currentVariation.children_price : 'N/A' ) +'</td></tr>';
      for(i = 0; i < openingSeasons.length; ++i) {

        var start = new Date(openingSeasons[i].start);
        var end   = new Date(openingSeasons[i].end);

        var startStr = start.getDate() + '/' + ( start.getMonth() + 1 ) + '/' + start.getFullYear();
        var endStr   = end.getDate() + '/' + ( end.getMonth() + 1 ) + '/' + end.getFullYear();

        infoTable += '<tr><td>' + startStr + ' - ' + endStr +'</td>';

        var childrenPrice = openingSeasons[i].children_price;
        var adultPrice    = openingSeasons[i].adult_price;
        var price         = openingSeasons[i].price;

        infoTable += '<td>'+ price +'</td>';
        infoTable += '<td>'+ adultPrice +'</td>';
        infoTable += '<td>'+ ( acceptChildren ? childrenPrice : 'N/A' ) +'</td></tr>';
      }


      infoTable += '</tbody>';
      infoTable += '</table>';

      $('#info-modal').remove(); 

      var modal = '<div id="info-modal" class="modal fade" style="font-family:open_sansregular">';
      modal += '<div class="modal-dialog">';
      modal += '<div class="modal-content">';
      modal += '<div class="modal-header">';
      modal += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
      modal += '<h4>Tarifas</h4>'
      modal += '</div>';
      modal += '<div class="modal-body">';
      modal += infoTable; 
      modal += '<div class="modal-footer">';
      modal += '* Costo fuera de temporada'
      modal += '</div>';
      modal += '</div>';
      modal += '</div>';
      modal += '</div>';
      modal += '</div>';

      var $modalObj = $(modal).hide(); 
      $('body').append($modalObj); 
      $modalObj.modal('show'); 
    });

    //$(document).on('click', '.close-info-modal', function(event) {
    //})

    $('body').on('change', '.addon-select', function(event) {
      var currentValue = $(this).val(); 

      var remainingPeople = peopleMax - currentValue; 

      $('.addon-select').not(this).each(function(index, value){
        $(this).find('option').each(function(index,value){
          people = parseInt($(this).attr('value'));
          if (remainingPeople < people) {
            $(this).hide(); 
          } else {
            $(this).show(); 
          }
        }); 
      }); 

      startDate        = pickerStart.get('select', 'yyyy-mm-dd');
      endDate          = pickerEnd.get('select', 'yyyy-mm-dd');
      startSet         = pickerStart.get('select'),
      startDateDisplay = pickerStart.get('select', 'dd mmmm yyyy');
      endSet           = pickerEnd.get('select');
      endDateDisplay   = pickerEnd.get('select', 'dd mmmm yyyy');

      if ( typeof startSet != 'undefined' && typeof endSet != 'undefined' 
          && startSet != null && endSet != null ) { 
            ebs_set_price(startDate, endDate, startDateDisplay, endDateDisplay);
          }
    }); 

    var additionalPeople = function(minPeople, maxPeople, childrenPrice, adultPrice, acceptChildren) {

      // No extra people: Hide extra people price info
      if (minPeople >= maxPeople) {
        $('.additional_people_info').hide(); 
      }

      var childrenPriceStr = parseFloat(childrenPrice);
      var adultPriceStr    = parseFloat(adultPrice);

      if ( isNaN(adultPriceStr) || (acceptChildren === '1' && isNaN(childrenPriceStr)) ) {
        $('.room_people').hide(); 
        return; 
      }

      var childrenSelect = '<select name="additional-children" class="additional-children addon-select tarful_select" style="width:4em">';
      var adultSelect    = '<select name="additional-adult" class="additional-adult addon-select tarful_select" style="width:4em">';

      for (i = 0; i <= maxPeople; ++i) {

        var childrenCurPrice = childrenPriceStr * i;
        var adultCurPrice    = adultPriceStr * i;
        var value            = i.toString();

        childrenSelect += '<option data-price="' + childrenCurPrice.toString() + '" value="' + value + '">';
        childrenSelect += value + '</option>';

        adultSelect += '<option data-price="' + adultCurPrice.toString() + '" value="' + value + '">';
        adultSelect += value + '</option>';
      }

      childrenSelect += '</select>';
      adultSelect += '</select>';

      if (acceptChildren) {
        $('.room_children_select').html(childrenSelect); 
        //$('.children_price').html(''); 
      } else {
        $('.children_row').hide(); 
      }

      $('.room_adult_select').html(adultSelect); 
      $('.additional-adult').val(minPeople); 
      $('.additional-adult').trigger('change'); 
      //$('.adult_price').html('Precios varían por noche');  
    }

    if ( productType === 'variable' ) {

      $pickerWrap.hide();

      $('.price').find('.wceb-price-format').hide();

      var variation_id = -1; 

      $('body').on('found_variation', '.variations_form', function(e, variation) {

        currentVariation = variation; 

        if ( ! variation.is_purchasable || ! variation.is_in_stock || ! variation.variation_is_visible || ! variation.is_bookable ) {
          $pickerWrap.hide();
          $embeddedCalendar.show(); 
          $('.price').find('.wceb-price-format').hide();
        } else {
          if (variation_id != variation.variation_id) {

            $embeddedCalendar.hide(); 
            variation_id = variation.variation_id; 


            $pickerWrap.slideDown( 200 );

            var variationPrice = formatPrice( variation.display_price );
            $('.booking_price').html('<span class="price"><span class="amount">' + variationPrice + '</span></span>');

            variationId = variation.variation_id;
            firstDate = ajax_object.first_date[variationId];
            bookingMin = ajax_object.min[variationId];


            // Variation booking info 
            peopleMin         = variation.people_min;
            peopleMax         = variation.people_max;
            acceptChildren    = variation.accept_children;
            childrenPrice     = variation.children_price;
            adultPrice        = variation.adult_price;
            lastAvailableDate = variation.last_available_date;

            var lastAvailableDateObj = null; 
            // Set last available date if not empty
            if (lastAvailableDate && lastAvailableDate != '0') {
              dateObj = new Date(lastAvailableDate); 
              dateObj.setDate(dateObj.getDate() + 1); 

              if (Object.prototype.toString.call(dateObj) === "[object Date]" && 
                  !isNaN(dateObj.getTime())) {

                maxOption = new Date(dateObj); 
                lastAvailableDateObj = new Date(dateObj);
              }
            }

            additionalPeople(peopleMin, peopleMax, childrenPrice, adultPrice, acceptChildren); 

            initPickers( firstDate, bookingMin, lastAvailableDateObj );

            $('body').trigger( 'pickers_init', [variation] );

            pickerStart.render();
            pickerEnd.render();

            $('body').trigger( 'after_pickers_init', [variation] );

            $('.price').find('.wceb-price-format').show();
          }
        }
      });

      $('body').on('reset_image', '.variations_form', function(e, variation) {
        $pickerWrap.hide();
      });

    } else if ( productType === 'grouped' ) {

      $pickerWrap.hide();

      var firstDate = ajax_object.first_date,
        bookingMin = ajax_object.min,
        bookingMax = ajax_object.max;

      initPickers( firstDate, bookingMin );

      $('.cart').on( 'change', '.quantity input.qty', function() {

        var ids = {},
          quantities = [];

        totalGroupedPrice = 0;

        $.each( $('.quantity input.qty'), function() {
          var qty = $(this).val(),
            name = $(this).attr('name'),
            id = name.split("\[").pop().split("\]").shift(),
            price = ajax_object.product_price[id];

          totalGroupedPrice += parseFloat( price * qty );

          if ( qty > 0 )
            ids[id] = qty;

          quantities.push( qty );
        });

        // Get highest quantity selected
        max_qty = Math.max.apply( Math, quantities );

        if ( max_qty > 0 ) {
          $pickerWrap.slideDown( 200 );
        } else {
          $pickerWrap.hide(); // Hide date inputs if no quantity is selected
        }

        formatted_price = formatPrice( totalGroupedPrice );
        $('p.booking_price .price .amount').html(formatted_price);

        $('body').trigger( 'pickers_init', [ids] );

        pickerStart.render();
        pickerEnd.render();

        $('body').trigger( 'after_pickers_init', [ids] );

      });

    } else {
      var firstDate = ajax_object.first_date,
        bookingMin = ajax_object.min,
        bookingMax = ajax_object.max;

      initPickers( firstDate, bookingMin );

      $('body').trigger( 'pickers_init' );

      pickerStart.render();
      pickerEnd.render();

      $('body').trigger( 'after_pickers_init' );
    }

    function initPickers( firstDate, bookingMin, lastAvailableDate ) {
      var firstDay = ebs_get_first_available_date( firstDate );

      if ( bookingMin > 0 ) { // If minimum booking duration is set
        endFirstDay = parseInt( firstDay ) + parseInt( bookingMin ); // Set endpicker start date to first date + minimum duration

        if ( calc_mode === 'days' )
          endFirstDay -= 1; // If in "Days" mode, remove one day

      } else { // If no minimum booking duration is set

        if ( calc_mode === 'nights' ) {
          endFirstDay = parseInt( firstDay ) + 1; // If in "Nights" mode, add one day
        } else {
          endFirstDay = firstDay;
        }

      }

      if ( firstDay <= 0 )
        firstDay = false;

      if ( endFirstDay <= 0 )
        endFirstDay = false;

      var minObject = createDateObject( false, firstDay ),
        endMinObject = createDateObject( false, endFirstDay ),
        highlight = createDateObject(),
        view = createDateObject( new Date( highlight.year, highlight.month, 1 ) );

      var max = createDateObject( new Date( maxYear, 11, 31 ) );
      // Valid last available date and less than max 
      if (typeof lastAvailableDate != 'undefined'  && 
          lastAvailableDate != null) {

        lastAvailableDate = createDateObject(lastAvailableDate); 
        if (lastAvailableDate.obj < max.obj) { 
          max = lastAvailableDate;
        } 	
      }

      var maxWithMinBooking = new Date(max.obj);
      maxWithMinBooking.setDate(max.obj.getDate() - (parseInt(bookingMin) === 0 ? 1 : parseInt(bookingMin))); 

      pickerStartItem.clear = null;
      pickerStartItem.select = undefined;

      pickerStartItem.min = minObject;
      pickerStartItem.max = createDateObject(maxWithMinBooking);
      pickerStartItem.highlight = highlight;
      pickerStartItem.view = view;
      pickerStartItem.disable = [];

      pickerStart.$node.val('');

      pickerEndItem.clear = null;
      pickerEndItem.select = undefined;
      pickerEndItem.min = endMinObject;
      pickerEndItem.max = max;
      pickerEndItem.highlight = highlight;
      pickerEndItem.view = view;
      pickerEndItem.disable = [];

      pickerEnd.$node.val('');

      return false;

    }

    function createDateObject( date, add ) {
      if ( ! date ) var date = new Date();
      if ( add ) date.setDate( date.getDate() + add );

      if ( date === 'infinity' ) {
        var dateObject = {
          date: Infinity,
          day: Infinity,
          month: Infinity,
          obj: Infinity,
          pick: Infinity,
          year: Infinity
        }

        return dateObject;
      }

      date.setHours(0,0,0,0);

      var dateObject = {
        date: date.getDate(),
        day: date.getDay(),
        month: date.getMonth(),
        obj: date,
        pick: date.getTime(),
        year: date.getFullYear()
      }

      return dateObject;
    }

    function ebs_get_first_available_date( firstDate ) {
      var firstDay = +firstDate;

      return firstDay;
    }

    ebs_clear_booking_session = function() {

      if ( session ) {

        var data = {
          action: 'clear_booking_session'
        };

        $.post(ajax_object.ajax_url, data, function( response ) {
          session = response;
        });
      }

    }

    ebs_get_min_and_max = function(disabledDate, operator) {

      var selectedMinDate = new Date( disabledDate.year, disabledDate.month, disabledDate.date ), // Selected date
        selectedMaxDate = new Date( disabledDate.year, disabledDate.month, disabledDate.date ); // Selected date

      if ( productType === 'variable' ) {

        var variationId = $('.variations_form').find('input.variation_id').val(),
          minimumDuration = parseInt( ajax_object.min[variationId] ),
          maximumDuration = parseInt( ajax_object.max[variationId] );

      } else {

        var minimumDuration = parseInt( ajax_object.min ),
          maximumDuration = parseInt( ajax_object.max );

      }

      var calc_mode = ajax_object.calc_mode; // "Days" or "Nights" mode

      if ( operator === 'plus' ) { // After setting start date

        if ( minimumDuration == 0 ) { // If no minimum duration was set

          if ( calc_mode === 'nights' )
            minimumDuration += 1; // Add one day for the "Nights" mode, as you book the night

        } else { // If a minimum duration is set

          if ( calc_mode === 'days' )
            minimumDuration -= 1; // Remove one day for the "Days" mode, as you can still book the same day

        }

        if ( maximumDuration == 0 )
          maximumDuration -= 1;

        if ( calc_mode === 'days' )
          maximumDuration -= 1;

      } else { // End date calendar

        if ( maximumDuration == 0 ) {

          selectedMinDate = new Date();

          if ( minimumDuration != 0 ) {
            maximumDuration = minimumDuration;
            maximumDuration -= 1;
          }

          minimumDuration = 1;

          if ( calc_mode === 'nights' ) {
            minimumDuration -= 1;
            maximumDuration += 1;
          }

        } else {

          temp = minimumDuration;
          minimumDuration = maximumDuration;
          maximumDuration = temp;

          if ( temp != 0 )
            maximumDuration -= 1;

          if ( calc_mode === 'days' ) {
            minimumDuration -= 1;
          } else {
            maximumDuration += 1;
          }

        }

      }

      if ( operator === 'plus' ) {
        selectedMinDate.setDate( selectedMinDate.getDate() + minimumDuration );
        selectedMaxDate.setDate( selectedMaxDate.getDate() + maximumDuration );
      } else {
        selectedMinDate.setDate( selectedMinDate.getDate() - minimumDuration );
        selectedMaxDate.setDate( selectedMaxDate.getDate() - maximumDuration );
      }

      // Check if minimum date is not superior
      var currentDay = new Date();
      var numberOfDaysToAdd = ebs_get_first_available_date( firstDate );

      if ( numberOfDaysToAdd != 0 )
        currentDay.setDate( currentDay.getDate() + numberOfDaysToAdd );

      if ( currentDay > selectedMinDate )
        selectedMinDate = currentDay;

      if ( maximumDuration < 0 )
        selectedMaxDate = false;

      if ( ! selectedMaxDate || maxOption < selectedMaxDate ) {
        selectedMaxDate = maxOption;
      }

      var minAndMax = {};
      minAndMax['min'] = selectedMinDate;
      minAndMax['max'] = selectedMaxDate;

      return minAndMax;

    }

    // WooCommerce Product Add-ons compatibility
    getAdditionalCosts = function() {

      var total = 0;

      $('form.cart').find( '.addon' ).each( function() {
        var addon_cost = 0;

        if ( $(this).is('.addon-custom-price') ) {
          addon_cost = $(this).val();
        } else if ( $(this).is('.addon-input_multiplier') ) {
          if( isNaN( $(this).val() ) || $(this).val() == "" ) { // Number inputs return blank when invalid
            $(this).val('');
            $(this).closest('p').find('.addon-alert').show();
          } else {
            if( $(this).val() != "" ){
              $(this).val( Math.ceil( $(this).val() ) );
            }
            $(this).closest('p').find('.addon-alert').hide();
          }
          addon_cost = $(this).data('price') * $(this).val();
        } else if ( $(this).is('.addon-checkbox, .addon-radio') ) {
          if ( $(this).is(':checked') )
            addon_cost = $(this).data('price');
        } else if ( $(this).is('.addon-select') ) {
          if ( $(this).val() )
            addon_cost = $(this).find('option:selected').data('price');
        } else {
          if ( $(this).val() )
            addon_cost = $(this).data('price');
        }

        if ( ! addon_cost ) {
          addon_cost = 0;
        } 

        total += addon_cost;
      });

      return total;

    }

    ebs_set_price = function(startDate, endDate, startDateDisplay, endDateDisplay) {
      children = {};

      if ( productType === 'variable' ) {
        product_id = $('input[name=product_id]').val();
      } else if ( productType === 'grouped' ) {
        product_id = $('input[name=add-to-cart]').val(),
          productChildren = ajax_object.children;

        $.each(productChildren, function( index, child ) {
          quantity = $('input[name="quantity[' + child + ']"]').val();
          if ( quantity > 0 ) {
            children[child] = quantity;
          }
        });

      } else {
        product_id = $('input[name=add-to-cart]').val();
      }

      variation_id = $('.variations_form').find('input.variation_id').val();

      // WooCommerce Product Add-ons compatibility
      var additionalCost = getAdditionalCosts();


      // Get extra people info
      var extraChildren = $('.additional-children').first().val(); 
      var extraAdult = $('.additional-adult').first().val(); 

      var data = {
        action: 'add_new_price',
        product_id: product_id,
        variation_id: variation_id,
        children: children,
        start: startDateDisplay,
        end: endDateDisplay,
        start_format: startDate,
        end_format: endDate,
        additional_cost: additionalCost,
        additional_children: extraChildren,
        additional_adult: extraAdult
      };

      var this_page = window.location.toString();

      $('form.cart').fadeTo('400', '0.6').block({message: null, overlayCSS: {background: 'transparent', backgroundSize: '16px 16px', opacity: 0.6 } } );

      $.post(ajax_object.ajax_url, data, function( response ) {

        $('.woocommerce-error, .woocommerce-message').remove();
        fragments = response.fragments;
        errors = response.errors;

        if ( errors ) {
          $.each(errors, function(key, value) {
            $(key).replaceWith(value);
          });
          initPickers( firstDate, bookingMin );
          pickerStart.render();
          pickerEnd.render();
        }

        if ( fragments ) {
          $.each(fragments, function(key, value) {
            $(key).replaceWith(value);
          });

          session = fragments.session;
        }

        $('form.cart').trigger( 'update_price', [ data, response ] );

        // Unblock
        $('form.cart').stop(true).css('opacity', '1').unblock();

      });
    }

    ebs_get_closest_disabled = function( time, picker, direction ) {
      var selectedDate = new Date( time ), // Get Selected date
        selectedDay = selectedDate.getDay(); // Get selected day (1, 2, 3, 4, 5, 6, 7)

      var disabled = picker.get('disable'),
        disabledTime = [];

      $.each( disabled, function( index, date ) {

        if ( date instanceof Array ) {

          var getDate = new Date( date[0], date[1], date[2] );
          disabledTime.push( getDate.getTime() );

        } else if ( date >= 1 && date <= 7 ) {

          if ( direction === 'superior' ) {

            var interval = Math.abs( selectedDay - date );

            if ( interval === 0 )
              interval = 7;

            interval = 7 - interval;

            var nextDisabledDay = selectedDate.setDate( selectedDate.getDate() + interval );

            disabledTime.push( nextDisabledDay );
            selectedDate = new Date( time ); // Reset selected date

          } else if ( direction === 'inferior' ) {

            var interval = Math.abs( selectedDay - date );

            previousDisabledDay = selectedDate.setDate( selectedDate.getDate() - interval );
            disabledTime.push( previousDisabledDay );

          }

        } else {
          disabledTime.push( date.getTime() );
        }

      });

      disabledTime.sort();
      var closestDisabled = closest( disabledTime, time, direction );

      return closestDisabled;
    }

    closest = function(arr, closestTo, direction) {

      minClosest = false;

      for ( var i = 0; i < arr.length; i++ ) { // Loop the array
        if ( direction === 'superior' ) {
          if ( arr[i] > closestTo ) { // Check if it's higher than your number
            minClosest = arr[i];
            break;
          } else {
            minClosest = false;
          }
        } else if ( direction === 'inferior' ) {
          if ( arr[i] < closestTo ) { // Check if it's lower than your number
            minClosest = arr[i];
          }
        }
      }

      return minClosest;

    }

    pickerStart.on({
      render: function() {
        pickerStart.$root.find('.picker__header').prepend('<div class="picker__title">' + ajax_object.start_text + '</div>');
      },
      set: function(startTime) {
        var pickerEndItem = pickerEnd.component.item,
          startSet = pickerStart.get('select'),
          endSet = pickerEnd.get('select');

        // Clear Start Picker
        if ( typeof startTime.clear != 'undefined' && startTime.clear == null ) {

          var minAfterClear = ebs_get_first_available_date( firstDate ) + parseInt( bookingMin );

          if ( calc_mode === 'nights' && bookingMin == 0 && minAfterClear != 0 )
            minAfterClear += 1;

          if ( minAfterClear == 0 ) {

            if ( calc_mode === 'nights') {
              minAfterClear = 1;
            }

          }

          if ( calc_mode === 'days' && bookingMin > 0 && minAfterClear != 0 )
            minAfterClear -= 1;

          var min = createDateObject( false, minAfterClear ),
            max = createDateObject( maxOption ),
            view = createDateObject( new Date(min.year,min.month,01) );

          pickerEndItem.disable = [];
          pickerEndItem.min = min;

          pickerEndItem.max = max;

          if ( endSet == null ) {
            pickerEndItem.highlight = min;
            pickerEndItem.view = view;
          }

          $('body').trigger('clear_start_picker', pickerEndItem );

          pickerEnd.render();

          ebs_clear_booking_session();
        }

        // Set Start Picker
        if ( startTime.select && typeof startTime.select != 'undefined' ) {

          startPick = startTime.select;
          startDate = pickerStart.get('select', 'yyyy-mm-dd');
          disabledDate = pickerStart.get('select');
          startDateDisplay = pickerStart.get('select', 'dd mmmm yyyy');

          // Hotfix for pickadate.js bug when selecting dates with keyboard, waiting for v4.0 fix
          if ( typeof startPick === 'object' ) {
            startPick = startTime.select.pick;
          }

          var minAndMax = ebs_get_min_and_max( disabledDate, 'plus' ),
            min = minAndMax.min,
            max = minAndMax.max;

          if ( ! max ) max = maxOption;

          // Check if there are disabled dates to before setting max
          var closestDisabled = ebs_get_closest_disabled( startPick, pickerEnd, 'superior' );

          if ( typeof startPick != 'undefined' ) {

            if ( closestDisabled ) {

              var date = new Date( closestDisabled ); // Convert to date

              if ( closestDisabled < max )
                var max = date;
            }
          }

          var min = createDateObject( min ),
            max = createDateObject( max ),
            view = createDateObject( new Date(min.year,min.month,01) );


          pickerEndItem.min = min;
          pickerEndItem.max = max;

          if ( endSet == null ) {
            pickerEndItem.highlight = min;
            pickerEndItem.view = view;
          }

          $('body').trigger('set_start_picker', [pickerEndItem, startPick] );

          pickerEnd.render();

          if ( startSet != null && endSet != null )
            ebs_set_price(startDate, endDate, endSet, endDateDisplay);

        } else {
          return false;
        }

      },
      close: function() {
        $(document.activeElement).blur();

        var startSet = pickerStart.get('select'),
          endSet = pickerEnd.get('select');

        if ( startSet != null && endSet == null )
          setTimeout(function() { pickerEnd.open(); }, 250);
      }
    });

    pickerEnd.on({
      render: function() {
        pickerEnd.$root.find('.picker__header').prepend('<div class="picker__title">' + ajax_object.end_text + '</div>');
      },
      set: function(endTime) {
        var startSet = pickerStart.get('select'),
          endSet = pickerEnd.get('select');

        // Clear End Picker
        if ( typeof endTime.clear != 'undefined' && endTime.clear == null ) {

          var minAfterClear = ebs_get_first_available_date( firstDate );

          var min = createDateObject( false, minAfterClear ),
            max = createDateObject( maxOption );
          highlight = createDateObject(),
            view = createDateObject( new Date(highlight.year,highlight.month,01) );

          var maxWithMinBooking = new Date(max.obj);
          maxWithMinBooking.setDate(max.obj.getDate() - (parseInt(bookingMin) === 0 ? 1 : parseInt(bookingMin))); 

          pickerStartItem.disable = [];
          pickerStartItem.min = min;
          pickerStartItem.max = createDateObject( maxWithMinBooking );

          if ( startSet == null ) {
            pickerStartItem.highlight = highlight;
            pickerStartItem.view = view;
          }

          $('body').trigger('clear_end_picker', pickerStartItem );

          pickerStart.render();

          ebs_clear_booking_session();
        }

        // Set End Picker
        if ( endTime.select && typeof endTime.select != 'undefined' ) {

          endPick = endTime.select;
          endDate = pickerEnd.get('select', 'yyyy-mm-dd');
          disabledEndDate = pickerEnd.get('select');
          endDateDisplay = pickerEnd.get('select', 'dd mmmm yyyy');

          // Hotfix for pickadate.js bug when selecting dates with keyboard, waiting for v4.0 fix
          if ( typeof endPick === 'object' ) {
            endPick = endTime.select.pick;
          }

          var minAndMax = ebs_get_min_and_max( disabledEndDate, 'minus' ),
            min = minAndMax.min,
            max = minAndMax.max;

          if ( ! max ) max = maxOption;

          var closestDisabled = ebs_get_closest_disabled( endPick, pickerStart, 'inferior' );

          if ( typeof endPick != 'undefined' ) {

            if ( closestDisabled ) {

              var closestDisabled = closestDisabled + 86400000, // Add one day to get the right date
                date = new Date( closestDisabled ); // Convert to date

              if ( closestDisabled > min )
                var min = date;
            }

          }

          var min = createDateObject( min ),
            max = createDateObject( max ),
            view = createDateObject( new Date(min.year,min.month,01) );

          var maxWithMinBooking = new Date(max.obj);
          maxWithMinBooking.setDate(max.obj.getDate() - (parseInt(bookingMin) === 0 ? 1 : parseInt(bookingMin))); 

          pickerStartItem.min = min;
          pickerStartItem.max = createDateObject(maxWithMinBooking);

          if ( startSet == null ) {
            pickerStartItem.highlight = min;
            pickerStartItem.view = view;
          }

          $('body').trigger('set_end_picker', [pickerStartItem, endPick] );

          pickerStart.render();

          if ( startSet != null && endSet != null )
            ebs_set_price(startDate, endDate, startDateDisplay, endDateDisplay);

        } else {
          return false;
        }

      },
      close: function() {
        $(document.activeElement).blur();
      }
    });

    function formatPrice( price ) {
      formatted_price = accounting.formatMoney( price, {
        symbol 		: ajax_object.currency_format_symbol,
        decimal 	: ajax_object.currency_format_decimal_sep,
        thousand	: ajax_object.currency_format_thousand_sep,
        precision 	: ajax_object.currency_format_num_decimals,
        format		: ajax_object.currency_format
      } );

      return formatted_price;
    }

    // WooCommerce Product Add-ons compatibility
    $('body').on('updated_addons', function() {

      var product_price = 0;

      if ( productType === 'variable' ) {
        var variationId = $('.variations_form').find('input.variation_id').val(),
          product_price = parseFloat( ajax_object.product_price[variationId] );
      } else if ( productType === 'grouped') {
        var product_price = totalGroupedPrice;
      } else {
        var product_price = parseFloat( ajax_object.product_price );
      }

      var addon_costs = getAdditionalCosts();

      if ( ! addon_costs )
        return;

      var total_price = parseFloat(addon_costs + product_price);

      if ( addon_costs > 0 ) {
        pickerStart.clear();
        pickerEnd.clear();
      }

      formatted_total = formatPrice( total_price );
      $('p.booking_price .price .amount').html(formatted_total);

    });

  });
})(jQuery);
