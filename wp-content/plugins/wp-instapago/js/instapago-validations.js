jQuery(function($){
    restrictNumeric = function(e) {
        var input;
        if (e.metaKey || e.ctrlKey) {
            return true;
        }
        if (e.which === 32) {
            return false;
        }
        if (e.which === 0) {
            return true;
        }
        if (e.which < 33) {
            return true;
        }
        input = String.fromCharCode(e.which);
        return !!/[\d\s]/.test(input);
    };

    avoidModifiers = function (e) {
        var $target, value;
        $target = $(e.currentTarget);
        value = $target.val();
        if (event.ctrlKey || event.altKey || (event.ctrlKey && event.altKey)) {
            e.preventDefault();
            return false;
        }
    };

    restrictAlpha = function (e) {
        var regex = new RegExp("^[a-zA-Zñ\ ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    }

    restrictAlphaChanged = function (e) {
        $target = $(e.currentTarget);
        var value = $target.val();
        var length = value.length; 
        var last_chr = value[length - 1]; 

        var regex = new RegExp("^[a-zA-Zñ\ ]+$");
        if (regex.test(last_chr)) {
            return true;
        }

        return $target.val(value.substr(0, length - 1));
        return false;
    }

    restrictNumericChanged = function (e) {
        $target = $(e.currentTarget);
        var value = $target.val();
        var length = value.length; 
        var last_chr = value[length - 1]; 

        var regex = new RegExp("^[0-9]+$");
        if (regex.test(last_chr)) {
            return true;
        }

        return $target.val(value.substr(0, length - 1));
        return false;
    }


    showFields = function (e) {
        if ($("#payment_method_instapago").is(':checked')) {
            $(".credit-card-details").show("slow"); 
        }

        if (!$("#payment_method_bacs").is(':checked')) {
            $(".transfer-details").hide("slow");
        }
    }

    hideFields = function (e) {
        if (!$("#payment_method_instapago").is(':checked')) {
            $(".credit-card-details").hide("slow"); 
        }

        if ($("#payment_method_bacs").is(':checked')) {
            $(".transfer-details").show("slow");
        }
    }

    $(document.body).on('keypress', '#instapago-card-holder', restrictAlpha); 
    $(document.body).on('keydown', '#instapago-card-holder', avoidModifiers); 
    $(document.body).on('keyup', '#instapago-card-holder', restrictAlphaChanged); 

    $(document.body).on('keypress', '#instapago-card-holder-id', restrictNumeric); 
    $(document.body).on('keydown', '#instapago-card-holder-id', avoidModifiers); 
    $(document.body).on('keyup', '#instapago-card-holder-id', restrictNumericChanged); 

    $(document.body).on('click', '#payment_method_instapago', showFields); 
    $(document.body).on('click', '#payment_method_bacs', hideFields); 

    // Show tranfer details on document ready
    $(".transfer-details").show("slow");
});
