<?php 
/*
 * File: wp-instapago.php
 * Plugin Name: wp-instapago
 * Plugin URI: 
 * Description: Wordpress plugin to handle payment using Banesco instapago 
 * Version: 0.1
 * Author: Juan Carlos Arocha
 * Author URI: 
 * License: MIT License
 * License URI: http://opensource.org/licenses/MIT
 * Text Domain: 
 * Domain Path: 
 *
 * */

defined( 'ABSPATH' ) or die('No script kiddies please! ');  

function init_instapago_gateway() {


    class WC_Gateway_instapago extends WC_Payment_Gateway {

        // Constructor: 
        function __construct() {
            // Member variables required by WooCommerce 
            $this->id                 = "instapago";
            $this->icon               = "";
            $this->has_fields         = true;
            $this->method_title       = "Tarjeta de crédito";
            $this->method_description = "Pago con tarjeta de crédito usando la plataforma instapago";

            // define and load setting fields (recommended by WooCommerce)
            $this->init_form_fields(); 
            $this->init_settings(); 

            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
            $this->title = $this->get_option('title'); 
        }

                // Defines the fields in the form
        function payment_fields() {
            $this->credit_card_form(array(), array(
                'card-holder-field' => '<p class="form-row col-lg-6 col-md-6 validate-required">
				<label for="' . esc_attr( $this->id ) . '-card-holder">' . __( 'TarjetaHabiente', 'woocommerce' ) . ' <span class="required">*</span></label>
                <input id="' . esc_attr( $this->id ) . '-card-holder" class="input-text wc-credit-card-form-card-holder" type="text" placeholder="Nombre y Apellido" autocomplete="off" name="' . ( $this->id . '-card-holder' ) . '" /> 
                </p>',
                'card-holder-id-field' => '<p class="form-row col-lg-6 col-md-6 validate-required">
				<label for="' . esc_attr( $this->id ) . '-card-holder-id">' . __( 'Cédula/RIF TarjetaHabiente', 'woocommerce' ) . ' <span class="required">*</span></label>
                <input id="' . esc_attr( $this->id ) . '-card-holder-id" class="input-text wc-credit-card-form-card-holder-id" type="text" placeholder="Cédula/RIF TarjetaHabiente" maxlength="8" autocomplete="off" name="' . ( $this->id . '-card-holder-id' ) . '" />
                </p>'
            )); 
        }

        // Validates the fields in the form
        function validate_fields() {

            // Begin presence validations
            if (!isset($_POST['instapago-card-number']) || empty($_POST['instapago-card-number'])) {
                wc_add_notice( '<strong> Número de tarjeta </strong> ' . __( 'is a required field.', 'woocommerce' ), 'error' );
                return false; 
            }

            if (!isset($_POST['instapago-card-holder']) || empty($_POST['instapago-card-holder'])) {
                wc_add_notice( '<strong> Tarjetahabiente </strong> ' . __( 'is a required field.', 'woocommerce' ), 'error' );
                return false; 
            }

            if (!isset($_POST['instapago-card-holder-id']) ||empty($_POST['instapago-card-holder-id'])) {
                wc_add_notice( '<strong> Cédula/RIF Tarjetahabiente </strong> ' . __( 'is a required field.', 'woocommerce' ), 'error' );
                return false; 
            }

            if (!isset($_POST['instapago-card-cvc']) || empty($_POST['instapago-card-cvc'])) {
                wc_add_notice( '<strong> CVC </strong> ' . __( 'is a required field.', 'woocommerce' ), 'error' );
                return false; 
            }

            if (!isset($_POST['instapago-card-expiry']) || empty($_POST['instapago-card-expiry'])) {
                wc_add_notice( '<strong> Fecha de vencimiento </strong> ' . __( 'is a required field.', 'woocommerce' ), 'error' );
                return false; 
            }
            // End presence validations

            $card_number    = str_replace(array(' ', '-'), '', $_POST['instapago-card-number']);
            $card_cvc       = str_replace(array(' ', '-'), '', $_POST['instapago-card-cvc']);
            $card_holder_id = str_replace(array(' ', '-'), '', $_POST['instapago-card-holder-id']);

            $card_number_len    = strlen($card_number);
            $card_cvc_len       = strlen($card_cvc);
            $card_holder_id_len = strlen($card_holder_id);

            // Validate format: Numeric fields
            if (!$this->validate_numeric_field($card_number) || $card_number_len < 15 || $card_number_len > 16 ) {
                wc_add_notice( '<strong> Número de tarjeta </strong> ' . __( 'es inválido.', 'woocommerce' ), 'error' );
                return false; 
            }
            if (!$this->validate_numeric_field($card_cvc) || $card_cvc_len !== 3 ) {
                wc_add_notice( '<strong> CVC </strong> ' . __( 'es inválido.', 'woocommerce' ), 'error' );
                return false; 
            }
            if (!$this->validate_numeric_field($card_holder_id) || $card_holder_id_len < 6 || $card_holder_id_len > 8 ) {
                wc_add_notice( '<strong> Cédula/RIF Tarjetahabiente </strong> ' . __( 'es inválido.', 'woocommerce' ), 'error' );
                return false; 
            }

            /*// Validate format: Alphabetic fields*/
            //if (!$this->validate_numeric_field($_POST['instapago-card-holder'])) {
                //wc_add_notice( '<strong> Card Holder </strong> ' . __( "{$_POST['instapago-card-holder']} is invalid.", 'woocommerce' ), 'error' );
                //return false; 
            /*}*/

            // Validate format: Alphanumeric fields
            if (!$this->validate_alphanumeric_field($card_number)) {
                wc_add_notice( '<strong> Card Number </strong> ' . __( 'is invalid.', 'woocommerce' ), 'error' );
                return false; 
            }
            if (!$this->validate_alphanumeric_field($card_cvc)) {
                wc_add_notice( '<strong> Card CVC </strong> ' . __( 'is invalid.', 'woocommerce' ), 'error' );
                return false; 
            }
            if (!$this->validate_alphanumeric_field($card_holder_id)) {
                wc_add_notice( '<strong> Card Holder Id </strong> ' . __( 'is invalid.', 'woocommerce' ), 'error' );
                return false; 
            }
            if (!$this->validate_alphanumeric_field($_POST['instapago-card-holder'])) {
                wc_add_notice( '<strong> Card Holder </strong> ' . __( "{$_POST['instapago-card-holder']} is invalid.", 'woocommerce' ), 'error' );
                return false; 
            }
            return true; 
        }

        // Intern validations
        function validate_numeric_field($field) {
            $numeric_regex = "/^[0-9]+$/";
            $matched       = preg_match($numeric_regex, $field);

            return ($matched == 1 ? true : false);
        }

        function validate_alphanumeric_field($field) {
            $special_regex = "/^[\W]+$/";
            $matched = preg_match($special_regex, $field); 
            return ($matched == 1 ? false : true);
        }

        function validate_alphabetic_field($field) {
            $alphabetic_regex = "/^[a-zA-Zñ\s]+$/";
            $matched = preg_match($alphabetic_regex, $field); 
            return ($matched == 1 ? true : false);
        }


        // Method to initialize the form fields
        function init_form_fields() {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable/Disable', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __( 'Enable instapago Payment', 'woocommerce'),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __('Title', 'woocommerce'),
                    'type' => 'text', 
                    'description' => __('Thsi controls the title which the user sees during checkout', 'woocommerce'),
                    'default' => __('Tarjeta de credito', 'woocommerce'),
                    'desc_tip' => true
                ),
                'description' => array(
                    'title' => __('Customer Message', 'woocommerce'),
                    'type' => 'textarea',
                    'default' => ''
                ),
                'privatekey' => array(
                    'title' => __('Private key', 'woocommerce'), 
                    'description' => __('Private key to use with Instapago'), 
                    'type' => 'text',
                    'default' => '',
                    'desc_tip' => true
                ),
                'publickey' => array(
                    'title' => __('Public key', 'woocommerce'), 
                    'description' => __('Public key to use with Instapago'), 
                    'type' => 'text',
                    'default' => '',
                    'desc_tip' => true
                )
            );
        }

        private function get_order_products_formatted($order) {
            // Concat quantity name product in string
            $products = $order->get_items(); 

            $last_product = end($products);
            reset($products); 
            $ordered_products = "";

            // Loop to iterate products
            foreach ( $products as $product ) {
                $ordered_products .= $product['qty'] . ' ';
                $ordered_products .= $product['name'];

                if ($product !== $last_product) {
                    $ordered_products .= ', ';
                }
            }

            return $ordered_products;
        }

        /*
         * Function: send_payment
         * Description: Sends payment to Instapago
         * args @order, @credit_card_info,
         */
        function send_payment($order, $credit_card_info) {

            $payment_uri = 'https://api.instapago.com/payment'; // Instapagos URL
            $status_id   = "2";                                 // 2  => pay, 1 => hold
            // Access keys to instapago
            $keyid              = $this->get_option('privatekey'); 
            $public_keyid       = $this->get_option('publickey'); 

            // Throw exception if none is given 
            if (empty($keyid) || empty($public_keyid)) {
                throw new Exception('Error al procesar la transacción, no se especificaron llaves de acceso');
            
            }

            $ordered_products = $this->get_order_products_formatted($order); 
            // Payment attributes
            $payment_attr = array(
                'KeyId'          => $keyid,
                'PublicKeyId'    => $public_keyid,
                'Amount'         => $order->order_total,
                'Description'    => "Compra de {$ordered_products}",
                'CardHolder'     => $credit_card_info['credit_card_holder'],
                'CardHolderID'   => $credit_card_info['credit_card_holder_id'],
                'CardNumber'     => str_replace(array(' ', '-'), '', $credit_card_info['credit_card_number']),
                'CVC'            => $credit_card_info['credit_card_cvc'],
                'ExpirationDate' => str_replace(' ', '', $credit_card_info['credit_card_expiry']),
                'StatusId'       => $status_id,
                'IP'             => $order->customer_ip_address,
                'OrderNumber'    => $order->get_order_number(),
                'City'           => $order->billing_city,
                'ZipCode'        => $order->billing_postcode,
                'State'          => $order->billing_state,
                'Address'        => $order->get_formatted_billing_address()
            ); 

            // Convert array to POST request: foo=bar&baz=qux....
            $payment_query = http_build_query($payment_attr); 

            // Make POST request using WP HTTP API
            return wp_remote_post($payment_uri, array(
                'method' => 'POST',
                'timeout' => 45, 
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking' => true,
                'headers' => array(),
                'body' => $payment_query,
                'cookies' => array()
            )); 
        }

        /*
         * Function: process_payment
         * Description: Handles the payment and sends it to the server
         * args: @order_id
         */
        function process_payment( $order_id ) {
            global $woocommerce; 

            // Try to perform instapago transaction
            try {

                // Cannot continue if any of these fields is missing
                if (!isset($_POST['instapago-card-number']) ||
                    !isset($_POST['instapago-card-holder']) || 
                    !isset($_POST['instapago-card-holder-id']) || 
                    !isset($_POST['instapago-card-cvc']) || 
                    !isset($_POST['instapago-card-expiry']) )  { 

                        throw new Exception('Error al procesar la transacción, datos de la tarjeta de crédito incompletos');
                }

                // Get the order
                $order = wc_get_order( $order_id ); 

                $credit_card_info = array(
                    "credit_card_number"    => esc_attr($_POST['instapago-card-number']),
                    "credit_card_cvc"       => esc_attr($_POST['instapago-card-cvc']),
                    "credit_card_expiry"    => esc_attr($_POST['instapago-card-expiry']),
                    "credit_card_holder"    => esc_attr($_POST['instapago-card-holder']),
                    "credit_card_holder_id" => esc_attr($_POST['instapago-card-holder-id'])
                );


                // Send payment to Instapago
                $server_response = $this->send_payment($order, $credit_card_info); 
                
               // General error in response
                if ( is_wp_error( $server_response ) ) {
                    wc_add_notice(__('Error en respuesta del servidor', 'woothemes'), 'error' );
                    return; 

                } else {
                    // Decode the response to get what we're interested in
                    $decoded_response = json_decode($server_response['body'], true); 

                    // If the payment was succesful
                    if ($decoded_response['success']) {
                        // Payment completed
                        $order->payment_complete(); 
                        // Reduce stock levels
                        $order->reduce_order_stock();
                        // Remove order from cart
                        $woocommerce->cart->empty_cart(); 

                        // Add success notice
                        wc_add_notice(__($decoded_response['message'], 'woothemes'), 'success' );


                        // Save and store in receipts database
                        $this->save_receipt_locally($order, $decoded_response['voucher']); 

                        // Return to thankyou redirect
                        return array(
                            'result' => 'success',
                            'redirect' => $this->get_return_url( $order ) 
                        ); 
                    } else {
                        // Add returned message by instapago
                        wc_add_notice(__($decoded_response['message'], 'woothemes'), 'error' );
                        return; 
                    }
                }
                
            } catch (Exception $e) {
                wc_add_notice(__($e->getMessage(), 'woothemes'), 'error' );
                return; 
            }
        }

        /*
         * Function: save_receipt_locally
         * Description: Saves the voucher from instapago in the local filesystem
         * and the database, and then it sends the voucher via email to the 
         * costumer
         * args: @order, @receipt
         */
        private function save_receipt_locally($order, $receipt) {

            $receipt_path = ABSPATH . 'wp-content/uploads/receipts/'; 
            $receipt_path .= date('Y') . '/' . date('m') . '/' . date('d'); 

            // Create directory if it doesn't exists
            if (!is_dir($receipt_path)) {
                mkdir($receipt_path, 0777, true); 
            }

            $receipt_path .= '/'. $order->id .'-instapago-receipt.html';

            $receipt_html = '<html><body>';
            $receipt_html .= $receipt;
            $receipt_html .= '</body></html>'; 

            // Open file and write
            $receipt_file = new DOMDocument('1.0'); 
            $receipt_file->loadHTML(htmlspecialchars_decode($receipt_html)); 
            $receipt_file->saveHTMLFile($receipt_path); 


            global $wpdb; 

            $table_name = $wpdb->prefix . "instapago_receipts";
            $charset_collate = $wpdb->get_charset_collate(); 

            $wpdb->insert(
                $table_name,
                array(
                    'order_id' => $order->id,
                    'receipt_url' => $receipt_path
                )
            );

            // Send receipt to client
            $this->order_receipt_mail($order, htmlspecialchars_decode($receipt_html));
        }

        private function order_receipt_mail($order, $receipt) {
            $ordered_products = $this->get_order_products_formatted($order); 

            $subject_user = '[ZOI Venezuela] Recibo de su compra';	 
            $message_user = '<html><body><br />';
            $message_user .= $receipt;
            $message_user .= '</body></html>';

            $from    = get_option( 'admin_email' );
            $email   = $order->billing_email;
            $headers = 'From: ZOI VENEZUELA' .' <'.$from.'>' . "\r\n" . 'Reply-To:  <'.$from.">\r\n";
            $headers .= 'Content-Type: text/html; charset=UTF-8'; 

            return wp_mail( $email, $subject_user, $message_user, $headers);
        }


    }
}

/**
 * Function: instapago_load_receipt
 * Description: Load the voucher HTML to the page where it's invoked
 * args: @order
 */
function instapago_load_receipt( $order ) {
    global $wpdb;

    $table_name = $wpdb->prefix . "instapago_receipts";

    $sql = "SELECT receipt_url FROM $table_name 
        WHERE order_id = ". $order->id . ";"; 

    $receipt_url = $wpdb->get_var( $sql ); 

    // Avoid warnings
    libxml_use_internal_errors(true);
    // Create HTML and load it
    $doc = new DOMDocument();
    $doc->loadHTMLFile($receipt_url);

    // Show HTML
    echo $doc->saveHTML(); 
}


function add_instapago( $methods )  {
    $methods[] = 'WC_Gateway_instapago'; 
    return $methods; 
}

function database_config() {
    global $wpdb; 

    $table_name = $wpdb->prefix . "instapago_receipts";
    $charset_collate = $wpdb->get_charset_collate(); 

    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        order_id bigint NOT NULL,
        receipt_url varchar(256) NOT NULL,
        UNIQUE KEY id (id) 
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php'); 

    dbDelta( $sql );

    $receipts_path = ABSPATH. 'wp-content/uploads/receipts';
    // Create path to save the receipts
    if (!is_dir($receipts_path)) {
        mkdir($receipts_path) or die('Could not create receipts path: '. $receipts_path);
    }
}

register_activation_hook( __FILE__, 'database_config' ); 



add_action('woocommerce_credit_card_form_end', 'add_instapago_validations'); 
function add_instapago_validations() {
    $plugin_url = plugins_url('wp-instapago/js'); 
    wp_enqueue_script('instapago-validations', "{$plugin_url}/instapago-validations.js");
}
// Add the class when the plugins are loaded
add_action('plugins_loaded', 'init_instapago_gateway'); 
// Attach class methods to woocommerce_payment_gateways 
add_filter('woocommerce_payment_gateways', 'add_instapago'); 

