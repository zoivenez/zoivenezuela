<?php

/* Path to the WordPress codebase you'd like to test. Add a backslash in the end. */
define( 'ABSPATH', dirname( __FILE__ ) . '/' );

// Test with multisite enabled.
// Alternatively, use the tests/phpunit/multisite.xml configuration file.
// define( 'WP_TESTS_MULTISITE', true );

// Force known bugs to be run.
// Tests with an associated Trac ticket that is still open are normally skipped.
// define( 'WP_TESTS_FORCE_KNOWN_BUGS', true );

// Test with WordPress debug mode (default).
define( 'WP_DEBUG', true );

// ** MySQL settings ** //

// This configuration file will be used by the copy of WordPress being tested.
// wordpress/wp-config.php will be ignored.

// WARNING WARNING WARNING!
// These tests will DROP ALL TABLES in the database with the prefix named below.
// DO NOT use a production database or one that is shared with something else.

require_once('wp-tests-credentials.php');

$table_prefix  = 'wp_';   // Only numbers, letters, and underscores please!

define( 'WP_TESTS_DOMAIN', 'example.org' );
define( 'WP_TESTS_EMAIL', 'admin@example.org' );
define( 'WP_TESTS_TITLE', 'Test ZOI' );

define( 'WP_PHP_BINARY', 'php' );

define( 'WPLANG', '' );

// Sendgrid configuration
define('SENDGRID_USERNAME', 'sendgrid_user'); 
define('SENDGRID_PASSWORD', 'sendgrid_password'); 
define('SENDGRID_FROM_NAME', 'ZOI Venezuela'); 
define('SENDGRID_FROM_EMAIL', 'hola@zoivenezuela.com'); 
define('SENDGRID_REPLY_TO', 'hola@zoivenezuela.com');

